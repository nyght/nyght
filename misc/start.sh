#! /bin/bash

RED='\033[0;31m'
BOLD='\033[1m'
RESET='\033[0m'

print_header() {
  echo -e "${RED}${BOLD}"
  echo "     _    _ |_ _|_"
  echo "    | |\/(_|| | | "
  echo "       /  _|      "
  echo -e "${RESET}"
}

print_devenv_instructions() {
  echo "To launch the required services (database, object storage, ...), run:"
  echo
  echo -e "    ${BOLD}> devenv up${RESET}"
  echo
}

print_instructions() {
  echo "Thanks for contributing! Your development environment is almost ready."
  echo "To start working on nyght, first open a shell and run the following"
  echo "command to setup the project: "
  echo
  echo -e "    ${BOLD}> mix setup${RESET}"
  echo
  echo "Then, if you want some dummy data for development, run this command:"
  echo
  echo -e "    ${BOLD}> mix seed${RESET}"
  echo

  print_devenv_instructions

  echo "Then, to run the server, run:"
  echo
  echo -e "    ${BOLD}> mix phx.server${RESET}"
  echo
  echo "And you're ready to go! Don't forget to check the contribution"
  echo "guidelines here:"
  echo
  echo -e "    ${BOLD}https://gitlab.com/nyght/nyght/-/blob/master/CONTRIBUTING.md${RESET}"
  echo
}

print_header
if [ ! -d "./deps/" ]; then
  print_instructions
else
  print_devenv_instructions
fi

echo "Here are some useful info about this dev environment:"
echo
echo -e "    - nyght URL (when the server runs): ${BOLD}http://localhost:4000${RESET}"
echo -e "    - database URL: ${BOLD}localhost:5432${RESET}"
echo -e "    - database password: ${BOLD}${POSTGRES_PASSWORD}${RESET}"
echo -e "    - object storage URL: ${BOLD}http://${MINIO_ADDRESS}${RESET}"
echo -e "    - object storage console URL: ${BOLD}http://${MINIO_CONSOLE_ADDRESS}${RESET}"
echo -e "    - object storage console username: ${BOLD}${MINIO_ROOT_USER}${RESET}"
echo -e "    - object storage console password: ${BOLD}${MINIO_ROOT_PASSWORD}${RESET}"
echo
