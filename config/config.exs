# This file is responsible for configuring your application
# and its dependencies with the aid of the Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :elixir, :time_zone_database, Tz.TimeZoneDatabase

config :nyght,
  ecto_repos: [Nyght.Repo],
  generators: [binary_id: true],
  session_options: [],
  routing: [multitenant: false]

config :flop, repo: Nyght.Repo

# Configures the endpoint
config :nyght, NyghtWeb.Endpoint,
  adapter: Bandit.PhoenixAdapter,
  url: [host: "localhost"],
  render_errors: [
    formats: [html: NyghtWeb.ErrorHTML, json: NyghtWeb.ErrorJSON],
    layout: false
  ],
  pubsub_server: Nyght.PubSub,
  live_view: [signing_salt: "kFgc96wL"]

config :nyght, NyghtWeb.Cldr, locales: ["en"]

config :nyght, Oban,
  repo: Nyght.Repo,
  plugins: [
    Oban.Plugins.Pruner,
    {Oban.Plugins.Cron,
     crontab: [
       {"@daily", Nyght.Workers.UserDeleteDaily}
     ]}
  ],
  queues: [default: 10]

config :nyght, :appsignal, frontend_key: nil

config :nyght, :s3,
  assets_bucket: "posters",
  uploads_bucket: "uploads"

config :ex_cldr,
  default_backend: NyghtWeb.Cldr

config :esbuild,
  version: "0.20.2",
  default: [
    args:
      ~w(js/app.js --bundle --target=es2016 --outdir=../priv/static/assets --loader:.woff=file --loader:.woff2=file --loader:.eot=file --loader:.ttf=file --loader:.svg=file),
    cd: Path.expand("../assets", __DIR__),
    env: %{"NODE_PATH" => Path.expand("../deps", __DIR__)}
  ]

config :tailwind,
  version: "4.0.9",
  default: [
    args: ~w(
      --input=assets/css/app.css
      --output=priv/static/assets/app.css
    ),
    cd: Path.expand("..", __DIR__)
  ]

# Configures the mailer
#
# By default it uses the "Local" adapter which stores the emails
# locally. You can see the emails in your browser, at "/dev/mailbox".
#
# For production it's recommended to configure a different adapter
# at the `config/runtime.exs`.
config :nyght, Nyght.Emails, adapter: Swoosh.Adapters.Local

# Swoosh API client is needed for adapters other than SMTP.
config :swoosh, :api_client, false

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :phoenix, :format_encoders, ics: Nyght.VObjects, vcf: Nyght.VObjects

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{config_env()}.exs"
