import Config

# Check if the app is running in a Gitlab CI pipeline. If so,
# service names will be used as hostname and some configuration changes.
ci? = System.get_env("CI") == "true"

# Only in tests, remove the complexity from the password hashing algorithm
config :bcrypt_elixir, :log_rounds, 1

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :nyght, Nyght.Repo,
  username: System.get_env("POSTGRES_USER") || "postgres",
  password: System.get_env("POSTGRES_PASSWORD") || "nyght_dev",
  database: System.get_env("POSTGRES_DB") || "nyght_test#{System.get_env("MIX_TEST_PARTITION")}",
  hostname: System.get_env("POSTGRES_HOST") || if(ci?, do: "db", else: "localhost"),
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: 10

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :nyght, NyghtWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "9SNR64oaGd2tNpJT6Dk9/Iyx8ljOERwfiUUCZaOjD+7mK31yASmL71LB46AJXfh5",
  server: false

config :ex_aws, :s3,
  scheme: "http://",
  host: if(ci?, do: "object-storage", else: "localhost"),
  port: 9000,
  access_key_id: System.get_env("MINIO_ROOT_USER") || "minioadmin",
  secret_access_key: System.get_env("MINIO_ROOT_USER") || "minioadmin"

# In test we don't send emails.
config :nyght, Nyght.Emails, adapter: Swoosh.Adapters.Test

# Execute Oban jobs directly, don't insert them into the DB
config :nyght, Oban, testing: :inline

# Print only warnings and errors during test
config :logger, level: :warning

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime
