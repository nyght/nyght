# Contribution guidelines

Thank you for considering contributing to nyght!

These guidelines are meant for new contributors, regardless of their level
of proficiency; following them allows the maintainers of nyght to more
effectively evaluate your contribution, and provide prompt feedback to
you. Additionally, by following these guidelines you clearly communicate
that you respect the time and effort that the people developing nyght put into
managing the project.

There are many things that we value:

* bug reporting and fixing
* documentation
* tests
* new features
* translation

Please, do not use the issue tracker for support questions.

## How to translate nyght

### For translators

nyght can be translated by using [Weblate](https://hosted.weblate.org/projects/nyght/).
Create an account on there, join the project, and start translating, No coding
is involved!

### For developers

To extract translated strings in nyght, run these commands:

```sh
mix gettext.extract
```

## How to report bugs and ask for new features

### Security

If you find a security vulnerability, do NOT open an issue. Email
<loic@nyght.ch> instead. In order to determine whether you are dealing
with a security issue, ask yourself these two questions:

* Can I access something that's not mine, or something I shouldn't
    have access to?
* Can I disable something for other people?

If the answer to either of those two questions are "yes", then you're
probably dealing with a security issue. Note that even if you answer
"no" to both questions, you may still be dealing with a security issue,
so if you're unsure, just email us.

### Bug reports

For any other bugs, [open a new issue](https://gitlab.com/nyght/nyght/-/issues/new?issuable_template=Bug%20Report)
with the "Bug Report" description template. Fill the description
accordingly.

For small issues, such as:

* spelling/grammar fixes in the documentation
* typo correction
* comment clean ups
* changes to metadata files (CI, .gitignore)
* build system changes
* source tree clean ups and reorganizations

You should directly [open a merge request](https://gitlab.com/nyght/nyght/-/merge_requests/new)
instead of filing a new issue.

### Features and enhancements

If you want to propose a new feature or enhancement, [open a new issue](https://gitlab.com/nyght/nyght/-/issues/new?issuable_template=Feature%20Request)
with the "Feature Request" description template. Fill the description
accordingly.

## Your first contribution

### Prerequisites

If you want to contribute to nyght, you will need to have a
working installation of the latest version of Elixir
(for example with the help of [`asdf`](https://thinkingelixir.com/install-elixir-using-asdf/).
You should also have a PostgreSQL server running.

### Getting started

You should first follow the setup instructions in the README. Then, when you're
ready, you should create your own branch to make your changes:

```sh
git checkout -b your-branch
```

After you've done some work, push your branch:

```sh
git push origin your-branch
```

You should then see a message from GitLab telling you to open a merge
request. Follow the link and create a new merge request by filling the
description accordingly. If you cannot see the message, you can [open a
merge request manually](https://gitlab.com/nyght/nyght/-/merge_requests/new).

### Commit guidelines

 1. **Commit often**
    You do not need to have a lot of changes to create a commit. On the
    contrary, you should try to split your changes into small logical steps
    and commit each of them. They should be consistent, self-contained (work
    independently of later commits), and pass the same test which previously
    did (or more) to ensure you do not introduce any regression. On the
    other hand, do not push it to the extreme: if your changes are tightly
    related and stay clear as a whole, there is no need to create multiple
    commits, even if they are applied to different files, functions, etc.

 2. **Push often**
    A good reason to push often is to have your work somewhere else than
    just on your computer in the event of something were to happen to
    local computer. Another reason is to facilitate code sharing:
    even if your branch is not at maturity, creating a merge request as
    "Draft" will allow you to discuss your progress, ask questions, and
    get feedbacks during the development process and not just at the end.

### Commit messages

The expected format for git commit messages is as follows:

```text
Explain shortly the commit

Longer explanation explaining exactly what's changed, whether any
external or private interfaces changed, what bugs were fixed 
and so forth. Be concise but not too brief.
```

* Always add a brief description of the commit to the first line of
    the commit and terminate by two newlines
* First line (the brief description) must only be one sentence and
    should start with a capital letter unless it starts with a lowercase
    symbol or identifier, and start with an infinitive verb ("Add",
    "Update", ...). Don't use a trailing period either. Don't exceed
    72 characters.
* The main description (the body) is normal prose and should use normal
    punctuation and capital letters where appropriate. Consider the commit
    message as an email sent to the developers (or yourself, six months
    down the line) detailing why you changed something. There's no need
    to specify the how: the changes can be inlined.

### Test your changes

Your changes need to pass the test suite to be merged. To run the test
suite, use the following command:

```sh
devenv test
```

### Coding style

The standard Elixir coding style is used in this project. You can format
your code with `mix format`.
