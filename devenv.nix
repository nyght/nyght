{ pkgs, lib, config, inputs, ... }:

let
  env = {
    MINIO_ROOT_USER = "minioadmin";
    MINIO_ROOT_PASSWORD = "minioadmin";
    MINIO_CONSOLE_ADDRESS = "localhost:9001";
    MINIO_ADDRESS = "localhost:9000";
    POSTGRES_PASSWORD = "nyght_dev";
  };
in 
  {
  packages = [
    pkgs.git pkgs.gitleaks
  ] ++ lib.optionals pkgs.stdenv.isLinux [ pkgs.inotify-tools ];

  env = env;

  enterShell = ''
    source ./misc/start.sh
  '';

  services.postgres = {
    enable = true;
    listen_addresses = "127.0.0.1";
    initialDatabases = [{ name = "nyght_dev"; } { name = "nyght_test"; }];
    initialScript = ''
      CREATE ROLE postgres WITH LOGIN PASSWORD '${env.POSTGRES_PASSWORD}' SUPERUSER;
    '';
  };

  services.minio = {
    enable = true;
    buckets = ["posters" "uploads"];
    consoleAddress = env.MINIO_CONSOLE_ADDRESS;
    listenAddress = env.MINIO_ADDRESS;
    accessKey = env.MINIO_ROOT_USER;
    secretKey = env.MINIO_ROOT_PASSWORD;
  };

  processes = lib.optionalAttrs (!config.devenv.isTesting) {
    nyght = {
      exec = "mix phx.server";
      process-compose = {
        depends_on.postgres.condition = "process_healthy";
        depends_on.minio.condition = "process_started";
      };
    };
  };

  languages.elixir = {
    enable = true;
  };
}
