defmodule Nyght.Files do
  @moduledoc """
  Functions to interact with files and S3 storage.
  """
  alias ExAws.S3

  def upload(bucket, path, content, opts \\ []) do
    bucket
    |> S3.put_object(path, content, opts)
    |> ExAws.request()
  end

  def download(bucket, path) do
    bucket
    |> S3.get_object(path)
    |> ExAws.request()
    |> case do
      {:ok, %{body: body}} -> {:ok, body}
      error -> error
    end
  end

  def delete(bucket, path) do
    bucket
    |> S3.delete_object(path)
    |> ExAws.request()
  end

  def delete_all(bucket, paths) do
    paths = Enum.reject(paths, &is_nil/1)

    bucket
    |> S3.delete_all_objects(paths)
    |> ExAws.request()
  end
end
