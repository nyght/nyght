defmodule Nyght.Shifts do
  @moduledoc """
  The Shifts context.
  """

  import Ecto.Query, warn: false

  alias Nyght.Events
  alias Nyght.Events.Event
  alias Nyght.Organizations.{Member, Organization}
  alias Nyght.Repo
  alias Nyght.Shifts.{Application, Shift}
  alias Nyght.Shifts.Events.{ShiftCreated, ShiftDeleted, ShiftUpdated}

  ############################################################
  # Shifts
  ############################################################

  @doc """
  Lists the shifts for a given period.
  """
  @spec list_shifts_for_period(Nyght.Organizations.Organization.t(), Date.t(), keyword) :: any
  def list_shifts_for_period(%Organization{id: org_id}, from, opts \\ []) do
    period = Keyword.get(opts, :period, "month")
    amount = Keyword.get(opts, :amount, 1)
    strategy = Keyword.get(opts, :strategy, :availability)
    role = Keyword.get(opts, :role)
    is_full = Keyword.get(opts, :full, true)

    Shift.get()
    |> Shift.filter(
      organization: org_id,
      strategy: strategy,
      full: is_full,
      role: role
    )
    |> Shift.filter_period(from, amount, period)
    |> Shift.include_all()
    |> Shift.with_opening_count()
    |> Shift.with_title()
    |> Repo.all()
  end

  @doc """
  Lists the shifts for a given user or event.
  """
  @spec list_shifts_for(
          Nyght.Accounts.User.t() | Nyght.Events.Event.t(),
          map(),
          keyword() | nil
        ) ::
          {:error, Flop.Meta.t()} | {:ok, {list(), Flop.Meta.t()}}
  def list_shifts_for(scope, params, opts \\ [])

  # TODO: remove the include_all function. It's needed for now because it preloads things.
  def list_shifts_for(%Member{id: member_id}, params, opts) do
    Shift.get()
    |> Shift.include_event()
    |> Shift.include_applications()
    |> Shift.include_all()
    |> maybe_filter_organizations(opts[:organization])
    |> Shift.filter_member(member_id)
    |> Shift.with_title()
    |> Shift.with_times()
    |> do_list_shifts(params, opts)
  end

  def list_shifts_for(%Event{id: event_id}, params, opts) do
    Shift.get()
    |> Shift.filter_event(event_id)
    |> Shift.include_all()
    |> Shift.with_times()
    |> Shift.with_title()
    |> Shift.with_opening_count()
    |> do_list_shifts(params, opts)
  end

  defp maybe_filter_organizations(query, nil), do: query
  defp maybe_filter_organizations(query, org_id), do: Shift.filter_org(query, org_id)

  defp do_list_shifts(query, params, _opts) do
    query
    |> Flop.validate_and_run(params, for: Shift)
  end

  @doc """
  Returns a stream of the staff working for the given event.

  This will list the `:accepted` and `:waiting_replacement` applications
  for the shifts of the event.

  Since it's a database stream, this has to be run in a transaction,
  like this:

    Repo.transaction(fn -> 
      stream = Shifts.stream_shifts_export(event)
      # then do stuff with the stream
    )
  """
  @spec stream_shifts_export(Nyght.Events.Event.t()) :: Enum.t()
  def stream_shifts_export(%Event{id: event_id}) do
    Shift.get()
    |> Shift.filter_event(event_id)
    |> Shift.select_staff_list(applications_status: [:accepted, :waiting_replacement])
    |> Repo.stream()
  end

  @doc """
  Gets a shift by its ID.
  """
  @spec get_shift!(any) :: Nyght.Shifts.Shift.t()
  def get_shift!(id) do
    Shift.get(id)
    |> Shift.include_all()
    |> Shift.with_times()
    |> Shift.with_title()
    |> Shift.with_opening_count()
    |> Repo.one!()
  end

  @doc """
  Returns the shifts corresponding to all the given event ids.

  This will included computed shift start and end times, their titles,
  and will be ordered by title and time.
  """
  @spec list_shifts_for_events(list(binary())) :: list(Nyght.Shifts.Shift.t())
  def list_shifts_for_events(events_id_list) do
    Shift.get()
    |> Shift.filter_events(events_id_list)
    |> Shift.with_opening_count()
    |> Shift.with_title()
    |> Shift.with_times()
    |> Shift.order_by_title_and_time()
    |> Repo.all()
  end

  @doc """
  Creates a shift.

  ## Examples

    iex> create_shift(event, %{field: value})
    {:ok, %Shift{}}

    iex> create_shift(event, %{field: bad_value})
    {:error, %Ecto.Changeset{}}

  """
  @spec create_shift(Nyght.Events.Event.t(), any) :: {:error, any} | {:ok, Nyght.Shifts.Shift.t()}
  def create_shift(%Event{} = event, attrs) do
    event
    |> Ecto.build_assoc(:shifts)
    |> Shift.changeset(attrs)
    |> Shift.put_time_offsets(event)
    |> Repo.insert()
    |> case do
      {:ok, shift} ->
        Events.broadcast!(
          __MODULE__,
          event.organization_id,
          event.id,
          %ShiftCreated{event_id: event.id, shift: shift}
        )

        {:ok, shift}

      err ->
        err
    end
  end

  @doc """
  Updates a shift.

  ## Examples

      iex> update_shift(shift, %{field: new_value})
      {:ok, %Shift{}}

      iex> update_shift(shift, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec update_shift(Nyght.Shifts.Shift.t(), map) ::
          {:error, any} | {:ok, Nyght.Shifts.Shift.t()}
  def update_shift(%Shift{} = shift, attrs) do
    shift
    |> Repo.preload([:event])
    |> Shift.changeset(attrs)
    |> Shift.put_time_offsets(shift.event)
    |> Repo.update()
    |> case do
      {:ok, updated} ->
        updated = Repo.preload(updated, [:event])

        Events.broadcast!(
          __MODULE__,
          updated.event.organization_id,
          updated.event_id,
          %ShiftUpdated{event_id: updated.event_id, shift: updated}
        )

        {:ok, updated}

      err ->
        err
    end
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking shift changes.

  ## Examples

      iex> change_shift(event, shift)
      %Ecto.Changeset{data: %Shift{}}
  """
  def change_shift(
        %Shift{} = shift,
        %Event{id: event_id, timezone: timezone} = event,
        attrs \\ %{}
      ) do
    attrs =
      Map.merge(attrs, %{event_id: event_id, timezone: timezone})
      |> prepare_params()

    Shift.changeset(shift, attrs)
    |> Shift.put_time_offsets(event)
  end

  @doc """
  Creates a copy of the given shift for the given event.
  """
  @spec copy_shift(Nyght.Shifts.Shift.t(), Nyght.Events.Event.t()) ::
          {:ok, Nyght.Shifts.Shift.t()} | {:error, any()}
  def copy_shift(%Shift{} = shift, %Event{} = to) do
    fields = Shift.__schema__(:fields)

    attrs =
      shift
      |> Map.take(fields)
      |> Map.drop([:id, :event_id])

    create_shift(to, attrs)
  end

  @doc """
  Assigns a member to a shift.
  """
  @spec apply_for_shift(Nyght.Shifts.Shift.t(), Nyght.Organizations.Member.t()) ::
          {:error, any} | {:ok, Nyght.Shifts.Application.t()}
  def apply_for_shift(%Shift{application_strategy: strat} = shift, %Member{} = member) do
    shift = Repo.preload(shift, [:shift_applications])
    member = Repo.preload(member, [:roles])

    # TODO: improve the change_shift_application to avoid having to pass all these params
    changeset =
      change_shift_application(%Application{}, %{
        shift_id: shift.id,
        member_id: member.id,
        status: if(strat == :availability, do: :pending, else: :accepted)
      })

    # TODO: use Ecto.Multi.put to put member and shift into the multi, everything will be cleaner
    Ecto.Multi.new()
    |> Ecto.Multi.run(:check_deletion_timestamp, &check_deletion_timestamp(&1, &2, member))
    |> Ecto.Multi.run(:check_opening_count, &check_opening_count(&1, &2, shift))
    |> Ecto.Multi.run(:check_roles, &check_roles(&1, &2, shift, member))
    |> replace_application(shift)
    |> Ecto.Multi.insert(:shift_application, changeset)
    |> Repo.transaction()
    |> application_return(shift)
  end

  def apply_for_shift(_, nil) do
    {:error, :user_cannot_be_nil}
  end

  defp replace_application(multi, %Shift{shift_applications: applications}) do
    to_replace = Enum.find(applications, &(&1.status == :waiting_replacement))

    if to_replace do
      changeset = change_shift_application(to_replace, %{status: :rejected})

      Ecto.Multi.update(multi, :update_replacement, changeset)
    else
      multi
    end
  end

  defp application_return({:ok, _} = res, shift) do
    shift = Repo.preload(shift, [:event])

    Events.broadcast!(
      __MODULE__,
      shift.event.organization_id,
      shift.event_id,
      %ShiftUpdated{event_id: shift.event_id, shift: shift}
    )

    res
  end

  defp application_return({:error, :check_deletion_timestamp, _, _}, _),
    do: {:error, :user_in_deletion}

  defp application_return({:error, :check_opening_count, _, _}, _), do: {:error, :full_shift}
  defp application_return({:error, :check_roles, _, _}, _), do: {:error, :unauthorized}
  defp application_return({:error, :shift_application, changeset, _}, _), do: {:error, changeset}

  defp check_opening_count(_repo, _params, %Shift{total_openings: nil}), do: {:ok, -1}

  defp check_opening_count(_repo, _params, %Shift{} = shift) do
    count = Shift.free_openings(shift)

    if count > 0 do
      {:ok, count}
    else
      {:error, count}
    end
  end

  defp check_roles(_repo, _params, %Shift{role: role}, %Member{roles: roles}) do
    if Enum.any?(roles, &(&1.id == role.id)) do
      {:ok, nil}
    else
      {:error, nil}
    end
  end

  defp check_deletion_timestamp(_repo, _params, %Member{id: member_id}) do
    query =
      from m in Member,
        join: u in assoc(m, :user),
        where: m.id == ^member_id and not is_nil(u.deletion_requested_at),
        select: u.deletion_requested_at

    Repo.one(query)
    |> case do
      nil ->
        {:ok, nil}

      _ ->
        {:error, :user_in_deletion}
    end
  end

  @doc """
  Makes a memeber leave a shift.

  This will change the status of the application to `:waiting_replacement`.
  """
  @spec leave_shift(Nyght.Shifts.Shift.t(), Nyght.Organizations.Member.t()) ::
          {:error, any} | {:ok, Nyght.Shifts.Application.t()}
  def leave_shift(%Shift{id: shift_id} = shift, %Member{id: member_id}) do
    shift = Repo.preload(shift, [:event])

    Application.get()
    |> Application.filter(shift: shift_id, member: member_id)
    |> Repo.one!()
    |> Application.changeset(%{status: :waiting_replacement})
    |> Repo.update()
    |> case do
      {:ok, updated_application} ->
        Events.broadcast!(
          __MODULE__,
          shift.event.organization_id,
          shift.event_id,
          %ShiftUpdated{event_id: shift.event_id, shift: shift}
        )

        {:ok, updated_application}

      err ->
        err
    end
  end

  @doc """
  Deletes a shift.

  ## Examples

      iex> delete_shift(shift)
      {:ok, %Shift{}}

      iex> delete_shift(shift)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_shift(Nyght.Shifts.Shift.t()) :: {:error, any} | {:ok, Nyght.Shifts.Shift.t()}
  def delete_shift(%Shift{} = shift) do
    shift = Repo.preload(shift, [:event])

    Repo.delete(shift)
    |> case do
      {:ok, _} ->
        Events.broadcast!(
          __MODULE__,
          shift.event.organization_id,
          shift.event_id,
          %ShiftDeleted{event_id: shift.event_id, shift: shift}
        )

        {:ok, shift}

      err ->
        err
    end
  end

  ############################################################
  # Applications
  ############################################################

  def list_applications_for(%Member{id: member_id}, params, opts \\ []) do
    Application.get()
    |> Application.filter_member(member_id)
    |> do_list_applications(params, opts)
  end

  defp do_list_applications(query, params, _opts) do
    query
    |> Application.join_for_flop()
    |> Flop.validate_and_run(params, for: Application)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking shift_application changes.

  ## Examples

      iex> change_price(shift_application)
      %Ecto.Changeset{data: %Application{}}
  """
  @spec change_shift_application(
          Nyght.Shifts.Application.t(),
          :invalid | %{optional(:__struct__) => none, optional(atom | binary) => any}
        ) :: Ecto.Changeset.t()
  def change_shift_application(%Application{} = sa, attrs \\ %{}) do
    Application.changeset(sa, attrs)
  end

  @doc """
  Gets a shift application by its ID.
  """
  @spec get_shift_application!(any) :: Nyght.Shifts.Application.t()
  def get_shift_application!(id), do: Repo.get!(Application, id) |> Repo.preload([:shift])

  @doc """
  Removes a shift application and updates the corresponding shift.
  """
  @spec delete_shift_application(Nyght.Shifts.Application.t()) ::
          {:error, Ecto.Changeset.t()} | {:ok, Nyght.Shifts.Application.t()}
  def delete_shift_application(%Application{} = application) do
    Application.changeset(application, %{status: :rejected})
    |> Repo.update()
    |> case do
      {:ok, updated} ->
        updated = Repo.preload(updated, shift: [:event])

        Events.broadcast!(
          __MODULE__,
          updated.shift.event.organization_id,
          updated.shift.event_id,
          %ShiftUpdated{event_id: updated.shift.event_id, shift: updated.shift}
        )

        {:ok, updated}

      err ->
        err
    end
  end

  @doc """
  Accepts a shift application and updates the corresponding shift.
  """
  @spec accept_shift_application(Nyght.Shifts.Application.t()) ::
          {:error, Ecto.Changeset.t()} | {:ok, Nyght.Shifts.Shift.t()}
  def accept_shift_application(%Application{} = application) do
    # TODO: this function is not tested!
    changeset = Application.changeset(application, %{status: :accepted})

    Ecto.Multi.new()
    |> Ecto.Multi.update(:application, changeset)
    |> Ecto.Multi.one(:shift, fn %{application: application} ->
      application.shift_id
      |> Shift.get()
      |> Shift.with_opening_count()
      |> Shift.preload([:shift_applications, :event])
    end)
    |> Ecto.Multi.run(:applications_to_update, fn _repo, %{shift: shift} ->
      if shift.application_strategy == :availability and
           shift.total_openings == shift.opening_count do
        to_change =
          shift.shift_applications
          |> Enum.filter(&(&1.status == :pending))
          |> Enum.map(& &1.id)

        {:ok, to_change}
      else
        {:ok, []}
      end
    end)
    |> Ecto.Multi.update_all(
      :update_applications,
      fn %{applications_to_update: apps} ->
        apps
        |> Application.get()
        |> Application.update_status(:rejected)
      end,
      []
    )
    |> Repo.transaction()
    |> case do
      {:ok, %{shift: shift}} ->
        Events.broadcast!(
          __MODULE__,
          shift.event.organization_id,
          shift.event_id,
          %ShiftUpdated{event_id: shift.event_id, shift: shift}
        )

        {:ok, shift}

      {:error, _, err, _} ->
        {:error, err}
    end
  end

  ############################################################
  # Misc
  ############################################################

  @doc """
  Checks if the given user has applied for the given shift.
  """
  @spec applied?(Nyght.Organizations.Member.t(), Nyght.Shifts.Shift.t()) :: boolean()
  def applied?(%Member{id: member_id}, %Shift{shift_applications: applications}) do
    Enum.any?(applications, fn a -> a.member_id == member_id end)
  end

  @doc """
  Checks if the given user has applied and has not been rejected for a shift in the future.
  """
  @spec applied_for_future_shift?(Nyght.Organizations.Member.t()) :: boolean()
  def applied_for_future_shift?(%Member{id: member_id}) do
    Application.get()
    |> Application.filter(
      member: member_id,
      status: [:accepted, :pending, :waiting_replacement]
    )
    |> Application.filter_by_future_shift()
    |> Repo.exists?()
  end

  @doc """
  Checks if the given user has been accepted for the shift or has not applied.
  """
  @spec accepted_or_not_applied?(Nyght.Organizations.Member.t(), Nyght.Shifts.Shift.t()) ::
          boolean()
  def accepted_or_not_applied?(%Member{id: member_id}, %Shift{} = shift) do
    if application = Enum.find(shift.shift_applications, &(&1.member_id == member_id)) do
      application.status == :accepted
    else
      true
    end
  end

  defp prepare_params(params) do
    for {k, v} <- params, do: {to_string(k), v}, into: %{}
  end
end
