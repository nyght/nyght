defmodule Nyght.TimezoneTools do
  @moduledoc """
  This module contains some functions to help with timezones.
  """

  @doc """
  Shifts the given input datetime into UTC and puts it in the output
  key.
  """
  @spec put_utc_time(Ecto.Changeset.t(), any, any, Calendar.time_zone()) :: Ecto.Changeset.t()
  def put_utc_time(changeset, _input_key, _output_key, nil), do: changeset

  def put_utc_time(changeset, input_key, output_key, original_timezone) do
    case Ecto.Changeset.get_field(changeset, input_key) do
      nil ->
        Ecto.Changeset.validate_required(changeset, [output_key])

      datetime ->
        converted =
          datetime
          |> DateTime.from_naive!(original_timezone)
          |> DateTime.shift_zone!("Etc/UTC")

        Ecto.Changeset.put_change(changeset, output_key, converted)
    end
  end

  @doc """
  Shifts the given input datetime into the specified timezone and puts
  it in the output key.
  """
  @spec put_local_time(map, any, any, Calendar.time_zone()) :: map
  def put_local_time(item, original_key, new_key, timezone) do
    Map.update(item, new_key, nil, fn _ ->
      Map.get(item, original_key)
      |> DateTime.shift_zone!(timezone)
      |> DateTime.to_naive()
    end)
  end
end
