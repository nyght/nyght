defmodule Nyght.Release do
  @moduledoc """
  Used for executing DB release tasks when run in production without Mix
  installed.
  """

  alias Ecto.Migrator

  alias Nyght.Authorizations
  alias Nyght.Organizations.Member
  alias Nyght.Shifts.Application, as: ShiftApplication

  require Logger

  import Ecto.Query

  @app :nyght

  @doc false
  def migrate do
    load_app()

    for repo <- repos() do
      {:ok, _, _} = Migrator.with_repo(repo, &Migrator.run(&1, :up, all: true))
    end
  end

  @doc false
  def rollback(repo, version) do
    load_app()
    {:ok, _, _} = Migrator.with_repo(repo, &Migrator.run(&1, :down, to: version))
  end

  @doc false
  def seed do
    load_app()

    priv_dir = "#{:code.priv_dir(@app)}"
    Code.eval_file(Path.join([priv_dir, "repo", "seeds.exs"]))
  end

  def install_permissions do
    load_app()

    Logger.info("Retrieving permissions and creating them in the database...")

    {ok, failed} =
      Authorizations.list_local_permissions_flat()
      |> Enum.map(fn {res, action} -> Authorizations.create_permission(action, res) end)
      |> Enum.reduce({0, 0}, fn
        {:ok, _}, {ok, failed} -> {ok + 1, failed}
        {:error, _}, {ok, failed} -> {ok, failed + 1}
      end)

    Logger.info(
      "Done! #{ok} permissions created, #{failed} permissions already existing or failed"
    )
  end

  def migrate_shift_applications do
    load_app()

    Logger.info("Updating the shift applications")

    query =
      from a in ShiftApplication,
        inner_join: u in assoc(a, :user),
        inner_join: s in assoc(a, :shift),
        inner_join: e in assoc(s, :event),
        inner_join: m in Member,
        on: m.user_id == u.id and m.organization_id == e.organization_id,
        update: [set: [member_id: m.id]]

    repo = List.first(repos())

    {count, _} = repo.update_all(query, [])
    Logger.info("Done! #{count} shift applications updated")
  end

  defp repos do
    Application.fetch_env!(@app, :ecto_repos)
  end

  defp load_app do
    Application.load(@app)
  end
end
