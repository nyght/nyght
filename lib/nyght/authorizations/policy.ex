defmodule Nyght.Authorizations.Policy do
  @moduledoc """
  This module defines the authorization policy behaviour.
  """

  @type action :: atom
  @type auth_result :: :ok | :error | {:error, reason :: any} | true | false

  @doc """
  Callback to authorize user's action on a resource
  """
  @callback authorize(
              member :: Nyght.Organizations.Member.t(),
              item :: any(),
              action :: action
            ) ::
              auth_result

  defmacro __using__(_) do
    quote do
      @behaviour Nyght.Authorizations.Policy
      @before_compile Nyght.Authorizations.Policy

      @permissions []
    end
  end

  defmacro __before_compile__(env) do
    permissions_attrs =
      Module.get_attribute(env.module, :permissions)
      |> Enum.sort()

    quote do
      @doc """
      false
      """
      def __permissions__ do
        unquote(permissions_attrs)
      end
    end
  end
end
