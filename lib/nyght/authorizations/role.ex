defmodule Nyght.Authorizations.Role do
  @moduledoc """
  This module defines the `Role` schema.
  """
  use Nyght.Utils.NyghtSchema,
    binding: :role,
    permissions: [:create, :list, :read, :update, :delete]

  alias Nyght.Authorizations
  alias Nyght.Authorizations.Permission
  alias Nyght.Organizations.{Member, Organization}

  @derive {
    Flop.Schema,
    filterable: [:name, :is_default],
    sortable: [:name, :is_default],
    default_order: %{
      order_by: [:name],
      order_directions: [:asc]
    }
  }

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  typed_schema "roles" do
    field(:description, :string, null: true)
    field(:name, :string)
    field(:is_default, :boolean, null: false, default: false)

    field(:permission_list, {:array, :string}, virtual: true, default: [])

    many_to_many(:permissions, Permission, join_through: "roles_permissions", on_replace: :delete)
    belongs_to(:organization, Organization)

    timestamps()
  end

  @doc false
  def changeset(role, attrs) do
    role
    |> cast(attrs, [:name, :description, :organization_id, :is_default])
    |> validate_required([:name, :organization_id])
    |> put_assoc(:permissions, fetch_permissions(attrs))
    |> unique_constraint([:organization_id, :name], error_key: :name)
  end

  def filter_default(query), do: from(q in query, where: q.is_default == true)

  defp fetch_permissions(%{permission_list: ids}),
    do: fetch_permissions(%{"permission_list" => ids})

  defp fetch_permissions(%{"permission_list" => permission_ids}) do
    permission_ids
    |> Enum.reject(&(&1 == ""))
    |> Authorizations.get_permissions()
  end

  defp fetch_permissions(_), do: []

  @impl true
  def authorize(%Member{} = member, _, action),
    do: Authorizations.member_can?(member, __MODULE__, action)

  def authorize(_member, _item, _action), do: :error
end
