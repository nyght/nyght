defmodule Nyght.Authorizations.Events do
  @moduledoc """
  This modules contains different structs representing events about authorizations.
  """

  defmodule RoleCreated do
    @moduledoc false

    defstruct role_id: nil
  end

  defmodule RoleUpdated do
    @moduledoc false

    defstruct role_id: nil
  end

  defmodule RoleDeleted do
    @moduledoc false

    defstruct role_id: nil, role: nil
  end
end
