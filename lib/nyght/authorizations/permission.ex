defmodule Nyght.Authorizations.Permission do
  @moduledoc """
  This module defines the `Permission` schema.

  A permission is a specific action granted to a role.
  """
  use TypedEctoSchema
  import Ecto.Changeset

  alias Nyght.Authorizations.Role
  alias Nyght.EctoAtom

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  typed_schema "permissions" do
    field(:resource, EctoAtom)
    field(:action, EctoAtom)

    many_to_many(:roles, Role, join_through: "roles_permissions", on_replace: :delete)
  end

  @required [:resource, :action]
  @doc false
  def changeset(permissions, attrs) do
    permissions
    |> cast(attrs, @required)
    |> validate_required(@required)
    |> unique_constraint(@required, message: "already exists")
  end
end
