defmodule Nyght.Organizations do
  @moduledoc """
  The Organizations context.
  """

  import Ecto.Query
  alias Nyght.Accounts.User
  alias Nyght.Authorizations
  alias Nyght.Authorizations.Role
  alias Nyght.Organizations
  alias Nyght.Organizations.{Member, MemberCategory, Organization}

  alias Nyght.Organizations.Events.{
    MemberCategoryCreated,
    MemberCategoryDeleted,
    MemberCategoryUpdated,
    MemberRequestApproved,
    MemberRequestCreated,
    MemberRequestRejected
  }

  alias Nyght.Repo

  #####################################################################
  # Organizations
  #####################################################################

  @doc """
  Returns a single organization.

  Raises `Ecto.NoResultsError` if the Organization does not exist.

  ## Examples

      iex> get_organization!(123)
      %Organization{}

      iex> get_organization!(456)
      ** (Ecto.NoResultsError)

  """
  @spec get_organization!(binary()) :: Nyght.Organizations.Organization.t()
  def get_organization!(id), do: Repo.get!(Organization, id)

  @doc """
  Returns the first organization, sorted by insertion date.
  """
  @spec get_first_organization :: Nyght.Organizations.Organization.t() | nil
  def get_first_organization do
    Organization.get_first()
    |> Repo.one()
  end

  @doc """
  Returns an organization corresponding to the given slug.
  """
  @spec get_organization_by_slug(binary()) :: Nyght.Organizations.Organization.t() | nil
  def get_organization_by_slug(nil), do: nil
  def get_organization_by_slug(slug), do: Repo.get_by(Organization, slug: slug)

  def get_organization_by_slug!(slug), do: Repo.get_by!(Organization, slug: slug)

  @doc """
  Creates a organization.
  """
  @spec create_organization(map()) ::
          {:ok, Nyght.Organizations.Organization.t()} | {:error, Ecto.Changeset.t()}
  def create_organization(attrs) do
    %Organization{}
    |> Organization.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a organization.
  """
  @spec update_organization(Nyght.Organizations.Organization.t(), map()) ::
          {:ok, Nyght.Organizations.Organization.t()} | {:error, Ecto.Changeset.t()}
  def update_organization(%Organization{} = organization, attrs) do
    organization
    |> Organization.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking organization changes.
  """
  @spec change_organization(Nyght.Organizations.Organization.t(), map()) :: Ecto.Changeset.t()
  def change_organization(%Organization{} = org, attrs \\ %{}) do
    Organization.changeset(org, attrs)
  end

  #####################################################################
  # Members
  #####################################################################

  @doc """
  Lists members.

  See Flop documentation for the filter, sorting and pagination
  parameters.

  ## Options

  The following options can be passed:

  - `preload`: list of preloads, see `Repo.preload/3` for examples.
    Defaults to `[]`

  """
  @spec list_members(Nyght.Organizations.Organization.t(), map(), keyword()) ::
          {:error, Flop.Meta.t()} | {:ok, {list(Nyght.Organizations.Member.t()), Flop.Meta.t()}}
  def list_members(%Organization{id: org_id}, %{} = params, opts \\ []) do
    opts = Keyword.put(opts, :status, :approved)

    Member.get()
    |> Member.filter_organization(org_id)
    |> do_list_member(params, opts)
  end

  @doc """
  Lists members requests.

  See Flop documentation for the filter, sorting and pagination
  parameters.

  ## Options

  The following options can be passed:

  - `preload`: list of preloads, see `Repo.preload/3` for examples.
    Defaults to `[]`

  """
  @spec list_member_requests(Nyght.Organizations.Organization.t(), map(), keyword()) ::
          {:error, Flop.Meta.t()} | {:ok, {list(Nyght.Organizations.Member.t()), Flop.Meta.t()}}
  def list_member_requests(%Organization{id: org_id}, %{} = params, opts \\ []) do
    opts = Keyword.put(opts, :status, :waiting_approval)

    Member.get()
    |> Member.filter_organization(org_id)
    |> do_list_member(params, opts)
  end

  @doc """
  Returns all the memberships for a user.
  """
  @spec list_memberships_for(Nyght.Accounts.User.t()) :: list(Nyght.Organizations.Member.t())
  def list_memberships_for(%User{id: user_id}) do
    Member.get()
    |> Member.filter_user(user_id)
    |> Member.preload([:roles, :organization])
    |> Repo.all()
  end

  @doc """
  Makes the specified user a member of the organization.

  If the member the auto-approval is enabled for the organization, the
  new member will be directly approved and will be assigned the
  organization's default roles.
  """
  @spec join(Nyght.Organizations.Organization.t(), Nyght.Accounts.User.t()) ::
          {:ok, Nyght.Organizations.Member.t()} | {:error, Ecto.Changeset.t()}
  def join(%Organization{is_auto_approval_enabled: false} = org, %User{id: user_id}) do
    org
    |> Ecto.build_assoc(:members)
    |> Member.changeset(%{"user_id" => user_id, "status" => :waiting_approval, "roles" => []})
    |> Repo.insert()
    |> broadcast_result!(&%MemberRequestCreated{member_id: &1.id})
  end

  def join(%Organization{is_auto_approval_enabled: true} = org, %User{id: user_id}) do
    default_roles =
      Authorizations.list_default_roles(org)
      |> Enum.map(& &1.id)

    org
    |> Ecto.build_assoc(:members)
    |> Member.changeset(%{
      "user_id" => user_id,
      "status" => :approved,
      "approved_at" => DateTime.utc_now(),
      "roles" => default_roles
    })
    |> Repo.insert()
    |> broadcast_result!(&%MemberRequestApproved{member_id: &1.id})
  end

  @doc "Approve a member request."
  @spec approve_member(Nyght.Organizations.Member.t(), map()) ::
          {:ok, Nyght.Organizations.Member.t()} | {:error, Ecto.Changeset.t()}
  def approve_member(member, attrs \\ %{})
  def approve_member(%Member{status: :approved}, _), do: {:error, :already_approved}

  def approve_member(member, attrs) do
    attrs =
      Map.merge(attrs, %{status: :approved, approved_at: DateTime.utc_now()})
      |> prepare_params()

    member
    |> Organizations.change_member(attrs)
    |> Repo.update()
    |> broadcast_result!(&%MemberRequestApproved{member_id: &1.id})
  end

  @doc """
  Deletes a member request.

  This only works for members waiting for approval. The user will be
  able to make another request after the rejection. To avoid the, ban
  the member.
  """
  def reject_member(%Member{status: :waiting_approval} = member) do
    Repo.delete(member)
    |> broadcast_result!(&%MemberRequestRejected{member_id: &1.id})
  end

  def reject_member(_), do: {:error, :not_waiting_approval}

  @doc """
  Returns an member by its id or raises an error if not found.
  """
  @spec get_member!(binary()) :: Nyght.Organizations.Member.t()
  def get_member!(id) do
    Repo.get!(Member, id)
    |> Repo.preload([:user, :roles])
  end

  def get_member!(%Organization{id: org_id}, %User{id: user_id}, opts \\ []) do
    do_get_member(org_id, user_id, opts)
    |> Repo.one!()
  end

  def get_member(%Organization{id: org_id}, %User{id: user_id}, opts \\ []) do
    do_get_member(org_id, user_id, opts)
    |> Repo.one()
  end

  @doc """
  Gets the member entry for a given user in a given organization or
  raises an error if not found.
  """
  @spec get_member_for!(Nyght.Organizations.Organization.t(), Nyght.Accounts.User.t()) ::
          Nyght.Organizations.Member.t()
  def get_member_for!(%Organization{id: org_id}, %User{id: user_id}, opts \\ []) do
    do_get_member(org_id, user_id, opts)
    |> Repo.one!()
  end

  @doc """
  Same as `get_member_for!/3` but doesn't raise error if no member is found.
  """
  @spec get_member_for(Nyght.Organizations.Organization.t(), Nyght.Accounts.User.t()) ::
          Nyght.Organizations.Member.t() | nil
  def get_member_for(org, user, opts \\ [])
  def get_member_for(_, nil, _), do: nil

  def get_member_for(%Organization{id: org_id}, %User{id: user_id}, opts) do
    do_get_member(org_id, user_id, opts)
    |> Repo.one()
  end

  defp do_get_member(org_id, user_id, opts) do
    Member.get()
    |> Member.filter_organization(org_id)
    |> Member.filter_user(user_id)
    |> Member.preload(Keyword.get(opts, :preload, []))
  end

  @doc """
  Creates a new member for the given user in the given organization.

  A list of `Nyght.Authorizations.Role` can be passed as a second
  parameter, if none is given, the member won't have any role.
  """
  @spec create_member(
          Nyght.Organizations.Organization.t(),
          Nyght.Accounts.User.t(),
          list(Nyght.Authorizations.Role.t()) | nil
        ) ::
          {:ok, Nyght.Organizations.Member.t()} | {:error, Ecto.Changeset.t()}
  def create_member(%Organization{id: org_id}, %User{id: user_id}, roles \\ []) do
    %Member{}
    |> Member.changeset(%{
      user_id: user_id,
      organization_id: org_id,
      roles: Enum.map(roles, & &1.id)
    })
    |> Repo.insert()
  end

  @doc """
  Updates a member.
  """
  @spec update_member(
          Nyght.Organizations.Member.t(),
          map()
        ) :: {:ok, Nyght.Organizations.Member.t()} | {:error, Ecto.Changeset.t()}
  def update_member(%Member{} = member, attrs) do
    member
    |> change_member(attrs)
    |> Repo.update()
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking member changes.
  """
  @spec change_member(Nyght.Organizations.Member.t(), map() | nil) :: Ecto.Changeset.t()
  def change_member(%Member{} = member, attrs \\ %{}) do
    member
    |> Repo.preload([:user, :roles, :categories])
    |> Member.changeset(attrs)
  end

  @doc """
  Deletes a member.
  """
  @spec delete_member(Nyght.Organizations.Member.t()) ::
          {:ok, Nyght.Organizations.Member.t()} | {:error, Ecto.Changeset.t()}
  def delete_member(%Member{} = member), do: Repo.delete(member)

  def member?(%Organization{id: org_id}, %User{id: user_id}) do
    Member.get()
    |> Member.filter_organization(org_id)
    |> Member.filter_user(user_id)
    |> Member.filter_status(:approved)
    |> Repo.exists?()
  end

  def member?(_, nil), do: false

  def search_member_by_name(name, %Organization{id: org_id}, %Role{id: role_id}) do
    similarity = 0.1

    query =
      from m in Member,
        left_join: u in assoc(m, :user),
        left_join: r in assoc(m, :roles),
        where:
          r.id == ^role_id and
            m.organization_id == ^org_id and
            fragment(
              "SIMILARITY(coalesce(?, '') || ' ' ||  coalesce(?, '') || ' ' || coalesce(?, ''), ?) > ?",
              u.first_name,
              u.nickname,
              u.last_name,
              ^name,
              ^similarity
            ),
        order_by:
          fragment(
            "1- SIMILARITY(coalesce(?, '') || ' ' ||  coalesce(?, '') || ' ' || coalesce(?, ''), ?) > ?",
            u.first_name,
            u.nickname,
            u.last_name,
            ^name,
            ^similarity
          ),
        limit: 10,
        preload: [user: u]

    Repo.all(query)
  end

  #####################################################################
  # MemberCategories
  #####################################################################

  @doc """
  Lists the member categories for a given organization.
  """
  @spec list_member_categories_for(Nyght.Organizations.Organization.t(), map()) ::
          {:error, Flop.Meta.t()}
          | {:ok, {list(Nyght.Organizations.MemberCategory.t()), Flop.Meta.t()}}
  def list_member_categories_for(%Organization{id: org_id}, %{} = params) do
    MemberCategory.get()
    |> MemberCategory.filter_organization(org_id)
    |> Flop.validate_and_run(params, for: MemberCategory)
  end

  @doc """
  Lists **all** the `Nyght.Organizations.MemberCategory` for an organization.

  Prefer `list_member_categories_for/2` for common use, it will be paginated and
  return Flop structs.
  """
  @spec list_all_member_categories(Nyght.Organizations.Organization.t()) ::
          list(Nyght.Organizations.MemberCategory.t())
  def list_all_member_categories(%Organization{id: org_id}) do
    MemberCategory.get()
    |> MemberCategory.filter_organization(org_id)
    |> Repo.all()
  end

  @doc """
  Returns a list of `Nyght.Organizations.MemberCategory` corresponding to the given id list.
  """
  @spec get_member_categories(list(binary())) :: list(Nyght.Organizations.MemberCategory.t())
  def get_member_categories(ids) do
    MemberCategory.get(ids)
    |> Repo.all()
  end

  @doc """
  Returns a member category by its id or raises an error if not found.
  """
  @spec get_member_category!(binary()) :: Nyght.Organizations.MemberCategory.t()
  def get_member_category!(id) do
    Repo.get!(MemberCategory, id)
  end

  @doc """
  Create a new member category for the given organization.
  """
  @spec create_member_category(Nyght.Organizations.Organization.t(), map()) ::
          {:ok, Nyght.Organizations.MemberCategory.t()} | {:error, Ecto.Changeset.t()}
  def create_member_category(%Organization{} = organization, attrs) do
    organization
    |> Ecto.build_assoc(:member_categories)
    |> change_member_category(attrs)
    |> Repo.insert()
    |> broadcast_result!(&%MemberCategoryCreated{category_id: &1.id})
  end

  @doc """
  Updates the given member category.
  """
  @spec update_member_category(Nyght.Organizations.MemberCategory.t(), map()) ::
          {:ok, Nyght.Organizations.MemberCategory.t()} | {:error, Ecto.Changeset.t()}
  def update_member_category(%MemberCategory{} = category, attrs) do
    category
    |> change_member_category(attrs)
    |> Repo.update()
    |> broadcast_result!(&%MemberCategoryUpdated{category_id: &1.id})
  end

  @doc """
  Returns a changeset for a member category.
  """
  @spec change_member_category(Nyght.Organizations.MemberCategory.t(), map()) ::
          Ecto.Changeset.t()
  def change_member_category(%MemberCategory{} = category, attrs \\ %{}) do
    MemberCategory.changeset(category, attrs)
  end

  @doc """
  Deletes the given member category.
  """
  @spec delete_member_category(Nyght.Organizations.MemberCategory.t()) ::
          {:ok, Nyght.Organizations.MemberCategory.t()} | {:error, Ecto.Changeset.t()}
  def delete_member_category(%MemberCategory{} = category) do
    Repo.delete(category)
    |> broadcast_result!(&%MemberCategoryDeleted{category_id: &1.id, category: &1})
  end

  ############################################################
  # Pubsub
  ############################################################

  @doc """
  Subscribe to events from all events of an organization.
  """
  @spec subscribe(Nyght.Organizations.Organization.t()) ::
          :ok | {:error, {:already_registered, pid}}
  def subscribe(%Organization{id: org_id}) do
    Phoenix.PubSub.subscribe(Nyght.PubSub, topic(org_id))
  end

  @doc """
  Broadcasts an event to all the listeners.
  """
  def broadcast!(source, org_id, event) do
    Phoenix.PubSub.broadcast!(Nyght.PubSub, topic(org_id), {source, event})
  end

  def broadcast_result!({:ok, %{organization_id: org_id} = item}, event) do
    broadcast!(__MODULE__, org_id, event.(item))

    {:ok, item}
  end

  def broadcast_result!({:error, _} = res, _), do: res

  defp topic(org_id), do: "org:#{org_id}"
  ############################################################
  # Misc
  ############################################################

  defp prepare_params(params) do
    for {k, v} <- params, do: {to_string(k), v}, into: %{}
  end

  defp do_list_member(query, params, opts) do
    query
    |> Member.join_user()
    |> Member.filter_status(Keyword.get(opts, :status))
    |> Member.preload(Keyword.get(opts, :preload, []))
    |> Flop.validate_and_run(params, for: Member)
  end
end
