defmodule Nyght.VObjects do
  @moduledoc """
  This module contains tools to export events and contacts into ical and vcard format.
  """

  defmodule VCalendar do
    @moduledoc """
    Represents a `VCALENDAR` object
    """
    @type t :: %VCalendar{
            __vobject_type__: binary(),
            prodid: binary(),
            version: float(),
            method: binary(),
            calscale: binary(),
            events: list(Nyght.VObjects.VEvent.t()) | Nyght.VObjects.VEvent.t()
          }

    defstruct __vobject_type__: "VCALENDAR",
              prodid: "-//Nyght//iCal API//EN",
              version: 2.0,
              method: "PUBLISH",
              calscale: "GREGORIAN",
              events: []
  end

  defmodule VEvent do
    @moduledoc """
    Represents a `VEVENT` object
    """
    @type t :: %VEvent{
            __vobject_type__: binary(),
            uid: binary(),
            summary: binary(),
            description: binary(),
            dtstamp: DateTime.t() | NaiveDateTime.t(),
            dtstart: DateTime.t() | NaiveDateTime.t(),
            dtend: DateTime.t() | NaiveDateTime.t(),
            "last-modified": DateTime.t() | NaiveDateTime.t(),
            status: binary(),
            class: binary(),
            url: binary()
          }

    defstruct [
      :uid,
      :summary,
      :description,
      :dtstamp,
      :dtstart,
      :dtend,
      :"last-modified",
      :status,
      :class,
      :url,
      __vobject_type__: "VEVENT"
    ]
  end

  defmodule VCard do
    @moduledoc """
    Represents a `VCARD` object
    """
    @type t :: %VCard{
            __vobject_type__: binary(),
            fn: binary(),
            nickname: binary(),
            tel: binary(),
            email: binary(),
            version: float()
          }

    defstruct [
      :fn,
      :nickname,
      :tel,
      :email,
      __vobject_type__: "VCARD",
      version: 4.0
    ]
  end

  alias Nyght.VObjects.VCalendar
  alias Nyght.VObjects.VEvent

  @doc false
  def encode_to_iodata!(object, _options \\ []) do
    object
    |> format_object()
    |> IO.iodata_to_binary()
  end

  @doc """
  Format a text to use in an ical object.
  """
  def format_text(key, content) do
    # TODO: find a better way to escape characters that need to be escaped.

    "#{key}:#{content}"
    |> String.replace(~r/\n/, "\\n")
    |> String.replace(~r/\t/, "\\t")
    |> String.codepoints()
    |> Enum.chunk_every(73)
    |> Enum.join("\r\n ")
    |> String.slice((String.length(key) + 1)..-1)
  end

  defp format_object(data) do
    content =
      data
      |> Map.from_struct()
      |> Map.drop([:__vobject_type__])
      |> Enum.map_join("\r\n", &format_kv/1)

    type = data.__vobject_type__

    "BEGIN:#{type}\r\n#{content}\r\nEND:#{type}"
  end

  defp format_kv({_, nil}), do: ""
  defp format_kv({:content, value}), do: value

  defp format_kv({key, %DateTime{} = value}) do
    key =
      key
      |> Atom.to_string()
      |> String.upcase()

    "#{key}:#{Calendar.strftime(value, "%Y%m%dT%H%M%S")}Z"
  end

  defp format_kv({key, %NaiveDateTime{} = value}) do
    key =
      key
      |> Atom.to_string()
      |> String.upcase()

    "#{key}:#{Calendar.strftime(value, "%Y%m%dT%H%M%S")}Z"
  end

  defp format_kv({_, %VEvent{} = value}) do
    format_object(value)
  end

  defp format_kv({key, value}) do
    key =
      key
      |> Atom.to_string()
      |> String.upcase()

    "#{key}:#{value}"
  end
end
