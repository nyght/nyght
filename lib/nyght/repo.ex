defmodule Nyght.Repo do
  @moduledoc false

  use Appsignal.Ecto.Repo,
    otp_app: :nyght,
    adapter: Ecto.Adapters.Postgres
end
