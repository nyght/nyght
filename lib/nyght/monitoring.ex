defmodule Nyght.Monitoring do
  @moduledoc """
  This module contains the plugs and other functions to abstract monitoring.
  """

  @doc """
  Adds a namespace to the root span.
  """
  def set_namespace(conn, opts) do
    Appsignal.Span.set_namespace(Appsignal.Tracer.root_span(), opts[:name])

    conn
  end

  @doc """
  Adds some context tags to the root span.
  """
  def set_context(%Plug.Conn{assigns: assigns} = conn, _opts) do
    set_tags(%{
      user_id: get_user_id(assigns),
      org_slug: get_org_slug(assigns)
    })

    conn
  end

  defp get_user_id(%{current_user: user}) when not is_nil(user), do: user.id
  defp get_user_id(_), do: nil

  defp get_org_slug(%{current_organization: org}) when not is_nil(org), do: org.slug
  defp get_org_slug(_), do: nil

  @doc """
  Adds tags to the root span.

  Be careful, this will override previous tags.
  """
  def set_tags(attrs) do
    Appsignal.Span.set_sample_data(Appsignal.Tracer.root_span(), "tags", attrs)
  end
end
