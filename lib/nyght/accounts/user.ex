defmodule Nyght.Accounts.User do
  @moduledoc false

  use TypedEctoSchema
  use Nyght.Authorizations.Policy

  import Ecto.Changeset
  import Ecto.Query

  alias __MODULE__
  alias Nyght.Accounts.{Password, User}
  alias Nyght.Authorizations
  alias Nyght.Organizations.Member

  @type diet() :: :pescetarian | :vegetarian | :vegan | nil

  @permissions [:read_details, :download]

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  typed_schema "users" do
    field(:email, :string)
    field(:password, :string, virtual: true, redact: true)
    field(:hashed_password, :string, redact: true)
    field(:current_password, :string, virtual: true, redact: true)
    field(:confirmed_at, :naive_datetime, null: true)

    field(:first_name, :string, null: true)
    field(:last_name, :string, null: true)
    field(:nickname, :string, null: true)

    field(:street_line_1, :string, null: true)
    field(:street_line_2, :string, null: true)
    field(:street_line_3, :string, null: true)
    field(:zip_code, :integer, null: true)
    field(:city, :string, null: true)

    field(:phone_number, :string, null: true)

    field(:diet, Ecto.Enum, values: [:pescetarian, :vegetarian, :vegan], null: true) ::
      User.diet()

    field(:allergies, :string, null: true)
    field(:pronouns, :string, null: true)

    field(:deletion_requested_at, :utc_datetime, null: true)

    has_many(:organizations, Member)

    timestamps()
  end

  @doc """
  A user changeset for registration.

  It is important to validate the length of both email and password.
  Otherwise databases may truncate the email without warnings, which
  could lead to unpredictable or insecure behaviour. Long passwords may
  also be very expensive to hash for certain algorithms.

  ## Options

    * `:hash_password` - Hashes the password so it can be stored securely
      in the database and ensures the password field is cleared to prevent
      leaks in the logs. If password hashing is not needed and clearing the
      password field is not desired (like when using this changeset for
      validations on a LiveView form), this option can be set to `false`.
      Defaults to `true`.

    * `:validate_email` - Validates the uniqueness of the email, in case
      you don't want to validate the uniqueness of the email (like when
      using this changeset for validations on a LiveView form before
      submitting the form), this option can be set to `false`.
      Defaults to `true`.
  """
  def registration_changeset(user, attrs, opts \\ []) do
    user
    |> cast(attrs, [
      :email,
      :password,
      :first_name,
      :last_name,
      :nickname,
      :phone_number,
      :street_line_1,
      :street_line_2,
      :street_line_3,
      :zip_code,
      :city,
      :deletion_requested_at
    ])
    |> validate_required([:first_name, :last_name, :phone_number])
    |> validate_email(opts)
    |> validate_password(opts)
    |> validate_address()
  end

  @doc """
  A user changeset for the user profile.
  """
  def profile_changeset(user, attrs) do
    user
    |> cast(attrs, [
      :first_name,
      :last_name,
      :nickname,
      :phone_number,
      :street_line_1,
      :street_line_2,
      :street_line_3,
      :zip_code,
      :city,
      :diet,
      :allergies,
      :pronouns,
      :deletion_requested_at
    ])
    |> validate_required([:first_name, :last_name, :phone_number])
    |> validate_address()
  end

  def diets do
    [:pescetarian, :vegetarian, :vegan]
  end

  def full_name(%__MODULE__{} = user, opts \\ []) do
    redacted = Keyword.get(opts, :redacted?, true)

    %__MODULE__{first_name: first_name, last_name: last_name, nickname: nickname} = user

    last_name =
      if redacted do
        "#{String.first(last_name)}."
      else
        last_name
      end

    if is_nil(nickname) do
      "#{first_name} #{last_name}"
    else
      "#{first_name} \"#{nickname}\" #{last_name}"
    end
  end

  def color(%User{id: id}), do: RandomColour.generate(seed: id, luminosity: "dark")

  defp validate_address(changeset) do
    changeset
    |> validate_required([:street_line_1, :zip_code, :city])
    |> validate_length(:street_line_1, max: 200)
    |> validate_length(:street_line_2, max: 200)
    |> validate_length(:street_line_3, max: 200)
    |> validate_length(:city, max: 100)
  end

  defp validate_email(changeset, opts) do
    changeset
    |> validate_required([:email])
    |> validate_format(:email, ~r/^[^\s]+@[^\s]+$/, message: "must have the @ sign and no spaces")
    |> validate_length(:email, max: 160)
    |> maybe_validate_unique_email(opts)
  end

  defp validate_password(changeset, opts) do
    changeset
    |> validate_required([:password])
    |> validate_length(:password, min: 8, max: 72)
    |> validate_format(:password, ~r/[a-z]/, message: "at least one lower case character")
    |> validate_format(:password, ~r/[A-Z]/, message: "at least one upper case character")
    |> validate_format(:password, ~r/[!?@#$%^&*_0-9]/,
      message: "at least one digit or punctuation character"
    )
    |> maybe_hash_password(opts)
  end

  defp maybe_hash_password(changeset, opts) do
    hash_password? = Keyword.get(opts, :hash_password, true)
    password = get_change(changeset, :password)

    if hash_password? && password && changeset.valid? do
      changeset
      # If using Bcrypt, then further validate it is at most 72 bytes long
      |> validate_length(:password, max: 72, count: :bytes)
      |> put_change(:hashed_password, Password.hash(password))
      |> delete_change(:password)
    else
      changeset
    end
  end

  defp maybe_validate_unique_email(changeset, opts) do
    if Keyword.get(opts, :validate_email, true) do
      changeset
      |> unsafe_validate_unique(:email, Nyght.Repo)
      |> unique_constraint(:email)
    else
      changeset
    end
  end

  @doc """
  A user changeset for changing the email.

  It requires the email to change otherwise an error is added.
  """
  def email_changeset(user, attrs, opts \\ []) do
    user
    |> cast(attrs, [:email])
    |> validate_email(opts)
    |> case do
      %{changes: %{email: _}} = changeset -> changeset
      %{} = changeset -> add_error(changeset, :email, "did not change")
    end
  end

  @doc """
  A user changeset for changing the password.

  ## Options

    * `:hash_password` - Hashes the password so it can be stored securely
      in the database and ensures the password field is cleared to prevent
      leaks in the logs. If password hashing is not needed and clearing the
      password field is not desired (like when using this changeset for
      validations on a LiveView form), this option can be set to `false`.
      Defaults to `true`.
  """
  def password_changeset(user, attrs, opts \\ []) do
    user
    |> cast(attrs, [:password])
    |> validate_confirmation(:password, message: "does not match password")
    |> validate_password(opts)
  end

  @doc """
  Confirms the account by setting `confirmed_at`.
  """
  def confirm_changeset(user) do
    now = NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)
    change(user, confirmed_at: now)
  end

  @doc """
  Check if the requests from Users to be deleted are older than a week.
  """
  def get_users_to_delete do
    from u in User,
      where: u.deletion_requested_at < ago(7, "day")
  end

  @doc """
  Verifies the password.

  If there is no user or the user doesn't have a password, we call
  `Bcrypt.no_user_verify/0` to avoid timing attacks.
  """
  def valid_password?(%Nyght.Accounts.User{hashed_password: hashed_password}, password)
      when is_binary(hashed_password) and byte_size(password) > 0 do
    Password.match?(password, hashed_password)
  end

  def valid_password?(_, _) do
    Password.dummy_calculation()
    false
  end

  @doc """
  Validates the current password otherwise adds an error to the changeset.
  """
  def validate_current_password(changeset, password) do
    changeset = cast(changeset, %{current_password: password}, [:current_password])

    if valid_password?(changeset.data, password) do
      changeset
    else
      add_error(changeset, :current_password, "is not valid")
    end
  end

  @impl true
  def authorize(%Member{user_id: user_id}, %User{id: user_id}, :read_details), do: :ok
  def authorize(%Member{user_id: user_id}, %User{id: user_id}, :download), do: :ok

  def authorize(%Member{} = member, _item, action),
    do: Authorizations.member_can?(member, __MODULE__, action)

  def authorize(_member, _item, _action), do: :error
end
