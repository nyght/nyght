defmodule Nyght.Accounts.Password do
  @moduledoc """
  This module contains functions to manage password hashing and checking.
  """
  @doc """
  Hashes a password with a randomly generated salt.
  """
  def hash(password) do
    Bcrypt.hash_pwd_salt(password)
  end

  @doc """
  Hashes a password with a randomly generated salt.
  """
  def match?(password, hash) do
    Bcrypt.verify_pass(password, hash)
  end

  @doc """
  Runs the password hash function, but always returns false.
  """
  def dummy_calculation do
    Bcrypt.no_user_verify()
  end
end
