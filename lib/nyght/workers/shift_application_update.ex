defmodule Nyght.Workers.ShiftApplicationUpdate do
  @moduledoc """
  Oban job to send shift application update emails to user.

  This job is created when someone approved or rejected a shift application
  for a specific user, and is scheduled to run 2 hours after. Each time one
  of the user's application is updated, the job gets rescheduled.

  This is done like this so all the updates get buffered and batched.
  """

  # 2 hours
  @update_delay 2 * 60 * 60

  use Oban.Worker,
    unique: [
      period: @update_delay,
      fields: [:args],
      keys: [:user_id]
    ]

  require Logger

  alias Nyght.{Emails, Organizations, Shifts}
  alias Oban.Job

  def update_delay, do: @update_delay

  @impl true
  def perform(%Job{args: %{"member_id" => member_id}, inserted_at: inserted_at}) do
    member = Organizations.get_member!(member_id)
    inserted_at = DateTime.add(inserted_at, -10)

    Logger.info("Running shift update notification job for member #{member_id}")

    with {:ok, {shifts, _meta}} <- Shifts.list_shifts_for(member, %{}, preload: [:event]),
         {:ok, shifts} <- filter_shifts(shifts, inserted_at),
         {:ok, email} <- Emails.deliver_shifts_update_message(member.user, shifts) do
      {:ok, email}
    else
      {:error, :empty} ->
        # this should never happend, unless a job is scheduled with a different
        # time interval
        {:cancel, :empty}

      err ->
        err
    end
  end

  defp filter_shifts([], _), do: {:error, :empty}

  defp filter_shifts(shifts, time) do
    # TODO: filter the shift in the SQL query after the updated_at and inserted_at
    # timestamps are migrated to utc timestamps.

    shifts =
      shifts
      |> Enum.filter(fn s ->
        List.first(s.shift_applications).updated_at
        |> DateTime.from_naive!("Etc/UTC")
        |> DateTime.compare(time) != :lt
      end)

    {:ok, shifts}
  end
end
