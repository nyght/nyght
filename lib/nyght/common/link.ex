defmodule Nyght.Common.Link do
  @moduledoc """
  This is an embedded schema to store links.
  """
  use TypedEctoSchema

  import Ecto.Changeset

  typed_embedded_schema do
    field(:name, :string)
    field(:url, :string)
  end

  @doc false
  def changeset(link, attrs) do
    link
    |> cast(attrs, [:name, :url])
    |> validate_required(:url)
    |> validate_format(:url, ~r/^http[s]?:\/\//, message: "should start with http(s)")
  end
end
