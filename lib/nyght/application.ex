defmodule Nyght.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    Ecto.DevLogger.install(Nyght.Repo)

    # credo:disable-for-lines:2
    Appsignal.Phoenix.LiveView.attach()
    Appsignal.Logger.Handler.add("phoenix")

    children = [
      # Start the Ecto repository
      Nyght.Repo,
      # Start Oban
      {Oban, Application.fetch_env!(:nyght, Oban)},
      # Start the Telemetry supervisor
      NyghtWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Nyght.PubSub},
      # Start the Endpoint (http/https)
      NyghtWeb.Endpoint
      # Start a worker by calling: Nyght.Worker.start_link(arg)
      # {Nyght.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Nyght.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    NyghtWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
