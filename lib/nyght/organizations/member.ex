defmodule Nyght.Organizations.Member do
  @moduledoc """
  This module defines the schema of a member of an organization.
  """
  use Nyght.Utils.NyghtSchema,
    binding: :member,
    permissions: [:approve, :reject, :list, :read, :update, :delete]

  alias Nyght.Accounts.User
  alias Nyght.Authorizations
  alias Nyght.Authorizations.Role
  alias Nyght.Organizations
  alias Nyght.Organizations.{Member, MemberCategory, MemberRole, Organization}

  @derive {
    Flop.Schema,
    filterable: [
      :full_name,
      :inserted_at
    ],
    sortable: [:full_name, :inserted_at],
    adapter_opts: [
      join_fields: [
        first_name: [
          binding: :user,
          field: :first_name,
          ecto_type: :string
        ],
        last_name: [
          binding: :user,
          field: :last_name,
          ecto_type: :string
        ]
      ],
      compound_fields: [full_name: [:first_name, :last_name]]
    ],
    default_order: %{
      order_by: [:full_name],
      order_directions: [:asc]
    }
  }

  @type status() :: :waiting_approval | :approved | :banned

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  typed_schema "members" do
    field(:approved_at, :utc_datetime)
    field(:status, Ecto.Enum, values: [:waiting_approval, :approved, :banned]) :: Member.status()

    belongs_to(:user, User)
    belongs_to(:organization, Organization)

    many_to_many(:roles, Role, join_through: MemberRole, on_replace: :delete)

    many_to_many(:categories, MemberCategory,
      join_through: "members_member_categories",
      join_keys: [member_id: :id, member_category_id: :id],
      on_replace: :delete
    )

    timestamps()
  end

  @required [:user_id, :organization_id]
  @doc false
  def changeset(member, attrs) do
    member
    |> cast(attrs, @required ++ [:status, :approved_at])
    |> validate_required(@required)
    |> put_roles(attrs)
    |> put_categories(attrs)
    |> unique_constraint(@required,
      name: :members_unique_member_index,
      message: "is already member",
      error_key: :user
    )
    |> unique_constraint(:roles,
      name: :members_roles_pkey,
      message: "has already this role",
      error_key: :roles
    )
  end

  defp put_roles(changeset, attrs) do
    case Map.get(attrs, :roles, Map.get(attrs, "roles")) do
      nil ->
        changeset

      role_ids ->
        roles = Authorizations.get_roles(role_ids)

        put_assoc(changeset, :roles, roles)
    end
  end

  defp put_categories(changeset, attrs) do
    case Map.get(attrs, :categories, Map.get(attrs, "categories")) do
      nil ->
        changeset

      category_ids ->
        categories = Organizations.get_member_categories(category_ids)

        put_assoc(changeset, :categories, categories)
    end
  end

  @doc "Filters the query by status"
  @spec filter_status(Ecto.Query.t(), atom() | list(atom()) | nil) :: Ecto.Query.t()
  def filter_status(query, nil), do: query

  def filter_status(query, status) when is_atom(status) do
    from q in query, where: q.status == ^status
  end

  def filter_status(query, status) when is_list(status) do
    from q in query, where: q.status in ^status
  end

  @doc "Filters the query by user id"
  @spec filter_user(Ecto.Query.t(), binary()) :: Ecto.Query.t()
  def filter_user(query, user_id) do
    from [member: m] in query, where: m.user_id == ^user_id
  end

  @doc "Filters the query by role id"
  @spec filter_role(Ecto.Query.t(), binary()) :: Ecto.Query.t()
  def filter_role(query, role_id) do
    from [member: m] in query,
      inner_join: r in assoc(m, :roles),
      where: r.id == ^role_id
  end

  @doc "Joins the user"
  @spec join_user(Ecto.Query.t()) :: Ecto.Query.t()
  def join_user(query) do
    if has_named_binding?(query, :user) do
      query
    else
      from [member: m] in query,
        left_join: u in assoc(m, :user),
        as: :user
    end
  end

  @impl true
  def authorize(%Member{user_id: user_id}, %Member{user_id: user_id}, :read), do: :ok

  def authorize(%Member{} = member, _item, action),
    do: Authorizations.member_can?(member, __MODULE__, action)

  def authorize(_member, _item, _action), do: :error
end
