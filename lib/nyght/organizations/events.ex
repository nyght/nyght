defmodule Nyght.Organizations.Events do
  @moduledoc """
  This modules contains different structs representing events about organizations.
  """

  defmodule MemberRequestCreated do
    @moduledoc false

    defstruct member_id: nil
  end

  defmodule MemberRequestApproved do
    @moduledoc false

    defstruct member_id: nil
  end

  defmodule MemberRequestRejected do
    @moduledoc false

    defstruct member_id: nil
  end

  defmodule MemberUpdated do
    @moduledoc false

    defstruct member_id: nil
  end

  defmodule MemberDeleted do
    @moduledoc false

    defstruct member_id: nil
  end

  defmodule OrganizationUpdated do
    @moduledoc false

    defstruct organization_id: nil
  end

  defmodule MemberCategoryCreated do
    @moduledoc false

    defstruct category_id: nil
  end

  defmodule MemberCategoryUpdated do
    @moduledoc false

    defstruct category_id: nil
  end

  defmodule MemberCategoryDeleted do
    @moduledoc false

    defstruct category_id: nil, category: nil
  end
end
