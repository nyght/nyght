defmodule Nyght.Organizations.Organization do
  @moduledoc """
  This module defines the `Organization` schema.
  """
  use Nyght.Utils.NyghtSchema,
    binding: :organization,
    permissions: [:read, :update, :delete]

  import TzExtra.Changeset

  alias Nyght.Authorizations
  alias Nyght.Authorizations.Role
  alias Nyght.Events.Event
  alias Nyght.Organizations.{Member, MemberCategory, Organization}

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  typed_schema "organizations" do
    field(:description, :string, null: true)
    field(:short_description, :string, null: true)
    field(:name, :string)
    field(:slug, :string)
    field(:timezone, :string, default: "Etc/UTC") :: Calendar.time_zone()

    field(:street_line_1, :string, null: true)
    field(:street_line_2, :string, null: true)
    field(:street_line_3, :string, null: true)
    field(:zip_code, :integer, null: true)
    field(:city, :string, null: true)

    field(:phone_number, :string, null: true)

    field(:is_auto_approval_enabled, :boolean, null: false, default: false)

    has_many(:events, Event)
    has_many(:members, Member)
    has_many(:member_categories, MemberCategory)
    has_many(:roles, Role)
    timestamps()
  end

  @attrs [
    :name,
    :slug,
    :description,
    :short_description,
    :timezone,
    :phone_number,
    :street_line_1,
    :street_line_2,
    :street_line_3,
    :zip_code,
    :city,
    :is_auto_approval_enabled
  ]
  @required [:name, :slug]
  @doc false
  def changeset(organization, attrs) do
    organization
    |> cast(attrs, @attrs)
    |> validate_required(@required)
    |> validate_time_zone_id(:timezone)
    |> unique_constraint(:slug)
    |> validate_length(:short_description, max: 255)
  end

  def get_first do
    from o in Organization,
      order_by: o.inserted_at,
      limit: 1
  end

  @impl true
  def authorize(%Member{} = member, _item, action),
    do: Authorizations.member_can?(member, __MODULE__, action)

  def authorize(_member, _item, _action), do: :error
end
