defmodule Nyght.Organizations.MemberRole do
  @moduledoc """
  This module contains the schema for the relationship between the
  members table and the roles table.
  """
  use Nyght.Utils.NyghtSchema,
    binding: :member_role

  alias Nyght.Authorizations.Role
  alias Nyght.Organizations.Member

  @primary_key false
  @foreign_key_type :binary_id
  typed_schema "members_roles" do
    belongs_to(:member, Member, primary_key: true)
    belongs_to(:role, Role, primary_key: true)
  end

  @required [:member_id, :role_id]
  @doc false
  def changeset(member, attrs) do
    member
    |> cast(attrs, @required)
    |> validate_required(@required)
    |> unique_constraint(@required,
      name: :members_roles_pkey,
      message: "has this role already"
    )
  end
end
