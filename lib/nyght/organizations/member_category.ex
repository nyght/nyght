defmodule Nyght.Organizations.MemberCategory do
  @moduledoc """
  This module represents a member category.

  This is not a role, it's some additional infromation on the member
  such as "regular" or "occasional" members.
  """
  alias Nyght.Authorizations
  alias Nyght.Organizations.{Member, Organization}

  use Nyght.Utils.NyghtSchema,
    binding: :member_category,
    permissions: [:create, :read, :update, :delete]

  @derive {
    Flop.Schema,
    filterable: [:name],
    sortable: [:name],
    default_order: %{
      order_by: [:name],
      order_directions: [:asc]
    }
  }
  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  typed_schema "member_categories" do
    field(:name, :string)

    belongs_to(:organization, Organization)

    many_to_many(:members, Member,
      join_through: "members_member_categories",
      join_keys: [member_id: :id, member_category_id: :id],
      on_replace: :delete
    )
  end

  @doc false
  def changeset(category, attrs) do
    category
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end

  @impl true
  def authorize(%Member{} = member, _item, action),
    do: Authorizations.member_can?(member, __MODULE__, action)

  def authorize(_member, _item, _action), do: :error
end
