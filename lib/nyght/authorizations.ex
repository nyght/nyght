defmodule Nyght.Authorizations do
  @moduledoc """
  The Authorizations context.
  """

  import Ecto.Query, warn: false

  require Logger

  alias Nyght.Authorizations.Events.{
    RoleCreated,
    RoleDeleted,
    RoleUpdated
  }

  alias Nyght.Authorizations.{Permission, Role}
  alias Nyght.Organizations
  alias Nyght.Organizations.{Member, Organization}
  alias Nyght.Repo

  @doc """
  Returns the list of roles.

  ## Examples

  iex> list_roles()
  [%Role{}, ...]

  """
  @spec list_roles(Nyght.Organizations.Organization.t()) :: any
  def list_roles(%Organization{id: org_id}) do
    query =
      from r in Role,
        where: r.organization_id == ^org_id,
        order_by: r.name

    Repo.all(query)
  end

  def list_roles_for(%Organization{id: org_id}, %{} = params) do
    Role.get()
    |> Role.filter_organization(org_id)
    |> Flop.validate_and_run(params, for: Role)
  end

  @doc """
  Returns the list of default roles.
  """
  @spec list_default_roles(Nyght.Organizations.Organization.t()) :: any
  def list_default_roles(%Organization{id: org_id}) do
    Role.get()
    |> Role.filter_organization(org_id)
    |> Role.filter_default()
    |> Repo.all()
  end

  @doc """
  Gets a single role.

  Raises `Ecto.NoResultsError` if the Role does not exist.

  ## Examples

      iex> get_role!(123)
      %Role{}

      iex> get_role!(456)
      ** (Ecto.NoResultsError)

  """
  @spec get_role!(any) :: nil | [%{optional(atom) => any}] | %{optional(atom) => any}
  def get_role!(id) do
    Role
    |> Repo.get!(id)
    |> Repo.preload(:permissions)
    |> with_permission_list()
  end

  def get_roles(ids) do
    Role.get(ids)
    |> Repo.all()
    |> Repo.preload(:permissions)
  end

  def with_permission_list(role) do
    permissions =
      role.permissions
      |> Enum.map(& &1.id)

    %Role{role | permission_list: permissions}
  end

  @doc """
  Creates a role.

  ## Examples

      iex> create_role(organization, %{field: value})
      {:ok, %Role{}}

      iex> create_role(organization, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_role(
          Nyght.Organizations.Organization.t(),
          :invalid | %{optional(:__struct__) => none, optional(atom | binary) => any}
        ) ::
          {:ok, Nyght.Authorizations.Role.t()} | {:error, Ecto.Changeset.t()}
  def create_role(%Organization{id: org_id}, attrs) do
    attrs = Map.merge(attrs, %{"organization_id" => org_id})

    %Role{}
    |> change_role(attrs)
    |> Repo.insert()
    |> Organizations.broadcast_result!(&%RoleCreated{role_id: &1.id})
  end

  @doc """
  Updates a role.

  ## Examples

      iex> update_role(role, %{field: new_value})
      {:ok, %Role{}}

      iex> update_role(role, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec update_role(
          Nyght.Authorizations.Role.t(),
          :invalid | %{optional(:__struct__) => none, optional(atom | binary) => any}
        ) :: {:ok, Nyght.Authorizations.Role.t()} | {:error, Ecto.Changeset.t()}
  def update_role(%Role{} = role, attrs) do
    role
    |> change_role(attrs)
    |> Repo.update()
    |> Organizations.broadcast_result!(&%RoleUpdated{role_id: &1.id})
  end

  @doc """
  Deletes a role.

  ## Examples

      iex> delete_role(role)
      {:ok, %Role{}}

      iex> delete_role(role)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_role(Nyght.Authorizations.Role.t()) ::
          {:ok, Nyght.Authorizations.Role.t()} | {:error, Ecto.Changeset.t()}
  def delete_role(%Role{} = role) do
    Repo.delete(role)
    |> Organizations.broadcast_result!(&%RoleDeleted{role_id: &1.id, role: &1})
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking role changes.

  ## Examples

      iex> change_role(role)
      %Ecto.Changeset{data: %Role{}}

  """
  @spec change_role(
          Nyght.Authorizations.Role.t(),
          :invalid | %{optional(:__struct__) => none, optional(atom | binary) => any}
        ) :: Ecto.Changeset.t()
  def change_role(%Role{} = role, attrs \\ %{}) do
    Role.changeset(role, attrs)
  end

  ## Permissions

  @doc """
  Gets a permission by its resource and action.

  ## Example

      iex> get_permission(User, :create)
      Nyght.Authorizations.Permission{resource: User, action: :create}

  """
  def get_permission!(action, resource) do
    query =
      from p in Permission,
        where: p.resource == ^resource and p.action == ^action

    Repo.one!(query)
  end

  @doc """
  Gets a permissions by their ids.

  ## Example

      iex> get_permissions([1234, 5678])
      [ Nyght.Authorizations.Permission{id: 1234resource: User, action: :create}, ...]

  """
  def get_permissions(ids) do
    query =
      from p in Permission,
        where: p.id in ^ids

    Repo.all(query)
  end

  @doc """
  Lists all the permissions in the database.
  """
  def list_permissions do
    query = from(p in Permission)

    Repo.all(query)
  end

  @doc """
  Lists all the permissions in the compiled code.
  """
  def list_local_permissions do
    for mod <- list_policy_modules(), perms = mod.__permissions__, into: [] do
      {mod, perms}
    end
  end

  @doc """
  Lists all the permissions in the compiled code in a flat list.
  """
  def list_local_permissions_flat do
    for mod <- list_policy_modules(), perm <- mod.__permissions__, into: [] do
      {mod, perm}
    end
  end

  defp list_policy_modules do
    :nyght
    |> Application.spec()
    |> Keyword.get(:modules)
    |> Enum.filter(&policy?/1)
  end

  defp policy?(module) do
    behaviours =
      module.__info__(:attributes)
      |> Keyword.get_values(:behaviour)
      |> List.flatten()

    Nyght.Authorizations.Policy in behaviours
  end

  @doc """
  Creates a new permission from an atom.
  """
  @spec create_permission(atom() | binary(), atom() | binary()) ::
          {:ok, Nyght.Authorizations.Role.t()} | {:error, Ecto.Changeset.t()}
  def create_permission(action, resource) do
    %Permission{}
    |> Permission.changeset(%{action: action, resource: resource})
    |> Repo.insert()
  end

  @doc """
  Grants a given role a permission.
  """
  @spec grant!(Nyght.Authorizations.Role.t(), Nyght.Authorizations.Permission.t()) :: any
  def grant!(%Role{id: role_id}, %Permission{} = perm) do
    role = get_role!(role_id)

    role
    |> Ecto.Changeset.change()
    |> Ecto.Changeset.put_assoc(:permissions, [perm | role.permissions])
    |> Repo.update()
  end

  ########################################################################
  # Helpers
  ########################################################################

  @doc """
  Returns the maximum `t:Nyght.Events.Event.status/0` a member can see according
  to their permissions.
  """
  @spec get_max_event_status(Nyght.Organizations.Member.t()) :: Nyght.Events.Event.status()
  def get_max_event_status(%Member{} = member) do
    cond do
      can?(member, Nyght.Events.Event, :read_unpublished) ->
        :unpublished

      can?(member, Nyght.Events.Event, :read_prepublished) ->
        :prepublished

      true ->
        :published
    end
  end

  @doc """
  Checks if a `Nyght.Organizations.Member` has a specific `Nyght.Authorizations.Role`
  """
  @spec has_role?(Nyght.Organizations.Member.t(), Nyght.Authorizations.Role.t()) :: boolean()
  def has_role?(%Member{roles: roles}, %Role{id: role_id}) do
    Enum.any?(roles, &(&1.id == role_id))
  end

  def has_role?(_, _), do: false

  @doc """
  Checks if a member can perform the given action on a specific item.

  This function will internally call the `authorize/3` function of the
  module or instance.
  """
  @spec can(Nyght.Organizations.Member.t() | nil, module() | struct(), atom()) ::
          :ok | {:error, any()}
  def can(member, item, action) when is_struct(item) do
    module = item.__struct__

    module.authorize(member, item, action)
    |> normalize_results()
  end

  def can(member, module, action) do
    module.authorize(member, module, action)
    |> normalize_results()
  end

  defp normalize_results(:ok), do: :ok
  defp normalize_results(true), do: :ok
  defp normalize_results(:error), do: {:error, :unauthorized}
  defp normalize_results({:error, reason}), do: {:error, reason}
  defp normalize_results(false), do: {:error, :unauthorized}

  @doc """
  Same as `Nyght.Authorizations.can/3` but returns a boolean.
  """
  @spec can?(Nyght.Organizations.Member.t() | nil, module() | struct(), atom()) :: boolean()
  def can?(member, item, action), do: can(member, item, action) == :ok

  @doc """
  Checks if the given member has the permission for the given action on
  the given resource *from the database*.

  This assumes the roles and permissions are preloaded for the member. This
  should not be used to check a member permission directly because this won't
  use the `authorize/3` function of the module. To check permissions,
  use `Nyght.Authorizations.can/3` instead.
  """
  @spec member_can?(Nyght.Organizations.Member.t(), module(), atom()) :: boolean()
  def member_can?(%Member{roles: roles}, resource, action) do
    roles
    |> Enum.flat_map(& &1.permissions)
    |> Enum.uniq_by(& &1.id)
    |> Enum.any?(&(&1.resource == resource and &1.action == action))
  end
end
