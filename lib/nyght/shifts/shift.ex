defmodule Nyght.Shifts.Shift do
  @moduledoc false
  use Nyght.Utils.NyghtSchema,
    binding: :shift,
    permissions: [:create, :list, :apply, :assign, :leave, :update, :delete]

  import Nyght.Helpers.EctoHelpers

  alias Nyght.Authorizations
  alias Nyght.Authorizations.Role
  alias Nyght.Events.Event
  alias Nyght.Organizations.Member
  alias Nyght.Shifts.{Application, Shift}

  @type application_strategies() :: :fcfs | :availability

  @derive {
    Flop.Schema,
    filterable: [:event_name],
    sortable: [:event_name, :title, :shift_starts_at],
    adapter_opts: [
      alias_fields: [:title, :shift_starts_at],
      join_fields: [
        event_name: [
          binding: :event,
          field: :name,
          ecto_type: :string
        ],
        application_status: [
          binding: :applications,
          field: :status,
          ecto_type: {:ecto_enum, [:accepted, :pending, :waiting_replacement, :rejected]}
        ]
      ]
    ]
  }

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  typed_schema "shifts" do
    field(:name, :string)
    field(:description, :string)
    field(:title, :string, virtual: true, default: "")

    field(:starts_at, :utc_datetime)
    field(:ends_at, :utc_datetime)

    field(:start_offset, :integer, null: false, default: 0)
    field(:end_offset, :integer, null: false, default: 0)

    field(:local_starts_at, :naive_datetime, virtual: true)
    field(:local_ends_at, :naive_datetime, virtual: true)

    field(:total_openings, :integer)
    field(:opening_count, :integer, virtual: true, default: 0)

    field(:application_strategy, Ecto.Enum, values: [:fcfs, :availability]) ::
      Shift.application_strategies()

    field(:delete, :boolean, virtual: true, default: false)

    has_many(:shift_applications, Application)

    belongs_to(:event, Event)
    belongs_to(:role, Role)

    timestamps()
  end

  @doc false
  def changeset(shift, attrs) do
    changeset =
      shift
      |> cast(attrs, [
        :event_id,
        :role_id,
        :name,
        :description,
        :application_strategy,
        :total_openings,
        :local_starts_at,
        :local_ends_at,
        :start_offset,
        :end_offset,
        :opening_count,
        :delete
      ])
      |> validate_length(:description, max: 255)

    if get_change(changeset, :delete) do
      %{changeset | action: :delete}
    else
      changeset
      |> validate_required([
        :event_id,
        :role_id,
        :application_strategy
      ])
      |> validate_total_openings()
    end
  end

  defp validate_total_openings(changeset) do
    case get_change(changeset, :total_openings) do
      nil ->
        changeset

      _ ->
        validate_number(changeset, :total_openings, greater_than: 0)
    end
  end

  @doc """
  Compute the start and end offsets from the event and puts them in the
  changeset.
  """
  @spec put_time_offsets(Ecto.Changeset.t(), Nyght.Events.Event.t()) :: Ecto.Changeset.t()
  def put_time_offsets(changeset, %Event{} = event) do
    changeset
    |> put_time_offset(event, event.starts_at, :local_starts_at, :start_offset)
    |> put_time_offset(event, event.ends_at, :local_ends_at, :end_offset)
  end

  defp put_time_offset(changeset, event, event_time, input_key, output_key) do
    if datetime = get_field(changeset, input_key) do
      datetime
      |> DateTime.from_naive(event.timezone)
      |> get_offset(changeset, event_time, input_key, output_key)
    else
      changeset
    end
  end

  defp get_offset({:ok, shift_datetime}, changeset, event_datetime, _, output_key) do
    offset =
      shift_datetime
      |> DateTime.shift_zone!("Etc/UTC")
      |> DateTime.diff(event_datetime)

    put_change(changeset, output_key, offset)
  end

  defp get_offset(
         {:ambiguous, _, shift_datetime},
         changeset,
         event_datetime,
         input_key,
         output_key
       ) do
    get_offset({:ok, shift_datetime}, changeset, event_datetime, input_key, output_key)
  end

  defp get_offset({:gap, _, _shift_datetime}, changeset, _event_datetime, input_key, _) do
    Ecto.Changeset.add_error(
      changeset,
      input_key,
      Gettext.dgettext(
        NyghtWeb.Gettext,
        "errors",
        "not a valid time because of daylight saving time"
      )
    )
  end

  @doc """
  Populates the `starts_at`/`ends_at` virtual fields (and the local versions).

  This will convert the time offsets into datetimes.
  """
  @spec with_times(Ecto.Query.t()) :: Ecto.Query.t()
  def with_times(query) do
    from [shift: s] in query,
      join: e in assoc(s, :event),
      select_merge: %{
        starts_at: selected_as(add_seconds(e.starts_at, s.start_offset), :shift_starts_at),
        ends_at: add_seconds(e.ends_at, s.end_offset),
        local_starts_at: shift_timezone(add_seconds(e.starts_at, s.start_offset), e.timezone),
        local_ends_at: shift_timezone(add_seconds(e.ends_at, s.end_offset), e.timezone)
      }
  end

  @doc """
  Populates the title virtual field.

  This field contains the shift's name if it's defined, or the role name otherwise.
  """
  @spec with_title(Ecto.Query.t()) :: Ecto.Query.t()
  def with_title(query) do
    from [shift: s, role: r] in include_role(query),
      select_merge: %{
        title: selected_as(coalesce(s.name, r.name), :title)
      }
  end

  @doc """
  Populates the `opening_count` virtual field.

  This adds a join and group by to the query, this is not trivial.
  """
  @spec with_opening_count(Ecto.Query.t()) :: Ecto.Query.t()
  def with_opening_count(query) do
    applications =
      Application.get()
      |> Application.filter_status(:accepted)

    sub_query =
      from [application: sa] in applications,
        group_by: sa.shift_id,
        select: %{shift_id: sa.shift_id, opening_count: count(sa.id)}

    from [shift: s] in query,
      left_join: oc in subquery(sub_query),
      on: s.id == oc.shift_id,
      select_merge: %{
        opening_count: coalesce(oc.opening_count, 0)
      }
  end

  @doc """
  Adds an order_by clause to the query.

  `with_times/1` and `with_title/1` must also be called to use this ordering.
  """
  @spec order_by_title_and_time(Ecto.Query.t()) :: Ecto.Query.t()
  def order_by_title_and_time(query) do
    from query,
      order_by: [selected_as(:title), selected_as(:shift_starts_at)]
  end

  @doc """
  Selects the staff list for exports.
  """
  @spec select_staff_list(Ecto.Query.t()) :: Ecto.Query.t()
  def select_staff_list(query, opts \\ []) do
    query =
      query
      |> Shift.include_event()
      |> Shift.include_role()
      |> Shift.include_applications()
      |> Application.filter_status(opts[:applications_status])
      |> Application.include_member()
      |> Member.join_user()

    from [member: m, user: u, shift: s, event: e, role: r] in query,
      select: %{
        first_name: u.first_name,
        last_name: u.last_name,
        email: u.email,
        street_line_1: u.street_line_1,
        street_line_2: u.street_line_2,
        street_line_3: u.street_line_3,
        zip_code: u.zip_code,
        city: u.city,
        phone: u.phone_number,
        shift: selected_as(coalesce(s.name, r.name), :shift),
        local_starts_at:
          selected_as(
            shift_timezone(add_seconds(e.starts_at, s.start_offset), e.timezone),
            :shift_starts_at
          ),
        local_ends_at: shift_timezone(add_seconds(e.ends_at, s.end_offset), e.timezone)
      },
      order_by: [selected_as(:shift_starts_at), selected_as(:shift), u.first_name]
  end

  @doc """
  Creates the query to join related items.
  """
  @spec include(Ecto.Query.t(), list(atom())) :: Ecto.Query.t()
  def include(query, args) do
    from query, preload: ^args
  end

  @doc false
  def include_all(query) do
    # TODO: there's too many preload
    from query,
      preload: [:event, :role, shift_applications: [member: [:user, :categories]]]
  end

  @doc """
  Creates the query to join the corresponding event.
  """
  @spec include_event(Ecto.Query.t()) :: Ecto.Query.t()
  def include_event(query) do
    if has_named_binding?(query, :event) do
      query
    else
      from [shift: s] in query,
        left_join: e in assoc(s, :event),
        as: :event
    end
  end

  @doc """
  Creates the query to join the corresponding role.
  """
  @spec include_role(Ecto.Query.t()) :: Ecto.Query.t()
  def include_role(query) do
    if has_named_binding?(query, :role) do
      query
    else
      from [shift: s] in query,
        left_join: r in assoc(s, :role),
        as: :role
    end
  end

  @doc """
  Creates the query to join shift applications.
  """
  @spec include_applications(Ecto.Query.t()) :: Ecto.Query.t()
  def include_applications(query) do
    if has_named_binding?(query, :application) do
      query
    else
      from [shift: s] in query,
        left_join: sa in assoc(s, :shift_applications),
        as: :application
    end
  end

  @doc """
  Applies a filter to the query.
  """
  @spec filter(Ecto.Query.t(), list({atom(), any()})) :: Ecto.Query.t()
  def filter(query, args) do
    Enum.reduce(args, query, fn
      {:id, id}, query ->
        filter_id(query, id)

      {:role, id}, query ->
        filter_role(query, id)

      {:event, event_id}, query ->
        filter_event(query, event_id)

      {:organization, org_id}, query ->
        filter_org(query, org_id)

      {:strategy, strategy}, query ->
        filter_application_strategy(query, strategy)

      {:full, is_full}, query ->
        filter_full(query, is_full)

      _, query ->
        query
    end)
  end

  @doc """
  Filter the full shifts or not.
  """
  @spec filter_full(Ecto.Query.t(), boolean()) :: Ecto.Query.t()
  def filter_full(query, true) do
    from [shift: s] in query, where: s.opening_count == s.total_openings
  end

  def filter_full(query, false) do
    from [shift: s] in query, where: s.opening_count != s.total_openings
  end

  @doc """
  Filters the given query by event id.
  """
  @spec filter_event(Ecto.Query.t(), any) :: Ecto.Query.t()
  def filter_event(query, event_id) do
    from [shift: s] in query, where: s.event_id == ^event_id
  end

  @doc """
  Filters the given query by a list of event ids.
  """
  @spec filter_events(Ecto.Query.t(), any) :: Ecto.Query.t()
  def filter_events(query, event_ids) do
    from [shift: s] in query, where: s.event_id in ^event_ids
  end

  @doc """
  Filters the shifts by organization.
  """
  @spec filter_org(Ecto.Query.t(), any) :: Ecto.Query.t()
  def filter_org(query, org_id) do
    query
    |> include_event()
    |> Event.filter_org(org_id)
  end

  @doc """
  Filters the shifts within the given period.
  """
  @spec filter_period(Ecto.Queryable.t(), Date.t(), integer(), binary()) :: Ecto.Query.t()
  def filter_period(query, from, amount, period \\ "month") do
    from [shift: s] in query,
      where:
        s.starts_at > fragment("(?)::date - ('1 ' || ?)::interval * ?", ^from, ^period, ^amount)
  end

  @doc """
  Filters the shifts that have not started
  """
  def filter_future_date(query) do
    from q in subquery(query),
      where: q.shift_starts_at >= ago(1, "second")
  end

  @doc """
  Filters the given query by role.
  """
  @spec filter_role(Ecto.Query.t(), any) :: Ecto.Query.t()
  def filter_role(query, id) do
    from [shift: s] in query, where: s.role_id == ^id
  end

  @doc """
  Filters the given query by member, using the shift applications.
  """
  @spec filter_member(Ecto.Query.t(), any) :: Ecto.Query.t()
  def filter_member(query, member_id) do
    from [shift: s] in query,
      inner_join: sa in assoc(s, :shift_applications),
      on: sa.member_id == ^member_id,
      preload: [shift_applications: sa]
  end

  @doc """
  Filters the given query by application strategy.
  """
  @spec filter_application_strategy(Ecto.Query.t(), Nyght.Shifts.Shift.application_strategies()) ::
          Ecto.Query.t()
  def filter_application_strategy(query, strategy) do
    from [shift: s] in query, where: s.application_strategy == ^strategy
  end

  @doc """
  Creates an update query to change the shift opening count.
  """
  @spec update_opening_count(Ecto.Query.t(), number()) :: Ecto.Query.t()
  def update_opening_count(query, increment \\ 1) do
    from s in query,
      update: [inc: [opening_count: ^increment]],
      select: s
  end

  @doc """
  Returns the number of free openings
  """
  @spec free_openings(Nyght.Shifts.Shift.t()) :: number
  def free_openings(%__MODULE__{total_openings: nil}), do: -1
  def free_openings(%__MODULE__{total_openings: total, opening_count: count}), do: total - count

  @doc """
  Returns if the shift has a maximum number of staff.
  """
  @spec has_max?(Nyght.Shifts.Shift.t()) :: boolean
  def has_max?(%Shift{total_openings: total}), do: not is_nil(total)

  @impl true
  def authorize(%Member{} = member, _item, action),
    do: Authorizations.member_can?(member, __MODULE__, action)

  def authorize(_member, _item, _action), do: :error
end
