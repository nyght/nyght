defmodule Nyght.Shifts.Events do
  @moduledoc """
  This modules contains different structs representing events about shifts.
  """

  defmodule ShiftCreated do
    @moduledoc false

    defstruct shift: nil, event_id: nil
  end

  defmodule ShiftUpdated do
    @moduledoc false

    defstruct shift: nil, event_id: nil
  end

  defmodule ShiftDeleted do
    @moduledoc false

    defstruct shift: nil, event_id: nil
  end
end
