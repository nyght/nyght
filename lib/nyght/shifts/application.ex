defmodule Nyght.Shifts.Application do
  @moduledoc """
  This module represents an application for a shift.
  """
  use Nyght.Utils.NyghtSchema, binding: :application, permissions: [:delete, :list, :update]
  alias Nyght.Accounts.User
  alias Nyght.Authorizations
  alias Nyght.Events.Event
  alias Nyght.Organizations.Member
  alias Nyght.Shifts.{Application, Shift}

  @type status() :: :accepted | :pending | :waiting_replacement | :rejected

  @derive {
    Flop.Schema,
    filterable: [
      :event_name,
      :shift_title,
      :status,
      :organization
    ],
    sortable: [:event_name, :shift_title, :status, :shift_starts_at, :organization],
    adapter_opts: [
      join_fields: [
        event_name: [
          binding: :event,
          field: :name,
          path: [:shift, :event, :name],
          ecto_type: :string
        ],
        organization: [
          binding: :organization,
          field: :name,
          path: [:shift, :event, :organization, :name],
          ecto_type: :string
        ],
        shift_title: [
          binding: :shift,
          field: :title,
          ecto_type: :string
        ],
        shift_starts_at: [
          binding: :shift,
          field: :shift_starts_at,
          ecto_type: :utc_datetime
        ]
      ]
    ],
    default_order: %{
      order_by: [:shift_starts_at, :event_name],
      order_directions: [:desc, :asc]
    }
  }

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  typed_schema "shift_applications" do
    field(:status, Ecto.Enum,
      values: [:accepted, :pending, :waiting_replacement, :rejected],
      default: :accepted
    ) ::
      Application.status()

    field(:selected, :boolean, virtual: true)

    belongs_to(:shift, Shift)
    belongs_to(:member, Member, null: true)

    # DEPRECATED: this field will be removed soon, use `member` instead
    belongs_to(:user, User, null: true)

    timestamps()
  end

  @doc false
  def changeset(application, attrs) do
    application
    |> cast(attrs, [:shift_id, :member_id, :status, :selected])
    |> cast_selected()
    |> validate_required([:shift_id, :member_id])
    |> unique_constraint([:shift_id, :member_id], message: "you already applied for this shift")
  end

  defp cast_selected(changeset) do
    if get_change(changeset, :selected) do
      put_change(changeset, :status, :accepted)
    else
      changeset
    end
  end

  @doc """
  Applies a filter to the query.
  """
  @spec filter(Ecto.Query.t(), list({atom(), any()})) :: Ecto.Query.t()
  def filter(query, args) do
    Enum.reduce(args, query, fn
      {:id, id}, query ->
        filter_id(query, id)

      {:organization, org_id}, query ->
        filter_org(query, org_id)

      {:member, member_id}, query ->
        filter_member(query, member_id)

      {:shift, shift_id}, query ->
        filter_shift(query, shift_id)

      {:months_ago, amount}, query ->
        filter_ago(query, amount)

      {:status, status}, query ->
        filter_status(query, status)

      _, query ->
        query
    end)
  end

  @doc """
  Filters the given query by application user id.
  """
  @spec filter_member(Ecto.Query.t(), any) :: Ecto.Query.t()
  def filter_member(query, member_id) do
    from [application: sa] in query, where: sa.member_id == ^member_id
  end

  @doc """
  Filters the given query by application shift id.
  """
  @spec filter_shift(Ecto.Query.t(), any) :: Ecto.Query.t()
  def filter_shift(query, shift_id) do
    from [application: sa] in query, where: sa.shift_id == ^shift_id
  end

  @doc """
  Filters the given query by application insertion date.
  """
  @spec filter_ago(Ecto.Query.t(), number(), binary()) :: Ecto.Query.t()
  def filter_ago(query, amount, period \\ "month") do
    from [application: sa] in query, where: sa.inserted_at > ago(^amount, ^period)
  end

  @doc """
  Filters the applications by organization.
  """
  @spec filter_org(Ecto.Query.t(), any) :: Ecto.Query.t()
  def filter_org(query, org_id) do
    query
    |> join_event()
    |> Event.filter_org(org_id)
  end

  @doc """
  Filters the applications by status.
  """
  @spec filter_status(Ecto.Query.t(), any) :: Ecto.Query.t()
  def filter_status(query, status) when is_atom(status) do
    from [application: sa] in query, where: sa.status == ^status
  end

  def filter_status(query, status) when is_list(status) do
    from [application: sa] in query, where: sa.status in ^status
  end

  @doc """
  Orders the query by the application's event start date.
  """
  @spec order_by_event_date(Ecto.Query.t()) :: Ecto.Query.t()
  def order_by_event_date(query) do
    query
    |> join_event()
    |> Event.order_by_start_date()
  end

  @doc """
  Filters the query by the application's shift, returning futur shifts.
  """
  @spec filter_by_future_shift(Ecto.Query.t()) :: Ecto.Query.t()
  def filter_by_future_shift(query) do
    query
    |> join_shift()
    |> Shift.with_times()
    |> Shift.filter_future_date()
  end

  def include_member(query) do
    if has_named_binding?(query, :member) do
      query
    else
      from [application: sa] in query,
        left_join: u in assoc(sa, :member),
        as: :member
    end
  end

  def join_event(query) do
    if has_named_binding?(query, :event) do
      query
    else
      from [application: sa] in query,
        join: s in assoc(sa, :shift),
        as: :shift,
        join: e in assoc(s, :event),
        as: :event,
        preload: [shift: {s, event: e}]
    end
  end

  def join_for_flop(query) do
    if has_named_binding?(query, :event) do
      query
    else
      sub =
        Shift.get()
        |> Shift.with_times()
        |> Shift.with_title()

      from [application: sa] in query,
        join: s in subquery(sub),
        on: s.id == sa.shift_id,
        as: :shift,
        join: e in assoc(s, :event),
        as: :event,
        join: o in assoc(e, :organization),
        as: :organization,
        preload: [shift: ^{sub, [event: [:organization]]}]
    end
  end

  defp join_shift(query) do
    if has_named_binding?(query, :shift) do
      query
    else
      from [application: sa] in query,
        join: s in assoc(sa, :shift),
        as: :shift
    end
  end

  @doc """
  Creates an update query to change the event status.
  """
  @spec update_status(Ecto.Query.t(), Nyght.Shifts.Application.status()) :: Ecto.Query.t()
  def update_status(query, status) do
    from a in query,
      update: [set: [status: ^status, updated_at: fragment("NOW()")]],
      select: a
  end

  @impl true
  def authorize(%Member{} = member, _item, action),
    do: Authorizations.member_can?(member, __MODULE__, action)

  def authorize(_member, _item, _action), do: :error
end
