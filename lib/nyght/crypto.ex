defmodule Nyght.Crypto do
  @moduledoc """
  Cryptographic functions.
  """

  @doc """
  Generate a strong random base64 string of the given size.
  """
  @spec generate_secure_string(number()) :: binary()
  def generate_secure_string(size) do
    size
    |> :crypto.strong_rand_bytes()
    |> Base.encode64()
  end

  @doc """
  Encrypt data using AES.
  """
  @spec aes_encrypt(binary(), binary(), binary()) :: binary()
  def aes_encrypt(data, key, iv) do
    data =
      data
      |> Base.encode64()
      |> pad()

    :aes_256_cbc
    |> :crypto.crypto_one_time(key, iv, data, true)
    |> Base.encode64()
  end

  @doc """
  Decrypt AES-encrypted content.
  """
  @spec aes_decrypt(binary(), binary(), binary()) :: {:ok, binary()} | {:error, any()}
  def aes_decrypt(data, key, iv) do
    key = Base.decode64!(key)
    iv = Base.decode64!(iv)
    encrypted_content = Base.decode64!(data)

    :aes_256_cbc
    |> :crypto.crypto_one_time(key, iv, encrypted_content, false)
    |> unpad()
    |> Base.decode64()
  end

  defp pad(data, block_size \\ 16) do
    to_add = block_size - rem(byte_size(data), block_size)
    data <> String.duplicate(<<to_add>>, to_add)
  end

  def unpad(data) do
    to_remove = :binary.last(data)
    :binary.part(data, 0, byte_size(data) - to_remove)
  end
end
