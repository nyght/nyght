defmodule Nyght.Utils.NyghtSchema do
  @moduledoc false

  defmacro __using__(opts \\ []) do
    permissions = Keyword.get(opts, :permissions)
    binding = opts[:binding]

    quote do
      use TypedEctoSchema

      import Ecto.Changeset
      import Ecto.Query, warn: false

      unquote(auth_helpers(permissions))
      unquote(get_helpers(binding))
      unquote(filter_helpers(binding))
    end
  end

  defp auth_helpers(nil) do
    quote do
    end
  end

  defp auth_helpers(permissions) do
    quote do
      use Nyght.Authorizations.Policy
      @permissions unquote(permissions)
    end
  end

  defp get_helpers(binding) do
    quote do
      @doc """
      Creates the base query.
      """
      @spec get :: Ecto.Query.t()
      def get do
        from e in __MODULE__, as: unquote(binding)
      end

      @doc """
      Creates the base query for a given item.
      """
      @spec get(binary() | list(binary())) :: Ecto.Query.t()
      def get(id) when is_binary(id) do
        filter_id(get(), id)
      end

      def get(ids) when is_list(ids) do
        filter_ids(get(), ids)
      end

      @doc """
      Add preloads to the query.
      """
      @spec preload(Ecto.Query.t(), list()) :: Ecto.Query.t()
      def preload(query, preloads) do
        from query, preload: ^preloads
      end
    end
  end

  defp filter_helpers(_binding) do
    quote do
      @doc """
      Filters the given query by id.
      """
      @spec filter_id(Ecto.Query.t(), binary()) :: Ecto.Query.t()
      def filter_id(query, id) do
        from q in query, where: q.id == ^id
      end

      @doc """
      Filters the given query with ids in the given list
      """
      def filter_ids(query, ids) do
        from q in query, where: q.id in ^ids
      end

      @doc """
      Filters the given query by organization id.
      """
      @spec filter_organization(Ecto.Query.t(), binary()) :: Ecto.Query.t()
      def filter_organization(query, id) do
        from q in query, where: q.organization_id == ^id
      end
    end
  end
end
