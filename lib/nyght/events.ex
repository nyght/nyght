defmodule Nyght.Events do
  @moduledoc """
  The Events context.
  """
  alias Nyght.Crypto
  alias Nyght.Events.{Attachment, Event, Type}
  alias Nyght.Events.Events.{EventCreated, EventDeleted, EventUpdated}
  alias Nyght.Files
  alias Nyght.Organizations
  alias Nyght.Organizations.Organization
  alias Nyght.Repo
  alias Nyght.Shifts

  @type neighbors :: %{
          prev_event_id: binary(),
          prev_event_name: binary(),
          next_event_id: binary(),
          next_event_name: binary()
        }

  @iv_size 16

  #####################################################################
  # Events
  #####################################################################

  @doc """
  Lists events.

  See Flop documentation for the filter, sorting and pagination
  parameters.

  ## Options

  The following options can be passed:

  - `with_local_times?`: if `true` populates, the event's
    `local_starts_at` and `local_ends_at` fields,
    containing the start and end datetime shifted at the timezone of
    the event. Defaults to `true`
  - `preload`: list of preloads, see`Repo.preload/3` for examples.
    Defaults to `[]`
  - `subset`: filters a subset of the events. Value can be `"upcoming"`
    to get upcoming events sorted by time ascending or `"past"` for
    past events sorted by time descending. The filtering and sorting
    can be overridden using Flop parameters.
  - `members_only?`: if `true`, includes the members-only events
  - `status`: includes events with specific status. By default, only the
    published events are returned, and the `status` options works with
    two levels:
    * `:published` will return only published events. This is the
      default value.
    * `:prepublished` will return published and prepublished events.
    * `:unpublished` will return all status.

  """
  @spec list_events(map(), keyword() | nil) ::
          {:error, Flop.Meta.t()} | {:ok, {list(), Flop.Meta.t()}}
  def list_events(%{} = params, opts \\ []) do
    Event.get()
    |> do_list_events(params, opts)
  end

  @doc """
  Lists the events for a specific organization.

  The same options and parameters as `list_events/2` are available.
  """
  @spec list_events_for(Nyght.Organizations.Organization.t(), map(), keyword() | nil) ::
          {:error, Flop.Meta.t()} | {:ok, {list(), Flop.Meta.t()}}
  def list_events_for(%Organization{} = org, %{} = params, opts \\ []) do
    Event.get()
    |> Event.scope(org)
    |> do_list_events(params, opts)
  end

  @doc """
  Returns Flop parameters preset.
  """
  @spec filter_preset(atom()) :: map()
  def filter_preset(:upcoming) do
    %{
      "filters" => %{
        "0" => %{
          "field" => "ends_at",
          "op" => ">=",
          "value" => DateTime.utc_now() |> DateTime.to_iso8601()
        }
      },
      "order_by" => ["starts_at"],
      "order_directions" => ["asc"]
    }
  end

  def filter_preset(:past) do
    %{
      "filters" => %{
        "0" => %{
          "field" => "ends_at",
          "op" => "<",
          "value" => DateTime.utc_now() |> DateTime.to_iso8601()
        }
      },
      "order_by" => ["starts_at"],
      "order_directions" => ["desc"]
    }
  end

  def filter_preset(_), do: %{}

  @doc """
  Returns an event by its id or raises an error if not found.

  ## Options

  The following options can be passed:

  - `with_local_times?`: if `true` populates, the event's
    `local_starts_at` and `local_ends_at` fields,
    containing the start and end datetime shifted at the
    timezone of the event. Defaults to `true`
  - `preload`: list of preloads, see`Repo.preload/3` for
    examples. Defaults to `[]`

  """
  @spec get_event!(binary()) :: Nyght.Events.Event.t()
  def get_event!(id, opts \\ []) do
    Event.get(id)
    |> maybe_with_local_times(Keyword.get(opts, :with_local_times?, true))
    |> Event.preload(Keyword.get(opts, :preload, []))
    |> Repo.one!()
  end

  @doc """
  Returns the next and previous event id and name for a given event.

  ## Options

  The following options can be specified:

  - `status`: the status of the neighbors to return, can be
    `:published`, `:prepublished` and `:unpublished`. Defaults to
    `:published`
  - `members_only?`: if `true`, include the members-only events. Defaults
    to `false`

  """
  @spec get_neighbors(Nyght.Events.Event.t(), keyword() | nil) :: Nyght.Events.neighbors()
  def get_neighbors(%Event{id: id, organization_id: organization_id}, opts \\ []) do
    Event.get_neighbors(id, organization_id, opts)
    |> Repo.one()
  end

  @doc """
  Creates a event for the given organization.

  See `Nyght.Events.Event.changeset/1` for the accepted and required
  attributes.

  An "after save" function can be passed, it will be executed after
  persisting the event but before broadcasting the creation.
  """
  @spec create_event(
          Nyght.Organizations.Organization.t(),
          map(),
          (Nyght.Events.Event.t() -> {:ok, any()} | {:error, any()})
        ) ::
          {:error, any} | {:ok, Nyght.Events.Event.t()}
  def create_event(%Organization{} = org, attrs, after_save \\ &{:ok, &1}) do
    org
    |> Ecto.build_assoc(:events)
    |> Event.changeset(attrs)
    |> Repo.insert()
    |> after_save(after_save)
    |> case do
      {:ok, event} ->
        broadcast!(__MODULE__, org.id, event.id, %EventCreated{event: event})

        {:ok, event}

      err ->
        err
    end
  end

  @doc """
  Updates an event.

  See `Nyght.Events.Event.changeset/1` for the accepted and required
  attributes.

  An "after save" function as described in `create_event/3` can also be
  specified.
  """
  @spec update_event(
          Nyght.Events.Event.t(),
          map(),
          (Nyght.Events.Event.t() -> {:ok, any()} | {:error, any()})
        ) ::
          {:error, any} | {:ok, Nyght.Events.Event.t()}
  def update_event(%Event{} = event, attrs, after_save \\ &{:ok, &1}) do
    event
    |> Repo.preload([:types, :prices, :shifts, bookings: [:performer]])
    |> Event.changeset(attrs)
    |> Repo.update()
    |> after_save(after_save)
    |> case do
      {:ok, event} ->
        broadcast!(__MODULE__, event.organization_id, event.id, %EventUpdated{event: event})

        {:ok, event}

      err ->
        err
    end
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking event changes.
  """
  @spec change_event(Nyght.Events.Event.t(), map() | nil) :: Ecto.Changeset.t()
  def change_event(%Event{} = event, attrs \\ %{}) do
    event
    |> Repo.preload([:types, :prices, :shifts, bookings: [:performer]])
    |> Event.changeset(attrs)
  end

  @doc """
  Returns a changeset for creating a copy of the given event.

  It will also copy prices and ticket links.
  """
  @spec copy_event(Nyght.Events.Event.t()) :: Ecto.Changeset.t()
  def copy_event(%Event{} = event) do
    event =
      event
      |> Repo.preload([:types, :prices])

    attrs =
      event
      |> Map.take([
        :name,
        :description,
        :additional_info,
        :poster,
        :poster_thumbnail,
        :starts_at,
        :ends_at,
        :local_starts_at,
        :local_ends_at,
        :timezone,
        :is_members_only,
        :is_soldout,
        :is_canceled,
        :status,
        :organization_id,
        :types
      ])
      |> Map.merge(%{
        prices: copy_prices(event.prices),
        ticket_links: copy_ticket_links(event.ticket_links)
      })

    change_event(%Event{}, attrs)
  end

  @doc """
  Copies the shifts from an event to another.
  """
  @spec copy_shifts(Nyght.Events.Event.t(), Nyght.Events.Event.t()) ::
          {:ok, Nyght.Events.Event.t()} | {:error, any()}
  def copy_shifts(%Event{} = from, %Event{} = to) do
    case Shifts.list_shifts_for(from, %{}) do
      {:ok, {shifts, _}} ->
        Enum.each(shifts, &Shifts.copy_shift(&1, to))
        {:ok, to}

      {:error, _} ->
        {:error, to}
    end
  end

  @doc """
  Unpublishes an event.

  ## Examples

      iex> unpublish_event(event)
      {:ok, %Event{status: :unpublished}}
  """
  @spec unpublish_event(Nyght.Events.Event.t()) ::
          {:ok, Nyght.Events.Event.t()} | {:error, Ecto.Changeset.t()}
  def unpublish_event(event) do
    change_event_status(event, %{status: :unpublished})
  end

  @doc """
  Sets an event in preview state.

  ## Examples

      iex> prepublish_event(event)
      {:ok, %Event{status: :prepublished}}
  """
  @spec prepublish_event(Nyght.Events.Event.t()) ::
          {:ok, Nyght.Events.Event.t()} | {:error, Ecto.Changeset.t()}
  def prepublish_event(event) do
    change_event_status(event, %{status: :prepublished})
  end

  @doc """
  Publishes an event.

  ## Examples

      iex> publish_event(event)
      {:ok, %Event{status: :published}}
  """
  @spec publish_event(Nyght.Events.Event.t()) ::
          {:ok, Nyght.Events.Event.t()} | {:error, Ecto.Changeset.t()}
  def publish_event(event) do
    change_event_status(event, %{status: :published})
  end

  @doc """
  Cancels an event.

  ## Examples

      iex> cancel_event(event)
      {:ok, %Event{is_canceled: true}}
  """
  @spec cancel_event(Nyght.Events.Event.t()) ::
          {:ok, Nyght.Events.Event.t()} | {:error, Ecto.Changeset.t()}
  def cancel_event(event) do
    change_event_status(event, %{is_canceled: true})
  end

  @doc """
  Deletes a event and its associated posters.

  ## Examples

      iex> delete_event(event)
      {:ok, %Event{}}

      iex> delete_event(event)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_event(Nyght.Events.Event.t()) :: {:error, any()} | {:ok, Nyght.Events.Event.t()}
  def delete_event(event) do
    with {:ok, _} <- delete_event_posters(event),
         {:ok, _} <- delete_event_attachments(event),
         {:ok, event} <- Repo.delete(event) do
      broadcast!(__MODULE__, event.organization_id, event.id, %EventDeleted{event: event})

      {:ok, event}
    else
      {:error, _} = err ->
        err
    end
  end

  #####################################################################
  # Event types
  #####################################################################

  @doc """
  Returns a list of all `Nyght.Events.Type` in the database.
  """
  @spec list_types :: list(Nyght.Events.Type.t())
  def list_types, do: Repo.all(Type)

  @doc """
  Creates a new `Nyght.Events.Type`.

  This function is mainly used for database seeding, as there's no
  publicly available feature using it.
  """
  @spec create_type(map()) :: {:ok, Nyght.Events.Type.t()} | {:error, Ecto.Changeset.t()}
  def create_type(attrs) do
    %Type{}
    |> Type.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Gets the `Nyght.Events.Type`s corresponding to the given list of ids.
  """
  @spec get_types(list(binary())) :: list(Nyght.Events.Type.t())
  def get_types(id_list) when is_list(id_list) do
    id_list
    |> Type.get()
    |> Repo.all()
  end

  #####################################################################
  # Attachments
  #####################################################################

  @doc """
  Attaches a file to the given event.

  This is made to be used with `Phoenix.LiveView.consume_uploaded_entries/3`,
  the entry being the second parameter.
  """
  @spec attach_file(
          Nyght.Events.Event.t(),
          Phoenix.LiveView.UploadEntry.t(),
          binary(),
          list() | nil
        ) :: {:ok, Nyght.Events.Attachment.t() | {:error, any()}}
  def attach_file(%Event{} = event, entry, content, roles \\ []) do
    Ecto.Multi.new()
    |> Ecto.Multi.insert(:attachment, attachment_changeset(event, entry, roles))
    |> Ecto.Multi.run(:content, &encrypt_attachment(&1, &2, event, content))
    |> Ecto.Multi.run(:upload, &upload_attachment_s3/2)
    |> Repo.transaction()
    |> case do
      {:ok, %{attachment: attachment}} ->
        {:ok, attachment}

      {:error, _, err, _} ->
        {:error, err}
    end
  end

  defp attachment_changeset(event, entry, roles) do
    %{uuid: uuid, client_name: file_name, client_type: content_type} = entry

    resource_path = "/attachments/#{event.id}/#{uuid}"
    encryption_iv = Crypto.generate_secure_string(@iv_size)

    event
    |> Ecto.build_assoc(:attachments)
    |> Attachment.changeset(%{
      file_name: file_name,
      resource_path: resource_path,
      content_type: content_type,
      encryption_iv: encryption_iv,
      roles: roles
    })
  end

  @doc """
  Gets a Flop list of attachments for the given event.
  """
  @spec list_attachments_for(Nyght.Events.Event.t(), map()) ::
          {:error, Flop.Meta.t()} | {:ok, {list(), Flop.Meta.t()}}
  def list_attachments_for(%Event{} = event, %{} = params) do
    Attachment.get()
    |> Attachment.scope(event)
    |> Attachment.preload([:roles])
    |> Flop.validate_and_run(params, for: Attachment)
  end

  @doc """
  Gets a specific attachment and its decrypted content by its id.
  """
  @spec get_attachment(binary(), keyword() | nil) ::
          {:ok, {Nyght.Events.Attachment.t(), binary()}} | {:error, any()}
  def get_attachment(id, opts \\ []) do
    with_content? = Keyword.get(opts, :with_content?, true)

    with {:ok, entry} <- get_attachment_entry(id),
         {:ok, content} <- maybe_with_content(entry, with_content?) do
      {:ok, {entry, content}}
    else
      err ->
        err
    end
  end

  defp maybe_with_content(entry, true) do
    bucket = Nyght.config([:s3, :uploads_bucket])

    with {:ok, encrypted_content} <- Files.download(bucket, entry.resource_path),
         {:ok, content} <-
           Crypto.aes_decrypt(encrypted_content, entry.event.encryption_key, entry.encryption_iv) do
      {:ok, content}
    else
      err ->
        err
    end
  end

  defp maybe_with_content(_entry, false), do: {:ok, nil}

  @doc """
  Creates an update changeset for an attachment.
  """
  @spec change_attachment(Nyght.Events.Attachment.t(), map() | nil) :: Ecto.Changeset.t()
  def change_attachment(%Attachment{} = attachment, attrs \\ %{}) do
    attachment
    |> Repo.preload([:roles])
    |> Attachment.update_changeset(attrs)
  end

  def update_attachment(%Attachment{} = attachment, attrs) do
    attachment
    |> change_attachment(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes an attachment in the database and object storage.
  """
  @spec delete_attachment(Nyght.Events.Attachment.t()) ::
          {:ok, Nyght.Events.Attachment.t()} | {:error, any()}
  def delete_attachment(%Attachment{} = attachment) do
    Ecto.Multi.new()
    |> Ecto.Multi.run(:delete_s3, &delete_attachment_s3(&1, &2, attachment))
    |> Ecto.Multi.delete(:delete_db, attachment)
    |> Repo.transaction()
    |> case do
      {:ok, %{delete_db: attachment}} ->
        {:ok, attachment}

      {:error, _, err, _} ->
        {:error, err}
    end
  end

  @doc """
  Deletes all attachments for a specific event.
  """
  @spec delete_event_attachments(Nyght.Events.Event.t()) ::
          {:ok, Nyght.Events.Attachment.t()} | {:error, any()}
  def delete_event_attachments(%Event{} = event) do
    query =
      Attachment.get()
      |> Attachment.scope(event)

    Ecto.Multi.new()
    |> Ecto.Multi.all(:attachments, query)
    |> Ecto.Multi.run(:delete_s3, &delete_all_attachments_s3/2)
    |> Ecto.Multi.delete_all(:delete_db, query)
    |> Repo.transaction()
    |> case do
      {:ok, %{delete_db: attachment}} ->
        {:ok, attachment}

      {:error, _, err, _} ->
        {:error, err}
    end
  end

  defp get_attachment_entry(id) do
    Attachment.get(id)
    |> Attachment.preload([:roles, :event])
    |> Repo.one()
    |> case do
      nil ->
        {:error, :not_found}

      res ->
        {:ok, res}
    end
  end

  defp encrypt_attachment(_, %{attachment: attachment}, %Event{encryption_key: key}, content) do
    key = Base.decode64!(key)
    iv = Base.decode64!(attachment.encryption_iv)

    {:ok, Crypto.aes_encrypt(content, key, iv)}
  end

  defp upload_attachment_s3(_, %{attachment: attachment, content: content}) do
    Nyght.config([:s3, :uploads_bucket])
    |> Files.upload(attachment.resource_path, content)
  end

  defp delete_attachment_s3(_, _, attachment) do
    Nyght.config([:s3, :uploads_bucket])
    |> Files.delete(attachment.resource_path)
  end

  defp delete_all_attachments_s3(_, %{attachments: attachments}) do
    paths = Enum.map(attachments, & &1.resource_path)

    Nyght.config([:s3, :uploads_bucket])
    |> Files.delete_all(paths)
  end

  #####################################################################
  # Pubsub
  #####################################################################

  @doc """
  Subscribe to events from all events of an organization.
  """
  @spec subscribe(Nyght.Events.Event.t()) ::
          :ok | {:error, {:already_registered, pid}}
  def subscribe(%Event{id: id}) do
    Phoenix.PubSub.subscribe(Nyght.PubSub, topic(id))
  end

  @doc """
  Broadcasts an event to all the listeners.
  """
  def broadcast!(source, org_id, event_id, event) do
    Phoenix.PubSub.broadcast!(Nyght.PubSub, topic(event_id), {source, event})
    Organizations.broadcast!(source, org_id, event)
  end

  defp topic(event_id), do: "event:#{event_id}"

  #####################################################################
  # Misc
  #####################################################################

  @doc """
  Attach a poster to the event and creates a thumbnail for it.
  """
  @spec attach_posters(binary(), binary()) :: {:ok, {binary(), binary()}} | {:error, any()}
  def attach_posters(src_path, entry) do
    with {:ok, image} <- Image.open(src_path),
         {:ok, poster} <- do_upload_poster(image, entry),
         {:ok, thumbnail} <- do_upload_thumbnail(image, entry) do
      {:ok, {poster, thumbnail}}
    else
      err ->
        err
    end
  end

  defp do_upload_poster(image, %{uuid: uuid}) do
    bucket = Nyght.config([:s3, :assets_bucket])
    filename = "#{uuid}.png"

    with {:ok, content} <- Image.write(image, :memory, suffix: ".png"),
         {:ok, _} <-
           Files.upload(bucket, filename, content, content_type: "image/png", acl: :public_read) do
      {:ok, filename}
    else
      err ->
        err
    end
  end

  defp do_upload_thumbnail(image, %{uuid: uuid}) do
    bucket = Nyght.config([:s3, :assets_bucket])
    filename = "#{uuid}_thumb.webp"

    with {:ok, thumbnail} <- Image.thumbnail(image, 500, crop: :center, height: 705),
         {:ok, content} <- Image.write(thumbnail, :memory, suffix: ".webp"),
         {:ok, _} <-
           Files.upload(bucket, filename, content, content_type: "image/webp", acl: :public_read) do
      {:ok, filename}
    else
      err ->
        err
    end
  end

  #####################################################################
  # Private functions
  #####################################################################

  defp do_list_events(query, params, opts) do
    flop_opts = [for: Event]

    with {:ok, flop} <- Flop.validate(params, flop_opts) do
      res =
        query
        |> Event.preload(Keyword.get(opts, :preload, []))
        |> Event.filter_members_only(Keyword.get(opts, :members_only?, false))
        |> Event.filter_status(Keyword.get(opts, :status, :published))
        |> maybe_with_local_times(Keyword.get(opts, :with_local_times?, true))
        |> Flop.with_named_bindings(flop, &Event.join_assoc/2, flop_opts)
        |> Flop.run(flop, flop_opts)

      {:ok, res}
    end
  end

  defp maybe_with_local_times(query, false), do: query
  defp maybe_with_local_times(query, true), do: Event.with_local_times(query)

  defp change_event_status(%Event{} = event, changes) do
    event
    |> Ecto.Changeset.change(changes)
    |> Repo.update()
    |> case do
      {:ok, event} ->
        broadcast!(__MODULE__, event.organization_id, event.id, %EventUpdated{event: event})

        {:ok, event}

      err ->
        err
    end
  end

  defp copy_ticket_links(links) do
    Enum.map(links, &Map.take(&1, [:name, :url]))
  end

  defp copy_prices(prices) do
    Enum.map(prices, &Map.take(&1, [:position, :label, :amount]))
  end

  defp delete_event_posters(%Event{} = event) do
    bucket = Nyght.config([:s3, :assets_bucket])

    case Files.delete_all(bucket, [event.poster, event.poster_thumbnail]) do
      {:ok, _} ->
        {:ok, event}

      err ->
        err
    end
  end

  defp delete_event_posters(err), do: err

  defp after_save({:ok, event}, func) do
    {:ok, _event} = func.(event)
  end

  defp after_save(error, _func), do: error
end
