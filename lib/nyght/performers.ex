defmodule Nyght.Performers do
  @moduledoc """
  The Performers context.
  """

  import Ecto.Query, warn: false
  alias Nyght.Performers.Booking
  alias Nyght.Repo

  alias Nyght.Events.Event
  alias Nyght.Organizations.Organization
  alias Nyght.Performers.Performer

  def list_performers_for(%Organization{} = org, %{} = params, _opts \\ []) do
    query =
      Performer.get()
      |> Performer.scope(org)

    Flop.validate_and_run(query, params, for: Performer)
  end

  @doc """
  Gets a single performer.

  Raises `Ecto.NoResultsError` if the Performer does not exist.

  ## Examples

      iex> get_performer!(123)
      %Performer{}

      iex> get_performer!(456)
      ** (Ecto.NoResultsError)

  """
  def get_performer!(id), do: Repo.get!(Performer, id)

  def search_by_name(name, %Organization{id: org_id}) do
    similarity = 0.1

    query =
      from p in Performer,
        where: p.organization_id == ^org_id,
        where: fragment("SIMILARITY(name, ?) > ?", ^name, ^similarity),
        order_by: fragment("1 - SIMILARITY(name, ?)", ^name),
        limit: 10

    Repo.all(query)
  end

  @doc """
  Creates a performer.

  ## Examples

      iex> create_performer(%{field: value})
      {:ok, %Performer{}}

      iex> create_performer(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_performer(%Organization{} = org, attrs \\ %{}) do
    attrs =
      Map.merge(attrs, %{organization_id: org.id})
      |> prepare_params()

    %Performer{}
    |> Performer.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a performer.

  ## Examples

      iex> update_performer(performer, %{field: new_value})
      {:ok, %Performer{}}

      iex> update_performer(performer, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_performer(%Performer{} = performer, attrs) do
    performer
    |> Performer.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a performer.

  ## Examples

      iex> delete_performer(performer)
      {:ok, %Performer{}}

      iex> delete_performer(performer)
      {:error, %Ecto.Changeset{}}

  """
  def delete_performer(%Performer{} = performer) do
    Repo.delete(performer)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking performer changes.

  ## Examples

      iex> change_performer(performer)
      %Ecto.Changeset{data: %Performer{}}

  """
  def change_performer(%Performer{} = performer, attrs \\ %{}) do
    Performer.changeset(performer, attrs)
  end

  def get_booking!(id), do: Repo.get!(Booking, id) |> Repo.preload([:performer])

  def list_bookings(%Event{id: event_id}) do
    Booking.get()
    |> Booking.by_event(event_id)
    |> Booking.include([:performer])
    |> Repo.all()
  end

  def change_booking_details(%Booking{} = booking, attrs \\ %{}) do
    Booking.details_changeset(booking, attrs)
  end

  def update_booking_details(%Booking{} = booking, attrs) do
    booking
    |> Booking.details_changeset(attrs)
    |> Repo.update()
  end

  def delete_booking(%Booking{} = booking) do
    Repo.delete(booking)
  end

  defp prepare_params(params) do
    for {k, v} <- params, do: {to_string(k), v}, into: %{}
  end
end
