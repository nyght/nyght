defmodule Nyght.EctoAtom do
  @moduledoc """
  Coerce a regular string into an atom
  ## Examples
  Note: only use this field when you have a fixed number of possible values (atoms are not garbage collected)
      iex> EctoFields.Atom.cast("started")
      {:ok, :started}
      iex> EctoFields.Atom.cast(:started)
      {:ok, :started}
      iex> EctoFields.Atom.cast(nil)
      {:ok, nil}
  """
  @behaviour Ecto.Type

  @max_atom_length 0xFF

  @doc false
  def type, do: :string

  @doc false
  def cast(nil), do: {:ok, nil}
  def cast(atom) when is_atom(atom), do: {:ok, atom}

  def cast(binary) when is_binary(binary) and byte_size(binary) <= @max_atom_length,
    do: {:ok, String.to_atom(binary)}

  def cast(_), do: :error

  @doc false
  def load(term), do: cast(term)

  @doc false
  def dump(nil), do: {:ok, nil}
  def dump(atom) when is_atom(atom), do: {:ok, Atom.to_string(atom)}

  def dump(binary) when is_binary(binary) and byte_size(binary) <= @max_atom_length,
    do: {:ok, binary}

  def dump(_), do: :error

  @doc false
  def embed_as(_), do: :self

  @doc false
  def equal?(a, b), do: a == b
end
