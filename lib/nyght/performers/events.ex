defmodule Nyght.Performers.Events do
  @moduledoc """
  This modules contains different structs representing events about performers and bookings.
  """

  defmodule PerformerCreated do
    @moduledoc false

    defstruct performer: nil
  end

  defmodule PerformerUpdated do
    @moduledoc false

    defstruct performer: nil
  end

  defmodule PerformerDeleted do
    @moduledoc false

    defstruct performer_id: nil
  end
end
