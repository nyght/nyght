defmodule Nyght.Performers.Performer do
  @moduledoc false

  use Nyght.Utils.NyghtSchema,
    binding: :performer,
    permissions: [:list, :create, :read, :update, :delete]

  alias Nyght.Authorizations
  alias Nyght.Common.Link
  alias Nyght.Organizations.{Member, Organization}

  @derive {
    Flop.Schema,
    filterable: [
      :name,
      :country
    ],
    sortable: [
      :name,
      :country
    ]
  }

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  typed_schema "performers" do
    field(:name, :string)
    field(:email, :string)
    field(:country, :string)
    field(:phone_number, :string)

    embeds_many(:links, Link, on_replace: :delete)

    belongs_to(:organization, Organization)

    timestamps()
  end

  @doc false
  def changeset(performer, attrs) do
    performer
    |> cast(attrs, [:name, :email, :phone_number, :country, :organization_id])
    |> cast_embed(:links, sort_param: :links_sort, drop_param: :links_drop)
    |> validate_required([:name, :organization_id])
    |> validate_length(:links, max: 5)
  end

  def scope(query, %Organization{id: id}) do
    where(query, [performer: p], p.organization_id == ^id)
  end

  @impl true
  def authorize(%Member{} = member, _item, action),
    do: Authorizations.member_can?(member, __MODULE__, action)

  def authorize(_member, _item, _action), do: :error
end
