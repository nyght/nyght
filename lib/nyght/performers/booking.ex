defmodule Nyght.Performers.Booking do
  @moduledoc """
  This represents the link between an event and a performer.
  """
  use TypedEctoSchema
  use Nyght.Authorizations.Policy

  import Ecto.Changeset
  import Ecto.Query, warn: false

  alias Nyght.Authorizations
  alias Nyght.Events.Event
  alias Nyght.Organizations.Member
  alias Nyght.Performers
  alias Nyght.Performers.Performer

  @type payment_type() :: :wire | :cash

  @permissions [:create, :update, :delete, :read_details]

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  typed_schema "bookings" do
    field(:position, :integer)

    field(:fee, :decimal)

    field(:payment_type, Ecto.Enum, values: [:wire, :cash]) ::
      Booking.payment_type()

    field(:recipient, :string)

    field(:people_count, :integer)
    field(:special_requests, :string)

    belongs_to(:event, Event)
    belongs_to(:performer, Performer)

    timestamps()
  end

  @doc false
  def changeset(booking, attrs) do
    booking
    |> cast(attrs, [:event_id, :performer_id])
    |> find_or_create_performer()
  end

  def changeset(booking, attrs, position) do
    booking
    |> changeset(attrs)
    |> change(position: position)
  end

  def details_changeset(booking, attrs) do
    cast(booking, attrs, [:fee, :payment_type, :recipient, :people_count, :special_requests])
  end

  defp find_or_create_performer(changeset) do
    case get_change(changeset, :performer_id) do
      nil ->
        cast_assoc(changeset, :performer)

      id ->
        performer = Performers.get_performer!(id)
        put_assoc(changeset, :performer, performer)
    end
  end

  @doc """
  Creates the base query for bookings.
  """
  @spec get :: Ecto.Query.t()
  def get do
    from e in __MODULE__, as: :booking
  end

  @doc """
  Creates the query to join related items.
  """
  @spec include(Ecto.Query.t(), list(atom())) :: Ecto.Query.t()
  def include(query, args) do
    from _e in query, preload: ^args
  end

  @doc """
  Filters the bookings by event id
  """
  @spec by_event(Ecto.Query.t(), any()) :: Ecto.Query.t()
  def by_event(query, event_id) do
    where(query, [booking: b], b.event_id == ^event_id)
  end

  @doc """
  Filters the bookings by performer id
  """
  @spec by_performer(Ecto.Query.t(), any()) :: Ecto.Query.t()
  def by_performer(query, performer_id) do
    where(query, [booking: b], b.performer_id == ^performer_id)
  end

  @impl true
  def authorize(%Member{} = member, _item, action),
    do: Authorizations.member_can?(member, __MODULE__, action)

  def authorize(_member, _item, _action), do: :error
end
