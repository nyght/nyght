defmodule Nyght.Events.Type do
  @moduledoc """
  This module represents an event type.
  """

  use TypedEctoSchema

  import Ecto.Changeset
  import Ecto.Query

  alias Nyght.Events.Event

  alias __MODULE__

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  typed_schema "types" do
    field(:name, :string)

    many_to_many(:events, Event, join_through: "events_types", on_replace: :delete)
  end

  @doc false
  def changeset(type, attrs) do
    type
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end

  @doc """
  Creates the base query for types.
  """
  @spec get :: Ecto.Query.t()
  def get do
    from t in Type, as: :type
  end

  @doc """
  Creates the base query for a given type or list of types.
  """
  @spec get(any) :: Ecto.Query.t()
  def get(ids) when is_list(ids) do
    from [type: t] in get(), where: t.id in ^ids
  end

  def get(id) do
    from [type: t] in get(), where: t.id == ^id
  end
end
