defmodule Nyght.Events.Attachment do
  @moduledoc """
  Represents a file attached to an event.
  """
  use Nyght.Utils.NyghtSchema,
    binding: :attachment,
    permissions: [:update, :create, :delete, :list]

  alias Nyght.Authorizations
  alias Nyght.Authorizations.Role
  alias Nyght.Events.Event
  alias Nyght.Organizations.Member

  @derive {
    Flop.Schema,
    filterable: [:file_name], sortable: [:file_name, :updated_at]
  }
  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  typed_schema "attachments" do
    field(:file_name, :string)
    field(:resource_path, :string)
    field(:content_type, :string)
    field(:encryption_iv, :string)

    belongs_to(:event, Event)

    many_to_many(:roles, Role, join_through: "attachments_roles", on_replace: :delete)

    timestamps()
  end

  @required [:event_id, :file_name, :resource_path, :content_type, :encryption_iv]
  @doc false
  def changeset(attachment, attrs) do
    attachment
    |> cast(attrs, @required)
    |> validate_required(@required)
    |> validate_length(:file_name, min: 4)
    |> put_roles(attrs)
    |> unique_constraint(:roles,
      name: :attachments_roles_pkey,
      message: "has already this role",
      error_key: :roles
    )
  end

  @doc false
  def update_changeset(attachment, attrs) do
    attachment
    |> cast(attrs, [:file_name])
    |> validate_length(:file_name, min: 4)
    |> put_roles(attrs)
    |> unique_constraint(:roles,
      name: :attachments_roles_pkey,
      message: "has already this role",
      error_key: :roles
    )
  end

  defp put_roles(changeset, attrs) do
    case Map.get(attrs, :roles, Map.get(attrs, "roles")) do
      nil ->
        changeset

      role_ids ->
        roles = Authorizations.get_roles(role_ids)

        put_assoc(changeset, :roles, roles)
    end
  end

  def scope(query, %Event{id: event_id}) do
    from [attachment: a] in query, where: a.event_id == ^event_id
  end

  defp check_user_roles(_, []), do: true

  defp check_user_roles(member_roles, roles) do
    roles = MapSet.new(roles, & &1.id)

    MapSet.new(member_roles, & &1.id)
    |> MapSet.disjoint?(roles)
    |> Kernel.not()
  end

  @impl true
  def authorize(%Member{roles: member_roles}, %__MODULE__{roles: roles}, :read),
    do: check_user_roles(member_roles, roles)

  def authorize(%Member{roles: member_roles}, %__MODULE__{roles: roles}, :download),
    do: check_user_roles(member_roles, roles)

  def authorize(%Member{} = member, _, action),
    do: Authorizations.member_can?(member, __MODULE__, action)

  def authorize(_member, _item, _action), do: :error
end
