defmodule Nyght.Events.Price do
  @moduledoc """
  This module represents an admission price for an event.
  """

  use TypedEctoSchema
  use Nyght.Authorizations.Policy

  import Ecto.Changeset

  alias Nyght.Authorizations
  alias Nyght.Events.Event
  alias Nyght.Organizations.Member

  @permissions [:create, :update, :delete]

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  typed_schema "prices" do
    field(:position, :integer)
    field(:amount, :decimal)
    field(:label, :string)

    belongs_to(:event, Event)

    timestamps()
  end

  def changeset(price, attrs, position) do
    price
    |> cast(attrs, [:label, :amount])
    |> validate_required([:label, :amount])
    |> validate_number(:amount, greater_than_or_equal_to: 0.0)
    |> change(position: position)
  end

  @impl true
  def authorize(%Member{} = member, _item, action),
    do: Authorizations.member_can?(member, __MODULE__, action)

  def authorize(_member, _item, _action), do: :error
end
