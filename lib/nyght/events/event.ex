defmodule Nyght.Events.Event do
  @moduledoc false

  use TypedEctoSchema
  use Nyght.Authorizations.Policy

  import Ecto.Changeset
  import Ecto.Query
  import Nyght.Helpers.EctoHelpers

  alias Nyght.Authorizations
  alias Nyght.Common.Link
  alias Nyght.Crypto
  alias Nyght.Events
  alias Nyght.Events.Attachment
  alias Nyght.Events.{Event, Price, Type}
  alias Nyght.Organizations.{Member, Organization}
  alias Nyght.Performers.Booking
  alias Nyght.Shifts.Shift

  @derive {
    Flop.Schema,
    filterable: [
      :name,
      :is_canceled,
      :is_soldout,
      :starts_at,
      :ends_at,
      :event_type
    ],
    sortable: [:name, :starts_at, :ends_at],
    adapter_opts: [
      join_fields: [
        event_type: [
          binding: :types,
          field: :name,
          ecto_type: :string
        ]
      ]
    ]
  }

  @type status() :: :unpublished | :prepublished | :published

  @permissions [:create, :list, :read_prepublished, :read_unpublished, :update, :delete, :cancel]

  @key_size 32
  @required_attrs [:name, :timezone, :status, :organization_id]
  @attrs [
    :description,
    :poster,
    :poster_thumbnail,
    :encryption_key,
    :local_starts_at,
    :local_ends_at,
    :starts_at,
    :ends_at,
    :is_members_only,
    :is_soldout,
    :is_canceled
  ]

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  typed_schema "events" do
    field(:name, :string)
    field(:description, :string)
    field(:additional_info, :string, null: true)
    field(:poster, :string, null: true)
    field(:poster_thumbnail, :string, null: true)

    field(:encryption_key, :string)

    field(:starts_at, :utc_datetime)
    field(:ends_at, :utc_datetime)
    field(:timezone, :string, default: "Etc/UTC") :: Calendar.time_zone()

    field(:local_starts_at, :naive_datetime, virtual: true)
    field(:local_ends_at, :naive_datetime, virtual: true)

    field(:next_event_id, :binary, null: true, virtual: true)
    field(:next_event_name, :string, null: true, virtual: true)
    field(:prev_event_id, :binary, null: true, virtual: true)
    field(:prev_event_name, :string, null: true, virtual: true)

    field(:is_members_only, :boolean, default: false)
    field(:is_soldout, :boolean, default: false)
    field(:is_canceled, :boolean, default: false)

    field(:status, Ecto.Enum,
      values: [:unpublished, :prepublished, :published],
      default: :unpublished
    ) ::
      Event.status()

    embeds_many(:ticket_links, Link, on_replace: :delete)

    belongs_to(:organization, Organization)

    has_many(:prices, Price)
    has_many(:shifts, Shift)
    has_many(:bookings, Booking, on_replace: :delete)
    has_many(:attachments, Attachment, on_replace: :delete)

    many_to_many(:types, Type, join_through: "events_types", on_replace: :delete)

    timestamps()
  end

  @doc """
  Creates the default changeset
  """
  @spec changeset(t(), map()) :: Ecto.Changeset.t()
  def changeset(event, attrs) do
    event
    |> cast(attrs, @required_attrs ++ @attrs)
    |> cast_prices()
    |> cast_bookings()
    |> cast_ticket_links()
    |> put_types(attrs)
    |> put_encryption_key()
    |> validate_required(@required_attrs)
    |> put_utc_times()
    |> validate_start_end_times(:starts_at, :ends_at)
  end

  defp cast_ticket_links(changeset) do
    cast_embed(changeset, :ticket_links,
      sort_param: :ticket_links_sort,
      drop_param: :ticket_links_drop
    )
  end

  defp cast_bookings(changeset) do
    cast_assoc(changeset, :bookings,
      sort_param: :bookings_sort,
      drop_param: :bookings_drop,
      with: &Booking.changeset/3
    )
  end

  defp cast_prices(changeset) do
    cast_assoc(changeset, :prices,
      sort_param: :prices_sort,
      drop_param: :prices_drop,
      with: &Price.changeset/3
    )
  end

  defp put_utc_times(changeset) do
    if timezone = get_field(changeset, :timezone) do
      changeset
      |> put_utc_time(:local_starts_at, :starts_at, timezone)
      |> put_utc_time(:local_ends_at, :ends_at, timezone)
    else
      changeset
    end
  end

  defp put_utc_time(changeset, input_key, output_key, original_timezone) do
    if datetime = get_field(changeset, input_key) do
      datetime
      |> DateTime.from_naive(original_timezone)
      |> put_utc_time_changes(changeset, input_key, output_key)
    else
      validate_required(changeset, [output_key])
    end
  end

  defp put_utc_time_changes({:ok, datetime}, changeset, _, output_key) do
    converted = DateTime.shift_zone!(datetime, "Etc/UTC")
    Ecto.Changeset.put_change(changeset, output_key, converted)
  end

  defp put_utc_time_changes({:ambiguous, _, datetime}, changeset, _, output_key) do
    converted = DateTime.shift_zone!(datetime, "Etc/UTC")
    Ecto.Changeset.put_change(changeset, output_key, converted)
  end

  defp put_utc_time_changes({:gap, _, _datetime}, changeset, input_key, _) do
    Ecto.Changeset.add_error(
      changeset,
      input_key,
      Gettext.dgettext(
        NyghtWeb.Gettext,
        "errors",
        "not a valid time because of daylight saving time"
      )
    )
  end

  defp put_types(changeset, attrs) do
    case Map.get(attrs, :types, Map.get(attrs, "types")) do
      nil ->
        changeset

      [%Type{} | _] = types ->
        put_assoc(changeset, :types, types)

      type_ids ->
        types = Events.get_types(type_ids)

        put_assoc(changeset, :types, types)
    end
  end

  defp put_encryption_key(changeset) do
    case get_field(changeset, :encryption_key) do
      nil ->
        key = Crypto.generate_secure_string(@key_size)

        put_change(changeset, :encryption_key, key)

      _ ->
        changeset
    end
  end

  @doc """
  Creates the base query for events.
  """
  @spec get :: Ecto.Query.t()
  def get do
    from e in Event, as: :event
  end

  @doc """
  Creates the base query for a given event.
  """
  @spec get(any) :: Ecto.Query.t()
  def get(id) do
    filter_id(get(), id)
  end

  def scope(query, %Organization{id: id}) do
    filter_org(query, id)
  end

  def preload(query, []), do: query

  def preload(query, fields) do
    from [event: e] in query,
      preload: ^fields
  end

  def join_assoc(query, :types) do
    join(query, :inner, [e], t in assoc(e, :types), as: :types)
  end

  def get_neighbors(event_id, org_id, opts \\ []) do
    neighbors_query =
      get()
      |> filter_status(Keyword.get(opts, :status, :published))
      |> filter_members_only(Keyword.get(opts, :members_only?, false))
      |> filter_org(org_id)

    neighbors_query =
      from e in neighbors_query,
        windows: [w: [order_by: e.starts_at]],
        select: %{
          id: e.id,
          prev_event_id: lag(e.id) |> over(:w) |> type(Ecto.UUID),
          prev_event_name: lag(e.name) |> over(:w),
          next_event_id: lead(e.id) |> over(:w) |> type(Ecto.UUID),
          next_event_name: lead(e.name) |> over(:w)
        }

    from [event: e] in get(event_id),
      left_join: n in subquery(neighbors_query),
      on: e.id == n.id,
      select: %{
        prev_event_id: n.prev_event_id,
        prev_event_name: n.prev_event_name,
        next_event_id: n.next_event_id,
        next_event_name: n.next_event_name
      }
  end

  def with_local_times(query) do
    from [event: e] in query,
      select_merge: %{
        local_starts_at: shift_timezone(e.starts_at, e.timezone),
        local_ends_at: shift_timezone(e.ends_at, e.timezone)
      }
  end

  @doc """
  Filters the given query by event id.
  """
  @spec filter_id(Ecto.Query.t(), any) :: Ecto.Query.t()
  def filter_id(query, id) do
    from e in query, where: e.id == ^id
  end

  @doc """
  Filters the events by organization.
  """
  @spec filter_org(Ecto.Query.t(), any) :: Ecto.Query.t()
  def filter_org(query, org_id) do
    from [event: e] in query, where: e.organization_id == ^org_id
  end

  @doc """
  Filters the events having an end time after the given time
  """
  @spec filter_after(Ecto.Queryable.t(), DateTime.t()) :: Ecto.Query.t()
  def filter_after(query, datetime) do
    from [event: e] in query,
      where: e.ends_at >= ^datetime,
      order_by: e.starts_at
  end

  @doc """
  Filters the events having an end time before the given time.
  """
  @spec filter_before(Ecto.Queryable.t(), DateTime.t()) :: Ecto.Query.t()
  def filter_before(query, datetime) do
    from [event: e] in query,
      where: e.ends_at <= ^datetime,
      order_by: [desc: e.starts_at]
  end

  @doc """
  Filters the events by status.

  The `:unpublished` status will give all the events, `:prepublished`
  will give the pre-published and published events, and `:published`
  will only give the published events.
  """
  @spec filter_status(Ecto.Queryable.t(), :prepublished | :published | :unpublished) ::
          Ecto.Queryable.t()
  def filter_status(query, :unpublished), do: query

  def filter_status(query, :published) do
    from [event: e] in query, where: e.status == :published
  end

  def filter_status(query, :prepublished) do
    from [event: e] in query, where: e.status in [:prepublished, :published]
  end

  @doc """
  Filters the events by their members-only flag.
  """
  @spec filter_members_only(Ecto.Query.t(), boolean()) :: Ecto.Query.t()
  def filter_members_only(query, true), do: query

  def filter_members_only(query, false) do
    from [event: e] in query, where: e.is_members_only == false
  end

  @doc """
  Filters the events within the given period.
  """
  @spec filter_period(Ecto.Queryable.t(), Date.t(), integer(), binary()) :: Ecto.Query.t()
  def filter_period(query, from, amount, period \\ "month") do
    from [event: e] in query,
      where:
        e.starts_at > fragment("(?)::date - ('1 ' || ?)::interval * ?", ^from, ^period, ^amount)
  end

  def paginate(query, offset, limit) do
    from [event: e] in query,
      limit: ^limit,
      offset: ^offset
  end

  @doc """
  Orders the query by the events start date.
  """
  @spec order_by_start_date(Ecto.Query.t()) :: Ecto.Query.t()
  def order_by_start_date(query) do
    from [event: e] in query, order_by: [desc: e.starts_at]
  end

  @doc """
  Creates an update query to change the event status.
  """
  @spec update_status(Ecto.Query.t(), Nyght.Events.Event.status()) :: Ecto.Query.t()
  def update_status(query, status) do
    from [event: e] in query,
      update: [set: [status: ^status, updated_at: fragment("NOW()")]],
      select: e
  end

  @doc """
  Creates an update query to change the event cancel flag.
  """
  @spec update_is_canceled(Ecto.Query.t(), boolean) :: Ecto.Query.t()
  def update_is_canceled(query, canceled?) do
    from [event: e] in query,
      update: [set: [is_canceled: ^canceled?, updated_at: fragment("NOW()")]],
      select: e
  end

  @doc """
  Validate a changeset start and end time.
  """
  def validate_start_end_times(changeset, start_field, end_field) do
    with starts_at when not is_nil(starts_at) <- get_field(changeset, start_field),
         ends_at when not is_nil(ends_at) <- get_field(changeset, end_field) do
      case DateTime.compare(starts_at, ends_at) do
        :gt ->
          changeset
          |> add_error(:starts_at, "cannot be after end time")
          |> add_error(:local_starts_at, "cannot be after end time")

        _ ->
          changeset
      end
    else
      _ ->
        changeset
    end
  end

  @doc false
  @impl true
  def authorize(%Member{} = member, %Event{} = event, :read),
    do: authorize_members_only(member, event) && authorize_status(member, event)

  def authorize(%Member{} = member, _, action),
    do: Authorizations.member_can?(member, __MODULE__, action)

  def authorize(nil, %Event{status: :published, is_members_only: false}, :read), do: :ok
  def authorize(_member, _item, _action), do: :error

  defp authorize_members_only(%Member{organization_id: org_id}, %Event{
         is_members_only: true,
         organization_id: org_id
       }),
       do: true

  defp authorize_members_only(_, _), do: false

  defp authorize_status(%Member{}, %Event{status: :published}), do: :ok

  defp authorize_status(%Member{} = member, %Event{status: :prepublished}),
    do: Authorizations.member_can?(member, __MODULE__, :read_prepublished)

  defp authorize_status(%Member{} = member, %Event{status: :unpublished}),
    do: Authorizations.member_can?(member, __MODULE__, :read_unpublished)
end
