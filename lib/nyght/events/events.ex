defmodule Nyght.Events.Events do
  @moduledoc """
  This modules contains different structs representing events about events.

  These are system events used in pubsub, not events in a calendar!
  """

  defmodule EventCreated do
    @moduledoc false

    defstruct event: nil
  end

  defmodule EventUpdated do
    @moduledoc false

    defstruct event: nil
  end

  defmodule EventDeleted do
    @moduledoc false

    defstruct event: nil
  end
end
