defmodule Nyght.Emails do
  @moduledoc false
  use Swoosh.Mailer, otp_app: :nyght

  import Swoosh.Email

  require Logger

  alias Nyght.Accounts.User
  alias Nyght.Emails.{Confirmation, EmailUpdate, PasswordReset, RequestAcceptation, ShiftsUpdate}
  alias Nyght.Organizations.Organization

  @doc """
  Delivers the account confirmation instructions email.
  """
  def deliver_confirmation_instructions(%User{} = user, url) do
    assigns = %{first_name: user.first_name, url: url}

    new()
    |> subject("Welcome to nyght!")
    |> render_body(Confirmation, assigns)
    |> send_email(user.email)
  end

  @doc """
  Delivers the password reset instructions email.
  """
  def deliver_reset_password_instructions(%User{} = user, url) do
    assigns = %{first_name: user.first_name, url: url}

    new()
    |> subject("Forgot your password?")
    |> render_body(PasswordReset, assigns)
    |> send_email(user.email)
  end

  @doc """
  Delivers the email address change instructions email.
  """
  def deliver_update_email_instructions(%User{} = user, url) do
    assigns = %{first_name: user.first_name, url: url}

    new()
    |> subject("Change your email address?")
    |> render_body(EmailUpdate, assigns)
    |> send_email(user.email)
  end

  @doc """
  Delivers the invitation email.
  """
  def deliver_invitation_instructions(%User{} = _user, _url) do
    {:error, nil}
  end

  @doc """
  Delivers the request acceptation email.
  """
  def deliver_request_acceptation_message(%User{} = user, %Organization{name: name}, url) do
    assigns = %{first_name: user.first_name, organization_name: name, url: url}

    new()
    |> subject("You're now a part of #{name}!")
    |> render_body(RequestAcceptation, assigns)
    |> send_email(user.email)
  end

  def deliver_shifts_update_message(%User{} = user, shifts) do
    assigns = %{
      first_name: user.first_name,
      shifts: shifts
    }

    new()
    |> subject("Your applications have been reviewed!")
    |> render_body(ShiftsUpdate, assigns)
    |> send_email(user.email)
  end

  defp send_email(email, to) do
    email =
      email
      |> from({"nyght", "dontreply@nyght.ch"})
      |> to(to)

    case deliver(email) do
      {:ok, _meta} ->
        {:ok, email}

      err ->
        Logger.error("Error while sending email #{inspect(email)} to #{to}: #{inspect(err)}")
        err
    end
  end

  defp render_body(email, template_module, assigns) do
    rendered_email = template_module.render(assigns)

    html_body(email, rendered_email)
  end
end
