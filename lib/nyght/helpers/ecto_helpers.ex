defmodule Nyght.Helpers.EctoHelpers do
  @moduledoc """
  This module contains helper functions and macro for Ecto.
  """

  defmacro shift_timezone(col, timezone) do
    quote do
      fragment("? at time zone 'utc' at time zone ?", unquote(col), unquote(timezone))
    end
  end

  defmacro add_seconds(col, sec) do
    quote do
      fragment("(? + ? * interval '1 second')", unquote(col), unquote(sec))
    end
  end
end
