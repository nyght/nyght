defmodule Mix.Tasks.Perm.Install do
  @moduledoc """
  This is the auth task used to manage the system's permissions.
  """
  use Mix.Task

  alias Nyght.Release

  @shortdoc "Refreshes the permissions in the database."
  @doc false
  def run(_) do
    # This will start our application
    Mix.Task.run("app.start")

    Release.install_permissions()
  end
end
