defmodule NyghtWeb do
  @moduledoc """
  The entrypoint for defining your web interface, such
  as controllers, views, channels and so on.

  This can be used in your application as:

      use NyghtWeb, :controller
      use NyghtWeb, :view

  The definitions below will be executed for every view,
  controller, etc, so keep them short and clean, focused
  on imports, uses and aliases.

  Do NOT define functions inside the quoted expressions
  below. Instead, define any helper function in modules
  and import those modules here.
  """
  require EEx

  @doc false
  def static_paths, do: ~w(assets fonts images uploads site.webmanifest favicon.ico robots.txt)

  @doc false
  def router do
    quote do
      use Phoenix.Router, helpers: false

      # Import common connection and controller functions to use in pipelines
      import Plug.Conn
      import Phoenix.Controller
      import Phoenix.LiveView.Router
    end
  end

  @doc false
  def controller do
    quote do
      use Phoenix.Controller,
        namespace: NyghtWeb,
        formats: [:html, :json, :ics, :vcf],
        layouts: [html: NyghtWeb.UI.Layouts]

      import Plug.Conn
      use Gettext, backend: NyghtWeb.Gettext

      unquote(verified_routes())
    end
  end

  def html do
    quote do
      use Phoenix.Component

      # Import convenience functions from controllers
      import Phoenix.Controller,
        only: [get_csrf_token: 0, view_module: 1, view_template: 1]

      # Include general helpers for rendering HTML
      unquote(html_helpers())
    end
  end

  @doc false
  def live_view do
    quote do
      use Phoenix.LiveView,
        layout: {NyghtWeb.UI.Layouts, :app}

      import NyghtWeb.LiveHelpers

      on_mount(NyghtWeb.Hooks.NavigationHook)

      unquote(html_helpers())
    end
  end

  @doc false
  def live_component do
    quote do
      use Phoenix.LiveComponent

      import NyghtWeb.LiveHelpers

      unquote(html_helpers())
    end
  end

  @doc false
  def channel do
    quote do
      use Phoenix.Channel
      use Gettext, backend: NyghtWeb.Gettext
    end
  end

  def email do
    quote do
      template_file = Path.basename(__ENV__.file, ".ex") <> ".mjml.eex"
      @template_path Path.join([__DIR__, template_file])
      @external_resource @template_path

      require EEx

      alias Nyght.Emails

      template = File.read!(@template_path)
      EEx.function_from_string(:defp, :render_eex, template, [:assigns])

      def render(assigns) do
        mjml_template = render_eex(assigns)

        {:ok, res} = Mjml.to_html(mjml_template)

        res
      end
    end
  end

  defp html_helpers do
    quote do
      # HTML escaping functionality
      import Phoenix.HTML

      # Core UI components and translation
      use NyghtUi
      use Gettext, backend: NyghtWeb.Gettext
      import NyghtWeb.UI.LayoutComponents

      # Shortcut for generating JS commands
      alias Phoenix.LiveView.JS

      # Routes generation with the ~p sigil
      unquote(verified_routes())
      unquote(cdn_routes())
    end
  end

  def verified_routes do
    quote do
      use Phoenix.VerifiedRoutes,
        endpoint: NyghtWeb.Endpoint,
        router: NyghtWeb.Router,
        statics: NyghtWeb.static_paths()
    end
  end

  def cdn_routes do
    quote do
      def cdn_url(path) when is_binary(path) and byte_size(path) > 0 do
        base_url = Nyght.config([:s3, :cdn_base_url])

        "#{base_url}/#{path}"
      end

      def cdn_url(_), do: nil
    end
  end

  @doc """
  When used, dispatch to the appropriate controller/view/etc.
  """
  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
end
