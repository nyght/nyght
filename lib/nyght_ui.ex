defmodule NyghtUi do
  @moduledoc """
  UI component library for nyght.
  """

  defmacro __using__(_) do
    quote do
      import NyghtUi.{
        Alert,
        Avatar,
        Banner,
        Button,
        Card,
        Content,
        DateTime,
        Dropdown,
        FlopConfig,
        Form,
        Generative,
        Icon,
        Modal,
        Navigation,
        Pill,
        Progress,
        Table
      }
    end
  end
end
