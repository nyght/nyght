defmodule NyghtUi.Avatar do
  @moduledoc false
  use Phoenix.Component

  @doc """
  Renders an avatar circle with initials.

  The initials will be the first letter of the first word and the first
  letter of the last word (if any) of the `name` attribute.

  If the labeled attribute is set, the name will be displayed besides,
  and additional content can be given in the inner block.

  If an `id` attribute is given, it will be used for generating a
  background color, otherwise the name will be used.

  ## Example

      <.avatar name="John Doe" />
      <.avatar name="Jane Doe" labeled>
        jane@doe.com
      </.avatar>

  """
  attr :id, :string, default: nil
  attr :name, :string, required: true
  attr :labeled, :boolean, default: false
  attr :class, :string, default: nil

  slot :inner_block

  def avatar(%{labeled: false} = assigns) do
    ~H"""
    <div
      class={[
        "inline-flex flex-none overflow-hidden relative justify-center items-center w-10 h-10 rounded-full",
        @class
      ]}
      style={"background-color: #{RandomColour.generate(seed: @id || @name, luminosity: "dark")}"}
      title={@name}
    >
      <span class="font-medium text-white">
        {initials(@name)}
      </span>
    </div>
    """
  end

  def avatar(%{labeled: true} = assigns) do
    ~H"""
    <div class={["flex items-center gap-3", @class]}>
      <.avatar name={@name} />
      <div class="flex flex-col">
        <div class="text-sm">{@name}</div>
        <div :if={@inner_block != []} class="text-sm font-normal leading-none text-slate-400">
          {render_slot(@inner_block)}
        </div>
      </div>
    </div>
    """
  end

  defp initials(name) do
    parts =
      name
      |> String.upcase()
      |> String.split()

    first =
      parts
      |> List.first("?")
      |> String.at(0)

    last =
      parts
      |> Enum.drop(1)
      |> List.last("")
      |> String.at(0)

    List.to_string([first, last || ""])
  end
end
