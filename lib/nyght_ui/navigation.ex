defmodule NyghtUi.Navigation do
  @moduledoc """
  This module contains the navigation components, such as buttons, anchors and different bars.
  """
  use Phoenix.Component

  alias NyghtUi.Icon
  alias Phoenix.LiveView.JS

  attr :with_sidebar, :boolean, default: false

  slot :logo
  slot :content, doc: "the the main content of the navbar"
  slot :right, doc: "the items on the right of the navbar"

  def navbar(assigns) do
    ~H"""
    <nav class={["bg-stone-50 py-4 md:px-4 md:py-2", @with_sidebar && "md:ml-16"]}>
      <div class="container mx-auto flex flex-wrap justify-between items-baseline gap-y-4 md:gap-y-0 md:gap-x-6">
        <div class="flex justify-between items-center w-full md:w-auto">
          <div :if={@logo != []}>
            {render_slot(@logo)}
          </div>
          <button
            :if={@with_sidebar}
            type="button"
            phx-click={show_sidebar()}
            class="inline-flex items-center p-2 ml-1 text-lg text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-hidden focus:ring-2 focus:ring-gray-200"
          >
            <span class="sr-only">Open sidebar</span>
            <Icon.icon name="ti-layout-sidebar-left-expand" />
          </button>
          <button
            type="button"
            phx-click={show_navbar()}
            class="inline-flex items-center p-2 ml-1 text-lg text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-hidden focus:ring-2 focus:ring-gray-200"
          >
            <span class="sr-only">Open main menu</span>
            <Icon.icon name="ti-menu-2" />
          </button>
        </div>
        <!-- Left menu items -->
        <div
          id="navbar-left"
          class="hidden w-full items-center flex-col gap-4 font-medium md:flex md:flex-row md:w-auto md:grow md:gap-6"
        >
          {render_slot(@content)}
        </div>
        <!-- Right menu items -->
        <div
          id="navbar-right"
          class="hidden w-full md:flex flex-row items-center gap-4 md:w-fit md:order-2 md:gap-6"
        >
          {render_slot(@right)}
        </div>
      </div>
    </nav>
    """
  end

  defp show_navbar(js \\ %JS{}) do
    js
    |> JS.toggle(to: "#navbar-left", display: "flex")
    |> JS.toggle(to: "#navbar-right", display: "flex")
    |> JS.hide(to: "#sidebar")
  end

  attr :active, :boolean, default: false
  attr :rest, :global, include: ~w(navigate href target method)

  slot :inner_block

  def navbar_link(assigns) do
    ~H"""
    <.link
      {@rest}
      class={[
        @active && "text-slate-900 border-b-2 border-brand",
        not @active && "text-slate-500 hover:text-slate-900"
      ]}
    >
      {render_slot(@inner_block)}
    </.link>
    """
  end

  slot :content
  slot :bottom

  def sidebar(assigns) do
    ~H"""
    <nav
      class="h-screen hidden fixed top-0 z-10 flex-col justify-between w-16 bg-slate-900 shadow-md md:inline-flex"
      id="sidebar"
      aria-label="sidebar"
    >
      <div class="py-4 px-3">
        <ul class="space-y-8 text-md text-center text-slate-500 text-xl">
          {render_slot(@content)}
        </ul>
      </div>

      <div :if={@bottom != []} class="py-4 px-3">
        <ul class="space-y-8 text-md text-center text-slate-500 text-xl">
          {render_slot(@bottom)}
        </ul>
      </div>
    </nav>
    """
  end

  defp show_sidebar(js \\ %JS{}) do
    JS.toggle(js, to: "#sidebar", display: "flex")
  end

  attr :active, :boolean, default: false
  attr :rest, :global, include: ~w(navigate href target method)

  slot :inner_block

  def sidebar_link(assigns) do
    ~H"""
    <li>
      <.link
        {@rest}
        class={[
          @active && "text-brand",
          not @active && "text-slate-500 hover:text-white"
        ]}
      >
        {render_slot(@inner_block)}
      </.link>
    </li>
    """
  end

  slot :inner_block
  slot :actions

  def tab_bar(assigns) do
    ~H"""
    <div class="flex flex-row w-full h-full">
      <ul class="h-full w-full flex-grow overflow-y-auto flex flex-row gap-4 font-medium">
        {render_slot(@inner_block)}
      </ul>
      <span :if={@actions != []} class="border-l border-stone-300 my-2 mx-4" />
      <ul :if={@actions != []} class="h-full flex flex-row gap-4 font-medium ">
        {render_slot(@actions)}
      </ul>
    </div>
    """
  end

  attr :active, :boolean, default: false
  attr :rest, :global, include: ~w(navigate patch href replace)

  slot :inner_block, required: true

  def tab(assigns) do
    ~H"""
    <li class={[active_style(@active), " border-b-2 whitespace-nowrap flex items-center"]}>
      <.link {@rest}>
        {render_slot(@inner_block)}
      </.link>
    </li>
    """
  end

  defp active_style(true), do: "text-slate-900 border-brand"
  defp active_style(_), do: "text-stone-500 border-transparent hover:text-slate-900"
end
