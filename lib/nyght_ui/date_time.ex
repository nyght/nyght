defmodule NyghtUi.DateTime do
  @moduledoc """
  This module contains HEEX components to display localized date and time
  """
  use Phoenix.Component

  attr :class, :string, default: nil
  attr :time, :any, required: true
  attr :format, :atom, default: :short, values: [:short, :medium, :long, :full]
  attr :timezone, :string
  attr :shift, :boolean, default: false

  def time(assigns) do
    assigns = assign_datetime(assigns, assigns.time)

    ~H"""
    <time datetime={@datetime} class={@class}>
      {@datetime}
    </time>
    """
  end

  attr :class, :string, default: nil
  attr :from, :any, required: true
  attr :to, :any, required: true
  attr :format, :atom, default: :short, values: [:short, :medium, :long, :full]
  attr :timezone, :string

  def time_interval(assigns) do
    assigns = assign_datetime_range(assigns)

    ~H"""
    <span class={@class}>
      {Cldr.DateTime.Interval.to_string!(@from, @to, NyghtWeb.Cldr, format: "H:mm - H:mm")}
    </span>
    """
  end

  attr :class, :string, default: nil
  attr :date, :any, required: true
  attr :format, :atom, default: :short, values: [:short, :medium, :long, :full]
  attr :timezone, :string
  attr :shift, :boolean, default: false

  def date(assigns) do
    assigns = assign_datetime(assigns, assigns.date)

    ~H"""
    <time datetime={@datetime} class={@class}>
      {Cldr.Date.to_string!(@datetime, NyghtWeb.Cldr, format: @format)}
    </time>
    """
  end

  attr :class, :string, default: nil
  attr :datetime, :any, required: true
  attr :format, :atom, default: :short, values: [:short, :medium, :long, :full]
  attr :timezone, :string
  attr :shift, :boolean, default: false

  def datetime(assigns) do
    assigns = assign_datetime(assigns, assigns.datetime)

    ~H"""
    <time datetime={@datetime} class={@class}>
      {Cldr.DateTime.to_string!(@datetime, NyghtWeb.Cldr, format: @format)}
    </time>
    """
  end

  defp assign_datetime(assigns, datetime) do
    assign(assigns, datetime: convert_datetime(assigns, datetime))
  end

  defp assign_datetime_range(assigns) do
    assigns
    |> update(:from, fn from -> convert_datetime(assigns, from) end)
    |> update(:to, fn to -> convert_datetime(assigns, to) end)
  end

  defp convert_datetime(%{shift: true, timezone: timezone}, %NaiveDateTime{} = datetime) do
    case DateTime.from_naive(datetime, "Etc/UTC") do
      {:ok, converted} ->
        DateTime.shift_zone!(converted, timezone)

      {:ambiguous, _, converted} ->
        DateTime.shift_zone!(converted, timezone)
    end
  end

  defp convert_datetime(%{timezone: timezone}, %NaiveDateTime{} = datetime) do
    case DateTime.from_naive(datetime, timezone) do
      {:ok, converted} ->
        converted

      {:ambiguous, _, converted} ->
        converted
    end
  end

  defp convert_datetime(%{shift: true, timezone: timezone}, %DateTime{} = datetime) do
    DateTime.shift_zone!(datetime, timezone)
  end

  defp convert_datetime(_assigns, datetime), do: datetime
end
