defmodule NyghtUi.Pill do
  @moduledoc false

  use Phoenix.Component

  import NyghtUi.Icon, only: [icon: 1]

  @doc """
  Renders a pill.

  ## Example

      <.pill kind={:success} text="Success pill" />
      <.pill kind={:warning} text="Pill with icon" icon="ti-warning" />
      <.pill kind={:danger}>
        Pill with stuff inside
      </.pill>

  """
  attr :kind, :atom, values: [:info, :success, :danger, :warning], default: :info
  attr :text, :string, default: nil
  attr :icon, :string, default: nil
  attr :class, :string, default: nil
  slot :inner_block

  def pill(assigns) do
    assigns = assign(assigns, :has_text, not is_nil(assigns.text) or assigns.inner_block != [])

    ~H"""
    <span class={[
      "inline-flex items-center rounded-full text-xs font-medium mr-2 whitespace-nowrap align-baseline",
      @has_text && "px-2.5 py-0.5",
      not @has_text && "px-0.5 py-0.5",
      pill_kind(@kind),
      @class
    ]}>
      <.icon
        :if={not is_nil(@icon)}
        name={@icon}
        class={["w-3.5 h-3.5", if(not is_nil(@text) or @inner_block != [], do: "mr-1.5")]}
      />
      {@text || render_slot(@inner_block)}
    </span>
    """
  end

  @doc """
  Renders a colored dot.

  ## Example

      <.dot kind={:success} />
      <.dot kind={:warning} />
      <.dot kind={:danger} />

  """
  attr :kind, :atom, values: [:info, :success, :danger, :warning], default: :success

  def dot(assigns) do
    ~H"""
    <span class={[
      "w-2 h-2 rounded-full inline-block",
      dot_kind(@kind)
    ]} />
    """
  end

  defp pill_kind(:success), do: "bg-emerald-200 text-emerald-900"
  defp pill_kind(:info), do: "bg-sky-200 text-sky-900"
  defp pill_kind(:danger), do: "bg-rose-200 text-rose-900"
  defp pill_kind(:warning), do: "bg-amber-200 text-amber-900"

  defp dot_kind(:success), do: "bg-emerald-500"
  defp dot_kind(:info), do: "bg-sky-500"
  defp dot_kind(:danger), do: "bg-rose-500"
  defp dot_kind(:warning), do: "bg-amber-500"
end
