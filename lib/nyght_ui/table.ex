defmodule NyghtUi.Table do
  @moduledoc """
  This module contains table components.
  """
  use Phoenix.Component

  @doc ~S"""
  Renders a table with generic styling.

  ## Examples

      <.table rows={@users}>
        <:col :let={user} label="id"><%= user.id %></:col>
        <:col :let={user} label="username"><%= user.username %></:col>
      </.table>
  """
  attr :id, :string, required: true
  attr :rows, :list, required: true
  attr :row_id, :any, default: nil, doc: "the function for generating the row id"
  attr :row_click, :any, default: nil, doc: "the function for handling phx-click on each row"
  attr :headerless, :boolean, default: false

  attr :row_item, :any,
    default: &Function.identity/1,
    doc: "the function for mapping each row before calling the :col and :action slots"

  attr :rest, :global

  slot :col, required: true do
    attr :label, :string
  end

  slot :footer, doc: "the slot for showing data in the last table row"
  slot :action, doc: "the slot for showing user actions in the last table column"

  def table(assigns) do
    assigns =
      with %{rows: %Phoenix.LiveView.LiveStream{}} <- assigns do
        assign(assigns, row_id: assigns.row_id || fn {id, _item} -> id end)
      end

    ~H"""
    <div id={@id} class="overflow-x-auto relative px-4 sm:px-0">
      <table class="w-full">
        <thead
          :if={not @headerless}
          class="text-left uppercase text-xs text-stone-500 leading-6 border-b border-stone-300"
        >
          <tr>
            <th :for={col <- @col} class="p-0 pl-2 pb-4 pr-6 font-normal">{col[:label]}</th>
            <th :if={@action != []} class="p-0 pb-4"><span class="sr-only">Actions</span></th>
          </tr>
        </thead>
        <tbody
          id={"#{@id}-body"}
          class="divide-y divide-stone-200"
          phx-update={match?(%Phoenix.LiveView.LiveStream{}, @rows) && "stream"}
          {@rest}
        >
          <tr :for={row <- @rows} id={@row_id && @row_id.(row)} class="group hover:bg-stone-50">
            <td
              :for={col <- @col}
              phx-click={@row_click && @row_click.(row)}
              class={["relative p-0", @row_click && "hover:cursor-pointer"]}
            >
              <div class="block py-4 pr-6 pl-2">
                <span class="relative">
                  {render_slot(col, @row_item.(row))}
                </span>
              </div>
            </td>
            <td :if={@action != []} class="pr-2 w-14 whitespace-nowrap">
              <span :for={action <- @action}>
                {render_slot(action, @row_item.(row))}
              </span>
            </td>
          </tr>
        </tbody>
        <tfoot>
          {render_slot(@footer)}
        </tfoot>
      </table>
    </div>
    """
  end
end
