defmodule NyghtUi.FlopConfig do
  @moduledoc false
  use Phoenix.Component

  import NyghtUi.Icon

  @doc """
  Returns the configuration for `Flop.Phoenix.table/1`.
  """
  @spec table_opts :: keyword()
  def table_opts do
    [
      container: true,
      container_attrs: [
        class:
          "lg:border lg:border-stone-300 lg:shadow-xs lg:rounded-md lg:bg-stone-50 lg:bg-white"
      ],
      table_attrs: [
        class: "w-full flex flex-row flex-no-wrap lg:table"
      ],
      thead_attrs: [
        class:
          "text-left text-sm text-stone-500 leading-6 hidden lg:table-header-group border-b border-stone-300"
      ],
      thead_th_attrs: [
        class:
          "py-2 px-4 font-normal whitespace-nowrap bg-stone-50 lg:first:rounded-l-md lg:last:rounded-r-md"
      ],
      tbody_attrs: [
        class: "flex flex-col lg:table-row-group w-full lg:divide-y lg:divide-stone-100 "
      ],
      tbody_tr_attrs: [
        class:
          "lg:hover:bg-stone-50 flex flex-col flex-no-wrap lg:table-row border lg:border-0 border-stone-300 mb-2 rounded-md shadow-xs lg:shadow-none"
      ],
      tbody_td_attrs: [
        class: "py-3 px-4 flex-1 lg:flex-none"
      ],
      symbol_unsorted: unsorted_icon(),
      symbol_asc: asc_icon(),
      symbol_desc: desc_icon()
    ]
  end

  defp asc_icon do
    assigns = %{}

    ~H"""
    <.icon name="ti-sort-ascending" class="ml-2 w-4 h-4" />
    """
  end

  defp desc_icon do
    assigns = %{}

    ~H"""
    <.icon name="ti-sort-descending" class="ml-2 w-4 h-4" />
    """
  end

  defp unsorted_icon do
    assigns = %{}

    ~H"""
    <.icon name="ti-arrows-sort" class="ml-2 w-4 h-4" />
    """
  end

  @doc """
  Returns the configuration for `Flop.Phoenix.pagination/1`.
  """
  @spec pagination_opts :: keyword()
  def pagination_opts do
    [
      current_link_attrs: [
        class: "flex justify-center items-center w-6 h-6 bg-brand rounded-full text-white",
        aria: [current: "page"]
      ],
      disabled_class: "hidden",
      previous_link_attrs: [
        aria: [label: "Go to previous page"],
        class: "order-first ti-arrow-left bg-brand"
      ],
      previous_link_content: nil,
      next_link_attrs: [
        aria: [label: "Go to next page"],
        class: "order-last ti-arrow-right bg-brand"
      ],
      next_link_content: nil,
      page_links: {:ellipsis, 5},
      pagination_link_aria_label: &"Go to page #{&1}",
      pagination_link_attrs: [class: "flex justify-center items-center w-6 h-6"],
      pagination_list_attrs: [class: "inline-flex space-x-4"],
      wrapper_attrs: [
        class: "inline-flex space-x-4 p-2 items-center",
        role: "navigation",
        aria: [label: "pagination"]
      ]
    ]
  end
end
