defmodule NyghtUi.Card do
  @moduledoc """
  This module provides card components.
  """
  use Phoenix.Component

  @doc """
  Renders a card
  """
  attr :class, :string, default: nil

  slot :image
  slot :inner_block, required: true

  def card(assigns) do
    ~H"""
    <div class={[
      "w-full bg-white rounded-md shadow-xs h-content flex flex-col border-stone-300 border",
      @class
    ]}>
      <div :if={@image != []} class="relative rounded-md overflow-hidden w-full aspect-a4 ">
        {render_slot(@image)}
      </div>
      <div class="px-4 py-2 flex flex-col">
        {render_slot(@inner_block)}
      </div>
    </div>
    """
  end
end
