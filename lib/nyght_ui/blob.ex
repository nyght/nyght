defmodule NyghtUi.Blob do
  @moduledoc """
  This module contains the tools needed to generate SVG blobs.
  """

  alias NyghtUi.Path

  @doc """
  Creates the basic points of the blob.
  """
  def create_points(center, blob_size, min_growth, edge_count) do
    outer_rad = blob_size / 2
    inner_rad = min_growth * (outer_rad / 10)

    slice_angle = 360 / edge_count

    for edge <- 0..(edge_count - 1) do
      angle = edge * slice_angle

      radius = random(inner_rad, outer_rad)
      to_point(center, radius, angle)
    end
  end

  @doc """
  Creates the Bezier control points between the basic points.
  """
  def create_control_points(points) do
    points = points ++ Enum.take(points, 1)

    points
    |> Enum.chunk_every(2, 1, :discard)
    |> Enum.map(fn [a | [b]] -> %{a: a, b: b, m: mid_point(a, b)} end)
  end

  @doc """
  Scales the blob by a given factor.
  """
  def scale({center_w, center_h}, points, factor) do
    center = make_point(center_w, center_h)

    Enum.map(points, fn %{a: a, b: b, m: m} ->
      %{
        a: scale_vec_from(a, factor, center),
        b: scale_vec_from(b, factor, center),
        m: scale_vec_from(m, factor, center)
      }
    end)
  end

  @doc """
  Creates the SVG path to render.
  """
  def make_path(points) do
    path =
      points
      |> List.last(points)
      |> Map.fetch!(:m)
      |> Path.move_to()

    Enum.reduce(points, path, fn %{a: a, m: ctrl}, acc ->
      Path.quadra(acc, a, ctrl)
    end)
  end

  @doc """
  Generates debug lines from the center of the blob to each base point.

  This directly generates renderable SVG. This can also be used as decoration since it's looking quite cool.
  """
  def debug_base_points(points, cx, cy) do
    Enum.reduce(points, "", fn pt, acc ->
      acc <> ~s[
        <line x1="#{cx}" y1="#{cy}" x2="#{pt.x}" y2="#{pt.y}" stroke="pink" stroke-dasharray="4" />
      ]
    end)
  end

  @doc """
  Generates a radom number between the given min and max.
  """
  def random(min, max), do: min + :rand.uniform() * (max - min)

  defp to_rad(deg), do: deg * (:math.pi() / 180)

  defp make_point(x, y), do: %{x: Float.round(x, 2), y: Float.round(y, 2)}

  defp mid_point(a, b), do: make_point((a.x + b.x) / 2, (a.y + b.y) / 2)

  defp scale_vec_from(%{x: x, y: y}, factor, %{x: cx, y: cy}) do
    nx = (x - cx) * factor + cx
    ny = (y - cy) * factor + cy

    make_point(nx, ny)
  end

  defp to_point({origin_x, origin_y}, radius, angle) do
    x = origin_x + radius * :math.cos(to_rad(angle))
    y = origin_y + radius * :math.sin(to_rad(angle))

    make_point(x, y)
  end
end
