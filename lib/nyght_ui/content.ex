defmodule NyghtUi.Content do
  @moduledoc """
  This module provides miscellaneous content formatting components.
  """
  use Phoenix.Component

  alias NyghtUi.Icon

  @doc """
  Renders a header with title.
  """
  attr :class, :string, default: nil
  attr :level, :string, values: ["h1", "h2", "h3", "h4", "h5"], default: "h1"
  attr :rest, :global

  slot :inner_block, required: true
  slot :subtitle
  slot :actions

  def header(assigns) do
    ~H"""
    <header class={[
      (@actions != [] and @level == "h1") &&
        "flex flex-col lg:flex-row lg:items-center lg:gap-6",
      (@actions != [] and @level != "h1") &&
        "flex flex-row items-center",
      @class
    ]}>
      <div {@rest} class="grow">
        <.dynamic_tag tag_name={@level}>
          {render_slot(@inner_block)}
        </.dynamic_tag>
        <div :if={@subtitle != []} class="text-sm leading-6 text-stone-600">
          {render_slot(@subtitle)}
        </div>
      </div>
      <div class="flex-none my-4 lg:my-0">
        {render_slot(@actions)}
      </div>
    </header>
    """
  end

  attr :icon, :string, default: "ti-mood-confuzed"
  attr :class, :string, default: nil

  slot :title
  slot :subtitle
  slot :inner_block

  def empty(assigns) do
    ~H"""
    <div class={["h-full w-full pt-28 px-4 flex items-center justify-center text-center", @class]}>
      <div class="flex flex-col items-center">
        <Icon.icon name={@icon} class="h-14 w-14 bg-stone-600" />
        <h6 class="mt-4">
          {render_slot(@title)}
        </h6>
        <p class="mt text-sm leading-6 text-stone-600">
          {render_slot(@subtitle)}
        </p>
        <div class="mt-6">
          {render_slot(@inner_block)}
        </div>
      </div>
    </div>
    """
  end

  attr :text, :string, default: nil
  attr :class, :string, default: nil
  attr :rest, :global

  def hr(assigns) do
    ~H"""
    <div
      class={[
        "py-3 flex items-center text-sm text-stone-800 before:flex-1 before:border-t before:border-stone-300 before:me-6 after:flex-1 after:border-t after:border-stone-300 after:ms-6",
        @class
      ]}
      {@rest}
    >
      {@text}
    </div>
    """
  end

  @doc """
  Renders markdown content.

  ## Example

      <.markdown content="**Hello**" />
  """

  attr :content, :string, required: true, doc: "the markdown content to render"
  attr :class, :string, default: nil

  def markdown(assigns) do
    ~H"""
    <div class={["prose prose-sm max-w-xl", @class]}>
      {@content |> format()}
    </div>
    """
  end

  defp format(content) do
    content
    |> Earmark.as_html!()
    |> HtmlSanitizeEx.markdown_html()
    |> Phoenix.HTML.raw()
  end

  @doc """
  Renders a label before the content.
  """
  attr :label, :string, doc: "the label text"
  attr :class, :string, default: nil

  slot :inner_block

  def labelled(assigns) do
    ~H"""
    <div class={@class}>
      <div class="text-sm text-brand">{@label}:</div>
      <div>{render_slot(@inner_block)}</div>
    </div>
    """
  end

  attr :class, :string, default: nil

  slot :item do
    attr :title, :string
  end

  def list(assigns) do
    ~H"""
    <dl class={["space-y-2", @class]}>
      <div :for={i <- @item} class="py">
        <dt class="text-sm text-stone-500 leading-3">{i.title}</dt>
        <dd>{render_slot(i)}</dd>
      </div>
    </dl>
    """
  end

  attr :class, :string, default: nil

  slot :inner_block

  def timeline(assigns) do
    ~H"""
    <ol class={["relative border-l border-slate-200", @class]}>
      {render_slot(@inner_block)}
    </ol>
    """
  end

  slot :title, required: true
  slot :time
  slot :inner_block

  def timeline_item(assigns) do
    ~H"""
    <li class="mb-10 ml-6">
      <span class="flex absolute -left-3 justify-center items-center w-6 h-6 bg-brand rounded-full ring-8 ring-white">
        <Icon.icon name="ti-calendar-event" class="text-white" />
      </span>
      <h3 class="flex items-center mb-1 text-lg font-semibold text-slate-900">
        {render_slot(@title)}
      </h3>
      <p class="block mb-2 text-sm font-normal leading-none text-slate-400">
        {render_slot(@time)}
      </p>
      <p class="mb-4 text-base font-normal text-slate-500">{render_slot(@inner_block)}</p>
    </li>
    """
  end

  @doc """
  Renders a content with an icon on its left.
  """
  attr :name, :string, required: true, doc: "the name of the icon"
  attr :class, :string, default: nil

  slot :inner_block

  def icon_text(assigns) do
    ~H"""
    <div class={["flex flex-row gap-2 items-baseline", @class]}>
      <Icon.icon name={@name} />
      <div>
        {render_slot(@inner_block)}
      </div>
    </div>
    """
  end

  attr :items, :list, required: true
  attr :separator, :string, default: ", "

  def joined_text(assigns) do
    content =
      assigns.items
      |> Enum.filter(&(not is_nil(&1)))
      |> Enum.join(assigns.separator)

    assigns = assign(assigns, content: content)

    ~H"""
    <span>{@content}</span>
    """
  end

  slot :logo
  slot :copyright

  slot :col, required: true do
    attr :label, :string
  end

  def footer(assigns) do
    ~H"""
    <footer class="p-4 bg-slate-900 sm:p-6 mt-6 md:mt-0">
      <div class="md:flex md:justify-between container mx-auto">
        <div class="mb-6 md:mb-0">
          {render_slot(@logo)}
        </div>
        <div class="grid grid-cols-2 gap-8">
          <div :for={col <- @col}>
            <h2 class="mb-6 text-sm font-semibold uppercase text-white">
              {col.label}
            </h2>
            <ul class="text-slate-400 space-y-2">
              {render_slot(col)}
            </ul>
          </div>
        </div>
      </div>
      <hr class="my-6 sm:mx-auto border-slate-700 lg:my-8 container mx-auto" />
      <span class="block text-sm text-center text-slate-400">
        {render_slot(@copyright)}
      </span>
    </footer>
    """
  end

  def brand_icon_from_url(url) do
    cond do
      String.match?(url, ~r/.*spotify.*/) ->
        "ti-brand-spotify"

      String.match?(url, ~r/.*youtube.*/) ->
        "ti-brand-youtube"

      String.match?(url, ~r/.*facebook.*/) ->
        "ti-brand-facebook"

      String.match?(url, ~r/.*instagram.*/) ->
        "ti-brand-instagram"

      String.match?(url, ~r/.*soundcloud.*/) ->
        "ti-brand-soundcloud"

      true ->
        "ti-link"
    end
  end
end
