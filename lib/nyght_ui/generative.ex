defmodule NyghtUi.Generative do
  @moduledoc false
  use Phoenix.Component

  import Phoenix.HTML

  alias NyghtUi.Blob

  @doc """
  Renders a random poster.

  The poster will have a A4 aspect ratio, and the generated art
  will take the given id as the seed.
  """
  attr :id, :string
  attr :width, :integer, default: 500
  attr :class, :string, default: nil

  def poster(assigns) do
    hash =
      :crypto.hash(:sha, assigns.id)
      |> :crypto.bytes_to_integer()

    height = assigns.width * 1.4142
    center = {assigns.width / 2, height / 2}

    {path, dbg_path} = generate_art(hash, center)

    assigns =
      assign(assigns, path: path, dbg_path: dbg_path, hash: hash, height: height)

    ~H"""
    <svg viewBox={"0 0 #{@width} #{@height}"} preserveAspectRatio="none" class={@class}>
      <defs>
        <radialGradient id="bg-gradient" cx="100%" cy="10%" r="75%">
          <stop offset="10%" stop-color="#334155" />
          <stop offset="95%" stop-color="#0f172a" />
        </radialGradient>
        <filter
          id="noise-filter"
          x="-20%"
          y="-20%"
          width="140%"
          height="140%"
          filterUnits="objectBoundingBox"
          primitiveUnits="userSpaceOnUse"
          color-interpolation-filters="linearRGB"
        >
          <feTurbulence
            type="fractalNoise"
            baseFrequency="0.102"
            numOctaves="4"
            seed={@hash}
            stitchTiles="stitch"
            x="0%"
            y="0%"
            width="100%"
            height="100%"
            result="turbulence"
          >
          </feTurbulence>
          <feSpecularLighting
            surfaceScale="15"
            specularConstant="0.75"
            specularExponent="20"
            lighting-color="#334155"
            x="0%"
            y="0%"
            width="100%"
            height="100%"
            in="turbulence"
            result="specularLighting"
          >
            <feDistantLight azimuth="3" elevation="100"></feDistantLight>
          </feSpecularLighting>
        </filter>
      </defs>
      <rect width={@width} height={@height} fill="url('#bg-gradient')" />
      <g>
        {raw(@dbg_path)}
        {raw(@path)}
      </g>

      <rect width={@width} height={@height} fill="#7957a8" filter="url(#noise-filter)"></rect>
    </svg>
    """
  end

  @doc """
  Renders a random hero background image.
  """
  attr :class, :string, default: nil

  def hero_image(assigns) do
    hash =
      :crypto.hash(:sha, DateTime.utc_now() |> DateTime.to_unix() |> to_string())
      |> :crypto.bytes_to_integer()

    _ = :rand.seed(:exsss, hash)

    width = 1000
    height = 600
    center = {3 * (width / 4), height / 2}
    {path, dbg_path} = generate_art(hash, center)

    assigns = assign(assigns, path: path, dbg: dbg_path, hash: hash)

    ~H"""
    <svg viewBox="0 0 300 600" class={@class}>
      <defs>
        <radialGradient id="bg-gradient" cx="100%" cy="10%" r="75%">
          <stop offset="10%" stop-color="#334155" />
          <stop offset="95%" stop-color="#0f172a" />
        </radialGradient>
        <filter
          id="noise-filter"
          x="-20%"
          y="-20%"
          width="140%"
          height="140%"
          filterUnits="objectBoundingBox"
          primitiveUnits="userSpaceOnUse"
          color-interpolation-filters="linearRGB"
        >
          <feTurbulence
            type="fractalNoise"
            baseFrequency="0.102"
            numOctaves="4"
            seed={@hash}
            stitchTiles="stitch"
            x="0%"
            y="0%"
            width="100%"
            height="100%"
            result="turbulence"
          >
          </feTurbulence>
          <feSpecularLighting
            surfaceScale="15"
            specularConstant="0.75"
            specularExponent="20"
            lighting-color="#334155"
            x="0%"
            y="0%"
            width="100%"
            height="100%"
            in="turbulence"
            result="specularLighting"
          >
            <feDistantLight azimuth="3" elevation="100"></feDistantLight>
          </feSpecularLighting>
        </filter>
      </defs>
      <g>
        {raw(@dbg)}
        {raw(@path)}
      </g>
    </svg>
    """
  end

  defp generate_art(hash, {c_x, c_y} = center) do
    _ = :rand.seed(:exsss, hash)

    pts = Blob.create_points(center, 400, 0.5, 16)
    pts_q = Blob.create_control_points(pts)

    max_paths = round(Blob.random(7, 22))
    scaling_factory = Blob.random(0.4, 0.8) * -1

    path =
      Enum.reduce(2..max_paths, "", fn i, acc ->
        p =
          Blob.scale(center, pts_q, i * scaling_factory)
          |> Blob.make_path()

        acc <> ~s[<path d="#{p}" fill="none" stroke="#FF595E" stroke-width="2px"/>]
      end)

    dbg_path =
      if :rand.uniform() > 0.5,
        do: Blob.debug_base_points(pts, c_x, c_y),
        else: ""

    {path, dbg_path}
  end
end
