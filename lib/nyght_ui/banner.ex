defmodule NyghtUi.Banner do
  @moduledoc """
  This module provides alert components.
  """
  use Phoenix.Component

  alias NyghtUi.Icon
  alias Phoenix.LiveView.JS

  attr :id, :string, required: true

  slot :inner_block

  def banner(assigns) do
    ~H"""
    <div
      id={@id}
      tabindex="-1"
      class="flex gap-8 justify-between items-start py-3 px-4 bg-rose-50 p-3 text-rose-900 shadow-md border-b border-t border-bittwersweet-500 sm:items-center"
    >
      {render_slot(@inner_block)}
      <button type="button" class="group p-2" aria-label="Close" phx-click={close_banner(@id)}>
        <Icon.icon name="ti-x" class="h-5 w-5 stroke-current opacity-40 group-hover:opacity-70" />
      </button>
    </div>
    """
  end

  defp close_banner(id, js \\ %JS{}) do
    JS.hide(js, to: "##{id}")
  end
end
