defmodule NyghtUi.Button do
  @moduledoc false
  use Phoenix.Component

  alias NyghtUi.Icon

  @doc """
  Renders a button.

  ## Example

      <.button>Hello</.button>
      <.button style={:secondary}>World</.button>
  """
  attr :style, :atom,
    values: [:primary, :secondary, :tertiary, :danger],
    default: :primary,
    doc: "the button's style"

  attr :size, :atom,
    values: [:small, :medium, :large],
    default: :medium,
    doc: "the button's size"

  attr :type, :atom,
    values: [:button, :submit],
    default: :button,
    doc: "the button type to render"

  attr :icon, :string, default: nil
  attr :class, :string, default: ""
  attr :rest, :global, include: ~w(disabled)

  slot :inner_block, required: true

  def button(assigns) do
    ~H"""
    <button type={@type} class={[button_style(@style), button_size(@size, @style), @class]} {@rest}>
      <Icon.icon :if={not is_nil(@icon)} name={@icon} class="mr-2 flex-none" />
      <span class="truncate">{render_slot(@inner_block)}</span>
    </button>
    """
  end

  @doc """
  Renders a icon button.

  ## Example

      <.icon_button name="ti-copy"/>
  """
  attr :name, :string,
    required: true,
    doc: "the name of the Tabler icon to render, must start with `ti-`"

  attr :type, :string,
    default: "button",
    values: ["button", "link"],
    doc: "The underlying HTML element"

  attr :class, :string, default: ""
  attr :icon_class, :string, default: ""
  attr :rest, :global, include: ~w(disabled href target navigate patch href replace)

  def icon_button(%{type: "button"} = assigns) do
    ~H"""
    <button
      class={[
        "rounded-md text-brand p-2 hover:bg-stone-100 disabled:opacity-30 disabled:cursor-not-allowed",
        @class
      ]}
      {@rest}
    >
      <Icon.icon name={@name} class={@icon_class} />
    </button>
    """
  end

  def icon_button(%{type: "link"} = assigns) do
    ~H"""
    <.link
      class={[
        "rounded-md text-brand p-2 hover:bg-stone-100 disabled:opacity-30 disabled:cursor-not-allowed",
        @class
      ]}
      {@rest}
    >
      <Icon.icon name={@name} class={@icon_class} />
    </.link>
    """
  end

  @doc """
  Renders a link.

  ## Example

      <.a navigate={~p"/hello"}>Hello</.a>
      <.a style={:secondary}>World</.a>
  """
  attr :style, :atom,
    values: [:primary, :secondary, :tertiary, :danger],
    default: :tertiary,
    doc: "the anchor's style"

  attr :size, :atom,
    values: [:small, :medium, :large],
    default: :medium,
    doc: "the anchors's size"

  attr :icon, :string, default: nil
  attr :disabled, :boolean, default: false
  attr :class, :string, default: ""
  attr :rest, :global, include: ~w(navigate patch href replace method)

  slot :inner_block, required: true

  def a(assigns) do
    ~H"""
    <.link class={[button_style(@style), button_size(@size, @style), @class]} {@rest}>
      <Icon.icon :if={not is_nil(@icon)} name={@icon} class="mr-2 flex-none" />
      <span class="truncate">{render_slot(@inner_block)}</span>
    </.link>
    """
  end

  defp button_style(:primary),
    do:
      "flex flex-row items-center justify-center w-full md:w-fit border rounded-md border-brand bg-brand text-white truncate hover:border-brand/50 hover:text-white/90 hover:shadow-md hover:shadow-brand/20 disabled:opacity-40 disabled:cursor-not-allowed transition-all"

  defp button_style(:danger),
    do:
      "flex flex-row items-center justify-center w-full md:w-fit border-2 rounded-md border-rose-600 bg-rose-600 text-white truncate hover:shadow-xs hover:shadow-rose-600/20 disabled:opacity-40 disabled:cursor-not-allowed transition-all"

  defp button_style(:secondary),
    do:
      "flex flex-row items-center justify-center w-full md:w-fit border rounded-md border-slate-900 text-slate-900 truncate hover:border-brand/50 hover:text-brand/90 hover:shadow-xs hover:shadow-brand/20 disabled:opacity-40 disabled:cursor-not-allowed transition-all"

  defp button_style(:tertiary), do: "text-brand hover:underline"

  defp button_size(:small, :tertiary), do: "h-min text-sm"
  defp button_size(:medium, :tertiary), do: "h-min"
  defp button_size(:large, :tertiary), do: "h-min text-xl"
  defp button_size(:small, _), do: "h-min px-2 py-1 text-sm"
  defp button_size(:medium, _), do: "h-min px-6 py-2"
  defp button_size(:large, _), do: "h-min px-8 py-3 text-xl"
end
