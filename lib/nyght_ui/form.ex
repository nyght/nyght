defmodule NyghtUi.Form do
  @moduledoc """
  This module contains different form helpers
  """
  use Phoenix.Component

  import NyghtUi.Dropdown
  use Gettext, backend: NyghtWeb.Gettext

  alias NyghtUi.{Button, Content, Icon, Pill, Progress}
  alias Phoenix.HTML.{Form, FormField}
  alias Phoenix.LiveView.{UploadConfig, UploadEntry}

  @doc """
  Renders a simple form.

  ## Examples

      <.simple_form for={@form} phx-change="validate" phx-submit="save">
        <.input field={@form[:email]} label="Email"/>
        <.input field={@form[:username]} label="Username" />
        <:actions>
          <.button>Save</.button>
        </:actions>
      </.simple_form>
  """
  attr :for, :any, required: true, doc: "the datastructure for the form"
  attr :as, :any, default: nil, doc: "the server side parameter to collect all input under"
  attr :class, :string, default: ""

  attr :rest, :global,
    include: ~w(autocomplete name rel action enctype method novalidate target multipart),
    doc: "the arbitrary HTML attributes to apply to the form tag"

  slot :inner_block, required: true

  slot :actions, doc: "the slot for form actions, such as a submit button" do
    attr :sticky, :boolean
  end

  def simple_form(assigns) do
    ~H"""
    <.form :let={f} for={@for} as={@as} class={["space-y-8", @class]} {@rest}>
      {render_slot(@inner_block, f)}
      <div
        :for={action <- @actions}
        class={[
          "mt-2 p-4 flex items-center justify-end gap-6",
          Map.get(action, :sticky, false) &&
            "sticky shadow-top border-t border-stone-300  bottom-0 bg-white"
        ]}
      >
        {render_slot(action, f)}
      </div>
    </.form>
    """
  end

  attr :borderless, :boolean, default: false
  attr :id, :any, default: nil

  slot :title, required: true, doc: "the title of the section"
  slot :description, doc: "the section description"
  slot :inner_block, required: true

  def form_section(assigns) do
    ~H"""
    <section
      id={@id}
      class={[
        "w-full flex flex-col md:flex-row",
        not @borderless && "border-t border-stone-200"
      ]}
    >
      <div class="md:w-1/4 p-4">
        <h5>{render_slot(@title)}</h5>
        <div :if={@description != []} class="text-sm leading-6 text-stone-600">
          {render_slot(@description)}
        </div>
      </div>
      <div class="md:w-3/4 p-4">
        {render_slot(@inner_block)}
      </div>
    </section>
    """
  end

  @doc """
  Renders an input with label and error messages.

  A `Phoenix.HTML.FormField` may be passed as argument,
  which is used to retrieve the input name, id, and values.
  Otherwise all attributes may be passed explicitly.

  ## Types

  This function accepts all HTML input types, considering that:

    * You may also set `type="select"` to render a `<select>` tag

    * `type="checkbox"` is used exclusively to render boolean values

    * For live file uploads, see `Phoenix.Component.live_file_input/1`

  See https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input
  for more information.

  ## Examples

      <.input field={@form[:email]} type="email" />
      <.input name="my-input" errors={["oh no!"]} />
  """
  attr :id, :any, default: nil
  attr :name, :any
  attr :label, :string
  attr :value, :any

  attr :type, :string,
    default: "text",
    values: ~w(checkbox color date datetime-local email file hidden month number password
               range radio search select tel text textarea time url week markdown
               multiselect checkbox_group radio_detailed)

  attr :field, Phoenix.HTML.FormField,
    doc: "a form field struct retrieved from the form, for example: @form[:email]"

  attr :required, :boolean, default: false
  attr :errors, :list, default: []
  attr :checked, :boolean, doc: "the checked flag for checkbox inputs"
  attr :prompt, :string, default: nil, doc: "the prompt for select inputs"
  attr :options, :list, doc: "the options to pass to Phoenix.HTML.Form.options_for_select/2"
  attr :multiple, :boolean, default: false, doc: "the multiple flag for select inputs"
  attr :class, :string, default: "w-full"
  attr :no_label, :boolean, default: false
  attr :selected, :any

  attr :rest, :global,
    include: ~w(accept autocomplete capture cols disabled form list max maxlength min minlength
                multiple pattern placeholder readonly required rows size step)

  slot :option, doc: "the slot for select input options" do
    attr :value, :any
    attr :selected, :boolean
  end

  def input(%{field: %Phoenix.HTML.FormField{} = field} = assigns) do
    errors = if Phoenix.Component.used_input?(field), do: field.errors, else: []

    assigns
    |> assign(field: nil, id: assigns.id || field.id)
    |> assign(:errors, Enum.map(errors, &translate_error(&1)))
    |> assign_new(:name, fn -> if assigns.multiple, do: field.name <> "[]", else: field.name end)
    |> assign_new(:value, fn -> field.value end)
    |> assign_new(:label, fn -> to_string(field.field) end)
    |> input()
  end

  def input(%{type: "markdown"} = assigns) do
    ~H"""
    <div id={@id} class={@class}>
      <.label :if={not @no_label} for={@id} required={@required}>{@label}</.label>
      <div id={"#{@id}-wrapper"} phx-update="ignore">
        <textarea
          phx-hook="MarkdownEditor"
          id={"#{@id}-textarea"}
          name={@name}
          class={[
            input_border(@errors),
            "mt-2 block min-h-[6rem] w-full rounded-lg border-stone-300 py-[7px] px-[11px]",
            "text-stone-900 focus:border-stone-400 focus:outline-hidden focus:ring-4 focus:ring-stone-800/5 sm:text-sm sm:leading-6",
            "phx-no-feedback:focus:border-stone-400 phx-no-feedback:focus:ring-stone-800/5"
          ]}
          {@rest}
        ><%= Form.normalize_value("textarea", @value) %></textarea>
      </div>
      <.error :for={msg <- @errors} message={msg} />
    </div>
    """
  end

  def input(%{type: "multiselect"} = assigns) do
    selected_labels =
      assigns.options
      |> Enum.filter(fn {_label, value} -> value in assigns.selected end)
      |> Enum.map(fn {label, _value} -> label end)

    assigns = assign(assigns, selected_labels: selected_labels)

    ~H"""
    <div id={@id} class={["relative", @class]}>
      <.label :if={not @no_label} required={@required}>{@label}</.label>
      <div
        class="w-full flex items-center h-10 px-3 mt-2 border border-gray-300 rounded-lg shadow-xs focus:outline-hidden focus:ring-stone-500 focus:border-stone-500 sm:text-sm"
        phx-click={toggle_dropdown(@id)}
        phx-window-keydown={close_dropdown(@id)}
        phx-key="escape"
      >
        <div class="flex grow">
          <Pill.pill :for={label <- @selected_labels} text={label} />
        </div>
        <Icon.icon name="ti-chevron-down" />
      </div>

      <div
        class="dropdown-content hidden border border-stone-300 absolute right-0 z-20 w-full mt-2 bg-stone-50 rounded-sm shadow-sm"
        aria-labelledby={@id}
        phx-click-away={close_dropdown(@id)}
      >
        <div class="flex flex-col py-2 text-stone-700">
          <label
            :for={{label, value} <- @options}
            for={"#{@name}-#{value}"}
            class="flex items-center gap-4 py-2 px-2 font-medium text-sm  hover:bg-stone-200"
          >
            <input
              type="checkbox"
              id={"#{@name}-#{value}"}
              name={@name}
              value={value}
              checked={value in @selected}
              class="mr-2 h-4 w-4 rounded-sm"
              {@rest}
            />
            {label}
          </label>
        </div>
      </div>
      <.error :for={msg <- @errors} message={msg} />
    </div>
    """
  end

  def input(%{type: "checkgroup"} = assigns) do
    ~H"""
    <div class="text-sm">
      <.label :if={not @no_label} for={@id} required={@required}>{@label}</.label>
      <div class="grid grid-cols-2 lg:grid-cols-4 gap-2 mt-2 text-sm items-baseline">
        <input type="hidden" name={@name} value="" />
        <label
          :for={{label, value} <- @options}
          for={"#{@name}-#{value}"}
          class="flex items-center gap-4 font-medium text-sm"
        >
          <input
            type="checkbox"
            id={"#{@name}-#{value}"}
            name={@name}
            value={value}
            checked={value in @value}
            class="rounded-sm border-stone-300 text-stone-900 focus:ring-stone-900"
            {@rest}
          />
          {label}
        </label>
      </div>
      <.error :for={msg <- @errors} message={msg} />
    </div>
    """
  end

  def input(%{type: "radio_detailed"} = assigns) do
    ~H"""
    <ul class="grid w-full gap-2 md:grid-cols-2 my-4">
      <li :for={opt <- @option} class="h-full">
        <input
          type="radio"
          id={"#{@id || @name}-#{opt.value}"}
          name={@name}
          value={opt.value}
          class="hidden peer"
          required={@required}
          checked={to_string(@value) == to_string(opt.value)}
        />
        <label
          for={"#{@id || @name}-#{opt.value}"}
          class="inline-flex h-full w-full p-5 bg-white border border-gray-300 rounded-lg cursor-pointer peer-checked:border-brand peer-checked:text-brand hover:bg-stone-50"
        >
          {render_slot(opt)}
        </label>
      </li>
    </ul>
    <.error :for={msg <- @errors} message={msg} />
    """
  end

  def input(%{type: "select"} = assigns) do
    ~H"""
    <div class={@class}>
      <.label :if={not @no_label} for={@id} required={@required}>{@label}</.label>
      <select
        id={@id}
        name={@name}
        class="mt-2 block w-full py-2 px-3 border border-gray-300 bg-white rounded-lg shadow-xs focus:outline-hidden focus:ring-stone-500 focus:border-stone-500 sm:text-sm"
        multiple={@multiple}
        {@rest}
      >
        <option :if={@prompt} value="">{@prompt}</option>
        {Form.options_for_select(@options, @value)}
      </select>
      <.error :for={msg <- @errors} message={msg} />
    </div>
    """
  end

  def input(%{type: "checkbox", value: value} = assigns) do
    assigns = assign_new(assigns, :checked, fn -> Form.normalize_value("checkbox", value) end)

    ~H"""
    <div class={@class}>
      <label class="flex items-center gap-4 text-sm leading-6 text-zinc-600">
        <input type="hidden" name={@name} value="false" />
        <input
          type="checkbox"
          id={@id}
          name={@name}
          value="true"
          checked={@checked}
          class="rounded-sm border-zinc-300 text-zinc-900 focus:ring-0"
          {@rest}
        />
        {@label}
      </label>
      <.error :for={msg <- @errors} message={msg} />
    </div>
    """
  end

  def input(%{type: "textarea"} = assigns) do
    ~H"""
    <div class={@class}>
      <.label :if={not @no_label} for={@id} required={@required}>{@label}</.label>
      <textarea
        id={@id || @name}
        name={@name}
        class={[
          input_border(@errors),
          "mt-2 block min-h-[6rem] w-full rounded-lg border-stone-300 py-[7px] px-[11px]",
          "text-stone-900 focus:border-stone-400 focus:outline-hidden focus:ring-4 focus:ring-stone-800/5 sm:text-sm sm:leading-6",
          "phx-no-feedback:focus:border-stone-400 phx-no-feedback:focus:ring-stone-800/5"
        ]}
        {@rest}
      ><%= Form.normalize_value("textarea", @value) %></textarea>
      <.error :for={msg <- @errors} message={msg} />
    </div>
    """
  end

  # All other inputs text, datetime-local, url, password, etc. are handled here...
  def input(assigns) do
    ~H"""
    <div class={@class}>
      <.label :if={not @no_label and @type != "hidden"} for={@id} required={@required}>
        {@label}
      </.label>
      <input
        type={@type}
        name={@name}
        id={@id}
        value={Form.normalize_value(@type, @value)}
        class={[
          input_border(@errors),
          not @no_label && "mt-2",
          "block w-full rounded-lg border-stone-300 py-[7px] px-[11px]",
          "text-stone-900 focus:outline-hidden focus:ring-4 sm:text-sm sm:leading-6",
          "phx-no-feedback:focus:border-stone-400 phx-no-feedback:focus:ring-stone-800/5"
        ]}
        {@rest}
      />
      <.error :for={msg <- @errors} message={msg} />
    </div>
    """
  end

  attr :id, :any, default: nil
  attr :name, :any
  attr :label, :string

  attr :field, FormField,
    doc: "a form field struct retrieved from the form, for example: @form[:email]"

  attr :upload, :any, required: true
  attr :required, :boolean, default: false
  attr :errors, :list, default: []
  attr :class, :string, default: "w-full"
  attr :no_label, :boolean, default: false
  attr :prompt, :string, default: nil, doc: "the prompt for the file input"
  attr :on_cancel, :string
  attr :rest, :global

  def file_upload(%{field: %FormField{} = field} = assigns) do
    assigns =
      assigns
      |> assign(field: nil, id: assigns.id || field.id)
      |> assign(:errors, Enum.map(field.errors, &translate_error(&1)))
      |> assign(:accepted, assigns.upload.accept |> String.replace(",", ", "))
      |> assign_new(:name, fn -> field.name end)
      |> assign_new(:value, fn -> field.value end)
      |> assign_new(:label, fn -> to_string(field.field) end)

    ~H"""
    <div class={@class}>
      <.label :if={not @no_label} class="mb-2" for={@id} required={@required}>{@label}</.label>

      <div class="flex items-center justify-center w-full">
        <label
          for={@upload.ref}
          phx-drop-target={@upload.ref}
          class={[
            Enum.count(@upload.entries) >= @upload.max_entries && "hidden",
            "flex flex-col items-center justify-center w-full h-64 border-2 border-stone-300 border-dashed rounded-lg cursor-pointer bg-stone-50 hover:bg-stone-100"
          ]}
        >
          <div class="flex flex-col items-center justify-center pt-5 pb-6 px-2 text-center">
            <Icon.icon name="ti-file-upload" class="w-8 h-8 text-stone-500" />
            <p class="mb-2 text-sm text-stone-500">
              <span class="font-semibold">Click to upload</span> or drag and drop
            </p>
            <p class="text-xs text-stone-500">
              {gettext("accepted files: %{accepted}", %{accepted: @accepted})}
            </p>
            <.live_file_input upload={@upload} class="hidden" />
          </div>
        </label>
      </div>

      <div class="flex flex-col gap-2 mt-2">
        <.file_upload_entry
          :for={entry <- @upload.entries}
          upload={@upload}
          entry={entry}
          on_cancel={@on_cancel}
          {@rest}
        />
      </div>
    </div>
    """
  end

  attr :entry, UploadEntry, required: true
  attr :upload, UploadConfig, required: true
  attr :on_cancel, :string
  attr :rest, :global

  defp file_upload_entry(assigns) do
    assigns = assign(assigns, :error, upload_errors(assigns.upload, assigns.entry))

    ~H"""
    <div class="w-full rounded-md shadow-xs flex flex-col border-stone-300 border relative">
      <div class="flex items-center gap-2 p-2">
        <div :if={@error == []} class=" p-4">
          <Icon.icon name={file_type_icon(@entry)} class="w-6 h-6 text-stone-500" />
        </div>
        <div :if={@error != []} class="p-4">
          <Icon.icon name="ti-file-alert" class="w-6 h-6 text-stone-500" />
        </div>
        <div class="flex flex-col grow ">
          <p class="text-sm font-semibold">{@entry.client_name}</p>
          <span :if={@entry.progress < 100} class="text-sm text-stone-600">
            {@entry.progress}%
          </span>
          <span :if={@entry.progress == 100} class="flex items-center text-emerald-500 text-sm">
            <Icon.icon name="ti-circle-check" />
            {dgettext("events", "Uploaded")}
          </span>
          <Progress.progress
            :if={upload_errors(@upload, @entry) == [] && @entry.progress < 100}
            progress={@entry.progress}
          />
          <div class="text-sm font-normal leading-none text-rose-500">
            <Content.joined_text items={@error |> Enum.map(&error_to_string/1)} />
          </div>
        </div>
        <Button.icon_button
          name="ti-trash"
          type="button"
          phx-click={@on_cancel}
          phx-value-ref={@entry.ref}
          aria-label="cancel"
          {@rest}
        />
      </div>
    </div>
    """
  end

  defp file_type_icon(%UploadEntry{client_type: "image/png"}), do: "ti-file-smile"

  defp file_type_icon(%UploadEntry{client_type: "image/jpeg"}), do: "ti-file-smile"

  defp file_type_icon(%UploadEntry{client_type: "application/pdf"}), do: "ti-file-text"

  defp file_type_icon(%UploadEntry{client_type: "application/msword"}), do: "ti-file-text"

  defp file_type_icon(%UploadEntry{
         client_type: "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
       }),
       do: "ti-file-text"

  defp file_type_icon(%UploadEntry{client_type: "application/vnd.oasis.opendocument.text"}),
    do: "ti-file-text"

  defp file_type_icon(%UploadEntry{client_type: "application/vnd.ms-excel"}),
    do: "ti-file-spreadsheet"

  defp file_type_icon(%UploadEntry{
         client_type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
       }),
       do: "ti-file-spreadsheet"

  defp file_type_icon(%UploadEntry{client_type: "application/vnd.oasis.opendocument.spreadsheet"}),
    do: "ti-file-spreadsheet"

  defp file_type_icon(_), do: "ti-file-unknown"

  @doc """
  Generate a multi-select dropdown.
  """
  attr :id, :any
  attr :name, :any
  attr :label, :string

  attr :field, Phoenix.HTML.FormField,
    doc: "a form field struct retrieved from the form, for example: @form[:email]"

  attr :errors, :list
  attr :required, :boolean, default: false
  attr :options, :list, doc: "the options to pass to Phoenix.HTML.Form.options_for_select/2"
  attr :rest, :global, include: ~w(disabled form readonly)
  attr :class, :string, default: nil
  attr :no_label, :boolean, default: false

  def multiselect_dropdown(%{field: %Phoenix.HTML.FormField{} = field} = assigns) do
    assigns
    |> assign(:type, "multiselect")
    |> assign(:multiple, true)
    |> assign(:selected, pick_selected(field.value))
    |> input()
  end

  defp pick_selected(value) do
    value
    |> Enum.map(fn val ->
      case val do
        %Ecto.Changeset{action: :update, data: data} ->
          data.id

        %{id: id} ->
          id

        value when is_binary(value) and value != "" ->
          value

        _ ->
          nil
      end
    end)
    |> Enum.reject(&is_nil(&1))
  end

  @doc """
  Generate a checkbox group for multi-select.
  """
  attr :id, :any
  attr :name, :any
  attr :label, :string, default: nil

  attr :field, Phoenix.HTML.FormField,
    doc: "a form field struct retrieved from the form, for example: @form[:email]"

  attr :errors, :list
  attr :required, :boolean, default: false
  attr :options, :list, doc: "the options to pass to Phoenix.HTML.Form.options_for_select/2"
  attr :rest, :global, include: ~w(disabled form readonly)
  attr :class, :string, default: nil

  def checkgroup(assigns) do
    assigns
    |> assign(:multiple, true)
    |> assign(:type, "checkgroup")
    |> input()
  end

  defp input_border([] = _errors),
    do: "border-stone-300 focus:border-stone-400 focus:ring-stone-800/5"

  defp input_border([_ | _] = _errors),
    do: "border-rose-400 focus:border-rose-400 focus:ring-rose-400/10"

  @doc """
  Renders a label.
  """
  attr :for, :string, default: nil
  attr :class, :string, default: ""
  attr :required, :boolean, default: false
  slot :inner_block, required: true

  def label(assigns) do
    ~H"""
    <label for={@for} class={["font-medium text-sm inline-block", @required && "required", @class]}>
      {render_slot(@inner_block)}
    </label>
    """
  end

  @doc """
  Generates a generic error message.
  """
  attr :message, :string, required: true

  def error(assigns) do
    ~H"""
    <p class="mt-2 text-rose-500">
      <Icon.icon name="ti-alert-circle" class="mr-2" />{@message}
    </p>
    """
  end

  @doc """
  Translates an error message using gettext.
  """
  def translate_error({msg, opts}) do
    # When using gettext, we typically pass the strings we want
    # to translate as a static argument:
    #
    #     # Translate "is invalid" in the "errors" domain
    #     dgettext("errors", "is invalid")
    #
    #     # Translate the number of files with plural rules
    #     dngettext("errors", "1 file", "%{count} files", count)
    #
    # Because the error messages we show in our forms and APIs
    # are defined inside Ecto, we need to translate them dynamically.
    # This requires us to call the Gettext module passing our gettext
    # backend as first argument.
    #
    # Note we use the "errors" domain, which means translations
    # should be written to the errors.po file. The :count option is
    # set by Ecto and indicates we should also apply plural rules.

    if count = opts[:count] do
      Gettext.dngettext(NyghtWeb.Gettext, "errors", msg, msg, count, opts)
    else
      Gettext.dgettext(NyghtWeb.Gettext, "errors", msg, opts)
    end
  end

  def error_to_string(:too_large), do: "too large"
  def error_to_string(:not_accepted), do: "you have selected an unacceptable file type"

  @doc """
  Translates the errors for a field from a keyword list of errors.
  """
  def translate_errors(errors, field) when is_list(errors) do
    for {^field, {msg, opts}} <- errors, do: translate_error({msg, opts})
  end
end
