defmodule NyghtUi.Path do
  @moduledoc """
  This module contains utilities to work with SVG paths.
  """

  @doc """
  Adds a new "move to" instruction.
  """
  def move_to(path \\ "", %{x: x, y: y}), do: path <> "M#{x},#{y}"

  @doc """
  Adds a new quadratic curve instruction.
  """
  def quadra(path \\ "", %{x: ctrl_x, y: ctrl_y}, %{x: x, y: y}),
    do: path <> "Q#{ctrl_x},#{ctrl_y},#{x},#{y}"

  @doc """
  Closes the path.
  """
  def close(path \\ ""), do: path <> "Z"
end
