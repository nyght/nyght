defmodule NyghtUi.Dropdown do
  @moduledoc false

  use Phoenix.Component

  alias Phoenix.LiveView.JS

  @doc """
  Renders a dropdown.

  ## Example

      <.dropdown>
        <.a>Actions</.a>
        <:item>Foo</:item>
        <:item separator/>
        <:item>Bar</:item>
      </.dropdown>
  """
  attr :id, :string
  attr :class, :string, default: ""
  attr :placement, :string, default: "bottom"

  slot :item do
    attr :separator, :boolean
  end

  slot :inner_block, required: true

  def dropdown(assigns) do
    ~H"""
    <div
      :if={Enum.any?(@item, fn item -> not Map.get(item, :separator, false) end)}
      id={@id}
      class={["relative inline-block", @class]}
      phx-hook="Dropdown"
      data-placement={@placement}
    >
      {render_slot(@inner_block)}
      <div
        class="border border-stone-300 absolute top-0 left-0 z-20 w-full min-w-40 bg-stone-100 rounded-sm shadow-sm py-1 text-sm text-slate-700 dropdown-content"
        aria-labelledby={@id}
      >
        <%= for item <- @item do %>
          <%= if Map.get(item, :separator, false) do %>
            <hr class="border-stone-300" />
          <% else %>
            <div class="hover:bg-stone-300 border-stone-300">
              {render_slot(item)}
            </div>
          <% end %>
        <% end %>
      </div>
    </div>
    """
  end

  def toggle_dropdown(id, js \\ %JS{}) do
    js
    |> JS.toggle(to: content_selector(id))
  end

  def close_dropdown(id, js \\ %JS{}) do
    js
    |> JS.hide(to: content_selector(id))
  end

  def content_selector(id), do: "##{id} .dropdown-content"
end
