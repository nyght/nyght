defmodule NyghtUi.Progress do
  @moduledoc false

  use Phoenix.Component

  @doc """
  Renders a progress bar.

  ## Example

    <.progress progress={20} label="Something loading with a percentage" />
    <.progress progress={{20, 30}} label="Something loading with a fraction" />
  """
  attr :progress, :any, default: 0, doc: "the progress percentage"
  attr :label, :string, default: nil, doc: "the label telling what the progress bar represents"
  attr :class, :string, default: nil
  attr :rest, :global

  slot :completed

  def progress(assigns) do
    ~H"""
    <div class={@class} {@rest}>
      <%= if percentage(@progress) >= 100 and @completed != [] do %>
        {render_slot(@completed)}
      <% else %>
        <div :if={not is_nil(@label)} class="flex flex-row justify-between text-sm mb-1">
          <span>{@label}</span>
          <.label progress={@progress} />
        </div>
        <div class={["w-full bg-stone-300 rounded-full h-1.5"]}>
          <div class="bg-brand h-1.5 rounded-full" style={"width: #{percentage(@progress)}%"}></div>
        </div>
      <% end %>
    </div>
    """
  end

  attr :progress, :any, default: 0, doc: "the progress percentage"

  defp label(%{progress: {count, total}} = assigns) do
    assigns = assign(assigns, total: total, count: count)

    ~H"""
    <span>
      <span class="font-bold">{@count}</span><span class="text-slate-400">/<%= @total || "∞" %></span>
    </span>
    """
  end

  defp label(assigns) do
    ~H"""
    <span>{@progress}%</span>
    """
  end

  defp percentage({_count, 0}), do: 0
  defp percentage({_count, nil}), do: 100
  defp percentage({count, total}), do: count / total * 100
  defp percentage(percent), do: percent
end
