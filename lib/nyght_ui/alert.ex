defmodule NyghtUi.Alert do
  @moduledoc """
  This module provides alert components.
  """
  use Phoenix.Component

  import NyghtUi.Modal, only: [show: 1, hide: 1, hide: 2]
  use Gettext, backend: NyghtWeb.Gettext

  alias NyghtUi.Icon
  alias Phoenix.LiveView.JS

  @doc """
  Renders flash notices.

  ## Examples

      <.flash kind={:info} flash={@flash} />
      <.flash kind={:info} phx-mounted={show("#flash")}>Welcome Back!</.flash>
  """
  attr :id, :string, default: "flash", doc: "the optional id of flash container"
  attr :flash, :map, default: %{}, doc: "the map of flash messages to display"
  attr :title, :string, default: nil
  attr :class, :string, default: nil

  attr :kind, :atom,
    values: [:info, :success, :error, :warning],
    default: :info,
    doc: "used for styling and flash lookup"

  attr :rest, :global, doc: "the arbitrary HTML attributes to add to the flash container"

  slot :inner_block, doc: "the optional inner block that renders the flash message"

  def flash(assigns) do
    ~H"""
    <div
      :if={msg = render_slot(@inner_block) || Phoenix.Flash.get(@flash, @kind)}
      id={@id}
      phx-click={JS.push("lv:clear-flash", value: %{key: @kind}) |> hide("##{@id}")}
      role="alert"
      class={[
        "fixed bottom-2 right-2 w-80 sm:w-96 z-50 rounded-lg p-3 ring-1",
        kind(@kind),
        @class
      ]}
      {@rest}
    >
      <p :if={@title} class="flex items-center gap-1.5 text-sm font-semibold leading-6">
        <Icon.icon :if={@kind == :info} name="ti-info-circle" class="h-4 w-4" />
        <Icon.icon :if={@kind == :error} name="ti-alert-circle" class="h-4 w-4" />
        {@title}
      </p>
      <p class="mt-2 text-sm leading-5">{msg}</p>
      <button type="button" class="group absolute top-1 right-1 p-2" aria-label={gettext("close")}>
        <Icon.icon name="ti-x" class="h-5 w-5 stroke-current opacity-40 group-hover:opacity-70" />
      </button>
    </div>
    """
  end

  @doc """
  Shows the flash group with standard titles and content.

  ## Examples

      <.flash_group flash={@flash} />
  """
  attr :flash, :map, required: true, doc: "the map of flash messages"

  def flash_group(assigns) do
    ~H"""
    <.flash id="flash-info" kind={:info} title={gettext("Info")} flash={@flash} />
    <.flash id="flash-warning" kind={:warning} title={gettext("Warning!")} flash={@flash} />
    <.flash id="flash-success" kind={:success} title={gettext("Success!")} flash={@flash} />
    <.flash id="flash-error" kind={:error} title={gettext("Error!")} flash={@flash} />
    <.flash
      id="disconnected"
      kind={:error}
      title={gettext("We can't find the internet")}
      phx-disconnected={show("#disconnected")}
      phx-connected={hide("#disconnected")}
      hidden
    >
      {gettext("Attempting to reconnect...")}
    </.flash>
    """
  end

  defp kind(:info), do: "bg-sky-50 text-sky-800 ring-sky-500 fill-sky-900"
  defp kind(:success), do: "bg-emerald-50 text-emerald-800 ring-emerald-500 fill-cyan-900"
  defp kind(:warning), do: "bg-amber-50 text-amber-800 ring-amber-500 fill-amber-900"
  defp kind(:error), do: "bg-rose-50 text-rose-900 ring-rose-500 fill-rose-900"
end
