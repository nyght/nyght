defmodule NyghtUi.Icon do
  @moduledoc false
  use Phoenix.Component

  @doc """
  Renders an icon.

  This component uses Tabler Icons, so it expects an icon name prefixed
  with `ti-`.

  ## Example

      <.icon name="ti-dashboard" />

  """
  attr :name, :string,
    required: true,
    doc:
      "the name of the icon, with the corresponding prefix (`ti-` for Tabler Icons for example)"

  attr :class, :any, default: nil
  attr :rest, :global

  def icon(%{name: "ti-" <> _} = assigns) do
    ~H"""
    <span class={["#{@name}", @class]} {@rest} />
    """
  end
end
