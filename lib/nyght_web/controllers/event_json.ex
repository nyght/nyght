defmodule NyghtWeb.EventJSON do
  @moduledoc """
  This module is the view for json format.
  """
  alias Nyght.Common.Link
  alias Nyght.Events.{Event, Price}
  alias Nyght.Performers.Performer

  @doc """
  Renders a list of event.
  """
  def index(%{events: events, meta: meta}) do
    %{
      data: for(event <- events, do: data(event)),
      metadata: meta(meta)
    }
  end

  @doc """
  Renders a single event.
  """
  def show(%{event: event}) do
    %{data: data(event)}
  end

  defp data(%Event{} = event) do
    %{
      id: event.id,
      name: event.name,
      types: Enum.map(event.types, & &1.name),
      description_md: event.description,
      status: event.status,
      is_soldout: event.is_soldout,
      starts_at: event.starts_at,
      ends_at: event.ends_at,
      timezone: event.timezone,
      admission_prices: Enum.map(event.prices, &price/1),
      ticket_links: Enum.map(event.ticket_links, &link/1),
      performers: Enum.map(event.bookings, &performer(&1.performer))
    }
  end

  defp price(%Price{} = price) do
    %{
      label: price.label,
      amount: price.amount
    }
  end

  defp link(%Link{url: url, name: name}) do
    %{
      url: url,
      name: name
    }
  end

  defp performer(%Performer{name: name, country: country, links: links}) do
    %{
      name: name,
      country: country,
      links: Enum.map(links, &link/1)
    }
  end

  defp meta(%Flop.Meta{} = meta) do
    %{
      page_size: meta.page_size,
      current_page: meta.current_page,
      total_pages: meta.total_pages,
      total_count: meta.total_count
    }
  end
end
