defmodule NyghtWeb.AttachmentController do
  use NyghtWeb, :controller

  alias Nyght.Authorizations
  alias Nyght.Events

  def download(%{assigns: assigns} = conn, %{"id" => id}) do
    %{current_member: member} = assigns

    with {:ok, {attachment, content}} <- Events.get_attachment(id),
         :ok <- Authorizations.can(member, attachment, :download) do
      conn
      |> put_resp_header("content-type", attachment.content_type)
      |> send_resp(200, content)
    else
      {:error, :not_found} ->
        conn
        |> put_status(:not_found)
        |> put_view(html: NyghtWeb.ErrorHTML, json: NyghtWeb.ErrorJSON)
        |> render(:"404", meta_attrs: [], hide_navigation: true)

      {:error, :unauthorized} ->
        conn
        |> put_status(:unauthorized)
        |> put_view(html: NyghtWeb.ErrorHTML, json: NyghtWeb.ErrorJSON)
        |> render(:"401", meta_attrs: [], hide_navigation: true)

      _ ->
        conn
        |> put_status(:internal_server_error)
        |> put_view(html: NyghtWeb.ErrorHTML, json: NyghtWeb.ErrorJSON)
        |> render(:"500", meta_attrs: [], hide_navigation: true)
    end
  end
end
