defmodule NyghtWeb.UserVCF do
  @moduledoc """
  This module is the view for vcard format.
  """
  alias Nyght.Accounts.User
  alias Nyght.VObjects.VCard

  @doc """
  Renders a user in vcard format.
  """
  def export(%{user: user}) do
    %VCard{
      email: user.email,
      fn: User.full_name(user, redacted?: false),
      nickname: user.nickname,
      tel: user.phone_number
    }
  end
end
