defmodule NyghtWeb.HealthController do
  @moduledoc """
  This module defines the health check controller.
  """
  use NyghtWeb, :controller

  import Ecto.Adapters.SQL

  alias Nyght.Repo

  @doc false
  def index(conn, _opts) do
    status = %{
      db: check_database()
    }

    json(conn, status)
  end

  defp check_database do
    try do
      query(Repo, "SELECT 1")
    rescue
      e in RuntimeError -> e
    end
    |> case do
      {:ok, _} -> "ok"
      _ -> "error"
    end
  end
end
