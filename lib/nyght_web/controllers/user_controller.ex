defmodule NyghtWeb.UserController do
  @moduledoc """
  This controllers contains the logic to display users in different formats.
  """
  use NyghtWeb, :controller

  alias Nyght.{Accounts, Shifts}

  action_fallback NyghtWeb.FallbackController

  @doc false
  def export(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)

    filename = Macro.underscore("#{user.first_name} #{user.last_name}") <> ".vcf"

    conn
    |> put_resp_header("content-disposition", ~s(attachment; filename="#{filename}"))
    |> render("export.vcf", user: user)
  end

  @doc false
  def schedule_delete(conn, _) do
    if Shifts.applied_for_future_shift?(conn.assigns.current_user) do
      conn
      |> put_flash(
        :error,
        dgettext(
          "profiles",
          "You can't ask to delete your account while having non-completed shifts."
        )
      )
      |> redirect(to: ~p"/me/edit")
    else
      Accounts.update_user_profile(conn.assigns.current_user, %{
        deletion_requested_at: NaiveDateTime.utc_now(:second)
      })

      conn
      |> put_flash(
        :success,
        dgettext("profiles", "Your profile will automatically be deleted in 7 days.")
      )
      |> redirect(to: ~p"/me/edit")
    end
  end

  @doc false
  def cancel_delete(conn, _) do
    Accounts.update_user_profile(conn.assigns.current_user, %{
      deletion_requested_at: nil
    })

    conn
    |> put_flash(:success, dgettext("profiles", "Your profile won't be deleted. Welcome back."))
    |> redirect(to: ~p"/me/edit")
  end
end
