defmodule NyghtWeb.EventController do
  @moduledoc """
  This controllers contains the logic to display events in different formats.
  """
  use NyghtWeb, :controller

  alias NimbleCSV.RFC4180, as: CSV
  alias Nyght.{Events, Repo, Shifts}

  action_fallback(NyghtWeb.FallbackController)

  @doc false
  def index(%{assigns: assigns, query_params: params} = conn, _params) do
    org = assigns.current_organization

    opts = [
      preload: [:types, :shifts, :prices, bookings: [:performer]]
    ]

    case Events.list_events_for(org, params, opts) do
      {:ok, {events, meta}} ->
        render(conn, :index, events: events, meta: meta)

      {:error, meta} ->
        error = traverse_errors(meta, &format_error_message/1)

        conn
        |> put_status(400)
        |> json(error)
    end
  end

  defp format_error_message({msg, opts}) do
    Regex.replace(~r"%{(\w+)}", msg, fn _, key ->
      opts
      |> Keyword.get(String.to_existing_atom(key), key)
      |> to_string()
    end)
  end

  defp traverse_errors(%Flop.Meta{errors: errors}, msg_func) do
    Enum.reduce(errors, %{}, fn {key, messages}, acc ->
      formatted =
        Enum.reduce(messages, [], fn e, acc ->
          val = msg_func.(e)
          [val | acc]
        end)

      Map.update(acc, key, formatted, &(formatted ++ &1))
    end)
  end

  @doc false
  def show(conn, %{"id" => id}) do
    event = Events.get_event!(id, preload: [:types, :shifts, :prices, bookings: [:performer]])
    render(conn, :show, event: event)
  end

  @doc false
  def export(conn, %{"id" => id}) do
    event = Events.get_event!(id)
    filename = Macro.underscore(event.name) <> ".ics"

    conn
    |> put_resp_header("content-disposition", ~s(attachment; filename="#{filename}"))
    |> render("export.ics", event: event, url: url(~p"/events/#{event}"))
  end

  def shifts_export(conn, %{"id" => id}) do
    event = Events.get_event!(id)
    filename = Macro.underscore(event.name) <> "_shifts.csv"

    conn =
      conn
      |> put_resp_content_type("text/csv")
      |> put_resp_header("content-disposition", "attachment; filename=\"#{filename}\"")
      |> send_chunked(:ok)

    {:ok, _} =
      chunk(
        conn,
        CSV.dump_to_iodata([
          [
            "first name",
            "last name",
            "email",
            "street line 1",
            "street line 2",
            "street line 3",
            "zip code",
            "city",
            "phone",
            "shift",
            "starts at",
            "ends at"
          ]
        ])
      )

    Repo.transaction(fn ->
      Shifts.stream_shifts_export(event)
      |> Stream.each(fn entry ->
        %{
          shift: shift,
          local_starts_at: starts_at,
          local_ends_at: ends_at,
          email: email,
          street_line_1: street_line_1,
          street_line_2: street_line_2,
          street_line_3: street_line_3,
          zip_code: zip_code,
          city: city,
          first_name: first_name,
          last_name: last_name,
          phone: phone
        } = entry

        row = [
          first_name,
          last_name,
          email,
          street_line_1,
          street_line_2,
          street_line_3,
          zip_code,
          city,
          phone,
          shift,
          starts_at,
          ends_at
        ]

        {:ok, _conn} = chunk(conn, CSV.dump_to_iodata([row]))
      end)
      |> Stream.run()
    end)

    conn
  end
end
