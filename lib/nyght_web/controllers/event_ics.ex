defmodule NyghtWeb.EventICS do
  @moduledoc """
  This module is the view for icalendar format.
  """
  alias Nyght.Events.Event
  alias Nyght.VObjects
  alias Nyght.VObjects.{VCalendar, VEvent}

  @doc """
  Reders an ics file for an event.
  """
  def export(%{event: event, url: url}) do
    %VCalendar{
      events: event(event, url)
    }
  end

  defp event(%Event{} = event, url) do
    %VEvent{
      uid: event.id,
      summary: VObjects.format_text("summary", event.name),
      description: VObjects.format_text("description", event.description),
      dtstamp: DateTime.utc_now(),
      dtstart: event.starts_at,
      dtend: event.ends_at,
      "last-modified": event.updated_at,
      status: "CONFIRMED",
      class: "PUBLIC",
      url: url
    }
  end
end
