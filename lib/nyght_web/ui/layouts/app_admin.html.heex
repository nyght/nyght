<main class="h-screen relative">
  <.sidebar>
    <:content>
      <.sidebar_link
        :if={Authorizations.can?(@current_member, Member, :list)}
        navigate={~p"/app/members"}
        active={@active_page == :users}
      >
        <.icon name="ti-users" />
      </.sidebar_link>
      <.sidebar_link
        :if={Authorizations.can?(@current_member, Performer, :list)}
        navigate={~p"/app/performers"}
        active={@active_page == :performers}
      >
        <.icon name="ti-microphone-2" />
      </.sidebar_link>
      <.sidebar_link
        :if={Authorizations.can?(@current_member, Event, :list)}
        navigate={~p"/app/events"}
        active={@active_page == :events}
      >
        <.icon name="ti-calendar-event" />
      </.sidebar_link>
      <.sidebar_link
        :if={
          Authorizations.can?(
            @current_member,
            Nyght.Organizations.Organization,
            :update
          )
        }
        navigate={~p"/app/organizations"}
        active={@active_page == :organizations}
      >
        <.icon name="ti-building-store" />
      </.sidebar_link>
    </:content>

    <:bottom>
      <.sidebar_link navigate={~p"/me"}>
        <.avatar name={User.full_name(@current_user, redacted?: false)} />
      </.sidebar_link>
    </:bottom>
  </.sidebar>

  <.navbar with_sidebar>
    <:content></:content>

    <:right>
      <.dropdown id="user-actions">
        <button>
          <.avatar name={User.full_name(@current_user, redacted?: false)} />
        </button>
        <:item>
          <.link class="inline-block w-full py-2 px-4" navigate={~p"/me"}>
            {gettext("My profile")}
          </.link>
        </:item>
        <:item>
          <.link class="inline-block w-full py-2 px-4" href={~p"/users/log_out"} method="delete">
            {gettext("Log out")}
          </.link>
        </:item>
      </.dropdown>
    </:right>
  </.navbar>

  <div class="min-h-screen md:ml-16">
    <.banner
      :if={not is_nil(@current_user) and not is_nil(@current_user.deletion_requested_at)}
      id="account-deletion-banner"
    >
      <p>
        {dgettext(
          "profiles",
          "Your account is scheduled to be deleted from nyght. This was a mistake?"
        )}

        <.a href={~p"/me/edit#delete-section-profil-edit"}>
          {dgettext("profiles", "Edit your profile to cancel")}
        </.a>
      </p>
    </.banner>
    <.banner
      :if={not is_nil(@current_user) and is_nil(@current_user.confirmed_at)}
      id="validation-banner"
    >
      <p>
        {dgettext(
          "users",
          "Verify your email to get the most out of Nyght. Didn't receive an email?"
        )}
        <.a href={~p"/users/confirm?email=#{@current_user.email}"}>
          {dgettext("users", "Resend confirmation email")}
        </.a>
      </p>
    </.banner>
    <.flash_group flash={@flash} />
    {@inner_content}
  </div>

  <.footer>
    <:logo>
      <.link href={~p"/"} class="flex items-center">
        <img src={~p"/images/apple-touch-icon.png"} class="mr-3 h-8" alt="Nyght Logo" />
        <span class="self-center text-2xl font-semibold whitespace-nowrap text-white">
          nyght
        </span>
      </.link>
    </:logo>

    <:col label={gettext("Community")}>
      <li>
        <.link href="https://nyght.ch" class="hover:underline">Nyght Homepage</.link>
      </li>
      <li>
        <.link href="https://gitlab.com/nyght/nyght" class="hover:underline">
          Gitlab
        </.link>
      </li>
      <li>
        <.link href="https://nyght.zulipchat.com/" class="hover:underline">Zulip Chat</.link>
      </li>
    </:col>

    <:col label={gettext("Sponsors")}>
      <li>
        <.link href="https://ebull.ch" class="hover:underline">Ebullition</.link>
      </li>
      <li>
        <.link href="https://zulipchat.com/" class="hover:underline">Zulip</.link>
      </li>
    </:col>
  </.footer>
</main>
