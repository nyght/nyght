defmodule NyghtWeb.UI.Layouts do
  @moduledoc false

  use NyghtWeb, :html

  alias Nyght.Accounts.User
  alias Nyght.Authorizations
  alias Nyght.Events.Event
  alias Nyght.Organizations
  alias Nyght.Organizations.{Member, Organization}
  alias Nyght.Performers.Performer

  embed_templates("layouts/*")

  @doc """
  Returns the list of menu items.
  """
  def menu_items(nil, _user), do: []

  def menu_items(org, user) do
    [
      %{
        path: ~p"/",
        page: :index,
        text: gettext("Home"),
        visible: true
      },
      %{
        path: ~p"/app/events",
        page: :events,
        text: gettext("Events"),
        visible: Organizations.member?(org, user)
      }
    ]
  end

  @doc ~S"""
  Returns the page title suffix according to the current organization.

  ## Example

      iex> title_suffix(nil)
      " · nyght"

      iex> title_suffix(%Nyght.Organizations.Organization{name: "My Org"})
      " · My Org · nyght"
  """
  def title_suffix(nil), do: " · nyght"
  def title_suffix(%Organization{name: name}), do: " · #{name} · nyght"

  def get_appsignal_frontend_key do
    Application.get_env(:nyght, :appsignal)
    |> Keyword.get(:frontend_key)
  end
end
