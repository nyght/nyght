defmodule NyghtWeb.UI.LayoutComponents do
  @moduledoc false
  use Phoenix.Component

  use Gettext, backend: NyghtWeb.Gettext

  alias NyghtUi.{Content, Icon, Navigation}
  alias Phoenix.LiveView.JS

  attr :border, :boolean, default: true
  attr :rest, :global

  slot :inner_block, required: true
  slot :subtitle
  slot :actions
  slot :tab_bar
  slot :filters

  def page_header(assigns) do
    ~H"""
    <div
      class={["w-full bg-stone-50 flex flex-col", @border && "border-b border-stone-300 shadow-xs"]}
      {@rest}
    >
      <div class={[
        "container mx-auto flex items-center",
        not (@tab_bar != [] or @filters != []) && "min-h-[13rem]",
        (@tab_bar != [] or @filters != []) && "min-h-[10rem]"
      ]}>
        <Content.header class="w-full">
          {render_slot(@inner_block)}

          <:subtitle :if={@subtitle != []}>
            {render_slot(@subtitle)}
          </:subtitle>

          <:actions :if={@actions != []}>
            <div class="flex flex-col-reverse gap-2 md:flex-row">
              {render_slot(@actions)}
            </div>
          </:actions>
        </Content.header>
      </div>
      <div
        :if={@tab_bar != [] or @filters != []}
        class="container mx-auto flex justify-start items-end h-12"
      >
        <Navigation.tab_bar>
          {render_slot(@tab_bar)}
          <:actions :if={@filters != []}>
            <Navigation.tab phx-click={JS.toggle(to: "#filters")}>
              <Icon.icon name="ti-filter" /> {gettext("Filters")}
            </Navigation.tab>
          </:actions>
        </Navigation.tab_bar>
      </div>
      <div :if={@filters != []} id="filters" class="border-t border-stone-300 bg-stone-100 hidden">
        <div class="container mx-auto">
          {render_slot(@filters)}
        </div>
      </div>
    </div>
    """
  end
end
