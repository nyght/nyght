defmodule NyghtWeb.Cldr do
  @moduledoc """
  Define a backend module that hosts our Cldr configuration and public
  API.

  Most function calls in Cldr will be calls to functions on this
  module.
  """
  use Cldr,
    otp_app: :nyght,
    providers: [Cldr.Calendar, Cldr.DateTime, Cldr.Number, Cldr.Territory],
    default_locale: "en",
    gettext: NyghtWeb.Gettext

  @doc """
  Returns the emoji flag and the name of the territory.
  """
  def full_territory_name(territory) do
    flag = flag_from_territory(territory)
    name = Cldr.Territory.from_territory_code!(territory, __MODULE__)

    "#{flag} #{name}"
  end

  @doc """
  Returns the emoji flag of the territory.
  """
  def flag_from_territory(territory) do
    case Cldr.Territory.to_unicode_flag(territory) do
      {:ok, flag} ->
        flag

      _ ->
        " "
    end
  rescue
    _ ->
      " "
  end
end
