defmodule NyghtWeb.Router do
  @moduledoc """
  This module defines the application Phoenix router.
  """

  use NyghtWeb, :router

  import Nyght.Monitoring
  import NyghtWeb.UserAuth
  import NyghtWeb.OrganizationAuth

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {NyghtWeb.UI.Layouts, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers

    plug :fetch_current_organization
    plug :fetch_current_user
    plug :fetch_current_member

    plug Cldr.Plug.PutLocale,
      apps: [:cldr, :gettext],
      from: [:accept_language],
      gettext: NyghtWeb.Gettext,
      cldr: NyghtWeb.Cldr

    plug Cldr.Plug.PutSession

    plug :set_context
  end

  pipeline :api do
    plug CORSPlug, origin: "*"
    plug :accepts, ["json"]

    plug :fetch_current_organization, session: false

    plug :set_namespace, name: "API"
  end

  # Routes unavailable to authenticated users
  scope "/", NyghtWeb do
    pipe_through [:browser, :redirect_if_user_is_authenticated]

    live_session :redirect_if_user_is_authenticated,
      on_mount: [
        {NyghtWeb.UserAuth, :redirect_if_user_is_authenticated},
        NyghtWeb.OrganizationAuth
      ] do
      live "/users/register", UserLive.Registration, :new
      live "/users/log_in", UserLive.Login, :new
      live "/users/reset_password", UserLive.ForgotPassword, :new
      live "/users/reset_password/:token", UserLive.ResetPassword, :edit
    end

    post "/users/log_in", UserSessionController, :create
  end

  # Routes requiring authentication
  scope "/app", NyghtWeb do
    pipe_through [:browser, :require_authenticated_user]

    get "/events/:id/shifts/export", EventController, :shifts_export
    get "/attachments/:id", AttachmentController, :download

    live_session :require_authenticated_org_user,
      on_mount: [{NyghtWeb.UserAuth, :ensure_authenticated}, NyghtWeb.OrganizationAuth],
      layout: {NyghtWeb.UI.Layouts, :app_admin} do
      live "/dashboard", DashboardLive, :index

      scope "/events", EventLive do
        live "/", IndexList, :upcoming
        live "/past", IndexList, :past
        live "/filter", IndexList, :filter
        live "/create", Form, :create
        live "/:id", EventIndex, :index
        live "/:id/staff", EventStaff, :staff
        live "/:id/attachments", EventAttachments, :attachments
        live "/:id/attachments/add", EventAttachments, :create_attachment
        live "/:id/attachments/:attachment_id/edit", EventAttachments, :edit_attachment
        live "/:id/edit", Form, :edit
        live "/:id/copy", Form, :copy
        live "/:id/shifts/add", EventStaff, :create_shift
        live "/:id/shifts/:shift_id/edit", EventStaff, :edit_shift
        live "/:id/shifts/:shift_id/assign", EventStaff, :assign
        live "/:id/bookings/:booking_id/edit", EventIndex, :edit_booking
      end

      scope "/performers", PerformerLive do
        live "/", Index, :index
        live "/create", Form, :create
        live "/:id", Show, :index
        live "/:id/edit", Form, :edit
      end

      live "/organizations", OrganizationLive.Settings, :index

      scope "/members" do
        live "/", MemberLive.Index, :index
        live "/requests", MemberLive.RequestsIndex, :requests
        live "/requests/:id", MemberLive.RequestsIndex, :request
        live "/roles", MemberLive.RolesIndex, :roles
        live "/roles/create", MemberLive.RoleForm, :create
        live "/roles/:id/edit", MemberLive.RoleForm, :edit
        live "/categories", MemberLive.CategoriesIndex, :categories
        live "/categories/create", MemberLive.CategoriesIndex, :create_category
        live "/categories/:id/edit", MemberLive.CategoriesIndex, :edit_category
        live "/:id/edit", ProfileLive.Show, :member_edit
        live "/:id", ProfileLive.Show, :member
      end
    end
  end

  # Routes that don't require authentication
  scope "/", NyghtWeb do
    pipe_through [:browser]

    get "/health", HealthController, :index
    delete "/users/log_out", UserSessionController, :delete

    live_session :optional_authenticated_user,
      on_mount: [{NyghtWeb.UserAuth, :mount_current_user}, NyghtWeb.OrganizationAuth] do
      live "/", OrganizationLive.Show, :index, as: :home

      live "/events/:id", EventLive.Show, :public_index

      live "/users/confirm/:token", UserLive.Confirmation, :edit
      live "/users/confirm", UserLive.ConfirmationInstructions, :new
    end
  end

  # Routes that require authentication
  scope "/", NyghtWeb do
    pipe_through [:browser, :require_authenticated_user]

    live_session :require_authenticated_user,
      on_mount: [{NyghtWeb.UserAuth, :ensure_authenticated}, NyghtWeb.OrganizationAuth] do
      live "/users/settings", UserLive.Settings, :edit
      live "/users/settings/confirm_email/:token", UserLive.Settings, :confirm_email
      live "/users/:id", ProfileLive.Show, :public

      live "/me", ProfileLive.Show, :personal
      live "/me/edit", ProfileLive.Edit, :index
      live "/me/edit/account", ProfileLive.Edit, :account
      live "/me/edit/password", ProfileLive.Edit, :password
      live "/me/edit/confirm_email/:token", ProfileLive.Edit, :confirm_email

      live "/join", MemberLive.Join, :index
    end

    delete "/me", UserController, :schedule_delete
    post "/me/edit", UserController, :cancel_delete
  end

  # Other scopes may use custom stacks.
  scope "/api", NyghtWeb do
    pipe_through :api

    scope "/v1" do
      resources "/events", EventController, only: [:index, :show]
    end
  end

  scope "/ical", NyghtWeb do
    pipe_through :browser

    get "/events/:id", EventController, :export
    get "/users/:id", UserController, :export
  end

  # Enables the Swoosh mailbox preview in development.
  #
  # Note that preview only shows emails that were sent by the same
  # node running the Phoenix server.
  if Mix.env() == :dev do
    import Phoenix.LiveDashboard.Router

    scope "/dev" do
      pipe_through :browser

      live_dashboard "/dashboard", metrics: NyghtWeb.Telemetry
      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end
end
