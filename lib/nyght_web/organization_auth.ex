defmodule NyghtWeb.OrganizationAuth do
  @moduledoc """
  Utility functions for organization and members authentication.
  """
  use NyghtWeb, :verified_routes

  import Plug.Conn

  require Logger

  alias Nyght.Organizations

  @multitenant? Application.compile_env(:nyght, :routing, [])
                |> Keyword.get(:multitenant, false)

  if @multitenant? do
    def get_organization!(nil), do: nil
    def get_organization!(id), do: Organizations.get_organization!(id)

    def get_organization_by_slug!(conn),
      do: get_subdomain(conn.host) |> Organizations.get_organization_by_slug!()

    defp get_subdomain(host) do
      subdomain_host = NyghtWeb.Endpoint.config(:url)[:host]

      if host =~ subdomain_host do
        String.replace(host, ~r/.?#{subdomain_host}/, "")
      end
    end
  else
    def get_organization!(_), do: Organizations.get_first_organization()
    def get_organization_by_slug!(_), do: Organizations.get_first_organization()
  end

  def fetch_current_organization(conn, opts) do
    org = get_organization_by_slug!(conn)
    use_session = Keyword.get(opts, :session, true)

    conn = assign(conn, :current_organization, org)

    if use_session do
      put_session(conn, :current_organization_id, if(org, do: org.id))
    else
      conn
    end
  end

  def fetch_current_member(%{assigns: assigns} = conn, _opts) do
    if user = assigns[:current_user] do
      org = assigns[:current_organization]
      member = Organizations.get_member(org, user, preload: [roles: [:permissions]])

      assign(conn, :current_member, member)
    else
      conn
    end
  end

  @doc false
  def on_mount(:default, _params, session, socket) do
    socket =
      socket
      |> assign_organization(session)
      |> assign_member()
      |> assign_locale(session)

    {:cont, socket}
  end

  defp assign_organization(socket, session) do
    Phoenix.Component.assign_new(socket, :current_organization, fn ->
      get_organization!(session["current_organization_id"])
    end)
  end

  defp assign_member(%{assigns: %{current_user: user, current_organization: org}} = socket) do
    Phoenix.Component.assign_new(socket, :current_member, fn ->
      if user && org do
        Organizations.get_member(org, user, preload: [roles: [:permissions]])
      end
    end)
  end

  defp assign_member(socket) do
    Phoenix.Component.assign_new(socket, :current_member, fn -> nil end)
  end

  defp assign_locale(socket, session) do
    {:ok, _locale} = Cldr.Plug.put_locale_from_session(session)

    socket
  end
end
