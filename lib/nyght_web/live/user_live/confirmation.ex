defmodule NyghtWeb.UserLive.Confirmation do
  @moduledoc """
  This is the account confirmation liveview.
  """

  use NyghtWeb, :live_view

  alias Nyght.Accounts

  @doc false
  def render(%{live_action: :edit} = assigns) do
    ~H"""
    <section class="flex h-screen w-screen">
      <div class="w-full lg:w-1/2 2xl:w-1/3 py-4 px-4 h-full flex flex-col overflow-auto">
        <.link navigate={~p"/"} class="text-brand font-bold">nyght</.link>

        <div class="flex items-center justify-center grow">
          <div class="w-full max-w-md h-fit m-10">
            <h1 class="mb-14">{dgettext("users", "Confirm your account")}</h1>
            <.simple_form for={@form} id="confirmation_form" phx-submit="confirm_account">
              <.input field={@form[:token]} type="hidden" />

              <:actions>
                <.button
                  type={:submit}
                  phx-disable-with={dgettext("users", "Confirming...")}
                  class="w-full phx-submit-loading:animate-pulse"
                >
                  {dgettext("users", "Confirm")}
                </.button>
              </:actions>
            </.simple_form>
          </div>
        </div>
      </div>
      <div class="hidden lg:block bg-image grow h-full"></div>
    </section>
    """
  end

  @doc false
  def mount(%{"token" => token}, _session, socket) do
    form = to_form(%{"token" => token}, as: "user")
    {:ok, assign(socket, form: form, hide_navigation: true), temporary_assigns: [form: nil]}
  end

  # Do not log in the user after confirmation to avoid a
  # leaked token giving the user access to the account.
  @doc false
  def handle_event("confirm_account", %{"user" => %{"token" => token}}, socket) do
    case Accounts.confirm_user(token) do
      {:ok, _} ->
        {:noreply,
         socket
         |> put_flash(:info, dgettext("users", "User confirmed successfully."))
         |> redirect(to: ~p"/")}

      :error ->
        # If there is a current user and the account was already confirmed,
        # then odds are that the confirmation link was already visited, either
        # by some automation or by the user themselves, so we redirect without
        # a warning message.
        case socket.assigns do
          %{current_user: %{confirmed_at: confirmed_at}} when not is_nil(confirmed_at) ->
            {:noreply, redirect(socket, to: ~p"/")}

          %{} ->
            {:noreply,
             socket
             |> put_flash(
               :error,
               dgettext("users", "User confirmation link is invalid or it has expired.")
             )
             |> redirect(to: ~p"/")}
        end
    end
  end
end
