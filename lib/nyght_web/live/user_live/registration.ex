defmodule NyghtWeb.UserLive.Registration do
  @moduledoc """
  This is the registration liveview.
  """

  alias Phoenix.LiveView.Socket
  use NyghtWeb, :live_view

  alias Nyght.Accounts
  alias Nyght.Accounts.User

  @doc false
  def render(assigns) do
    ~H"""
    <section class="flex h-screen w-screen">
      <div class="w-full lg:w-1/2 2xl:w-1/3 py-4 px-4 h-full flex flex-col overflow-auto">
        <.link navigate={~p"/"} class="text-brand font-bold">nyght</.link>

        <div class="flex items-center justify-center grow">
          <div class="w-full max-w-md h-fit m-10">
            <h1 class="mb-14">{dgettext("users", "Welcome!")}</h1>

            <.simple_form
              id="registration_form"
              for={@form}
              phx-submit="save"
              phx-change="validate"
              phx-trigger-action={@trigger_submit}
              action={~p"/users/log_in?_action=registered"}
              method="post"
            >
              <.error
                :if={@check_errors}
                message="Oops, something went wrong! Please check the errors below."
              />

              <.input
                field={@form[:email]}
                type="email"
                class="my-2"
                label={dgettext("users", "Email")}
                required
              />
              <.input
                field={@form[:password]}
                type="password"
                class="my-2"
                label={dgettext("users", "Password")}
                required
              />

              <div class="my-8 flex flex-row ">
                <.input
                  field={@form[:first_name]}
                  class="w-1/3 mr-2"
                  label={dgettext("users", "First name")}
                  required
                />
                <.input
                  field={@form[:last_name]}
                  class="w-1/3 mr-2"
                  label={dgettext("users", "Last name")}
                  required
                />
                <.input field={@form[:nickname]} class="w-1/3" label={dgettext("users", "Nickname")} />
              </div>

              <.input
                field={@form[:street_line_1]}
                class="my-2"
                label={dgettext("users", "Street line 1")}
                required
              />
              <.input
                field={@form[:street_line_2]}
                class="my-2"
                label={dgettext("users", "Street line 2")}
              />

              <div class="my-2 flex flex-row ">
                <.input
                  field={@form[:zip_code]}
                  class="w-1/3 mr-2"
                  label={dgettext("users", "ZIP code")}
                  required
                />
                <.input field={@form[:city]} class="w-2/3" label={dgettext("users", "City")} required />
              </div>

              <.input
                field={@form[:phone_number]}
                class="mt-8"
                label={dgettext("users", "Phone number")}
                required
              />

              <:actions>
                <.button
                  type={:submit}
                  phx-disable-with={dgettext("users", "Creating account...")}
                  class="w-full my-4 phx-submit-loading:animate-pulse"
                >
                  {dgettext("users", "Register")}
                </.button>
              </:actions>
            </.simple_form>

            <hr class="my-4" />

            <p class="text-center">
              {dgettext("users", "Already have an account?")}
              <.link navigate={~p"/users/log_in"} class="text-brand hover:underline">
                {dgettext("users", "Log in")}
              </.link>
            </p>
          </div>
        </div>
      </div>
      <div class="hidden lg:block bg-image grow h-full"></div>
    </section>
    """
  end

  @doc false
  def mount(_params, _session, socket) do
    changeset = Accounts.change_user_registration(%User{})

    socket =
      socket
      |> assign(trigger_submit: false, check_errors: false, hide_navigation: true)
      |> assign_form(changeset)

    {:ok, socket, temporary_assigns: [form: nil]}
  end

  @doc false
  def handle_event("save", %{"user" => user_params}, %Socket{host_uri: host_uri} = socket) do
    case Accounts.register_user(user_params) do
      {:ok, user} ->
        {:ok, _} =
          Accounts.deliver_user_confirmation_instructions(
            user,
            &url(host_uri, ~p"/users/confirm/#{&1}")
          )

        changeset = Accounts.change_user_registration(user)
        {:noreply, socket |> assign(trigger_submit: true) |> assign_form(changeset)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, socket |> assign(check_errors: true) |> assign_form(changeset)}
    end
  end

  def handle_event("validate", %{"user" => user_params}, socket) do
    changeset = Accounts.change_user_registration(%User{}, user_params)
    {:noreply, assign_form(socket, Map.put(changeset, :action, :validate))}
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    form = to_form(changeset, as: "user")

    if changeset.valid? do
      assign(socket, form: form, check_errors: false)
    else
      assign(socket, form: form)
    end
  end
end
