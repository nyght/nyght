defmodule NyghtWeb.UserLive.ConfirmationInstructions do
  @moduledoc """
  This is the confirmation instruction resending liveview.
  """

  use NyghtWeb, :live_view

  alias Nyght.Accounts
  alias Phoenix.LiveView.Socket

  @doc false
  def render(assigns) do
    ~H"""
    <section class="flex h-screen w-screen">
      <div class="w-full lg:w-1/2 2xl:w-1/3 py-4 px-4 h-full flex flex-col overflow-auto">
        <.link navigate={~p"/"} class="text-brand font-bold">nyght</.link>

        <div class="flex items-center justify-center grow">
          <div class="w-full max-w-md h-fit m-10">
            <h1 class="mb-14">{dgettext("users", "Resend confirmation instructions")}</h1>
            <.simple_form for={@form} id="resend_confirmation_form" phx-submit="send_instructions">
              <.input
                field={@form[:email]}
                type="email"
                class="my-2"
                label={dgettext("users", "Email")}
                required
              />

              <:actions>
                <.button
                  type={:submit}
                  phx-disable-with="Sending..."
                  class="w-full phx-submit-loading:animate-pulse"
                >
                  {dgettext("users", "Resend")}
                </.button>
              </:actions>
            </.simple_form>
          </div>
        </div>
      </div>
      <div class="hidden lg:block bg-image grow h-full"></div>
    </section>
    """
  end

  @doc false
  def mount(params, _session, socket) do
    email = Map.get(params, "email", "")

    {:ok, assign(socket, form: to_form(%{"email" => email}, as: "user"), hide_navigation: true)}
  end

  @doc false
  def handle_event(
        "send_instructions",
        %{"user" => %{"email" => email}},
        %Socket{host_uri: host_uri} = socket
      ) do
    if user = Accounts.get_user_by_email(email) do
      Accounts.deliver_user_confirmation_instructions(
        user,
        &url(host_uri, ~p"/users/confirm/#{&1}")
      )
    end

    info =
      dgettext(
        "users",
        "If your email is in our system and it has not been confirmed yet, you will receive an email with instructions shortly."
      )

    {:noreply,
     socket
     |> put_flash(:info, info)
     |> redirect(to: ~p"/")}
  end
end
