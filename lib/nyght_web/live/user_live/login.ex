defmodule NyghtWeb.UserLive.Login do
  @moduledoc """
  This is the login liveview.
  """

  use NyghtWeb, :live_view

  alias Phoenix.Flash

  @doc false
  def render(assigns) do
    ~H"""
    <section class="flex h-screen w-screen">
      <div class="w-full lg:w-1/2 2xl:w-1/3 py-4 px-4 h-full flex flex-col overflow-auto">
        <.link navigate={~p"/"} class="text-brand font-bold">nyght</.link>

        <div class="flex items-center justify-center grow">
          <div class="w-full max-w-md h-fit m-10">
            <h1 class="mb-14">{dgettext("users", "Welcome back!")}</h1>
            <.simple_form id="login_form" for={@form} action={~p"/users/log_in"} phx-update="ignore">
              <.input
                type="email"
                field={@form[:email]}
                class="my-2"
                label={dgettext("users", "Email")}
                required
              />
              <.input
                type="password"
                field={@form[:password]}
                class="my-2"
                label={dgettext("users", "Password")}
                required
              />
              <.input
                type="checkbox"
                field={@form[:remember_me]}
                class="my-2"
                label={dgettext("users", "Remember me")}
              />
              <.a navigate={~p"/users/reset_password"} class="flex w-full justify-center">
                {dgettext("users", "Forgot your password?")}
              </.a>
              <:actions>
                <.button type={:submit} class="w-full my-4">
                  Log in <span aria-hidden="true">→</span>
                </.button>
              </:actions>
            </.simple_form>

            <hr class="my-4" />

            <p class="text-center">
              {dgettext("users", "Don't have an account yet?")}
              <.link navigate={~p"/users/register"} class="text-brand hover:underline">
                {dgettext("users", "Register now!")}
              </.link>
            </p>
          </div>
        </div>
      </div>
      <div class="hidden lg:block bg-image grow h-full"></div>
    </section>
    """
  end

  @doc false
  def mount(_params, _session, socket) do
    email = Flash.get(socket.assigns.flash, :email)
    form = to_form(%{"email" => email}, as: "user")
    {:ok, assign(socket, form: form, hide_navigation: true), temporary_assigns: [form: form]}
  end
end
