defmodule NyghtWeb.UserLive.ForgotPassword do
  @moduledoc """
  This is the password reset instruction sending liveview.
  """

  alias Phoenix.LiveView.Socket
  use NyghtWeb, :live_view

  alias Nyght.Accounts

  @doc false
  def render(assigns) do
    ~H"""
    <section class="flex h-screen w-screen">
      <div class="w-full lg:w-1/2 2xl:w-1/3 py-4 px-4 h-full flex flex-col overflow-auto">
        <.link navigate={~p"/"} class="text-brand font-bold">nyght</.link>

        <div class="flex items-center justify-center grow">
          <div class="w-full max-w-md h-fit m-10">
            <h1 class="mb-14">{dgettext("users", "Forgot your password?")}</h1>
            <.simple_form for={@form} id="reset_password_form" phx-submit="send_email">
              <.input
                field={@form[:email]}
                type="email"
                class="my-2"
                label={dgettext("users", "Email")}
                required
              />

              <:actions>
                <.button
                  type={:submit}
                  phx-disable-with={dgettext("users", "Sending...")}
                  class="w-full phx-submit-loading:animate-pulse"
                >
                  {dgettext("users", "Send instructions")}
                </.button>
              </:actions>
            </.simple_form>
          </div>
        </div>
      </div>
      <div class="hidden lg:block bg-image grow h-full"></div>
    </section>
    """
  end

  @doc false
  def mount(_params, _session, socket) do
    {:ok, assign(socket, form: to_form(%{}, as: "user"), hide_navigation: true)}
  end

  @doc false
  def handle_event(
        "send_email",
        %{"user" => %{"email" => email}},
        %Socket{host_uri: host_uri} = socket
      ) do
    if user = Accounts.get_user_by_email(email) do
      Accounts.deliver_user_reset_password_instructions(
        user,
        &url(host_uri, ~p"/users/reset_password/#{&1}")
      )
    end

    info =
      dgettext(
        "users",
        "If your email is in our system, you will receive instructions to reset your password shortly."
      )

    {:noreply,
     socket
     |> put_flash(:info, info)
     |> redirect(to: ~p"/")}
  end
end
