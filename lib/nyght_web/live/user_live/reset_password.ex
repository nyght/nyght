defmodule NyghtWeb.UserLive.ResetPassword do
  @moduledoc """
  This is the password reset liveview.
  """

  use NyghtWeb, :live_view

  alias Nyght.Accounts

  @doc false
  def render(assigns) do
    ~H"""
    <section class="flex h-screen w-screen">
      <div class="w-full lg:w-1/2 2xl:w-1/3 py-4 px-4 h-full flex flex-col overflow-auto">
        <.link navigate={~p"/"} class="text-brand font-bold">nyght</.link>

        <div class="flex items-center justify-center grow">
          <div class="w-full max-w-md h-fit m-10">
            <h1 class="mb-14">{dgettext("users", "Reset your password")}</h1>
            <.simple_form
              for={@form}
              id="reset_password_form"
              phx-submit="reset_password"
              phx-change="validate"
            >
              <.input
                field={@form[:password]}
                type="password"
                class="my-2"
                label={dgettext("users", "Password")}
                required
              />
              <.input
                field={@form[:password_confirmation]}
                type="password"
                class="my-2"
                label={dgettext("users", "Password confirmation")}
                required
              />
              <:actions>
                <.button
                  type={:submit}
                  phx-disable-with={dgettext("users", "Resetting...")}
                  class="w-full phx-submit-loading:animate-pulse"
                >
                  {dgettext("users", "Reset password")}
                </.button>
              </:actions>
            </.simple_form>
          </div>
        </div>
      </div>
      <div class="hidden lg:block bg-image grow h-full"></div>
    </section>
    """
  end

  @doc false
  def mount(params, _session, socket) do
    socket =
      socket
      |> assign_user_and_token(params)
      |> assign(hide_navigation: true)

    form_source =
      case socket.assigns do
        %{user: user} ->
          Accounts.change_user_password(user)

        _ ->
          %{}
      end

    {:ok, assign_form(socket, form_source), temporary_assigns: [form: nil]}
  end

  # Do not log in the user after reset password to avoid a
  # leaked token giving the user access to the account.
  @doc false
  def handle_event("reset_password", %{"user" => user_params}, socket) do
    case Accounts.reset_user_password(socket.assigns.user, user_params) do
      {:ok, _} ->
        {:noreply,
         socket
         |> put_flash(:info, dgettext("users", "Password reset successfully."))
         |> redirect(to: ~p"/users/log_in")}

      {:error, changeset} ->
        {:noreply, assign(socket, :changeset, Map.put(changeset, :action, :insert))}
    end
  end

  def handle_event("validate", %{"user" => user_params}, socket) do
    changeset = Accounts.change_user_password(socket.assigns.user, user_params)
    {:noreply, assign_form(socket, Map.put(changeset, :action, :validate))}
  end

  defp assign_user_and_token(socket, %{"token" => token}) do
    if user = Accounts.get_user_by_reset_password_token(token) do
      assign(socket, user: user, token: token)
    else
      socket
      |> put_flash(:error, dgettext("users", "Reset password link is invalid or it has expired."))
      |> redirect(to: ~p"/")
    end
  end

  defp assign_form(socket, %{} = source) do
    assign(socket, :form, to_form(source, as: "user"))
  end
end
