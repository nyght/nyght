defmodule NyghtWeb.PerformerLive.Form do
  @moduledoc """
  This is the performer edition and creation page.
  """
  use NyghtWeb, :live_view

  alias Nyght.Performers
  alias Nyght.Performers.Performer

  @doc false
  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @doc false
  def active_menu_item(_), do: :performers

  @doc false
  @impl true
  def handle_params(params, _uri, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :create, _params) do
    socket
    |> assign(page_title: dgettext("performers", "Add a new performer"))
    |> assign(performer: %Performer{organization_id: socket.assigns.current_organization.id})
    |> enforce_permission(Performer, :create)
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(page_title: dgettext("performers", "Edit a performer"))
    |> assign(performer: Performers.get_performer!(id))
    |> enforce_permission(Performer, :update)
  end

  @doc false
  @impl true
  def handle_info({NyghtWeb.PerformerLive.FormComponent, {:created, performer}}, socket) do
    socket =
      socket
      |> put_flash(:success, dgettext("performers", "Performer successfully created!"))
      |> push_navigate(to: ~p"/app/performers/#{performer}")

    {:noreply, socket}
  end

  def handle_info({NyghtWeb.PerformerLive.FormComponent, {:updated, performer}}, socket) do
    socket =
      socket
      |> put_flash(:success, dgettext("performers", "Performer successfully updated!"))
      |> push_navigate(to: ~p"/app/performers/#{performer}")

    {:noreply, socket}
  end
end
