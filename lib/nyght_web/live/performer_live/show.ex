defmodule NyghtWeb.PerformerLive.Show do
  @moduledoc """
  This is the performer show page.
  """
  use NyghtWeb, :live_view

  alias Nyght.Performers
  alias Nyght.Performers.Performer

  @doc false
  @impl true
  def mount(%{"id" => performer_id}, _session, socket) do
    performer = Performers.get_performer!(performer_id)

    socket =
      socket
      |> assign(page_title: performer.name)
      |> assign(performer: performer)
      |> enforce_permission(Performer, :read)

    {:ok, socket}
  end

  @doc false
  def active_menu_item(_), do: :performers
end
