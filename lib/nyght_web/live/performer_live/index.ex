defmodule NyghtWeb.PerformerLive.Index do
  @moduledoc """
  This module defines the performers administration page.
  """
  use NyghtWeb, :live_view

  alias Nyght.Performers
  alias Nyght.Performers.Performer

  @impl true
  def mount(_params, _session, socket) do
    socket =
      socket
      |> assign(page_title: dgettext("performers", "Performers"))
      |> stream(:performers, [])
      |> enforce_permission(Performer, :list)

    {:ok, socket}
  end

  @doc false
  def active_menu_item(_), do: :performers

  @impl true
  def handle_params(params, _uri, socket) do
    socket =
      socket
      |> stream(:performers, [], reset: true)
      |> fetch_performers(params)

    {:noreply, socket}
  end

  @impl true
  def handle_event("update-filters", params, socket) do
    params = Map.delete(params, "_target")
    {:noreply, push_patch(socket, to: ~p"/app/performers?#{params}")}
  end

  defp fetch_performers(%{assigns: _assigns} = socket, params) do
    case Performers.list_performers_for(socket.assigns.current_organization, params) do
      {:ok, {performers, meta}} ->
        socket
        |> assign(:meta, meta)
        |> assign(:filter_form, to_form(meta))
        |> stream(:performers, performers, reset: true)

      {:error, _meta} ->
        push_navigate(socket, to: ~p"/app/performers")
    end
  end

  defp filter_opts do
    [
      name: [
        label: dgettext("performers", "name"),
        op: :ilike
      ],
      country: [
        label: dgettext("performers", "country"),
        type: "select",
        prompt: "",
        options: country_filter_options()
      ]
    ]
  end

  defp country_filter_options do
    Cldr.Territory.country_codes()
    |> Enum.map(fn code ->
      name =
        case Cldr.Territory.from_territory_code(code, NyghtWeb.Cldr) do
          {:ok, name} ->
            name

          {:error, _} ->
            nil
        end

      {name, code}
    end)
    |> Enum.filter(fn {name, _} -> not is_nil(name) end)
  end
end
