defmodule NyghtWeb.PerformerLive.FormComponent do
  @moduledoc """
  This module provides a form component to create and edit performers.

  ## Example

       <.live_component
        module={NyghtWeb.PerformerLive.FormComponent}
        id="performer-form"
        action={@live_action}
        performer={@performer}
        organization={@current_organization}
      />
  """

  require Logger
  use NyghtWeb, :live_component

  alias Nyght.Performers

  @doc false
  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.simple_form
        for={@form}
        id="event-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.form_section borderless>
          <:title>{dgettext("performers", "General")}</:title>
          <:description>
            {dgettext("performers", "The main information about the performer.")}
          </:description>

          <.input field={@form[:name]} required label={dgettext("performers", "Name")} />
          <.input
            label={dgettext("performers", "Country")}
            field={@form[:country]}
            type="select"
            options={Enum.map(@countries, &{&1.name, &1.code})}
            prompt={dgettext("performers", "Select performer's origin")}
          />
        </.form_section>

        <.form_section>
          <:title>{dgettext("performers", "Contact")}</:title>
          <:description>
            {dgettext("performers", "The contact information for the performer's management.")}
          </:description>

          <.input field={@form[:email]} type="email" label={dgettext("performers", "Email")} />
          <.input
            field={@form[:phone_number]}
            type="tel"
            class="mt-8"
            label={dgettext("performers", "Phone number")}
          />
        </.form_section>

        <.form_section>
          <:title>{dgettext("performers", "Links")}</:title>
          <:description>
            {dgettext(
              "performers",
              "The places where the public can find the performer's works. This will be visible by everybody."
            )}
          </:description>
          <.inputs_for :let={fp} field={@form[:links]}>
            <input type="hidden" name="performer[links_sort][]" value={fp.index} />
            <div class="w-full flex flex-row items-start justify-between gap-x-2 mb-2">
              <.input
                field={fp[:url]}
                placeholder={dgettext("performers", "Link URL")}
                no_label
                required
              />
              <label class="rounded-md text-stone-900 p-2 hover:bg-stone-100 cursor-pointer ">
                <input type="checkbox" name="performer[links_drop][]" value={fp.index} class="hidden" />
                <.icon name="ti-trash" />
              </label>
            </div>
          </.inputs_for>

          <label class="block cursor-pointer text-brand hover:underline">
            <input type="checkbox" name="performer[links_sort][]" class="hidden" />
            + {dgettext("performers", "Add a new link")}
          </label>

          <input type="hidden" name="performer[links_drop][]" />
        </.form_section>

        <:actions>
          <.button
            type={:submit}
            phx-disable-with={gettext("Saving...")}
            class="phx-submit-loading:animate-pulse"
          >
            {gettext("Save")}
          </.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @doc false
  @impl true
  def update(%{performer: performer} = assigns, socket) do
    changeset =
      Performers.change_performer(performer, %{organization_id: assigns.organization.id})

    countries =
      Cldr.Territory.country_codes()
      |> Enum.map(&%{code: &1, name: name_from_territory(&1)})
      |> Enum.filter(&(&1.name != ""))
      |> Enum.sort(&(&1.name < &2.name))

    socket =
      socket
      |> assign(assigns)
      |> assign(form: to_form(changeset))
      |> assign(countries: countries)

    {:ok, socket}
  end

  @doc false
  @impl true
  def handle_event("validate", %{"performer" => performer_params}, socket) do
    form =
      socket.assigns.performer
      |> Performers.change_performer(performer_params)
      |> Map.put(:action, :validate)
      |> to_form()

    {:noreply, assign(socket, form: form)}
  end

  def handle_event("save", %{"performer" => performer_params}, socket) do
    save_performer(socket, socket.assigns.action, performer_params)
  end

  defp save_performer(socket, :create, performer_params) do
    case Performers.create_performer(socket.assigns.organization, performer_params) do
      {:ok, performer} ->
        notify_parent({:created, performer})
        {:noreply, socket}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, form: to_form(changeset))}
    end
  end

  defp save_performer(socket, :edit, performer_params) do
    case Performers.update_performer(socket.assigns.performer, performer_params) do
      {:ok, performer} ->
        notify_parent({:updated, performer})
        {:noreply, socket}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, form: to_form(changeset))}
    end
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})

  defp name_from_territory(territory) do
    case Cldr.Territory.from_territory_code(territory, NyghtWeb.Cldr) do
      {:ok, name} ->
        name

      {:error, {_, err}} ->
        Logger.error("Error with territory translation: #{err}")

        ""
    end
  end
end
