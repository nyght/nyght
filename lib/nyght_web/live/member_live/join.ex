defmodule NyghtWeb.MemberLive.Join do
  @moduledoc """
  This live view allows users to join an organization.
  """
  use NyghtWeb, :live_view

  alias Nyght.Organizations

  @doc false
  @impl true
  def mount(_params, _session, %{assigns: assigns} = socket) do
    org = assigns.current_organization

    socket =
      socket
      |> assign(page_title: dgettext("members", "Join"))
      |> assign(member: Organizations.get_member_for(org, assigns.current_user))

    {:ok, socket}
  end

  @impl true
  def handle_event("member_request", _, socket) do
    {:noreply, request_membership(socket)}
  end

  defp request_membership(%{assigns: assigns} = socket) do
    assigns.current_organization
    |> Organizations.join(assigns.current_user)
    |> case do
      {:ok, _} ->
        socket
        |> push_navigate(to: ~p"/")
        |> put_flash(
          :success,
          dgettext(
            "organizations",
            "Your request has been successfully sent and it will be shortly reviewed!"
          )
        )

      {:error, _} ->
        socket
        |> push_navigate(~p"/")
        |> put_flash(
          :error,
          gettext("Something went wrong, please try again later.")
        )
    end
  end
end
