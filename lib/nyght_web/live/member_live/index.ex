defmodule NyghtWeb.MemberLive.Index do
  @moduledoc """
  This module defines the user administration page.
  """
  use NyghtWeb, :live_view

  import NyghtWeb.MemberLive.MembersComponents

  alias Nyght.Accounts.User
  alias Nyght.Organizations
  alias Nyght.Organizations.Events.MemberRequestCreated
  alias Nyght.Organizations.Member

  @impl true
  def mount(_params, _session, socket) do
    socket = assign(socket, page_title: dgettext("members", "Members"))

    if connected?(socket) do
      Organizations.subscribe(socket.assigns.current_organization)
    end

    {:ok, socket}
  end

  @doc false
  def active_menu_item(_), do: :users

  @impl true
  def handle_params(params, _uri, %{assigns: assigns} = socket) do
    socket =
      socket
      |> assign(:params, params)
      |> apply_action(assigns.live_action, params)

    {:noreply, socket}
  end

  @impl true
  def handle_event("ban", %{"id" => id}, socket) do
    member = Organizations.get_member!(id)

    {:noreply, ban(socket, member)}
  end

  @impl true
  def handle_info({_, %MemberRequestCreated{}}, %{assigns: assigns} = socket) do
    {:noreply, fetch_members(socket, assigns.params)}
  end

  def handle_info(_, socket), do: {:noreply, socket}

  defp ban(socket, member) do
    with :ok <- can(socket, member, :delete),
         {:ok, _} <- Organizations.delete_member(member) do
      socket
      |> put_flash(
        :success,
        dgettext("members", "This member has been successfully removed!")
      )
      |> stream_delete(:members, member)
    else
      {:error, :unauthorized} ->
        put_flash(socket, :error, gettext("You don't have the permission to do that."))

      {:error, _} ->
        put_flash(socket, :error, dgettext("members", "Error while removing this member."))
    end
  end

  defp apply_action(socket, :index, params) do
    socket
    |> enforce_permission(Member, :list)
    |> fetch_members(params)
  end

  defp fetch_members(%{assigns: assigns} = socket, params) do
    %{current_organization: org} = assigns

    opts = [
      preload: [:user, :roles, :categories]
    ]

    case Organizations.list_members(org, params, opts) do
      {:ok, {members, meta}} ->
        socket
        |> assign(:meta, meta)
        |> assign(:filter_form, to_form(meta))
        |> stream(:members, members, reset: true)

      {:error, _meta} ->
        push_navigate(socket, to: ~p"/app/members")
    end
  end

  def member_row_click({_id, member}, current_member) do
    if can?(current_member, Member, :read) do
      JS.navigate(~p"/app/members/#{member.user}")
    else
      JS.navigate(~p"/users/#{member.user}")
    end
  end

  attr :id, :any, required: true
  attr :current_member, :any, required: true
  attr :member, :any, required: true
  attr :rest, :global

  defp member_actions(assigns) do
    ~H"""
    <.dropdown id={"#{@id}-actions"} {@rest}>
      <.icon_button name="ti-dots" />
      <:item :if={can?(@current_member, @member.user, :download)}>
        <.link
          class="inline-block w-full py-2 px-4"
          href={~p"/ical/users/#{@member.user}"}
          target="_blank"
        >
          {dgettext("members", "Download")}
        </.link>
      </:item>

      <:item separator />

      <:item :if={can?(@current_member, @member, :update)}>
        <.link class="inline-block w-full py-2 px-4" navigate={~p"/app/members/#{@member.user}/edit"}>
          {dgettext("members", "Edit")}
        </.link>
      </:item>

      <:item separator />

      <:item :if={can?(@current_member, @member, :delete)}>
        <button class="text-left w-full py-2 px-4" phx-click="ban" phx-value-id={@member.id}>
          {dgettext("members", "Ban")}
        </button>
      </:item>
    </.dropdown>
    """
  end
end
