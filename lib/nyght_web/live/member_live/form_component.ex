defmodule NyghtWeb.MemberLive.FormComponent do
  @moduledoc """
  This module contains the form to edit or create a member.
  """

  use NyghtWeb, :live_component

  alias Nyght.Authorizations
  alias Nyght.Emails
  alias Nyght.Organizations

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.simple_form
        for={@form}
        phx-change="validate"
        phx-submit="submit"
        phx-target={@myself}
        class="w-full mt-6"
      >
        <.multiselect_dropdown
          field={@form[:roles]}
          options={@roles}
          label={dgettext("members", "Roles")}
        />

        <.multiselect_dropdown
          field={@form[:categories]}
          options={@categories}
          label={dgettext("members", "Categories")}
        />

        <div class="mt-6 flex flex-wrap justify-end w-full">
          <.button
            type={:submit}
            phx-disable-with={gettext("Saving...")}
            class="phx-submit-loading:animate-pulse"
          >
            <.icon name="ti-user-check" class="mr-2" />{dgettext("members", "Approve")}
          </.button>
        </div>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(assigns, socket) do
    roles =
      Authorizations.list_roles(assigns.organization)
      |> Enum.map(&{&1.name, &1.id})

    categories =
      Organizations.list_all_member_categories(assigns.organization)
      |> Enum.map(&{&1.name, &1.id})

    socket =
      socket
      |> assign(assigns)
      |> assign(roles: roles)
      |> assign(categories: categories)
      |> assign_form()

    {:ok, socket}
  end

  @impl true
  def handle_event("validate", params, socket) do
    {:noreply, assign_form(socket, normalize_params(params))}
  end

  def handle_event("submit", params, socket) do
    {:noreply, save(socket, socket.assigns.action, normalize_params(params))}
  end

  defp normalize_params(params), do: Map.get(params, "member", %{"roles" => []})

  defp assign_form(%{assigns: assigns} = socket, params \\ %{}) do
    form =
      assigns.member
      |> Organizations.change_member(params)
      |> to_form()

    assign(socket, form: form)
  end

  defp save(%{assigns: assigns, host_uri: host_uri} = socket, :accept_request, params) do
    %{current_member: current_member, organization: org, member: member} = assigns

    with :ok <- Authorizations.can(current_member, member, :approve),
         {:ok, member} <- Organizations.approve_member(member, params) do
      notify_parent({:accepted, member})

      Emails.deliver_request_acceptation_message(
        member.user,
        org,
        url(host_uri, ~p"/app/events")
      )

      socket
      |> put_flash(
        :success,
        dgettext("members", "You have accepted a new staff member!")
      )
      |> push_patch(to: assigns.patch, replace: true)
    else
      {:error, :unauthorized} ->
        socket
        |> put_flash(:error, gettext("You don't have the permission to do that."))
        |> push_patch(to: assigns.patch, replace: true)

      {:error, _} ->
        socket
        |> put_flash(:error, dgettext("members", "Failed to accept the user."))
        |> push_patch(to: assigns.patch, replace: true)
    end
  end

  defp save(%{assigns: assigns} = socket, :edit, params) do
    %{current_member: current_member, member: member} = assigns

    with :ok <- Authorizations.can(current_member, member, :update),
         {:ok, member} <- Organizations.update_member(member, params) do
      notify_parent({:updated, member})

      socket
      |> put_flash(
        :success,
        gettext("This member has been successfully updated!")
      )
      |> push_patch(to: assigns.patch, replace: true)
    else
      {:error, :unauthorized} ->
        socket
        |> put_flash(:error, gettext("You don't have the permission to do that."))
        |> push_patch(to: assigns.patch, replace: true)

      {:error, _} ->
        socket
        |> put_flash(:error, dgettext("members", "Failed to edit this member."))
        |> push_patch(to: assigns.patch, replace: true)
    end
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end
