defmodule NyghtWeb.MemberLive.RolesIndex do
  use NyghtWeb, :live_view

  import NyghtWeb.MemberLive.MembersComponents

  alias Nyght.Authorizations

  alias Nyght.Authorizations.Events.{
    RoleCreated,
    RoleDeleted,
    RoleUpdated
  }

  alias Nyght.Authorizations.Role
  alias Nyght.Organizations

  @impl true
  def mount(params, _session, socket) do
    socket =
      socket
      |> stream(:roles, [])
      |> assign(:params, params)
      |> assign(:page_title, dgettext("members", "Roles"))
      |> enforce_permission(Role, :list)

    if connected?(socket) do
      Organizations.subscribe(socket.assigns.current_organization)
    end

    {:ok, socket}
  end

  @doc false
  def active_menu_item(_), do: :users

  @impl true
  def handle_params(params, _uri, socket) do
    socket =
      socket
      |> assign(:params, params)
      |> assign_roles()

    {:noreply, socket}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    id
    |> Authorizations.get_role!()
    |> Authorizations.delete_role()

    {:noreply, socket}
  end

  @impl true
  def handle_info({_, %RoleCreated{}}, socket) do
    {:noreply, assign_roles(socket)}
  end

  def handle_info({_, %RoleUpdated{role_id: id}}, socket) do
    updated = Authorizations.get_role!(id)

    {:noreply, stream_insert(socket, :roles, updated)}
  end

  def handle_info({_, %RoleDeleted{role: role}}, socket) do
    {:noreply, stream_delete(socket, :roles, role)}
  end

  def handle_info(_, socket), do: {:noreply, socket}

  defp assign_roles(%{assigns: assigns} = socket) do
    %{current_organization: org, params: params} = assigns

    case Authorizations.list_roles_for(org, params) do
      {:ok, {roles, meta}} ->
        socket
        |> assign(:meta, meta)
        |> assign(:filter_form, to_form(meta))
        |> stream(:roles, roles, reset: true)

      {:error, _meta} ->
        push_navigate(socket, to: ~p"/app/members/roles")
    end
  end
end
