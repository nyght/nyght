<.members_header live_action={@live_action} />

<.modal
  :if={@live_action in [:create_category, :edit_category]}
  id="categories-modal"
  show
  on_cancel={JS.patch(~p"/app/members/categories")}
>
  <:title :if={@live_action == :create_category}>
    {dgettext("members", "New member category")}
  </:title>
  <:title :if={@live_action == :edit_category}>
    {dgettext("members", "Edit member category")}
  </:title>
  <.live_component
    id="category-form"
    patch={~p"/app/members/categories"}
    module={CategoryFormComponent}
    action={@live_action}
    category={@category}
    current_user={@current_user}
    current_organization={@current_organization}
  />
</.modal>

<main class="container mx-auto py-12">
  <.empty :if={@meta.total_count == 0} icon="ti-inbox-off">
    <:title>{dgettext("members", "No member categories")}</:title>
    <:subtitle>
      {dgettext("members", "Member categories will be listed here, but for now, there's none")}
    </:subtitle>
  </.empty>

  <div class="flex flex-row justify-between mb-6">
    <div class="flex flex-row gap-2"></div>
    <div class="flex flex-row gap-2">
      <.a
        style={:secondary}
        size={:small}
        patch={~p"/app/members/categories/create"}
        icon="ti-plus"
      >
        {dgettext("members", "New category")}
      </.a>
    </div>
  </div>

  <Flop.Phoenix.table
    :if={@meta.total_count > 0}
    id="categories-list"
    meta={@meta}
    items={@streams.member_categories}
    path={~p"/app/members/categories"}
    opts={table_opts()}
  >
    <:col :let={{_id, category}} field={:name} label={dgettext("members", "name")}>
      <div class="flex flex-row justify-between items-baseline">
        <.a patch={~p"/app/members/categories/#{category}/edit"}>
          {category.name}
        </.a>
        <.icon_button
          phx-click="delete"
          phx-value-id={category.id}
          name="ti-trash"
          class="lg:hidden"
        />
      </div>
    </:col>

    <:action
      :let={{_id, category}}
      tbody_td_attrs={[class: "py-3 px-4 hidden lg:flex lg:flex-none"]}
      col_class="lg:w-16"
    >
      <.icon_button
        phx-click="delete"
        phx-value-id={category.id}
        name="ti-trash"
        class="hidden lg:block"
      />
    </:action>
  </Flop.Phoenix.table>
</main>
