defmodule NyghtWeb.MemberLive.RoleForm do
  @moduledoc """
  This module defines the role administration page.
  """
  use NyghtWeb, :live_view

  alias Nyght.Authorizations
  alias Nyght.Authorizations.Role

  @impl true
  def mount(_params, _session, socket) do
    perms =
      Authorizations.list_permissions()
      |> Enum.map(&{resource_title(&1.resource), {to_string(&1.action), &1.id}})
      |> Enum.group_by(fn {resource, _} -> resource end, fn {_, perm} -> perm end)

    {:ok, assign(socket, permissions: perms)}
  end

  @doc false
  def active_menu_item(_), do: :users

  @impl true
  def handle_params(params, _uri, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(%{assigns: assigns} = socket, :create, _params) do
    changeset =
      Authorizations.change_role(%Role{}, %{organization_id: assigns.current_organization.id})

    socket
    |> enforce_permission(Role, :create)
    |> assign(role: %Role{})
    |> assign(form: to_form(changeset))
    |> assign(page_title: dgettext("roles", "Create a new role"))
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    role = Authorizations.get_role!(id)
    changeset = Authorizations.change_role(role)

    socket
    |> enforce_permission(role, :update)
    |> assign(role: role)
    |> assign(form: to_form(changeset))
    |> assign(page_title: dgettext("roles", "Edit a role"))
  end

  @impl true
  def handle_event("validate", %{"role" => params}, %{assigns: assigns} = socket) do
    form =
      assigns.role
      |> Authorizations.change_role(params)
      |> Map.put(:action, :validate)
      |> to_form()

    {:noreply, assign(socket, form: form)}
  end

  def handle_event("save", %{"role" => params}, %{assigns: assigns} = socket) do
    {:noreply, save(socket, assigns.live_action, params)}
  end

  defp save(%{assigns: %{role: role}} = socket, :edit, params) do
    with :ok <- can(socket, role, :update),
         {:ok, _role} <- Authorizations.update_role(role, params) do
      socket
      |> put_flash(:success, dgettext("roles", "The role has been saved!"))
      |> push_navigate(to: ~p"/app/members/roles")
    else
      {:error, :unauthorized} ->
        put_flash(socket, :error, gettext("You don't have the permission to do that."))

      {:error, changeset} ->
        assign(socket, form: to_form(changeset))
    end
  end

  defp save(%{assigns: assigns} = socket, :create, params) do
    with :ok <- can(socket, Role, :create),
         {:ok, _role} <- Authorizations.create_role(assigns.current_organization, params) do
      socket
      |> put_flash(:success, dgettext("roles", "The role has been created!"))
      |> push_navigate(to: ~p"/app/members/roles")
    else
      {:error, :unauthorized} ->
        put_flash(socket, :error, gettext("You don't have the permission to do that."))

      {:error, changeset} ->
        assign(socket, form: to_form(changeset))
    end
  end

  defp resource_title(resource) do
    resource
    |> Atom.to_string()
    |> String.split(".")
    |> List.last()
  end
end
