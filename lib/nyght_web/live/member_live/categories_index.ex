defmodule NyghtWeb.MemberLive.CategoriesIndex do
  use NyghtWeb, :live_view

  import NyghtWeb.MemberLive.MembersComponents

  alias Nyght.Organizations

  alias Nyght.Organizations.Events.{
    MemberCategoryCreated,
    MemberCategoryDeleted,
    MemberCategoryUpdated
  }

  alias Nyght.Organizations.MemberCategory
  alias NyghtWeb.MemberLive.CategoryFormComponent

  @impl true
  def mount(params, _session, socket) do
    socket =
      socket
      |> stream(:member_categories, [])
      |> assign(:params, params)
      |> assign(page_title: dgettext("members", "Member categories"))
      |> enforce_permission(MemberCategory, :read)

    if connected?(socket) do
      Organizations.subscribe(socket.assigns.current_organization)
    end

    {:ok, socket}
  end

  @impl true
  def handle_params(params, _uri, socket) do
    socket =
      socket
      |> assign(:params, params)
      |> assign_current_category(params)
      |> assign_member_categories()

    {:noreply, socket}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    id
    |> Organizations.get_member_category!()
    |> Organizations.delete_member_category()

    {:noreply, socket}
  end

  @impl true
  def handle_info({_, %MemberCategoryCreated{}}, socket) do
    {:noreply, assign_member_categories(socket)}
  end

  def handle_info({_, %MemberCategoryUpdated{category_id: id}}, socket) do
    updated = Organizations.get_member_category!(id)

    {:noreply, stream_insert(socket, :member_categories, updated)}
  end

  def handle_info({_, %MemberCategoryDeleted{category: category}}, socket) do
    {:noreply, stream_delete(socket, :member_categories, category)}
  end

  def handle_info(_, socket), do: {:noreply, socket}

  def active_menu_item(_), do: :users

  defp assign_current_category(socket, %{"id" => id}) do
    assign(socket, :category, Organizations.get_member_category!(id))
  end

  defp assign_current_category(socket, _) do
    assign(socket, :category, %MemberCategory{})
  end

  defp assign_member_categories(%{assigns: assigns} = socket) do
    %{current_organization: org, params: params} = assigns

    case Organizations.list_member_categories_for(org, params) do
      {:ok, {members, meta}} ->
        socket
        |> assign(:meta, meta)
        |> assign(:filter_form, to_form(meta))
        |> stream(:member_categories, members, reset: true)

      {:error, _meta} ->
        push_navigate(socket, to: ~p"/app/members/categories")
    end
  end
end
