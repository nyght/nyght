defmodule NyghtWeb.MemberLive.RequestsIndex do
  use NyghtWeb, :live_view

  import NyghtWeb.MemberLive.MembersComponents

  alias Nyght.Accounts.User
  alias Nyght.Organizations
  alias Nyght.Organizations.Events.MemberRequestCreated
  alias Nyght.Organizations.Member

  @impl true
  def mount(params, _session, socket) do
    socket =
      socket
      |> stream(:member_requests, [])
      |> assign(:params, params)
      |> assign(:page_title, dgettext("members", "Requests"))
      |> enforce_permission(Member, :approve)

    if connected?(socket) do
      Organizations.subscribe(socket.assigns.current_organization)
    end

    {:ok, socket}
  end

  @doc false
  def active_menu_item(_), do: :users

  @impl true
  def handle_params(params, _uri, socket) do
    {:noreply, apply_action(socket, params, socket.assigns.live_action)}
  end

  @impl true
  def handle_event("ignore_request", %{"id" => id}, socket) do
    request = Organizations.get_member!(id)

    {:noreply, ignore_request(socket, request)}
  end

  @impl true
  def handle_info({_, %MemberRequestCreated{}}, socket) do
    {:noreply, assign_member_requests(socket)}
  end

  def handle_info(_, socket), do: {:noreply, socket}

  defp apply_action(socket, params, :requests) do
    socket
    |> assign(:params, params)
    |> assign_current_member_request(params)
    |> assign_member_requests()
  end

  defp apply_action(socket, params, :request) do
    assign_current_member_request(socket, params)
  end

  defp assign_member_requests(%{assigns: assigns} = socket) do
    %{current_organization: org, params: params} = assigns

    opts = [
      preload: [:user, :roles]
    ]

    case Organizations.list_member_requests(org, params, opts) do
      {:ok, {members, meta}} ->
        socket
        |> assign(:meta, meta)
        |> assign(:filter_form, to_form(meta))
        |> stream(:member_requests, members, reset: true)

      {:error, _meta} ->
        push_navigate(socket, to: ~p"/app/members/requests")
    end
  end

  defp assign_current_member_request(socket, %{"id" => id}) do
    assign(socket, :request, Organizations.get_member!(id))
  end

  defp assign_current_member_request(socket, _) do
    assign(socket, :request, %Member{})
  end

  defp ignore_request(socket, request) do
    with :ok <- can(socket, request, :reject),
         {:ok, _} <- Organizations.reject_member(request) do
      socket
      |> put_flash(:success, dgettext("members", "The request has been ignored."))
      |> stream_delete(:member_requests, request)
    else
      {:error, :unauthorized} ->
        put_flash(socket, :error, gettext("You don't have the permission to do that."))

      {:error, _} ->
        put_flash(socket, :error, dgettext("members", "Failed to ignore the request."))
    end
  end
end
