defmodule NyghtWeb.MemberLive.MembersComponents do
  use NyghtWeb, :html

  attr :live_action, :any, required: true

  def members_header(assigns) do
    ~H"""
    <.page_header>
      {dgettext("members", "User Management")}

      <:tab_bar>
        <.tab patch={~p"/app/members"} active={@live_action == :index}>
          {dgettext("members", "Members")}
        </.tab>
        <.tab navigate={~p"/app/members/requests"} active={@live_action == :requests}>
          {dgettext("members", "Requests")}
        </.tab>
        <.tab navigate={~p"/app/members/categories"} active={@live_action == :categories}>
          {dgettext("members", "Categories")}
        </.tab>
        <.tab navigate={~p"/app/members/roles"} active={@live_action == :roles}>
          {dgettext("members", "Roles")}
        </.tab>
      </:tab_bar>
    </.page_header>
    """
  end
end
