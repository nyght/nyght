defmodule NyghtWeb.MemberLive.CategoryFormComponent do
  use NyghtWeb, :live_component

  alias Ecto.Changeset
  alias Nyght.Authorizations
  alias Nyght.Organizations
  alias Nyght.Organizations.MemberCategory

  @impl true
  def update(assigns, socket) do
    socket =
      socket
      |> assign(assigns)
      |> assign_new(:form, fn ->
        to_form(Organizations.change_member_category(assigns.category))
      end)

    {:ok, socket}
  end

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.simple_form id={@id} for={@form} phx-submit="save" phx-change="validate" phx-target={@myself}>
        <.input field={@form[:name]} label={gettext("Name")} />
        <:actions>
          <.button type={:submit} phx-disable-with={gettext("Saving...")}>
            {dgettext("members", "Save category")}
          </.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def handle_event("save", %{"member_category" => params}, %{assigns: assigns} = socket) do
    {:noreply, save_category(socket, assigns.action, params)}
  end

  def handle_event("validate", %{"member_category" => params}, socket) do
    form =
      %MemberCategory{}
      |> Organizations.change_member_category(params)
      |> to_form(action: :validate)

    {:noreply, assign(socket, form: form)}
  end

  defp save_category(%{assigns: assigns} = socket, :create_category, params) do
    with :ok <- Authorizations.can(assigns.current_member, MemberCategory, :create),
         {:ok, _category} <-
           Organizations.create_member_category(assigns.current_organization, params) do
      socket
      |> put_flash(:success, dgettext("members", "New member category successfully created!"))
      |> push_patch(to: socket.assigns.patch)
    else
      {:error, :unauthorized} ->
        socket
        |> put_flash(:error, gettext("You don't have the permission to do that."))
        |> push_patch(to: socket.assigns.patch)

      {:error, %Changeset{} = changeset} ->
        assign(socket, form: to_form(changeset))
    end
  end

  defp save_category(%{assigns: assigns} = socket, :edit_category, params) do
    with :ok <- Authorizations.can(assigns.current_member, MemberCategory, :update),
         {:ok, _category} <-
           Organizations.update_member_category(assigns.category, params) do
      socket
      |> put_flash(:success, dgettext("members", "Member category successfully updated!"))
      |> push_patch(to: socket.assigns.patch)
    else
      {:error, :unauthorized} ->
        socket
        |> put_flash(:error, gettext("You don't have the permission to do that."))
        |> push_patch(to: socket.assigns.patch)

      {:error, %Changeset{} = changeset} ->
        assign(socket, form: to_form(changeset))
    end
  end
end
