defmodule NyghtWeb.Hooks.NavigationHook do
  @moduledoc """
  This module contains the hook for navigation.
  """
  import Phoenix.Component
  import Phoenix.LiveView

  @doc false
  def on_mount(:default, _params, _session, socket) do
    socket = attach_hook(socket, :active_page, :handle_params, &set_active_page/3)

    {:cont, socket}
  end

  defp set_active_page(_params, _url, socket) do
    socket =
      socket
      |> assign_new(:hide_navigation, fn -> false end)
      |> assign_new(:meta_attrs, fn -> [] end)

    if Keyword.has_key?(socket.view.__info__(:functions), :active_menu_item) do
      module = socket.view
      item = module.active_menu_item(socket.assigns.live_action)

      {:cont, assign(socket, active_page: item)}
    else
      {:cont, assign(socket, active_page: nil)}
    end
  end
end
