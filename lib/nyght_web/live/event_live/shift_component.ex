defmodule NyghtWeb.EventLive.ShiftComponent do
  @moduledoc """
  This component displays a shift information and the members working.
  """
  use NyghtWeb, :live_component

  alias Nyght.Accounts.User
  alias Nyght.Authorizations
  alias Nyght.Organizations.Member
  alias Nyght.Shifts
  alias Nyght.Shifts.Shift
  alias Nyght.Workers.ShiftApplicationUpdate

  @impl true
  def render(assigns) do
    ~H"""
    <div id={@id} class="w-full rounded-md flex flex-col border-stone-300 shadow-xs border my-4">
      <.header
        level="h5"
        class="py-2 px-4 cursor-pointer"
        phx-click={toggle_details(@id)}
        phx-target={@myself}
      >
        {@shift.title}, <.time_interval from={@shift.local_starts_at} to={@shift.local_ends_at} />

        <:subtitle>
          <span class="flex flex-row items-center">
            <.pill icon={
              if @shift.application_strategy == :fcfs, do: "ti-hand-finger", else: "ti-checks"
            } />
            <span :if={Shift.has_max?(@shift)}>
              {dngettext(
                "shifts",
                "%{count} application out of %{total} openings",
                "%{count} applications out of %{total} openings",
                @shift.opening_count,
                total: @shift.total_openings
              )}
            </span>
            <span :if={not Shift.has_max?(@shift)}>
              {dngettext(
                "shifts",
                "%{count} application, no maximum openings",
                "%{count} applications, no maximum openings",
                @shift.opening_count
              )}
            </span>
          </span>
        </:subtitle>

        <:actions>
          <.dropdown id={"shift-#{@shift.id}-actions"}>
            <.icon_button name="ti-dots" />

            <:item :if={can?(@current_member, @shift, :update)}>
              <.link
                class="inline-block w-full py-2 px-4"
                patch={~p"/app/events/#{@shift.event}/shifts/#{@shift}/edit"}
                replace={true}
              >
                {gettext("Edit")}
              </.link>
            </:item>
            <:item :if={can?(@current_member, @shift, :create)}>
              <.link class="inline-block w-full py-2 px-4" phx-click="copy_shift" phx-target={@myself}>
                {gettext("Copy")}
              </.link>
            </:item>
            <:item separator />
            <:item :if={can?(@current_member, @shift, :delete)}>
              <button class="text-left w-full py-2 px-4" phx-click="delete_shift" phx-target={@myself}>
                {gettext("Delete")}
              </button>
            </:item>
          </.dropdown>
          <.icon_button
            id={"#{@id}-caret"}
            name="ti-caret-down"
            icon_class="transition-all toggle-button"
            phx-click={toggle_details(@id)}
            phx-target={@myself}
          />
          <.icon_button
            :if={
              can?(@current_member, @shift, :apply) &&
                Authorizations.has_role?(@current_member, @shift.role) &&
                Shifts.accepted_or_not_applied?(@current_member, @shift) &&
                not Shifts.applied?(@current_member, @shift)
            }
            name="ti-user-check"
            phx-click="apply_shift"
            phx-target={@myself}
          />

          <.icon_button
            :if={
              can?(@current_member, @shift, :leave) &&
                Authorizations.has_role?(@current_member, @shift.role) &&
                Shifts.applied?(@current_member, @shift)
            }
            name="ti-user-x"
            phx-click="leave_shift"
            phx-target={@myself}
            disabled={not Shifts.accepted_or_not_applied?(@current_member, @shift)}
          />
        </:actions>
      </.header>

      <div :if={not is_nil(@shift.description)} class="px-4 py-4 rounded-b-md">
        {@shift.description}
      </div>

      <div id={"#{@id}-details"} class="hidden border-t toggle-body">
        <.empty :if={Enum.empty?(@shift.shift_applications)} icon="ti-user-off">
          <:title>{dgettext("shifts", "Nobody has applied yet!")}</:title>
          <:subtitle>
            {dgettext(
              "shifts",
              "Be the first to work for this event by clicking the apply button!"
            )}
          </:subtitle>
        </.empty>

        <.table
          :if={not Enum.empty?(@shift.shift_applications)}
          id={"#{@id}-table"}
          rows={@shift.shift_applications}
          row_click={&member_row_click(&1, @current_member)}
          headerless
        >
          <:col :let={%{member: member} = shift_application} label={gettext("user")}>
            <div class={if shift_application.status == :rejected, do: "opacity-30", else: ""}>
              <.avatar
                name={
                  if member,
                    do:
                      User.full_name(member.user,
                        redacted?: !can?(@current_member, member.user, :read_details)
                      ),
                    else: dgettext("shifts", "Deleted user")
                }
                labeled
              >
                <%= if can?(@current_member, Member, :read) do %>
                  <span class="divide-dot">
                    <span>
                      {if not is_nil(member),
                        do: member.user.email,
                        else: dgettext("shifts", "Deleted user")}
                    </span>
                    <span :if={not is_nil(member) and not is_nil(member.user.pronouns)}>
                      {member.user.pronouns}
                    </span>
                  </span>
                <% else %>
                  {if not is_nil(member) and not is_nil(member.user.pronouns),
                    do: member.user.pronouns}
                <% end %>
              </.avatar>
            </div>
          </:col>

          <:col :let={%{member: member} = shift_application} label={gettext("category")}>
            <div
              :if={member}
              class={if shift_application.status == :rejected, do: "opacity-30", else: ""}
            >
              <.pill :for={cat <- member.categories} text={cat.name} />
            </div>
          </:col>

          <:col :let={shift_application} label={dgettext("shifts", "application time")}>
            <.datetime
              timezone={@shift.event.timezone}
              datetime={shift_application.updated_at}
              class={if shift_application.status == :rejected, do: "opacity-30", else: ""}
              shift
            />
          </:col>

          <:col :let={shift_application} label={gettext("status")}>
            <.pill
              :if={shift_application.status == :accepted}
              kind={:success}
              icon="ti-check"
              text={dgettext("shifts", "Accepted")}
            />
            <.pill
              :if={shift_application.status == :pending}
              kind={:warning}
              icon="ti-question-mark"
              text={dgettext("shifts", "Pending")}
            />
            <.pill
              :if={shift_application.status == :waiting_replacement}
              kind={:warning}
              icon="ti-exclamation-mark"
              text={dgettext("shifts", "Waiting for replacement")}
            />
            <.pill
              :if={shift_application.status == :rejected}
              kind={:danger}
              class="opacity-30"
              icon="ti-x"
              text={dgettext("shifts", "Rejected")}
            />
          </:col>

          <:action :let={application}>
            <span class="flex flex-row flex-nowrap justify-end h-fit">
              <.icon_button
                :if={
                  (can?(@current_member, application, :update) && application.status == :rejected) or
                    application.status == :pending
                }
                phx-click="accept_shift_application"
                phx-value-id={application.id}
                phx-target={@myself}
                name="ti-user-check"
              />

              <.icon_button
                :if={can?(@current_member, application, :delete) && application.status != :rejected}
                phx-click="delete_shift_application"
                phx-value-id={application.id}
                phx-target={@myself}
                name="ti-user-minus"
              />
            </span>
          </:action>

          <:footer>
            <tr :if={!Shift.has_max?(@shift) or Shift.free_openings(@shift) > 0}>
              <td class="text-center border-t border-b border-dashed py-4 pr-6 pl-2" colspan="4">
                <span :if={Shift.has_max?(@shift)}>
                  {dngettext(
                    "shifts",
                    "There's still %{count} opening left.",
                    "There's still %{count} openings left.",
                    Shift.free_openings(@shift)
                  )}
                </span>
                <span :if={not Shift.has_max?(@shift)}>
                  {dgettext("shifts", "There's no required staff count")}
                </span>
              </td>
            </tr>
          </:footer>
        </.table>

        <div class="flex flex-row justify-end gap-x-3 mt-4 p-3 items-center">
          <.a
            :if={can?(@current_member, Shift, :assign) && Shift.free_openings(@shift) != 0}
            patch={~p"/app/events/#{@shift.event}/shifts/#{@shift}/assign"}
            replace={true}
          >
            {dgettext("shifts", "Assign someone")}
          </.a>

          <.button
            :if={
              can?(@current_member, @shift, :apply) &&
                Authorizations.has_role?(@current_member, @shift.role) &&
                Shifts.accepted_or_not_applied?(@current_member, @shift) &&
                !Shifts.applied?(@current_member, @shift)
            }
            class="w-full md:w-fit"
            icon="ti-user-check"
            phx-click="apply_shift"
            phx-target={@myself}
          >
            {dgettext("shifts", "Apply!")}
          </.button>

          <.button
            :if={
              can?(@current_member, @shift, :leave) &&
                Authorizations.has_role?(@current_member, @shift.role) &&
                Shifts.applied?(@current_member, @shift)
            }
            class="w-full md:w-fit"
            icon="ti-user-x"
            phx-click="leave_shift"
            phx-target={@myself}
            disabled={!Shifts.accepted_or_not_applied?(@current_member, @shift)}
          >
            {dgettext("shifts", "Leave")}
          </.button>
        </div>
      </div>
    </div>
    """
  end

  @impl true
  def update(assigns, socket) do
    {:ok, assign(socket, assigns)}
  end

  @impl true
  def handle_event("delete_shift", _, %{assigns: assigns} = socket) do
    with :ok <- can(socket, assigns.shift, :delete),
         {:ok, _} <- Shifts.delete_shift(assigns.shift) do
      {:noreply, socket}
    else
      {:error, _} = err ->
        {:noreply, handle_error(err, socket)}
    end
  end

  def handle_event("leave_shift", _, %{assigns: assigns} = socket) do
    with :ok <- can(socket, assigns.shift, :leave),
         {:ok, _} <- Shifts.leave_shift(assigns.shift, assigns.current_member) do
      {:noreply, socket}
    else
      {:error, _} = err ->
        {:noreply, handle_error(err, socket)}
    end
  end

  def handle_event("apply_shift", _, %{assigns: assigns} = socket) do
    with :ok <- can(socket, assigns.shift, :apply),
         {:ok, _} <- Shifts.apply_for_shift(assigns.shift, assigns.current_member) do
      {:noreply, socket}
    else
      {:error, :user_in_deletion} ->
        {:noreply,
         put_flash(
           socket,
           :error,
           dgettext("shifts", "Your account is about to be deleted. You can't join any shift.")
         )}

      {:error, :full_shift} ->
        {:noreply, put_flash(socket, :error, dgettext("shifts", "This shift is full."))}

      {:error, _} = err ->
        {:noreply, handle_error(err, socket)}
    end
  end

  def handle_event("delete_shift_application", %{"id" => id}, socket) do
    shift_application = Shifts.get_shift_application!(id)

    with :ok <- can(socket, shift_application, :delete),
         {:ok, _} <- Shifts.delete_shift_application(shift_application) do
      {:noreply,
       put_flash(
         socket,
         :success,
         dgettext("shifts", "The user has been successfully removed from this shift!")
       )}
    else
      {:error, _} = err ->
        {:noreply, handle_error(err, socket)}
    end
  end

  def handle_event("accept_shift_application", %{"id" => id}, socket) do
    shift_application = Shifts.get_shift_application!(id)

    with :ok <- can(socket, shift_application, :update),
         {:ok, _} <- Shifts.accept_shift_application(shift_application),
         {:ok, _} <- schedule_email(shift_application.member_id) do
      {:noreply,
       put_flash(
         socket,
         :success,
         dgettext("shifts", "The user has been successfully accepted from this shift!")
       )}
    else
      {:error, _} = err ->
        {:noreply, handle_error(err, socket)}
    end
  end

  def handle_event("copy_shift", _, %{assigns: assigns} = socket) do
    %{event: event, shift: shift} = assigns

    with :ok <- can(socket, shift, :create),
         {:ok, _} <- Shifts.copy_shift(shift, event) do
      {:noreply,
       put_flash(
         socket,
         :success,
         dgettext("shifts", "The shift has been copied!")
       )}
    else
      {:error, _} = err ->
        {:noreply, handle_error(err, socket)}
    end
  end

  def member_row_click(%{member: %Member{} = member}, current_member) do
    if can?(current_member, Member, :read) do
      JS.navigate(~p"/app/members/#{member}")
    else
      JS.navigate(~p"/users/#{member.user_id}")
    end
  end

  def member_row_click(%{member: nil}, _), do: %JS{}

  def toggle_details(id, js \\ %JS{}) do
    js
    |> JS.toggle_class("hidden", to: "##{id}-details")
    |> JS.toggle_class("rotate-180", to: "##{id}-caret > span")
  end

  defp handle_error({:error, :unauthorized}, socket),
    do: put_flash(socket, :error, gettext("You don't have the permission to do that."))

  defp handle_error({:error, _}, socket),
    do: put_flash(socket, :error, gettext("Something went wrong, please try again later."))

  defp schedule_email(member_id) do
    %{member_id: member_id}
    |> ShiftApplicationUpdate.new(
      schedule_in: ShiftApplicationUpdate.update_delay(),
      replace: [scheduled: [:scheduled_at]]
    )
    |> Oban.insert()
  end
end
