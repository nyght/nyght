defmodule NyghtWeb.EventLive.EventShiftStatusComponent do
  @moduledoc """
  This component shows the progress of an event's shifts.
  """
  use NyghtWeb, :live_component

  alias Nyght.Shifts
  alias Nyght.Shifts.Shift
  alias Phoenix.LiveView.JS

  @impl true
  def render(assigns) do
    ~H"""
    <div id={@id} class={@class}>
      <%= if not Enum.empty?(@shifts) do %>
        <.progress
          progress={@stats}
          label={dgettext("events", "Total")}
          phx-click={JS.toggle(to: "#event-#{@event_id}-shifts-details")}
        >
          <:completed>
            <.icon name="ti-circle-check" class="mr-2" />{dgettext("events", "Complete!")}
          </:completed>
        </.progress>

        <div id={"event-#{@event_id}-shifts-details"} class="hidden">
          <hr class="my-4" />
          <.progress
            :for={shift <- @shifts}
            progress={{shift.opening_count, shift.total_openings}}
            label={shift.title}
            class="mt-2"
          />
        </div>
      <% else %>
        <p class="text-gray-500">{dgettext("events", "No shifts")}</p>
      <% end %>
    </div>
    """
  end

  @impl true
  def update_many(list_of_assigns) do
    shifts =
      list_of_assigns
      |> Enum.map(fn {assigns, _socket} -> assigns.event_id end)
      |> Shifts.list_shifts_for_events()
      |> Enum.group_by(& &1.event_id)

    Enum.map(list_of_assigns, fn {assigns, socket} ->
      socket
      |> assign(assigns)
      |> assign(:shifts, shifts[assigns.event_id] || [])
      |> assign_new(:class, fn -> "" end)
      |> assign_progress()
    end)
  end

  defp assign_progress(%{assigns: assigns} = socket) do
    stats = Enum.reduce(assigns.shifts, {0, 0}, &reduce_stats/2)

    assign(socket, :stats, stats)
  end

  defp reduce_stats(%Shift{total_openings: nil, opening_count: count}, {filled, total}) do
    {filled + count, total + count}
  end

  defp reduce_stats(%Shift{total_openings: total_openings, opening_count: count}, {filled, total}) do
    {filled + count, total + total_openings}
  end
end
