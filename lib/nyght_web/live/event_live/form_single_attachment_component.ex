defmodule NyghtWeb.EventLive.FormSingleAttachmentComponent do
  @moduledoc """
  This component is the form used to edit a single attachment.
  """
  require Logger
  use NyghtWeb, :live_component

  alias Nyght.Authorizations
  alias Nyght.Events

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.simple_form for={@form} phx-submit="save" phx-change="validate" phx-target={@myself}>
        <.input field={@form[:file_name]} label={dgettext("events", "File name")} />
        <.multiselect_dropdown
          field={@form[:roles]}
          label={dgettext("events", "Roles")}
          options={@roles}
        />

        <.button type={:submit}>
          {gettext("Save")}
        </.button>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{attachment: attachment} = assigns, socket) do
    changeset = Events.change_attachment(attachment)

    roles =
      Authorizations.list_roles(assigns.current_organization)
      |> Enum.map(&{&1.name, &1.id})

    socket =
      socket
      |> assign(assigns)
      |> assign(form: to_form(changeset))
      |> assign(roles: roles)

    {:ok, socket}
  end

  @impl true
  def handle_event("validate", %{"attachment" => params}, %{assigns: assigns} = socket) do
    form =
      assigns.attachment
      |> Events.change_attachment(params)
      |> Map.put(:action, :validate)
      |> to_form()

    {:noreply, assign(socket, :form, form)}
  end

  def handle_event("save", %{"attachment" => params}, %{assigns: assigns} = socket) do
    with :ok <- Authorizations.can(assigns.current_member, assigns.attachment, :update),
         {:ok, attachment} <- Events.update_attachment(assigns.attachment, params) do
      notify_parent({:updated, attachment})

      socket =
        socket
        |> put_flash(:success, dgettext("events", "Attachment successfully updated!"))
        |> push_patch(to: assigns.patch, replace: true)

      {:noreply, socket}
    else
      {:error, :unauthorized} ->
        socket
        |> put_flash(:error, dgettext("events", "You don't have the permission to do that."))
        |> push_patch(to: assigns.patch, replace: true)

      {:error, changeset} ->
        assign(socket, changeset: changeset)
    end
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end
