defmodule NyghtWeb.EventLive.EventStaff do
  use NyghtWeb, :live_view

  alias Nyght.Accounts.User
  alias Nyght.Events.Events.{EventDeleted, EventUpdated}
  alias Nyght.Shifts.Events.{ShiftCreated, ShiftDeleted, ShiftUpdated}
  alias Nyght.Shifts.Shift
  alias Nyght.{Authorizations, Events, Shifts}

  alias NyghtWeb.EventLive.EventsComponents

  @impl true
  def mount(%{"id" => event_id} = params, _session, socket) do
    %{current_member: member} = socket.assigns

    member? = !is_nil(member)
    max_status = Authorizations.get_max_event_status(member)

    event =
      Events.get_event!(event_id,
        status: max_status,
        members_only?: member?,
        preload: [:prices, [bookings: :performer], :organization, :types]
      )

    neighbors =
      Events.get_neighbors(event, status: max_status, members_only?: member?)

    if connected?(socket) do
      Events.subscribe(event)
    end

    socket =
      socket
      # |> assign_metadata(event)
      |> assign(page_title: event.name)
      |> assign(event: event)
      |> assign(neighbors: neighbors)
      |> assign_shifts(params)
      |> stream(:attachments, [])
      |> enforce_permission(event, :read)

    {:ok, socket}
  end

  @doc false
  def active_menu_item(_), do: :events

  @impl true
  def handle_params(params, _uri, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  ##################################################
  # Events
  ##################################################
  @impl true
  def handle_event("keyboard_shortcut", %{"key" => "ArrowRight"}, %{assigns: assigns} = socket) do
    {:noreply, navigate_to_event(socket, assigns.neighbors.next_event_id)}
  end

  def handle_event("keyboard_shortcut", %{"key" => "ArrowLeft"}, %{assigns: assigns} = socket) do
    {:noreply, navigate_to_event(socket, assigns.neighbors.prev_event_id)}
  end

  def handle_event(
        "keyboard_shortcut",
        %{"ctrlKey" => true, "key" => "e"},
        %{assigns: assigns} = socket
      ) do
    {:noreply, push_navigate(socket, to: ~p"/app/events/#{assigns.event}/edit")}
  end

  def handle_event("keyboard_shortcut", _, socket) do
    {:noreply, socket}
  end

  def handle_event("toggle_shifts", _, socket) do
    {:noreply, update(socket, :shifts_folded, &(not &1))}
  end

  def handle_event("toggle_show_all", _, socket) do
    {:noreply, update(socket, :shifts_show_all, &(not &1))}
  end

  ##################################################
  # Messages
  ##################################################
  @impl true
  def handle_info({_, %EventUpdated{event: event}}, socket) do
    {:noreply, assign(socket, :event, event)}
  end

  def handle_info({_, %EventDeleted{}}, socket) do
    socket =
      socket
      |> put_flash(:error, gettext("This event has been deleted."))
      |> push_navigate(to: ~p"/")

    {:noreply, socket}
  end

  def handle_info({_, %ShiftCreated{}}, socket) do
    {:noreply, assign_shifts(socket)}
  end

  def handle_info({_, %ShiftUpdated{}}, socket) do
    {:noreply, assign_shifts(socket)}
  end

  def handle_info({_, %ShiftDeleted{}}, socket) do
    {:noreply, assign_shifts(socket)}
  end

  def handle_info(_, socket), do: {:noreply, socket}

  ##################################################
  # Implementation
  ##################################################
  defp navigate_to_event(socket, nil), do: socket

  defp navigate_to_event(socket, event_id) do
    push_navigate(socket, to: ~p"/app/events/#{event_id}/staff", replace: true)
  end

  def assign_shifts(%{assigns: assigns} = socket, params \\ %{}) do
    %{current_member: member, event: event} = assigns

    with {:ok, {shifts, _}} <- Shifts.list_shifts_for(event, params) do
      shifts =
        shifts
        |> Enum.group_by(fn %Shift{role: role} ->
          if Authorizations.has_role?(member, role), do: :for_user, else: :others
        end)
        |> Map.put_new(:for_user, [])
        |> Map.put_new(:others, [])

      show_all = Authorizations.can?(member, Shift, :assign)

      socket
      |> assign(shifts: shifts)
      |> assign(shifts_folded: true)
      |> assign(shifts_show_all: show_all)
    end
  end

  defp apply_action(socket, :edit_shift, %{"shift_id" => shift_id}) do
    assign(socket, shift: Shifts.get_shift!(shift_id))
  end

  defp apply_action(socket, :assign, %{"shift_id" => shift_id}) do
    assign(socket, shift: Shifts.get_shift!(shift_id))
  end

  defp apply_action(socket, _, _params), do: socket

  def fold_shifts(true) do
    %JS{}
    |> JS.remove_class("hidden", to: ".toggle-body")
    |> JS.add_class("rotate-180", to: ".toggle-button")
    |> JS.push("toggle_shifts")
  end

  def fold_shifts(false) do
    %JS{}
    |> JS.add_class("hidden", to: ".toggle-body")
    |> JS.remove_class("rotate-180", to: ".toggle-button")
    |> JS.push("toggle_shifts")
  end
end
