defmodule NyghtWeb.EventLive.IndexList do
  @moduledoc """
  This module defines the event administration page.
  """
  use NyghtWeb, :live_view

  alias Nyght.Authorizations
  alias Nyght.Events
  alias Nyght.Events.Event
  alias Nyght.Organizations
  alias Nyght.Shifts.Events.{ShiftCreated, ShiftDeleted, ShiftUpdated}
  alias Phoenix.LiveView.JS

  @impl true
  def mount(_params, _session, socket) do
    org = socket.assigns.current_organization

    if connected?(socket), do: Organizations.subscribe(org)

    socket =
      socket
      |> enforce_permission(Event, :list)
      |> assign(:page_title, dgettext("events", "Events"))
      |> stream(:events, [])

    {:ok, socket}
  end

  @doc false
  def active_menu_item(_), do: :events

  @impl true
  def handle_params(params, _uri, socket) do
    socket =
      socket
      |> stream(:events, [], reset: true)
      |> fetch_events(params)

    {:noreply, socket}
  end

  @impl true
  def handle_event("update-filters", params, socket) do
    params = Map.delete(params, "_target")
    {:noreply, push_patch(socket, to: tab_url(socket.assigns.live_action, params))}
  end

  def handle_event("unpublish", %{"event-id" => event_id}, socket) do
    event = Events.get_event!(event_id, preload: [:types, :shifts])

    {:noreply, update_event(socket, event, &Events.unpublish_event/1)}
  end

  def handle_event("publish", %{"event-id" => event_id}, socket) do
    event = Events.get_event!(event_id, preload: [:types, :shifts])

    {:noreply, update_event(socket, event, &Events.publish_event/1)}
  end

  def handle_event("prepublish", %{"event-id" => event_id}, socket) do
    event = Events.get_event!(event_id, preload: [:types, :shifts])

    {:noreply, update_event(socket, event, &Events.prepublish_event/1)}
  end

  def handle_event("cancel", %{"event-id" => event_id}, socket) do
    event = Events.get_event!(event_id, preload: [:types, :shifts])

    {:noreply, update_event(socket, event, &Events.cancel_event/1, :cancel)}
  end

  def handle_event("delete", %{"event-id" => event_id}, socket) do
    event = Events.get_event!(event_id, preload: [:types, :shifts])

    with :ok <- can(socket, event, :delete),
         {:ok, event} <- Events.delete_event(event) do
      socket =
        socket
        |> stream_delete(:events, event)
        |> put_flash(:success, dgettext("events", "The event has been successfully removed!"))

      {:noreply, socket}
    else
      {:error, :unauthorized} ->
        {:noreply,
         put_flash(socket, :error, gettext("You don't have the permission to do that."))}

      {:error, _} ->
        {:noreply, put_flash(socket, :error, dgettext("events", "Could not delete this event"))}
    end
  end

  @impl true
  def handle_info({_, %ShiftCreated{} = message}, socket) do
    send_update(NyghtWeb.EventLive.EventShiftStatusComponent,
      id: "events-#{message.event_id}-shift-progress",
      event_id: message.event_id
    )

    {:noreply, socket}
  end

  def handle_info({_, %ShiftUpdated{} = message}, socket) do
    send_update(NyghtWeb.EventLive.EventShiftStatusComponent,
      id: "events-#{message.event_id}-shift-progress",
      event_id: message.event_id
    )

    {:noreply, socket}
  end

  def handle_info({_, %ShiftDeleted{} = message}, socket) do
    send_update(NyghtWeb.EventLive.EventShiftStatusComponent,
      id: "events-#{message.event_id}-shift-progress",
      event_id: message.event_id
    )

    {:noreply, socket}
  end

  def handle_info(_, socket), do: {:noreply, socket}

  defp fetch_events(%{assigns: assigns} = socket, params) do
    %{current_organization: org, current_member: member, live_action: tab} = assigns

    params =
      Events.filter_preset(tab)
      |> Map.merge(params)

    opts = [
      status: Authorizations.get_max_event_status(member),
      members_only?: true,
      preload: [:types, :shifts]
    ]

    case Events.list_events_for(org, params, opts) do
      {:ok, {events, meta}} ->
        socket
        |> assign(:meta, meta)
        |> assign(:filter_form, to_form(meta))
        |> stream(:events, events, reset: true)

      {:error, _meta} ->
        push_navigate(socket, to: ~p"/app/events")
    end
  end

  defp update_event(socket, event, action, perm \\ :update) do
    with :ok <- can(socket, event, perm),
         {:ok, event} <- action.(event) do
      stream_insert(socket, :events, event)
    else
      {:error, :unauthorized} ->
        put_flash(socket, :error, gettext("You don't have the permission to do that."))

      {:error, _} ->
        put_flash(socket, :error, dgettext("events", "Could not update this event"))
    end
  end

  defp tab_url(tab, params \\ "")
  defp tab_url(:upcoming, params), do: ~p"/app/events?#{params}"
  defp tab_url(tab, params), do: ~p"/app/events/#{tab}?#{params}"

  defp filter_opts do
    [
      starts_at: [
        label: dgettext("events", "from"),
        type: "datetime-local",
        op: :>=
      ],
      ends_at: [
        label: dgettext("events", "to"),
        type: "datetime-local",
        op: :<=
      ],
      name: [
        label: dgettext("events", "name"),
        op: :ilike
      ]
    ]
  end
end
