defmodule NyghtWeb.EventLive.EventAttachments do
  use NyghtWeb, :live_view

  alias Flop.Meta

  alias Nyght.{Authorizations, Events}
  alias Nyght.Events.Attachment
  alias NyghtWeb.EventLive.EventsComponents

  @impl true
  def mount(%{"id" => event_id} = params, _session, socket) do
    %{current_member: member} = socket.assigns

    member? = !is_nil(member)
    max_status = Authorizations.get_max_event_status(member)

    event =
      Events.get_event!(event_id,
        status: max_status,
        members_only?: member?,
        preload: [:prices, [bookings: :performer], :organization, :types]
      )

    neighbors =
      Events.get_neighbors(event, status: max_status, members_only?: member?)

    if connected?(socket) do
      Events.subscribe(event)
    end

    socket =
      socket
      |> assign(page_title: event.name)
      |> assign(event: event)
      |> assign(neighbors: neighbors)
      |> assign_attachments(params)
      |> enforce_permission(event, :read)

    {:ok, socket}
  end

  @doc false
  def active_menu_item(_), do: :events

  @impl true
  def handle_params(params, _uri, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  @impl true
  def handle_event("delete_attachment", %{"attachment-id" => id}, socket) do
    {:noreply, delete_attachment(socket, id)}
  end

  def handle_event("keyboard_shortcut", %{"key" => "ArrowRight"}, %{assigns: assigns} = socket) do
    {:noreply, navigate_to_event(socket, assigns.neighbors.next_event_id)}
  end

  def handle_event("keyboard_shortcut", %{"key" => "ArrowLeft"}, %{assigns: assigns} = socket) do
    {:noreply, navigate_to_event(socket, assigns.neighbors.prev_event_id)}
  end

  def handle_event(
        "keyboard_shortcut",
        %{"ctrlKey" => true, "key" => "e"},
        %{assigns: assigns} = socket
      ) do
    {:noreply, push_navigate(socket, to: ~p"/app/events/#{assigns.event}/edit")}
  end

  def handle_event("keyboard_shortcut", _, socket) do
    {:noreply, socket}
  end

  defp navigate_to_event(socket, nil), do: socket

  defp navigate_to_event(socket, event_id) do
    push_navigate(socket, to: ~p"/app/events/#{event_id}/attachments", replace: true)
  end

  defp assign_attachments(%{assigns: assigns} = socket, params \\ %{}) do
    %{event: event, current_member: member} = assigns

    case Events.list_attachments_for(event, params) do
      {:ok, {attachments, meta}} ->
        attachments = Enum.filter(attachments, &Authorizations.can?(member, &1, :read))
        meta = %Meta{meta | total_count: Enum.count(attachments)}

        socket
        |> assign(:meta, meta)
        |> stream(:attachments, attachments, reset: true)

      {:error, _meta} ->
        push_navigate(socket, to: ~p"/app/events/#{assigns.event}/attachments")
    end
  end

  defp apply_action(socket, :edit_attachment, %{"attachment_id" => id}) do
    with {:ok, {attachment, _}} <- Events.get_attachment(id, with_content?: false),
         :ok <- can(socket, attachment, :update) do
      assign(socket, :attachment, attachment)
    else
      {:error, :unauthorized} ->
        put_flash(socket, :error, gettext("You don't have the permission to do that."))

      {:error, _} ->
        put_flash(
          socket,
          :error,
          dgettext("events", "Could not get this attachment")
        )
    end
  end

  defp apply_action(socket, _, _params), do: socket

  defp delete_attachment(socket, id) do
    with {:ok, {attachment, _}} <- Events.get_attachment(id, with_content?: false),
         :ok <- can(socket, attachment, :delete),
         {:ok, _attachment} <- Events.delete_attachment(attachment) do
      socket
      |> put_flash(:success, dgettext("events", "Attachment successfully deleted"))
      |> assign_attachments()
    else
      {:error, :unauthorized} ->
        put_flash(socket, :error, gettext("You don't have the permission to do that."))

      {:error, _} ->
        put_flash(
          socket,
          :error,
          dgettext("events", "Could not delete this attachment")
        )
    end
  end
end
