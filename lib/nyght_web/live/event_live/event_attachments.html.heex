<.live_component
  id="event-header"
  module={EventsComponents}
  current_member={@current_member}
  current_organization={@current_organization}
  live_action={@live_action}
  event={@event}
/>

<.modal
  :if={@live_action == :edit_attachment}
  id="edit-attachment-modal"
  show
  on_cancel={JS.patch(~p"/app/events/#{@event}/attachments", replace: true)}
>
  <.live_component
    id="attachment-form"
    module={NyghtWeb.EventLive.FormSingleAttachmentComponent}
    current_organization={@current_organization}
    current_user={@current_user}
    attachment={@attachment}
    patch={~p"/app/events/#{@event}/attachments"}
  />
</.modal>

<main class="container mx-auto py-4" phx-window-keyup="keyboard_shortcut">
  <.empty :if={@meta.total_count == 0} icon="ti-file-off">
    <:title>{dgettext("events", "No attachment")}</:title>
    <:subtitle>
      {dgettext("events", "There are no attached files for this event.")}
    </:subtitle>
  </.empty>

  <Flop.Phoenix.table
    :if={@meta.total_count > 0}
    id="attachments-list"
    meta={@meta}
    items={@streams.attachments}
    path={~p"/app/events/#{@event}/attachments"}
    opts={table_opts()}
  >
    <:col :let={{_id, attachment}} label={dgettext("events", "file name")} field={:file_name}>
      <div class="lg:max-w-80 truncate">
        <.a href={~p"/app/attachments/#{attachment}"} target="_blank">
          {attachment.file_name}
        </.a>
      </div>
    </:col>

    <:col
      :let={{_id, attachment}}
      label={dgettext("events", "restricted to roles")}
      field={:roles}
    >
      <.pill :for={role <- attachment.roles} text={role.name} />
    </:col>
    <:col :let={{_id, attachment}} label={dgettext("events", "updated at")} field={:updated_at}>
      <.datetime datetime={attachment.updated_at} timezone={@current_organization.timezone} shift />
    </:col>
    <:action :let={{_id, attachment}}>
      <.a
        :if={Authorizations.can?(@current_member, Attachment, :update)}
        patch={~p"/app/events/#{@event}/attachments/#{attachment}/edit"}
      >
        {dgettext("events", "Edit")}
      </.a>
    </:action>
    <:action :let={{_id, attachment}}>
      <.a
        :if={Authorizations.can?(@current_member, attachment, :delete)}
        phx-click="delete_attachment"
        phx-value-attachment-id={attachment.id}
      >
        {dgettext("events", "Delete")}
      </.a>
    </:action>
  </Flop.Phoenix.table>
</main>
