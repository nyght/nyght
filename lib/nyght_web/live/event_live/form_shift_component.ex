defmodule NyghtWeb.EventLive.FormShiftComponent do
  @moduledoc """
  This component is the form to create or update a shift.
  """
  use NyghtWeb, :live_component

  alias Nyght.Authorizations
  alias Nyght.Shifts
  alias Nyght.Shifts.Shift

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header level="h3">
        {dgettext("shifts", "New Shift")}

        <:subtitle>
          {dgettext(
            "shifts",
            "Create a new staff time shift. Organization members can apply to the shifts according to their roles."
          )}
        </:subtitle>
      </.header>
      <.simple_form for={@form} phx-target={@myself} phx-change="validate" phx-submit="save">
        <.input field={@form[:timezone]} type="hidden" />
        <div class="w-full">
          <.input
            field={@form[:application_strategy]}
            type="radio_detailed"
            label={dgettext("shifts", "Application strategy")}
          >
            <:option value={:fcfs}>
              <div class="inline-flex justify-between w-full">
                <div class="block">
                  <div class="w-full text-lg font-semibold">
                    {String.capitalize(dgettext("shifts", "fcfs"))}
                  </div>
                  <div class="w-full">
                    {dgettext("shifts", "Users who apply will be directly accepted.")}
                  </div>
                </div>
                <div class="inline-flex h-full items-center">
                  <.icon name="ti-hand-finger" class="text-xl ml-3" />
                </div>
              </div>
            </:option>

            <:option value={:availability}>
              <div class="inline-flex justify-between w-full">
                <div class="block">
                  <div class="w-full text-lg font-semibold">
                    {String.capitalize(dgettext("shifts", "availability"))}
                  </div>
                  <div class="w-full">
                    {dgettext(
                      "shifts",
                      "Users give their availabilities and wait for the planning to be validated."
                    )}
                  </div>
                </div>
                <div class="inline-flex h-full items-center">
                  <.icon name="ti-checks" class="text-xl ml-3" />
                </div>
              </div>
            </:option>
          </.input>

          <div class="my-2 flex flex-col md:flex-row gap-2 w-full">
            <.input
              label={dgettext("shifts", "Role")}
              field={@form[:role_id]}
              type="select"
              options={Enum.map(@roles, &{&1.name, &1.id})}
              class="md:w-1/3"
              required
            />
            <.input
              label={dgettext("shifts", "Total openings")}
              field={@form[:total_openings]}
              class="md:w-1/3"
              type="number"
              min="1"
              step="1"
              placeholder={dgettext("shifts", "No max")}
            />
            <.input field={@form[:name]} class="md:w-1/3" label={dgettext("shifts", "Name")} />
          </div>
          <.input
            field={@form[:description]}
            class="my-2 w-full"
            label={dgettext("shifts", "Description")}
          />

          <div class="my-2 flex flex-col md:flex-row gap-2 w-full">
            <.input
              label={dgettext("shifts", "Starts at")}
              field={@form[:local_starts_at]}
              class="md:w-1/2"
              type="datetime-local"
              required
            />
            <.input
              label={dgettext("shifts", "Ends at")}
              field={@form[:local_ends_at]}
              class="md:w-1/2"
              type="datetime-local"
              required
            />
          </div>
        </div>

        <:actions>
          <.button
            type={:submit}
            phx-disable-with={gettext("Saving...")}
            class="phx-submit-loading:animate-pulse"
          >
            {gettext("Save")}
          </.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{action: :create} = assigns, socket) do
    shift = %Shift{}
    roles = Authorizations.list_roles(assigns.organization)

    form =
      Shifts.change_shift(shift, assigns.event, %{
        role_id: List.first(roles).id,
        local_starts_at: assigns.event.local_starts_at,
        local_ends_at: assigns.event.local_ends_at,
        application_strategy: :fcfs
      })
      |> to_form()

    socket =
      socket
      |> assign(assigns)
      |> assign(shift: shift)
      |> assign(form: form)
      |> assign(roles: roles)

    {:ok, socket}
  end

  def update(%{action: :edit} = assigns, socket) do
    roles = Authorizations.list_roles(assigns.organization)

    form = Shifts.change_shift(assigns.shift, assigns.event) |> to_form()

    socket =
      socket
      |> assign(assigns)
      |> assign(form: form)
      |> assign(roles: roles)

    {:ok, socket}
  end

  @impl true
  def handle_event("validate", %{"shift" => shift_params}, socket) do
    form =
      socket.assigns.shift
      |> Shifts.change_shift(socket.assigns.event, shift_params)
      |> Map.put(:action, :validate)
      |> to_form()

    {:noreply, assign(socket, form: form)}
  end

  def handle_event("save", %{"shift" => shift_params}, socket) do
    {:noreply, save_shift(socket, socket.assigns.action, shift_params)}
  end

  defp save_shift(%{assigns: assigns} = socket, :create, shift_params) do
    with :ok <- Authorizations.can(assigns.member, Shift, :create),
         {:ok, shift} <- Shifts.create_shift(assigns.event, shift_params) do
      notify_parent({:created, shift})

      socket
      |> put_flash(:success, dgettext("shifts", "New shift successfully added!"))
      |> push_patch(to: assigns.patch, replace: true)
    else
      {:error, :unauthorized} ->
        socket
        |> put_flash(:error, dgettext("shifts", "You don't have the permission to do that."))
        |> push_patch(to: assigns.patch, replace: true)

      {:error, changeset} ->
        assign(socket, changeset: changeset)
    end
  end

  defp save_shift(%{assigns: assigns} = socket, :edit, shift_params) do
    with :ok <- Authorizations.can(assigns.member, Shift, :update),
         {:ok, shift} <- Shifts.update_shift(assigns.shift, shift_params) do
      notify_parent({:updated, shift})

      socket
      |> put_flash(:success, dgettext("shifts", "Shift successfully updated!"))
      |> push_patch(to: assigns.patch, replace: true)
    else
      {:error, :unauthorized} ->
        socket
        |> put_flash(:error, dgettext("shifts", "You don't have the permission to do that."))
        |> push_patch(to: assigns.patch, replace: true)

      {:error, changeset} ->
        assign(socket, changeset: changeset)
    end
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end
