defmodule NyghtWeb.EventLive.FormPerformerSearchComponent do
  @moduledoc """
  This is the search bar for performers.

  This component is used in the event form and needs to be place in a
  booking form.
  """
  use NyghtWeb, :live_component

  alias Nyght.Performers

  @doc false
  @impl true
  def render(assigns) do
    ~H"""
    <div class="relative w-full block" phx-hook="SearchBar" id={@id}>
      <.input field={@form[:performer_id]} class="search-selected-id" type="hidden" />
      <.inputs_for :let={fp} field={@form[:performer]}>
        <.input field={fp[:organization_id]} value={@organization.id} type="hidden" />
        <.input
          field={fp[:name]}
          no_label
          placeholder={dgettext("performers", "Performer's name")}
          phx-change="name_changed"
          phx-target={@myself}
          phx-debounce="300"
          autocomplete="off"
        />
      </.inputs_for>

      <div
        :if={not Enum.empty?(@results)}
        class="dropdown-menu border border-stone-300 absolute right-0 z-20 w-full mt-2 bg-stone-50 rounded-sm shadow-sm"
      >
        <div
          :for={result <- @results}
          class="block px-4 py-2 text-sm text-stone-700 hover:bg-stone-100"
          phx-click="select"
          phx-target={@myself}
          phx-value-value={result.id}
        >
          {result.name}
        </div>
      </div>
    </div>
    """
  end

  @doc false
  @impl true
  def update(assigns, socket) do
    socket =
      socket
      |> assign(assigns)
      |> assign(results: [])

    {:ok, socket}
  end

  @doc false
  @impl true
  def handle_event("name_changed", params, %{assigns: assigns} = socket) do
    [form, bookings_form, index, performer_form, _] = params["_target"]

    name = params[form][bookings_form][index][performer_form]["name"]

    results = Performers.search_by_name(name, assigns.organization)

    {:noreply, assign(socket, results: results)}
  end

  def handle_event("select", %{"value" => value}, socket) do
    socket =
      socket
      |> push_event("selected", %{id: socket.assigns.id, selected: value})
      |> assign(results: [])

    {:noreply, socket}
  end
end
