defmodule NyghtWeb.EventLive.FormAttachmentsComponent do
  @moduledoc """
  This component is the form used to attach files to an event.
  """
  require Logger
  use NyghtWeb, :live_component

  alias Nyght.Authorizations
  alias Nyght.Events

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.simple_form for={@form} phx-submit="save" phx-change="validate" phx-target={@myself}>
        <.multiselect_dropdown
          field={@form[:roles]}
          label={dgettext("events", "Roles")}
          options={@roles}
        />

        <.file_upload
          phx-target={@myself}
          label={dgettext("events", "Files")}
          field={@form[:attachments]}
          upload={@uploads.attachments}
          on_cancel="cancel_upload"
          class="my-2"
          prompt={dgettext("events", "Select files to attach to this event")}
        />

        <.button type={:submit}>
          Attach
        </.button>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(assigns, socket) do
    form_fields = %{"roles" => [], "attachments" => []}

    roles =
      Authorizations.list_roles(assigns.current_organization)
      |> Enum.map(&{&1.name, &1.id})

    socket =
      socket
      |> assign(assigns)
      |> assign(form: to_form(form_fields))
      |> assign(roles: roles)
      |> allow_upload(:attachments,
        accept: ~w(.pdf .jpg .jpeg .png),
        max_entries: 5,
        auto_upload: true,
        max_file_size: 50_000_000
      )

    {:ok, socket}
  end

  @impl true
  def handle_event("validate", params, socket) do
    form_fields = Map.merge(%{"roles" => [], "attachments" => []}, params)

    socket = assign(socket, form: to_form(form_fields))
    {:noreply, socket}
  end

  def handle_event("cancel_upload", %{"ref" => ref}, socket) do
    {:noreply, cancel_upload(socket, :attachments, ref)}
  end

  def handle_event("save", params, %{assigns: assigns} = socket) do
    event = assigns.event

    consume_uploaded_entries(socket, :attachments, fn f, entry ->
      content = File.read!(f.path)

      case Events.attach_file(event, entry, content, Map.get(params, "roles", [])) do
        {:ok, _} = ok ->
          ok

        err ->
          Logger.error("Error while attaching file to event #{event.id}: #{inspect(err)}")
          {:postpone, err}
      end
    end)

    socket =
      socket
      |> push_patch(to: assigns.patch, replace: true)

    {:noreply, socket}
  end
end
