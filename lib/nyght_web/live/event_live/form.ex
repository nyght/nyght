defmodule NyghtWeb.EventLive.Form do
  @moduledoc """
  This module defines the event administration page.
  """
  use NyghtWeb, :live_view

  require Logger

  alias Nyght.Events
  alias Nyght.Events.Event
  alias Nyght.Organizations.Organization
  alias Nyght.Performers.{Booking, Performer}

  @doc false
  @impl true
  def mount(_params, _session, socket) do
    types =
      Events.list_types()
      |> Enum.map(&{&1.name, &1.id})

    socket =
      socket
      |> assign(event_types: types)
      |> assign(uploaded_files: [])
      |> allow_upload(:poster,
        accept: ~w(.jpg .jpeg .gif .png),
        max_file_size: 15_000_000,
        max_entries: 1
      )

    {:ok, socket}
  end

  @doc false
  def active_menu_item(_), do: :events

  @impl true
  def handle_params(params, _uri, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  @impl true
  def handle_event("validate", %{"event" => params}, %{assigns: assigns} = socket) do
    form =
      assigns.event
      |> Events.change_event(params)
      |> Map.put(:action, :validate)
      |> to_form()

    {:noreply, assign(socket, form: form)}
  end

  def handle_event("save", %{"event" => params}, socket) do
    {:noreply, save(socket, socket.assigns.live_action, params)}
  end

  def handle_event("cancel_upload", %{"ref" => ref}, socket) do
    {:noreply, cancel_upload(socket, :poster, ref)}
  end

  defp apply_action(%{assigns: assigns} = socket, :create, params) do
    org = assigns.current_organization
    starts_at = get_starts_at(org, params)

    event = %Event{
      organization_id: org.id,
      timezone: org.timezone,
      status: :unpublished,
      local_starts_at: starts_at,
      local_ends_at: starts_at |> NaiveDateTime.add(5, :hour)
    }

    socket
    |> enforce_permission(Event, :create)
    |> assign(event: event)
    |> assign(form: to_form(Events.change_event(event)))
    |> assign(page_title: dgettext("events", "Create a new event"))
  end

  defp apply_action(socket, :copy, %{"id" => event_id}) do
    original_event = Events.get_event!(event_id)
    event_changeset = Events.copy_event(original_event)

    socket
    |> enforce_permission(Event, :create)
    |> assign(event: %Event{})
    |> assign(original_event: original_event)
    |> assign(form: to_form(event_changeset))
    |> assign(page_title: dgettext("events", "Copy an event"))
  end

  defp apply_action(socket, :edit, %{"id" => event_id}) do
    event = Events.get_event!(event_id)

    socket
    |> enforce_permission(event, :update)
    |> assign(event: event)
    |> assign(form: to_form(Events.change_event(event)))
    |> assign(page_title: dgettext("events", "Edit an event"))
  end

  defp save(%{assigns: assigns} = socket, :create, params) do
    with :ok <- can(socket, Event, :create),
         params <- put_poster_urls(socket, params),
         {:ok, event} <-
           Events.create_event(assigns.current_organization, params, &consume_poster(socket, &1)) do
      socket
      |> put_flash(
        :success,
        dgettext("events", "The event has been successfully created. It's not published yet.")
      )
      |> push_navigate(to: ~p"/app/events/#{event}", replace: true)
    else
      error ->
        handle_error(socket, error)
    end
  end

  defp save(%{assigns: assigns} = socket, :copy, params) do
    with :ok <- can(socket, Event, :create),
         {:ok, event} <-
           Events.create_event(assigns.current_organization, params, &consume_poster(socket, &1)),
         {:ok, event} <- Events.copy_shifts(assigns.original_event, event) do
      socket
      |> put_flash(
        :success,
        dgettext("events", "The event has been successfully copied. It's not published yet.")
      )
      |> push_navigate(to: ~p"/app/events/#{event}", replace: true)
    else
      error ->
        handle_error(socket, error)
    end
  end

  defp save(%{assigns: assigns} = socket, :edit, params) do
    with :ok <- can(socket, assigns.event, :update),
         params <- put_poster_urls(socket, params),
         {:ok, event} <- Events.update_event(assigns.event, params, &consume_poster(socket, &1)) do
      socket
      |> put_flash(:success, dgettext("events", "The event has been successfully updated."))
      |> push_navigate(to: ~p"/app/events/#{event}")
    else
      error ->
        handle_error(socket, error)
    end
  end

  defp handle_error(socket, {:error, :unauthorized}) do
    put_flash(socket, :error, gettext("You don't have the permission to do that."))
  end

  defp handle_error(socket, {:error, changeset}) do
    assign(socket, form: to_form(changeset))
  end

  defp put_poster_urls(socket, params) do
    case uploaded_entries(socket, :poster) do
      {[entry], []} ->
        Map.merge(params, %{
          "poster" => "#{entry.uuid}.png",
          "poster_thumbnail" => "#{entry.uuid}_thumb.webp"
        })

      _ ->
        params
    end
  end

  defp consume_poster(socket, %Event{} = event) do
    # TODO: better error handling in case of upload error
    consume_uploaded_entries(socket, :poster, fn %{path: path}, entry ->
      case Events.attach_posters(path, entry) do
        {:ok, res} ->
          {:ok, res}

        {:error, err} ->
          {:postponed, err}
      end
    end)

    {:ok, event}
  end

  defp get_starts_at(%Organization{timezone: timezone}, params) do
    with {:ok, date_param} <- Map.fetch(params, "starts_at"),
         {:ok, date} <- Date.from_iso8601(date_param) do
      DateTime.new!(date, ~T[22:00:00])
    else
      _ ->
        DateTime.now!(timezone)
    end
    |> DateTime.to_naive()
  end
end
