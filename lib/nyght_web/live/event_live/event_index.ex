defmodule NyghtWeb.EventLive.EventIndex do
  use NyghtWeb, :live_view

  alias Nyght.{Authorizations, Events}
  alias Nyght.Events.Events.{EventDeleted, EventUpdated}
  alias Nyght.Performers.Booking
  alias NyghtWeb.EventLive.EventsComponents

  @impl true
  def mount(%{"id" => event_id}, _session, socket) do
    %{current_member: member} = socket.assigns

    member? = !is_nil(member)
    max_status = Authorizations.get_max_event_status(member)

    event =
      Events.get_event!(event_id,
        status: max_status,
        members_only?: member?,
        preload: [:prices, [bookings: :performer], :organization, :types]
      )

    neighbors =
      Events.get_neighbors(event, status: max_status, members_only?: member?)

    if connected?(socket) do
      Events.subscribe(event)
    end

    socket =
      socket
      # |> assign_metadata(event)
      |> assign(page_title: event.name)
      |> assign(event: event)
      |> assign(neighbors: neighbors)
      |> enforce_permission(event, :read)

    {:ok, socket}
  end

  @doc false
  def active_menu_item(_), do: :events

  @impl true
  def handle_event("keyboard_shortcut", %{"key" => "ArrowRight"}, %{assigns: assigns} = socket) do
    {:noreply, navigate_to_event(socket, assigns.neighbors.next_event_id)}
  end

  def handle_event("keyboard_shortcut", %{"key" => "ArrowLeft"}, %{assigns: assigns} = socket) do
    {:noreply, navigate_to_event(socket, assigns.neighbors.prev_event_id)}
  end

  def handle_event(
        "keyboard_shortcut",
        %{"ctrlKey" => true, "key" => "e"},
        %{assigns: assigns} = socket
      ) do
    {:noreply, push_navigate(socket, to: ~p"/app/events/#{assigns.event}/edit")}
  end

  def handle_event("keyboard_shortcut", _, socket) do
    {:noreply, socket}
  end

  @impl true
  def handle_info({_, %EventUpdated{event: event}}, socket) do
    {:noreply, assign(socket, :event, event)}
  end

  def handle_info({_, %EventDeleted{}}, socket) do
    socket =
      socket
      |> put_flash(:error, gettext("This event has been deleted."))
      |> push_navigate(to: ~p"/")

    {:noreply, socket}
  end

  def handle_info(_, socket), do: {:noreply, socket}

  defp navigate_to_event(socket, nil), do: socket

  defp navigate_to_event(socket, event_id) do
    push_navigate(socket, to: ~p"/app/events/#{event_id}", replace: true)
  end
end
