defmodule NyghtWeb.EventLive.FormAssignComponent do
  @moduledoc """
  This component is the form to assign a member to a shift.
  """
  use NyghtWeb, :live_component

  import Ecto.Changeset

  alias Nyght.Accounts.User
  alias Nyght.Authorizations
  alias Nyght.Organizations
  alias Nyght.Organizations.Member
  alias Nyght.Shifts

  @types %{query: :string}

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.simple_form for={@form} phx-change="search" phx-submit="submit" phx-target={@myself}>
        <div class="relative w-full block">
          <.input
            field={@form[:query]}
            value={@name}
            placeholder={dgettext("members", "Search for a staff member by name")}
            autofocus
            no_label
            autocomplete="off"
            phx-debounce="300"
          />

          <div
            :if={not Enum.empty?(@results)}
            class="dropdown-menu border border-stone-300 absolute right-0 z-20 w-full mt-2 bg-stone-50 rounded-sm shadow-sm"
          >
            <div
              :for={%Member{user: user} <- @results}
              class="block px-4 py-2 text-sm text-stone-700 hover:bg-stone-100"
              phx-click="select"
              phx-target={@myself}
              phx-value-value={user.id}
            >
              <.avatar
                name={
                  User.full_name(user, redacted?: !Authorizations.can?(@member, user, :read_details))
                }
                labeled
              />
            </div>
          </div>
        </div>

        <div class="mt-6 flex flex-wrap justify-end w-full">
          <.button
            type={:submit}
            phx-disable-with={gettext("Saving...")}
            class="phx-submit-loading:animate-pulse"
          >
            <.icon name="ti-user-check" class="mr-2" />{dgettext("members", "Assign")}
          </.button>
        </div>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(assigns, socket) do
    form = search_changeset() |> to_form(as: "search_bar")

    socket =
      socket
      |> assign(assigns)
      |> assign(form: form)
      |> assign(results: [])
      |> assign(name: "")
      |> assign(selected_user: nil)

    {:ok, socket}
  end

  @impl true
  def handle_event("search", %{"search_bar" => %{"query" => ""}}, socket) do
    {:noreply, assign(socket, results: [], selected_user: nil)}
  end

  def handle_event("search", %{"search_bar" => query}, %{assigns: assigns} = socket) do
    query
    |> search_changeset()
    |> case do
      %{valid?: true, changes: %{query: query}} = changeset ->
        res = Organizations.search_member_by_name(query, assigns.organization, assigns.shift.role)

        {:noreply,
         assign(socket,
           results: res,
           selected_user: nil,
           form: changeset |> to_form(as: "search_bar")
         )}

      changeset ->
        {:noreply, assign(socket, form: changeset |> to_form(as: "search_bar"))}
    end
  end

  def handle_event("submit", _, %{assigns: assigns} = socket) do
    with :ok <- Authorizations.can(assigns.member, assigns.shift, :update),
         {:ok, _} <- Shifts.apply_for_shift(assigns.shift, assigns.selected_user) do
      notify_parent({:updated, assigns.shift})

      socket =
        socket
        |> assign(name: "")
        |> assign(selected_user: nil)
        |> assign(results: [])
        |> put_flash(
          :success,
          gettext("The user has been successfully assigned to this shift!")
        )
        |> push_patch(to: assigns.patch, replace: true)

      {:noreply, socket}
    else
      {:error, :unauthorized} ->
        socket =
          socket
          |> put_flash(:error, gettext("You don't have the permission to do that."))
          |> push_patch(to: assigns.patch, replace: true)

        {:noreply, socket}

      {:error, _} ->
        socket =
          socket
          |> put_flash(:error, gettext("Failed to assign the user."))
          |> push_patch(to: assigns.patch, replace: true)

        {:noreply, socket}
    end
  end

  def handle_event("select", %{"value" => value}, %{assigns: assigns} = socket) do
    selected = Enum.find(assigns.results, fn result -> result.user_id == value end)

    socket =
      socket
      |> assign(
        results: [],
        name:
          User.full_name(selected.user,
            redacted?: !Authorizations.can?(assigns.member, selected.user, :read_details)
          ),
        selected_user: selected
      )

    {:noreply, socket}
  end

  defp search_changeset(attrs \\ %{}) do
    cast({%{}, @types}, attrs, [:query])
    |> validate_required([:query])
    |> update_change(:query, &String.trim/1)
    |> validate_length(:query, min: 2)
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end
