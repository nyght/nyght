defmodule NyghtWeb.EventLive.EventsComponents do
  use NyghtWeb, :live_component

  alias Nyght.{Authorizations, Events}
  alias Nyght.Events.{Attachment, Event}
  alias Nyght.Shifts.Shift

  @impl true
  def update(assigns, socket) do
    socket =
      socket
      |> assign(assigns)

    {:ok, socket}
  end

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.page_header>
        <.date
          date={@event.local_starts_at}
          format={:full}
          timezone={@event.timezone}
          class="uppercase text-stone-500 text-xs font-normal tracking-normal block"
        />
        {@event.name}

        <:subtitle>
          <span class="flex items-center flex-wrap">
            <span class="flex items-center">
              <.pill
                :if={
                  Authorizations.can?(@current_member, @event, :update) && @event.status == :published
                }
                kind={:success}
                icon="ti-eye"
                text={dgettext("events", "Published")}
              />
              <.pill
                :if={
                  Authorizations.can?(@current_member, @event, :update) &&
                    @event.status == :prepublished
                }
                kind={:warning}
                icon="ti-asterisk"
                text={dgettext("events", "Prepublished")}
              />
              <.pill
                :if={
                  Authorizations.can?(@current_member, @event, :update) &&
                    @event.status == :unpublished
                }
                kind={:danger}
                icon="ti-eye-off"
                text={dgettext("events", "Unpublished")}
              />
              <.pill
                :if={@event.is_canceled}
                kind={:danger}
                icon="ti-calendar-off"
                text={dgettext("events", "Canceled")}
              />
              <.pill
                :if={@event.is_soldout}
                kind={:danger}
                icon="ti-ticket-off"
                text={dgettext("events", "Sold out")}
              />
              <.pill
                :if={@event.is_members_only}
                icon="ti-vip"
                text={dgettext("events", "Members-only")}
              />
            </span>
          </span>
        </:subtitle>
        <:actions>
          <.dropdown id="event-actions" class="flex-1">
            <.button style={:secondary} size={:large} class="w-full">
              {gettext("Other actions")} <.icon name="ti-chevron-down" />
            </.button>
            <:item :if={Authorizations.can?(@current_member, Shift, :create)}>
              <.link class="inline-block w-full py-2 px-4" phx-click={show_modal("add-shift-modal")}>
                {dgettext("events", "Add a new shift")}
              </.link>
            </:item>

            <:item separator />

            <:item :if={Authorizations.can?(@current_member, Attachment, :create)}>
              <.link
                class="inline-block w-full py-2 px-4"
                phx-click={show_modal("add-attachment-modal")}
              >
                {dgettext("events", "Attach a file")}
              </.link>
            </:item>

            <:item separator />

            <:item :if={Authorizations.can?(@current_member, Event, :create)}>
              <.link class="inline-block w-full py-2 px-4" navigate={~p"/app/events/#{@event}/copy"}>
                {dgettext("events", "Copy")}
              </.link>
            </:item>

            <:item separator />

            <:item :if={
              @event.status in [:prepublished, :published] &&
                Authorizations.can?(@current_member, @event, :update)
            }>
              <button class="text-left w-full py-2 px-4" phx-click="unpublish" phx-target={@myself}>
                {dgettext("events", "Unpublish")}
              </button>
            </:item>
            <:item :if={!@event.is_canceled && Authorizations.can?(@current_member, @event, :cancel)}>
              <button class="text-left w-full py-2 px-4" phx-click="cancel" phx-target={@myself}>
                {dgettext("events", "Cancel")}
              </button>
            </:item>
            <:item :if={
              @event.status in [:prepublished, :unpublished] &&
                Authorizations.can?(@current_member, @event, :update)
            }>
              <button class="text-left w-full py-2 px-4" phx-click="publish" phx-target={@myself}>
                {dgettext("events", "Publish")}
              </button>
            </:item>
            <:item :if={
              @event.status == :unpublished && Authorizations.can?(@current_member, @event, :update)
            }>
              <button class="text-left w-full py-2 px-4" phx-click="prepublish" phx-target={@myself}>
                {dgettext("events", "Prepublish")}
              </button>
            </:item>

            <:item separator />

            <:item :if={Authorizations.can?(@current_member, @event, :delete)}>
              <button class="text-left w-full py-2 px-4" phx-click="delete" phx-target={@myself}>
                {dgettext("events", "Delete")}
              </button>
            </:item>
          </.dropdown>

          <.a
            :if={Authorizations.can?(@current_member, @event, :update)}
            style={:primary}
            size={:large}
            navigate={~p"/app/events/#{@event}/edit"}
            class="flex-1"
          >
            <.icon name="ti-edit" class="mr-2" />{gettext("Edit")}
          </.a>
        </:actions>
        <:tab_bar>
          <.tab navigate={~p"/app/events/#{@event}"} active={@live_action == :index}>
            {dgettext("events", "General")}
          </.tab>
          <.tab
            :if={Authorizations.can?(@current_member, Shift, :list)}
            navigate={~p"/app/events/#{@event}/staff"}
            active={@live_action in [:staff, :create_shift, :edit_shift]}
          >
            {dgettext("events", "Staff")}
          </.tab>
          <.tab
            :if={Authorizations.can?(@current_member, Attachment, :list)}
            navigate={~p"/app/events/#{@event}/attachments"}
            active={@live_action == :attachments}
          >
            {dgettext("events", "Attachments")}
          </.tab>
        </:tab_bar>
      </.page_header>

      <.modal id="add-attachment-modal">
        <.live_component
          id="attachments-form"
          module={NyghtWeb.EventLive.FormAttachmentsComponent}
          current_organization={@current_organization}
          event={@event}
          patch={~p"/app/events/#{@event}/attachments"}
        />
      </.modal>

      <.modal id="add-shift-modal">
        <.live_component
          module={NyghtWeb.EventLive.FormShiftComponent}
          id="add-shift-component"
          organization={@current_organization}
          action={:create}
          member={@current_member}
          event={@event}
          patch={~p"/app/events/#{@event}/staff"}
        />
      </.modal>
    </div>
    """
  end

  @impl true
  def handle_event("unpublish" = action, _, socket) do
    {:noreply, update_status(socket, action, &Events.unpublish_event/1)}
  end

  def handle_event("publish" = action, _, socket) do
    {:noreply, update_status(socket, action, &Events.publish_event/1)}
  end

  def handle_event("prepublish" = action, _, socket) do
    {:noreply, update_status(socket, action, &Events.prepublish_event/1)}
  end

  def handle_event("cancel" = action, _, socket) do
    {:noreply, update_status(socket, action, &Events.cancel_event/1, :cancel)}
  end

  def handle_event("delete" = action, _, %{assigns: assigns} = socket) do
    with :ok <- can(socket, assigns.event, :delete),
         {:ok, _} <- Events.delete_event(assigns.event) do
      socket =
        socket
        |> put_flash(:success, dgettext("events", "The event has been successfully removed!"))
        |> push_navigate(to: ~p"/app/events")

      {:noreply, socket}
    else
      {:error, :unauthorized} ->
        socket =
          socket
          |> put_flash(:error, gettext("You don't have the permission to do that."))
          |> push_navigate(~p"/app/events/#{assigns.event}")

        {:noreply, socket}

      {:error, _} ->
        socket =
          socket
          |> put_flash(
            :error,
            dgettext("events", "Could not %{action} this event", %{action: action})
          )
          |> push_navigate(~p"/app/events/#{assigns.event}")

        {:noreply, socket}
    end
  end

  defp update_status(%{assigns: assigns} = socket, action, function, permission \\ :update) do
    with :ok <- can(socket, assigns.event, permission),
         {:ok, event} <- function.(assigns.event) do
      assign(socket, event: event)
    else
      {:error, :unauthorized} ->
        socket =
          socket
          |> put_flash(:error, gettext("You don't have the permission to do that."))
          |> push_navigate(~p"/app/events/#{assigns.event}")

        {:noreply, socket}

      {:error, _} ->
        socket =
          socket
          |> put_flash(
            :error,
            dgettext("events", "Could not %{action} this event", %{action: action})
          )
          |> push_navigate(~p"/app/events/#{assigns.event}")

        {:noreply, socket}
    end
  end
end
