defmodule NyghtWeb.OrganizationLive.Settings do
  @moduledoc """
  This module is the organization settings page
  """
  use NyghtWeb, :live_view

  alias Nyght.Organizations
  alias Nyght.Organizations.Organization

  @impl true
  def mount(_params, _session, socket) do
    org = socket.assigns.current_organization

    form =
      Organizations.change_organization(org)
      |> to_form()

    socket =
      socket
      |> enforce_permission(Organization, :update)
      |> assign(page_title: dgettext("organizations", "Organization settings"))
      |> assign(form: form)

    {:ok, socket}
  end

  @doc false
  def active_menu_item(_), do: :organizations

  @doc false
  @impl true
  def handle_event("validate", %{"organization" => params}, socket) do
    form =
      socket.assigns.current_organization
      |> Organizations.change_organization(params)
      |> Map.put(:action, :validate)
      |> to_form()

    {:noreply, assign(socket, form: form)}
  end

  @doc false
  @impl true
  def handle_event("save", %{"organization" => params}, %{assigns: assigns} = socket) do
    {:noreply, save(socket, assigns.live_action, params)}
  end

  defp save(%{assigns: assigns} = socket, :index, params) do
    with :ok <- can(socket, assigns.current_organization, :update),
         {:ok, _} <- Organizations.update_organization(assigns.current_organization, params) do
      socket
      |> put_flash(
        :success,
        dgettext("organizations", "The organization has been successfully updated.")
      )
      |> push_navigate(to: ~p"/app/organizations")
    else
      {:error, :unauthorized} ->
        put_flash(socket, :error, gettext("You don't have the permission to do that."))

      {:error, changeset} ->
        assign(socket, form: to_form(changeset))
    end
  end
end
