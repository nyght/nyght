defmodule NyghtWeb.OrganizationLive.Show do
  @moduledoc """
  This module defines the home live view.
  """
  use NyghtWeb, :live_view

  alias Nyght.Organizations
  alias Nyght.Organizations.Organization
  alias Nyght.Organizations.Events.{MemberRequestApproved, MemberRequestRejected}

  @doc false
  @impl true
  def mount(_params, _session, %{assigns: assigns} = socket) do
    org = assigns.current_organization

    socket =
      socket
      |> assign(page_title: dgettext("organizations", "Home"))
      |> assign_metadata(org)

    if connected?(socket) do
      Organizations.subscribe(org)
    end

    {:ok, socket}
  end

  @impl true
  def handle_info({_, %MemberRequestApproved{member_id: id}}, %{assigns: assigns} = socket) do
    if assigns.member.id == id do
      updated = Organizations.get_member_for(assigns.current_organization, assigns.current_user)

      socket =
        socket
        |> assign(member: updated)
        |> put_flash(
          :success,
          dgettext("organizations", "Your membership request has been approved!")
        )

      {:noreply, socket}
    else
      {:noreply, socket}
    end
  end

  def handle_info({_, %MemberRequestRejected{member_id: id}}, %{assigns: assigns} = socket) do
    if assigns.member.id == id do
      updated = Organizations.get_member_for(assigns.current_organization, assigns.current_user)

      socket =
        socket
        |> assign(member: updated)
        |> put_flash(
          :error,
          dgettext("organizations", "Your membership request has been rejected.")
        )

      {:noreply, socket}
    else
      {:noreply, socket}
    end
  end

  def handle_info(_, socket), do: {:noreply, socket}

  @doc false
  def active_menu_item(_), do: :index

  def assign_metadata(socket, %Organization{} = org) do
    socket
    |> assign(
      meta_attrs: [
        %{name: "og:title", content: org.name},
        %{name: "og:description", content: org.short_description},
        %{name: "og:type", content: "website"},
        %{name: "og:url", content: ~p"/"},
        %{name: "description", content: org.short_description}
      ]
    )
  end
end
