defmodule NyghtWeb.ProfileLive.Edit do
  @moduledoc """
  This is the profile edition page.
  """
  use NyghtWeb, :live_view

  alias Nyght.Accounts
  alias Nyght.Accounts.User
  alias Nyght.Organizations

  @doc false
  @impl true
  def mount(%{"token" => token}, _session, socket) do
    socket =
      case Accounts.update_user_email(socket.assigns.current_user, token) do
        :ok ->
          put_flash(socket, :info, "Email changed successfully.")

        :error ->
          put_flash(socket, :error, "Email change link is invalid or it has expired.")
      end

    {:ok, push_navigate(socket, to: ~p"/me/edit/account")}
  end

  def mount(_params, _session, %{assigns: assigns} = socket) do
    is_member = Organizations.member?(assigns.current_organization, assigns.current_user)

    if is_member do
      {:ok, socket, layout: {NyghtWeb.UI.Layouts, :app_admin}}
    else
      {:ok, socket}
    end
  end

  @impl true
  def handle_params(params, _uri, %{assigns: assigns} = socket) do
    {:noreply, apply_action(socket, assigns.live_action, params)}
  end

  @doc false
  @impl true
  def handle_event("validate", %{"user" => params}, %{assigns: assigns} = socket) do
    form =
      assigns.current_user
      |> Accounts.change_user_profile(params)
      |> Map.put(:action, :validate)
      |> to_form()

    {:noreply, assign(socket, form: form)}
  end

  def handle_event("save", %{"user" => params}, socket) do
    {:noreply, save(socket, params)}
  end

  def handle_event("validate_email", params, socket) do
    %{"current_password" => password, "user" => user_params} = params

    email_form =
      socket.assigns.current_user
      |> Accounts.change_user_email(user_params)
      |> Map.put(:action, :validate)
      |> to_form()

    {:noreply, assign(socket, email_form: email_form, email_form_current_password: password)}
  end

  def handle_event("update_email", params, socket) do
    %{"current_password" => password, "user" => user_params} = params
    user = socket.assigns.current_user

    case Accounts.apply_user_email(user, password, user_params) do
      {:ok, applied_user} ->
        Accounts.deliver_update_email_instructions(
          applied_user,
          user.email,
          &url(~p"/me/edit/confirm_email/#{&1}")
        )

        info = "A link to confirm your email change has been sent to the new address."
        {:noreply, socket |> put_flash(:info, info) |> assign(email_form_current_password: nil)}

      {:error, changeset} ->
        {:noreply, assign(socket, :email_form, to_form(Map.put(changeset, :action, :insert)))}
    end
  end

  def handle_event("validate_password", params, socket) do
    %{"current_password" => password, "user" => user_params} = params

    password_form =
      socket.assigns.current_user
      |> Accounts.change_user_password(user_params)
      |> Map.put(:action, :validate)
      |> to_form()

    {:noreply, assign(socket, password_form: password_form, current_password: password)}
  end

  def handle_event("update_password", params, socket) do
    %{"current_password" => password, "user" => user_params} = params
    user = socket.assigns.current_user

    case Accounts.update_user_password(user, password, user_params) do
      {:ok, user} ->
        password_form =
          user
          |> Accounts.change_user_password(user_params)
          |> to_form()

        {:noreply, assign(socket, trigger_submit: true, password_form: password_form)}

      {:error, changeset} ->
        {:noreply, assign(socket, password_form: to_form(changeset))}
    end
  end

  defp save(%{assigns: assigns} = socket, params) do
    Accounts.update_user_profile(assigns.current_user, params)
    |> case do
      {:ok, _user} ->
        socket
        |> put_flash(:info, dgettext("profiles", "Your profile has been successfully updated."))
        |> push_navigate(to: ~p"/me")

      {:error, changeset} ->
        {:noreply, assign(socket, form: to_form(changeset))}
    end
  end

  defp apply_action(%{assigns: assigns} = socket, :index, _params) do
    diets =
      User.diets()
      |> Enum.map(fn diet ->
        name =
          diet
          |> Atom.to_string()
          |> String.capitalize()

        {Gettext.dgettext(NyghtWeb.Gettext, "profiles", name), diet}
      end)

    form =
      Accounts.change_user_profile(assigns.current_user)
      |> to_form()

    socket
    |> assign(form: form)
    |> assign(diets: diets)
  end

  defp apply_action(%{assigns: assigns} = socket, :account, _params) do
    user = assigns.current_user
    email_changeset = Accounts.change_user_email(user)

    socket
    |> assign(:email_form_current_password, nil)
    |> assign(:current_email, user.email)
    |> assign(:email_form, to_form(email_changeset))
  end

  defp apply_action(%{assigns: assigns} = socket, :password, _params) do
    user = assigns.current_user
    password_changeset = Accounts.change_user_password(user)

    socket
    |> assign(:current_password, nil)
    |> assign(:password_form, to_form(password_changeset))
    |> assign(:current_email, user.email)
    |> assign(:trigger_submit, false)
  end
end
