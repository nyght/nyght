defmodule NyghtWeb.ProfileLive.Show do
  @moduledoc """
  This is the profile page.
  """
  use NyghtWeb, :live_view

  alias Nyght.Accounts
  alias Nyght.Accounts.User
  alias Nyght.Authorizations
  alias Nyght.Organizations
  alias Nyght.Shifts

  @doc false
  @impl true
  def mount(params, _session, %{assigns: assigns} = socket) do
    %{current_member: member} = assigns

    user = get_user(socket, params)
    can_read_details = Authorizations.can?(member, user, :read_details)

    full_name = User.full_name(user, redacted?: !can_read_details)

    socket =
      socket
      |> assign(user: user)
      |> assign(member: member)
      |> assign(page_title: dgettext("profiles", "%{name}'s profile", %{name: full_name}))
      |> assign(full_name: full_name)
      |> stream(:applications, [])

    if member do
      {:ok, assign(socket, :member, member), layout: {NyghtWeb.UI.Layouts, :app_admin}}
    else
      {:ok, socket}
    end
  end

  def active_menu_item(:member), do: :users
  def active_menu_item(:member_edit), do: :users
  def active_menu_item(_), do: nil

  defp get_user(_, %{"id" => id}), do: Accounts.get_user!(id)
  defp get_user(%{assigns: %{current_user: user}}, _), do: user

  @impl true
  def handle_params(params, _uri, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  @impl true
  def handle_event("ban", _, socket) do
    {:noreply, ban(socket)}
  end

  @impl true
  def handle_info({NyghtWeb.MemberLive.FormComponent, {:updated, member}}, socket) do
    {:noreply, assign(socket, member: member)}
  end

  defp apply_action(%{assigns: assigns} = socket, action, params) do
    member = assigns.member

    case Shifts.list_applications_for(member, params) do
      {:ok, {applications, meta}} ->
        socket
        |> assign(:meta, meta)
        |> stream(:applications, applications, reset: true)

      _err ->
        push_navigate(socket, to: get_url(action, assigns))
    end
  end

  defp get_url(:personal, _), do: ~p"/me"
  defp get_url(:public, assigns), do: ~p"/users/#{assigns.user}"
  defp get_url(:member, assigns), do: ~p"/app/members/#{assigns.user}"
  defp get_url(:member_edit, assigns), do: ~p"/app/members/#{assigns.user}/edit"

  defp ban(%{assigns: assigns} = socket) do
    with :ok <- can(socket, assigns.member, :delete),
         {:ok, _} <- Organizations.delete_member(assigns.member) do
      socket
      |> put_flash(:success, dgettext("members", "This member has been successfully removed!"))
      |> push_navigate(to: ~p"/app/members")
    else
      {:error, :unauthorized} ->
        put_flash(socket, :error, gettext("You don't have the permission to do that."))

      {:error, _} ->
        put_flash(socket, :error, dgettext("members", "Error while removing this member."))
    end
  end
end
