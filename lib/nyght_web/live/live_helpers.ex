defmodule NyghtWeb.LiveHelpers do
  @moduledoc """
  This module defines helpers and components to use in HEEX templates.
  """
  use Phoenix.Component

  use Phoenix.VerifiedRoutes,
    endpoint: NyghtWeb.Endpoint,
    router: NyghtWeb.Router,
    statics: NyghtWeb.static_paths()

  import Phoenix.LiveView

  alias Nyght.Authorizations

  @doc """
  Check if the current user has the given permission, and redirect if it has not.
  """
  @spec enforce_permission(
          Phoenix.LiveView.Socket.t(),
          any(),
          any()
        ) :: Phoenix.LiveView.Socket.t()
  def enforce_permission(%{assigns: assigns} = socket, resource, action) do
    if Authorizations.can?(assigns[:current_member], resource, action) do
      socket
    else
      socket
      |> put_flash(:error, "You don't have the permission to see this page")
      |> redirect(to: ~p"/")
    end
  end

  @doc """
  Returns the given text or translated "N/A".
  """
  def text_or_not_acquired(nil), do: Gettext.gettext(NyghtWeb.Gettext, "N/A")
  def text_or_not_acquired(text), do: text

  @doc """
  Returns the given text or translated "None".
  """
  def text_or_none(nil), do: Gettext.gettext(NyghtWeb.Gettext, "None")
  def text_or_none(text), do: text

  @doc """
  Check if the current user has the given permission
  """
  @spec can?(Phoenix.LiveView.Socket.t(), any(), any()) :: boolean()
  def can?(%Phoenix.LiveView.Socket{assigns: assigns}, resource, action) do
    Authorizations.can?(assigns.current_member, resource, action)
  end

  defdelegate can?(member, item, action), to: Authorizations

  @spec can(Phoenix.LiveView.Socket.t(), any(), any()) :: :ok | {:error, any()}
  def can(%Phoenix.LiveView.Socket{assigns: assigns}, resource, action) do
    Authorizations.can(assigns.current_member, resource, action)
  end

  defdelegate can(member, item, action), to: Authorizations
end
