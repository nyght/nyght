alias Nyght.{Accounts, Authorizations, Events, Organizations, Repo}

# credo:disable-for-this-file
defmodule SeedHelpers do
  def make_future_or_past_date(true) do
    Faker.DateTime.forward(30)
    |> DateTime.truncate(:second)
  end

  def make_future_or_past_date(false) do
    Faker.DateTime.backward(120)
    |> DateTime.truncate(:second)
  end

  def make_insertion_datetime do
    Faker.DateTime.backward(120)
    |> DateTime.truncate(:second)
    |> DateTime.to_naive()
  end

  def make_event(index, org_id) do
    starts_at = SeedHelpers.make_future_or_past_date(rem(index, 2) == 0)
    ends_at = DateTime.add(starts_at, 5 * 60 * 60)
    inserted_at = make_insertion_datetime()

    %{
      name: Faker.Lorem.sentence(3, ""),
      description: "#{Faker.Markdown.markdown()} aaa",
      additional_info: Faker.Lorem.sentence(),
      status: :published,
      is_soldout: rem(index, 8) == 0,
      is_canceled: rem(index, 9) == 0,
      is_members_only: rem(index, 12) == 0,
      organization_id: org_id,
      starts_at: starts_at,
      ends_at: ends_at,
      inserted_at: inserted_at,
      updated_at: inserted_at
    }
  end

  def make_user do
    inserted_at = make_insertion_datetime()

    %{
      email: Faker.Internet.email(),
      hashed_password: "123456789012",
      first_name: Faker.Person.first_name(),
      last_name: Faker.Person.last_name(),
      phone_number: Faker.Phone.EnUs.phone(),
      street_line_1: Faker.Address.street_address(),
      zip_code: Faker.random_between(1000, 9000),
      city: Faker.Address.city(),
      inserted_at: inserted_at,
      updated_at: inserted_at
    }
  end

  def make_member(user_id, org_id) do
    inserted_at = make_insertion_datetime()

    %{
      user_id: user_id,
      organization_id: org_id,
      inserted_at: inserted_at,
      updated_at: inserted_at
    }
  end
end

{:ok, org} =
  Organizations.create_organization(%{
    name: "Ebullition",
    slug: "ebull",
    timezone: "Europe/Zurich",
    description:
      "Ebullition, c'est une salle de concert créée en 1991 qui accueille depuis plus de 20 ans les artistes à la recherche d'une salle de concert alternative en Suisse. Qualité, originalité, et bonne ambiance garantie!

Rock/Pop/Folk/Funk/Ska/Punk/Reggae/Metal/Hardcore/Electro/Rap/Hip-Hop/..."
  })

{:ok, staff_role} =
  Authorizations.create_role(org, %{
    "name" => "Baron",
    "description" => "Staff bar"
  })

{:ok, admin_role} =
  Authorizations.create_role(org, %{
    "name" => "Admin",
    "description" => "Organization administrator"
  })

Authorizations.list_permissions()
|> Enum.map(&Authorizations.grant!(admin_role, &1))

{:ok, admin_user} =
  Accounts.register_user(%{
    first_name: Faker.Person.first_name(),
    last_name: Faker.Person.last_name(),
    phone_number: Faker.Phone.EnUs.phone(),
    street_line_1: Faker.Address.street_address(),
    zip_code: Faker.random_between(1000, 9000),
    city: Faker.Address.city(),
    email: "root@nyght.ch",
    password: "Passw0rd!"
  })

Organizations.create_member(org, admin_user, [admin_role])

events =
  for i <- 1..100 do
    SeedHelpers.make_event(i, org.id)
  end

users =
  for i <- 1..100 do
    SeedHelpers.make_user()
  end

Repo.insert_all(Events.Event, events)
{_, created_users} = Repo.insert_all(Accounts.User, users, returning: true)

members =
  created_users
  |> Enum.take_every(4)
  |> Enum.map(fn u -> SeedHelpers.make_member(u.id, org.id) end)

Repo.insert_all(Organizations.Member, members)
