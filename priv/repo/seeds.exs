# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Nyght.Repo.insert!(%Nyght.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Nyght.Events

default_types = ["party", "concert", "movie", "theatre", "humour", "exhibition", "online"]

default_types
|> Enum.map(&%{name: &1})
|> Enum.each(&Events.create_type/1)
