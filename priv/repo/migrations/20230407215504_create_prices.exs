defmodule Nyght.Repo.Migrations.CreatePrices do
  use Ecto.Migration

  def change do
    create table(:prices, primary_key: false) do
      add :id, :binary_id, primary_key: true

      add :event_id, references(:events, type: :binary_id, on_delete: :delete_all),
        nullable: false

      add :label, :string
      add :amount, :decimal, nullable: false

      timestamps()
    end

    create index(:prices, [:event_id])
    create constraint(:prices, :price_must_be_positive, check: "amount >= 0")
  end
end
