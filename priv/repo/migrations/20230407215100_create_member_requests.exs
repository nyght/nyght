defmodule Nyght.Repo.Migrations.CreateMemberRequests do
  use Ecto.Migration

  def change do
    create table(:member_requests, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :organization_id, references(:organizations, on_delete: :nothing, type: :binary_id)
      add :user_id, references(:users, on_delete: :nothing, type: :binary_id)

      timestamps()
    end

    create index(:member_requests, [:organization_id])
    create index(:member_requests, [:user_id])

    create unique_index(:member_requests, [:organization_id, :user_id])
  end
end
