defmodule Nyght.Repo.Migrations.CreateEvents do
  use Ecto.Migration

  @enum_type_name :event_status

  def change do
    execute(
      "CREATE TYPE #{@enum_type_name} AS ENUM ('unpublished', 'prepublished', 'published', 'canceled')",
      "DROP TYPE #{@enum_type_name}"
    )

    create table(:events, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :organization_id, references(:organizations, type: :binary_id)

      add :name, :string
      add :description, :text
      add :additional_info, :text, nullable: true

      add :starts_at, :utc_datetime
      add :ends_at, :utc_datetime

      add :is_soldout, :boolean
      add :is_members_only, :boolean, default: false
      add :is_canceled, :boolean
      add :status, @enum_type_name

      add :poster, :string
      add :poster_thumbnail, :string

      timestamps()
    end

    create index(:events, [:organization_id])
  end
end
