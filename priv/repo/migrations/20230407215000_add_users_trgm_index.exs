defmodule Nyght.Repo.Migrations.AddUsersTrgmIndex do
  use Ecto.Migration

  @disable_ddl_transaction true
  @disable_migration_lock true

  def change do
    execute "CREATE EXTENSION IF NOT EXISTS pg_trgm", ""
    execute "CREATE EXTENSION IF NOT EXISTS fuzzystrmatch", ""

    create index(:users, ["email gin_trgm_ops"], using: "GIN", name: "users_email_trgm_index")

    create index(
             :users,
             [
               "(coalesce(first_name, '') || ' ' || coalesce(nickname, '') || ' ' || coalesce(last_name, '')) gin_trgm_ops"
             ],
             using: "GIN",
             name: "users_full_name_trgm_index"
           )
  end
end
