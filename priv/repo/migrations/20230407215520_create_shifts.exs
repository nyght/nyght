defmodule Nyght.Repo.Migrations.CreateShifts do
  use Ecto.Migration

  @strategies_enum_type_name :application_strategies

  def change do
    execute(
      "CREATE TYPE #{@strategies_enum_type_name} AS ENUM ('fcfs', 'availability')",
      "DROP TYPE #{@strategies_enum_type_name}"
    )

    create table(:shifts, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :event_id, references(:events, on_delete: :delete_all, type: :binary_id), null: false
      add :role_id, references(:roles, on_delete: :delete_all, type: :binary_id), null: false

      add :name, :string
      add :description, :string

      add :application_strategy, @strategies_enum_type_name, null: false, default: "fcfs"

      add :starts_at, :utc_datetime, null: false
      add :ends_at, :utc_datetime, null: false

      add :total_openings, :integer, default: 1
      add :opening_count, :integer, default: 0

      timestamps()
    end

    create index(:shifts, [:event_id])
    create index(:shifts, [:role_id])

    create constraint(:shifts, :total_openings_is_positive,
             check: "total_openings >= 0 OR total_openings IS NULL"
           )
  end
end
