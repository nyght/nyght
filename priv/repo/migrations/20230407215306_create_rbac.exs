defmodule Nyght.Repo.Migrations.CreateRBAC do
  use Ecto.Migration

  def change do
    # Roles
    create table(:roles, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string
      add :description, :text, nullable: true

      add :organization_id, references(:organizations, type: :binary_id)

      timestamps()
    end

    create index(:roles, [:organization_id])
    create unique_index(:roles, [:name])

    # Permissions
    create table(:permissions, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :resource, :string
      add :action, :string
    end

    create unique_index(:permissions, [:resource, :action])

    # Roles-permissions
    create table(:roles_permissions, primary_key: false) do
      add :role_id, references(:roles, type: :binary_id), primary_key: true
      add :permission_id, references(:permissions, type: :binary_id), primary_key: true
    end

    create index(:roles_permissions, [:role_id])
    create index(:roles_permissions, [:permission_id])

    # Members
    create table(:members, primary_key: false) do
      add :id, :binary_id, primary_key: true

      add :user_id,
          references(:users, type: :binary_id, on_delete: :delete_all, on_update: :update_all)

      add :organization_id,
          references(:organizations,
            type: :binary_id,
            on_delete: :delete_all,
            on_update: :update_all
          )

      timestamps()
    end

    create unique_index(:members, [:user_id, :organization_id],
             name: :members_unique_member_index
           )

    create index(:members, [:user_id])
    create index(:members, [:organization_id])

    # Members-roles
    create table(:members_roles, primary_key: false) do
      add :member_id,
          references(:members, type: :binary_id, on_delete: :delete_all, on_update: :update_all),
          primary_key: true

      add :role_id,
          references(:roles, type: :binary_id, on_delete: :delete_all, on_update: :update_all),
          primary_key: true
    end

    create index(:members_roles, [:member_id])
    create index(:members_roles, [:role_id])
    create unique_index(:members_roles, [:member_id, :role_id])
  end
end
