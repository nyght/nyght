defmodule Nyght.Repo.Migrations.FixRolesIndex do
  use Ecto.Migration

  @disable_ddl_transaction true
  @disable_migration_lock true

  def change do
    drop index(:roles, [:name])
    create unique_index(:roles, [:organization_id, :name], concurrently: true)
  end
end
