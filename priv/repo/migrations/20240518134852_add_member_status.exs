defmodule Nyght.Repo.Migrations.AddMemberStatus do
  use Ecto.Migration

  @enum_type_name :member_status

  def change do
    execute(
      "CREATE TYPE #{@enum_type_name} AS ENUM ('waiting_approval', 'approved', 'banned')",
      "DROP TYPE #{@enum_type_name}"
    )

    alter table(:members) do
      add :approved_at, :utc_datetime, null: true
      add :status, @enum_type_name, default: "approved"
    end
  end
end
