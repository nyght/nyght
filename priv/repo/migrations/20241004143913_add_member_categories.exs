defmodule Nyght.Repo.Migrations.AddMemberCategories do
  use Ecto.Migration

  def change do
    create table(:member_categories, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :organization_id, references(:organizations, type: :binary_id, on_delete: :delete_all)
      add :name, :string, null: false
    end

    create table(:members_member_categories, primary_key: false) do
      add :member_id, references(:members, type: :binary_id, on_delete: :delete_all),
        primary_key: true

      add :member_category_id,
          references(:member_categories, type: :binary_id, on_delete: :delete_all),
          primary_key: true
    end

    create unique_index(:members_member_categories, [:member_id, :member_category_id])
  end
end
