defmodule Nyght.Repo.Migrations.ChangeShiftRelationship do
  use Ecto.Migration

  @disable_ddl_transaction true
  @disable_migration_lock true
  def change do
    alter table(:shift_applications) do
      add :member_id, references(:members, on_delete: :delete_all, type: :binary_id)
    end

    create unique_index(:shift_applications, [:shift_id, :member_id], concurrently: true)
  end
end
