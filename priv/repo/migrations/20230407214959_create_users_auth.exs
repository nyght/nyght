defmodule Nyght.Repo.Migrations.CreateUsersAuth do
  use Ecto.Migration

  @enum_type_name :diet

  def change do
    execute "CREATE EXTENSION IF NOT EXISTS citext", ""

    execute(
      "CREATE TYPE #{@enum_type_name} AS ENUM ('pescetarian', 'vegetarian', 'vegan', 'other')",
      "DROP TYPE #{@enum_type_name}"
    )

    create table(:users, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :email, :citext, null: false
      add :hashed_password, :string, null: false
      add :confirmed_at, :naive_datetime

      add :first_name, :string
      add :last_name, :string
      add :nickname, :string

      add :diet, @enum_type_name
      add :allergies, :string
      add :pronouns, :string

      add :street_line_1, :string
      add :street_line_2, :string
      add :street_line_3, :string
      add :zip_code, :integer
      add :city, :string

      add :phone_number, :string

      timestamps()
    end

    create unique_index(:users, [:email])

    create table(:users_tokens, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :user_id, references(:users, type: :binary_id, on_delete: :delete_all), null: false
      add :token, :binary, null: false
      add :context, :string, null: false
      add :sent_to, :string
      timestamps(updated_at: false)
    end

    create index(:users_tokens, [:user_id])
    create unique_index(:users_tokens, [:context, :token])
  end
end
