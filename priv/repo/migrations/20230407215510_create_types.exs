defmodule Nyght.Repo.Migrations.CreateTypes do
  use Ecto.Migration

  def change do
    create table(:types, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string
    end

    create table(:events_types, primary_key: false) do
      add :event_id, references(:events, type: :binary_id, on_delete: :delete_all),
        primary_key: true

      add :type_id, references(:types, type: :binary_id, on_delete: :delete_all),
        primary_key: true
    end

    create index(:events_types, [:event_id])
    create index(:events_types, [:type_id])
  end
end
