defmodule Nyght.Repo.Migrations.ImproveBookings do
  use Ecto.Migration
  @payments_enum_type_name :payment_type

  def change do
    execute(
      "CREATE TYPE #{@payments_enum_type_name} AS ENUM ('wire', 'cash')",
      "DROP TYPE #{@payments_enum_type_name}"
    )

    alter table(:performers) do
      add :links, {:array, :map}, default: []
    end

    alter table(:bookings) do
      add :fee, :decimal
      add :payment_type, @payments_enum_type_name
      add :recipient, :string

      add :people_count, :integer
      add :special_requests, :text
    end
  end
end
