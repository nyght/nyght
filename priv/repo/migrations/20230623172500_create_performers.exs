defmodule Nyght.Repo.Migrations.CreatePerformers do
  use Ecto.Migration

  def change do
    create table(:performers, primary_key: false) do
      add :id, :binary_id, primary_key: true

      add :organization_id, references(:organizations, on_delete: :delete_all, type: :binary_id),
        null: false

      add :name, :string, null: false
      add :email, :citext
      add :phone_number, :string
      add :country, :string

      timestamps()
    end

    create(
      index(:performers, ["name gin_trgm_ops"], using: "GIN", name: "performers_name_trgm_index")
    )

    create table(:bookings, primary_key: false) do
      add :id, :binary_id, primary_key: true

      add :event_id, references(:events, type: :binary_id, on_delete: :delete_all),
        nullable: false

      add :performer_id, references(:performers, on_delete: :delete_all, type: :binary_id),
        nullable: false

      add :position, :integer, null: false, default: 0

      timestamps()
    end

    create index(:bookings, [:event_id])
  end
end
