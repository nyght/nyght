defmodule Nyght.Repo.Migrations.RemoveOpeningCount do
  use Ecto.Migration

  def up do
    alter table(:shifts) do
      remove :opening_count
    end
  end

  def down do
    alter table(:shifts) do
      add :opening_count, :integer, default: 0
    end
  end
end
