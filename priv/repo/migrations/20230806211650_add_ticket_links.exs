defmodule Nyght.Repo.Migrations.AddTicketLinks do
  use Ecto.Migration

  def change do
    alter table(:events) do
      add :ticket_links, {:array, :map}, default: []
    end
  end
end
