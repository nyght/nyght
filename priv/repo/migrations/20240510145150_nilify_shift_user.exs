defmodule Nyght.Repo.Migrations.NilifyShiftUser do
  use Ecto.Migration

  def change do
    alter table(:shift_applications) do
      modify :user_id, references(:users, on_delete: :nilify_all, type: :binary_id),
        from: references(:users, on_delete: :delete_all, type: :binary_id)
    end
  end
end
