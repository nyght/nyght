defmodule Nyght.Repo.Migrations.AddAutoApproval do
  use Ecto.Migration

  def change do
    alter table(:organizations) do
      add :is_auto_approval_enabled, :boolean, default: false, null: false
    end
  end
end
