defmodule Nyght.Repo.Migrations.AddDefaultRole do
  use Ecto.Migration

  def change do
    alter table(:roles) do
      add :is_default, :boolean, null: false, default: false
    end
  end
end
