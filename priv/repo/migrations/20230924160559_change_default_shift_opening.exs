defmodule Nyght.Repo.Migrations.ChangeDefaultShiftOpening do
  use Ecto.Migration

  def change do
    alter table(:shifts) do
      modify :total_openings, :integer, default: nil, from: {:integer, default: 1}
    end
  end
end
