defmodule Nyght.Repo.Migrations.FixAttachmentsConstraints do
  use Ecto.Migration

  def change do
    alter table(:attachments_roles) do
      modify :attachment_id,
             references(:attachments,
               type: :binary_id,
               on_delete: :delete_all,
               on_update: :update_all
             ),
             from: references(:attachments, type: :binary_id)

      modify :role_id,
             references(:roles, type: :binary_id, on_delete: :delete_all, on_update: :update_all),
             from: references(:roles, type: :binary_id)
    end
  end
end
