defmodule Nyght.Repo.Migrations.AddShiftOffsets do
  use Ecto.Migration

  def change do
    alter table(:shifts) do
      add :start_offset, :integer, null: false, default: 0
      add :end_offset, :integer, null: false, default: 0

      modify :starts_at, :utc_datetime, null: true, from: {:utc_datetime, null: true}
      modify :ends_at, :utc_datetime, null: true, from: {:utc_datetime, null: true}
    end

    alter table(:events) do
      add :timezone, :string, null: false, default: "Etc/UTC"
    end
  end
end
