defmodule Nyght.Repo.Migrations.CreateShiftApplications do
  use Ecto.Migration

  @status_enum_type_name :application_status

  def change do
    execute(
      "CREATE TYPE #{@status_enum_type_name} AS ENUM ('accepted', 'pending', 'waiting_replacement', 'rejected')",
      "DROP TYPE #{@status_enum_type_name}"
    )

    create table(:shift_applications, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :shift_id, references(:shifts, on_delete: :delete_all, type: :binary_id)
      add :user_id, references(:users, on_delete: :delete_all, type: :binary_id)
      add :status, @status_enum_type_name, null: false, default: "accepted"

      timestamps()
    end

    create index(:shift_applications, [:shift_id])
    create index(:shift_applications, [:user_id])

    create unique_index(:shift_applications, [:shift_id, :user_id])
  end
end
