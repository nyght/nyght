defmodule Nyght.Repo.Migrations.AddShortDescription do
  use Ecto.Migration

  def change do
    alter table(:organizations) do
      add :short_description, :string, size: 255
    end
  end
end
