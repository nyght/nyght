defmodule Nyght.Repo.Migrations.AddAttachments do
  use Ecto.Migration

  def change do
    alter table(:events) do
      add :encryption_key, :string
    end

    execute "CREATE EXTENSION IF NOT EXISTS pgcrypto", ""

    execute(
      "UPDATE events SET encryption_key = encode(gen_random_bytes(32), 'base64')",
      ""
    )

    create table(:attachments, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :event_id, references(:events, type: :binary_id)

      add :encryption_iv, :string, nullable: false
      add :file_name, :string, nullable: false
      add :resource_path, :string, nullable: false
      add :content_type, :string, nullable: false

      timestamps()
    end

    create table(:attachments_roles, primary_key: false) do
      add :attachment_id, references(:attachments, type: :binary_id), primary_key: true
      add :role_id, references(:roles, type: :binary_id), primary_key: true
    end
  end
end
