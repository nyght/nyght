defmodule Nyght.Repo.Migrations.CreateOrganizations do
  use Ecto.Migration

  def change do
    create table(:organizations, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string
      add :slug, :string
      add :description, :text

      add :timezone, :text, default: "Etc/UTC"

      add :street_line_1, :string
      add :street_line_2, :string
      add :street_line_3, :string
      add :zip_code, :integer
      add :city, :string

      add :phone_number, :string

      timestamps()
    end

    create unique_index(:organizations, [:slug])
  end
end
