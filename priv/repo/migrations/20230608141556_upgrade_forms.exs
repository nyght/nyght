defmodule Nyght.Repo.Migrations.UpgradeForms do
  use Ecto.Migration

  def change do
    alter table(:prices) do
      add :position, :integer
    end

    alter table(:events) do
      modify :description, :text, null: true, from: :text
    end
  end
end
