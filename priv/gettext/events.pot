## This file is a PO Template file.
##
## "msgid"s here are often extracted from source code.
## Add new messages manually only if they're dynamic
## messages that can't be statically extracted.
##
## Run "mix gettext.extract" to bring this file up to
## date. Leave "msgstr"s empty as changing them here has no
## effect: edit them in PO (.po) files instead.
#
msgid ""
msgstr ""

#, elixir-autogen, elixir-format
msgid "Add a new admission price"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Add a new shift"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Add a performer"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Admission price"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Admission price label"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Booking"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Cancel"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Canceled"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Complete!"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Copy"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Copy an event"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Could not %{action} this event"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Could not delete this event"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Could not update this event"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Create a new event"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Delete"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Edit"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Edit an event"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Events"
msgstr ""

#, elixir-autogen, elixir-format
msgid "General"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Members-only"
msgstr ""

#, elixir-autogen, elixir-format
msgid "New Event"
msgstr ""

#, elixir-autogen, elixir-format
msgid "No description for this event."
msgstr ""

#, elixir-autogen, elixir-format
msgid "No shifts"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Other events"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Past Events"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Performers"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Prepublish"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Prepublished"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Pricing"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Publish"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Published"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Sold out"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Staff"
msgstr ""

#, elixir-autogen, elixir-format
msgid "The different admission prices for the event. If none are given, the entrance will be free."
msgstr ""

#, elixir-autogen, elixir-format
msgid "The different performers for the event."
msgstr ""

#, elixir-autogen, elixir-format
msgid "The event has been successfully created. It's not published yet."
msgstr ""

#, elixir-autogen, elixir-format
msgid "The event has been successfully removed!"
msgstr ""

#, elixir-autogen, elixir-format
msgid "The event has been successfully updated."
msgstr ""

#, elixir-autogen, elixir-format
msgid "The main information about the event."
msgstr ""

#, elixir-autogen, elixir-format
msgid "There are no shifts for this event."
msgstr ""

#, elixir-autogen, elixir-format
msgid "Total"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Unpublish"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Unpublished"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Upcoming Events"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Upload a poster for this event"
msgstr ""

#, elixir-autogen, elixir-format
msgid "date and time"
msgstr ""

#, elixir-autogen, elixir-format
msgid "name"
msgstr ""

#, elixir-autogen, elixir-format
msgid "status"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Description"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Ends at"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Event type"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Is member-only"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Name"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Poster"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Starts at"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Add a new link"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Link Name"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Link URL"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Link to buy ticket for this event."
msgstr ""

#, elixir-autogen, elixir-format
msgid "The event has been successfully copied. It's not published yet."
msgstr ""

#, elixir-autogen, elixir-format
msgid "Tickets"
msgstr ""

#, elixir-autogen, elixir-format
msgid "1 performer"
msgid_plural "%{count} performers"
msgstr[0] ""
msgstr[1] ""

#, elixir-autogen, elixir-format
msgid "from"
msgstr ""

#, elixir-autogen, elixir-format
msgid "to"
msgstr ""

#, elixir-autogen, elixir-format
msgid "shifts summary"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Events will be listed here, but for now, there's none"
msgstr ""

#, elixir-autogen, elixir-format
msgid "No events"
msgstr ""
