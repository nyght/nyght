��          �   %   �      P     Q     `     t     |     �     �     �     �     �     �     �     �     �     �     �     �       
   /     :     G  7   a  )   �  ^   �     "     *  �  /     �     �                    #     :  	   @     J     c     o     u     y     �  '   �  ,   �     �     �       (     <   E  +   �  n   �          "                                                                            	         
                                        Add a new link Add a new performer Contact Country Edit Edit a performer Email General Last update: Link URL Links Name New Performer Origin Performer successfully created! Performer successfully updated! Performer's name Performers Phone number Select performer's origin The contact information for the performer's management. The main information about the performer. The places where the public can find the performer's works. This will be visible by everybody. country name Project-Id-Version: nyght
PO-Revision-Date: 2024-05-24 17:23+0200
Last-Translator: 
Language-Team: French
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Crowdin-Project: nyght
X-Crowdin-Project-ID: 530302
X-Crowdin-Language: fr
X-Crowdin-File: /master/priv/gettext/performers.pot
X-Crowdin-File-ID: 37
X-Generator: Poedit 3.4.4
 Ajouter un nouveau lien Ajouter un·e artiste Contact Pays Modifier Modifier un·e artiste Email Général Dernière mise à jour : URL du lien Liens Nom Nouvelle·au artiste Origine L'artiste a été créé avec succès ! L'artiste a été mis à jour avec succès ! Nom de l'artiste Artistes Numéro de téléphone Sélectionnez l’origine de l’artiste Les informations de contact pour le management de l'artiste. Les informations principales sur l'artiste. Les endroits où le public peut trouver des travaux de l'artiste. Ces liens seront visibles par tout le monde. pays nom 