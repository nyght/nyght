��    *      l  ;   �      �  H   �     �       	             7     <     D     V     [     n     s     �  
   �     �  R   �  	   �       @        O     \     e     x     �     �     �  #   �  	   �     �     �     �  9     �   >  G   �  J     n   i  +   �  5     ,   :     g     p  �  x  `   &
     �
     �
  	   �
     �
     �
     �
     �
     �
            "   !     D     J  	   R  q   \     �     �  @   �          '     .     E     S     j     r  +   �     �     �     �     �  8   �  �   .  d   
  M   o  �   �  .   G  9   v  F   �     �                 *           '       (                $   &                              %         	                    )                              "            
           !                                 #    %{count} event in the last 2 months %{count} events in the last 2 months %{name}'s profile Account deletion Allergies Cancel account deletion City Contact Delete my account Diet Diet and allergies Edit Edit your profile to cancel Email First name General Give us your diet or allergies. This information will be useful for meal planning. Last name Location Member of %{count} organization Member of %{count} organizations Member since Nickname Nyght member since Organizations Phone number Pronouns Recent Activity Some general information about you. Starts at Street line 1 Street line 2 Street line 3 The schedluded deletion of your account will be canceled. You can delete your account at any time. Once you click on the following link you have 7 days to change your mind before your data is wiped from Nyght. You can't ask to delete your account while having non-completed shifts. Your account is scheduled to be deleted from Nyght.ch. This was a mistake? Your contact information. It will only be visible to the staff managers of the organizations you're a part of. Your profile has been successfully updated. Your profile will automatically be deleted in 7 days. Your profile won't be deleted. Welcome back. ZIP code applied Project-Id-Version: nyght
PO-Revision-Date: 2024-05-24 17:22+0200
Last-Translator: 
Language-Team: French
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Crowdin-Project: nyght
X-Crowdin-Project-ID: 530302
X-Crowdin-Language: fr
X-Crowdin-File: /master/priv/gettext/profiles.pot
X-Crowdin-File-ID: 39
X-Generator: Poedit 3.4.4
 %{count} évènement durant les 2 derniers mois %{count} évènements durant les 2 derniers mois Profil de %{name} Suppresion de compte Allergies Annuler la suppresion du compte Ville Contact Supprimer mon compte Régime Régime et allergies Modifier Modifiez votre compte pour annuler Email Prénom Général Dites-nous votre régime alimentaire ou allergies. Ces informations seront utiles pour la préparation des repas. Nom Lieux Membre de %{count} organisation Membre de %{count} organisations Membre depuis Surnom Membre de Nyght depuis Organisations Numéro de téléphone Pronoms Activités récentes Quelques informations générales sur vous. Commence le Ligne d'adresse 1 Ligne d'adresse 2 Ligne d'adresse 3 La suppresion prévue de votre compte va être annulée. Vous pouvez supprimer votre compte à n'importe quel moment. Une fois que vous aurez cliquez sur le lien suivant, vous aurez 7 jours pour changer d'avis avant que vos données soient supprimées de Nyght à tout jamais. Vous ne pouvez pas demander la suppression de votre en ayant des shifts non-complétés ou annulés. Votre compte est programmé pour être supprimé de Nyght. Est-ce une erreur? Vos informations de contact. Elles seront visible par les personnes responsables du staff dans les organisations dont vous faites partie. Votre profil a été mis à jour avec succès. Votre compte sera automatiquement supprimé dans 7 jours. La suppression de votre compte a été annulée. Bienvenue à nouveau. Code postal inscrit·e le 