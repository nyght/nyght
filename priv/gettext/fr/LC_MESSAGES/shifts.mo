��    )      d  ;   �      �  N   �     �     �            A     e   ^     �     �     �     �     �  	   �     �               /     G     T     \     e     j     �  	   �  8   �  7   �       J   0     {     �  
   �     �  J   �  *        ,  )   D  >   n     �     �     �  �  �  M   �	     �	     
     
     (
  e   ;
  t   �
          "     0     =     E     I  .   `     �     �  *   �     �  
   �            )        =     V  >   b  ;   �     �  2   �     .     N     _     m  Y   �  =   �       ,   6  P   c     �     �     �            $   
                '                  #                           )                   %                     	                                  &                                !      "   (        %{count} opening left out of %{total}  %{count} openings left out of %{total}  Accepted Application strategy Apply! Assign someone Be the first to work for this event by clicking the apply button! Create a new staff time shift. Organization members can apply to the shifts according to their roles. Description Ends at Fold all Leave Name New Shift New shift successfully added! No max No maximum openings Nobody has applied yet! Other shifts Pending Rejected Role Shift successfully updated! Shifts for you Starts at The user has been successfully accepted from this shift! The user has been successfully removed from this shift! There's no required staff count There's still %{count} opening left. There's still %{count} openings left. This shift is full. Total openings Unfold all User deleted Users give their availabilities and wait for the planning to be validated. Users who apply will be directly accepted. Waiting for replacement You don't have the permission to do that. Your account is about to be deleted. You can't join any shift. application time availability fcfs Project-Id-Version: nyght
PO-Revision-Date: 2024-05-24 17:20+0200
Last-Translator: 
Language-Team: French
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Crowdin-Project: nyght
X-Crowdin-Project-ID: 530302
X-Crowdin-Language: fr
X-Crowdin-File: /master/priv/gettext/shifts.pot
X-Crowdin-File-ID: 43
X-Generator: Poedit 3.4.4
X-Poedit-Bookmarks: 18,-1,-1,-1,-1,-1,-1,-1,-1,-1
 %{count} place restante sur %{total}  %{count} places restantes sur %{total}  Accepté Type de plage horaire S'inscrire ! Assigner quelqu'un Soyez le·la premier·ère à vous inscrire à cet évènement en cliquant sur le bouton ci-dessous ! Créer une nouvelle plage horaire pour le staff. Les membres de l'organisation pourront postuler suivant leur rôle. Description Se termine à Tout replier Quitter Nom Nouvelle plage horaire Nouvelle plage horaire ajoutée avec succès ! Pas de nombre requis Pas de nombre requis Personne ne s'est inscrit pour l'instant ! Autres plages horaire En attente Rejeté Rôle Plage horaire mise à jour avec succès ! Plages horaire pour vous Commence à L'utilisateur a bien été accepté pour cette plage horaire ! L'utilisateur a bien été retiré de cette plage horaire ! Il n'y a pas de nombre requis Il reste %{count} place. Il reste %{count} places. Cette plage horaire est pleine. Nombre de places Tout déplier Utilisateur supprimé Les utilisateurs donnent leurs disponibilités et attendent que le planning soit validé. Les utilisateurs qui s'inscrivent sont directement acceptés. En attente de remplacement Vous n'avez pas la permission de faire ceci. Votre compte est en cours de suppresion. Vous ne pouvez pas rejoindre de shifts. heure d'inscription disponibilité premier arrivé, premier servi 