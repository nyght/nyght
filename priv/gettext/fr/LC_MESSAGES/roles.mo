��    	      d      �       �      �      �                      %     *     E  �  ^     	     !     3     ?     Q     j     n      �                                          	    Create a new role Default role Description Edit a role Last update: Name The role has been created! The role has been saved! Project-Id-Version: nyght
PO-Revision-Date: 2024-05-24 17:21+0200
Last-Translator: 
Language-Team: French
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Crowdin-Project: nyght
X-Crowdin-Project-ID: 530302
X-Crowdin-Language: fr
X-Crowdin-File: /master/priv/gettext/roles.pot
X-Crowdin-File-ID: 41
X-Generator: Poedit 3.4.4
 Créer un nouveau rôle Rôle par défaut Description Modifier un rôle Dernière mise à jour : Nom Le rôle a été ajouté ! Le modèle a été enregistré ! 