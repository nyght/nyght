## This file is a PO Template file.
##
## "msgid"s here are often extracted from source code.
## Add new messages manually only if they're dynamic
## messages that can't be statically extracted.
##
## Run "mix gettext.extract" to bring this file up to
## date. Leave "msgstr"s empty as changing them here has no
## effect: edit them in PO (.po) files instead.
#
msgid ""
msgstr ""

#, elixir-autogen, elixir-format
msgid "%{count} event in the last 2 months"
msgid_plural "%{count} events in the last 2 months"
msgstr[0] ""
msgstr[1] ""

#, elixir-autogen, elixir-format
msgid "Member of %{count} organization"
msgid_plural "Member of %{count} organizations"
msgstr[0] ""
msgstr[1] ""

#, elixir-autogen, elixir-format
msgid "%{name}'s profile"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Allergies"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Contact"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Diet"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Diet and allergies"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Edit"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Email"
msgstr ""

#, elixir-autogen, elixir-format
msgid "General"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Give us your diet or allergies. This information will be useful for meal planning."
msgstr ""

#, elixir-autogen, elixir-format
msgid "Location"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Member since"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Nyght member since"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Organizations"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Phone number"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Recent Activity"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Some general information about you."
msgstr ""

#, elixir-autogen, elixir-format
msgid "Starts at"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Your contact information. It will only be visible to the staff managers of the organizations you're a part of."
msgstr ""

#, elixir-autogen, elixir-format
msgid "Your profile has been successfully updated."
msgstr ""

#, elixir-autogen, elixir-format
msgid "applied"
msgstr ""

#, elixir-autogen, elixir-format
msgid "City"
msgstr ""

#, elixir-autogen, elixir-format
msgid "First name"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Last name"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Nickname"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Pronouns"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Street line 1"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Street line 2"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Street line 3"
msgstr ""

#, elixir-autogen, elixir-format
msgid "ZIP code"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Account deletion"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Cancel account deletion"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Delete my account"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Edit your profile to cancel"
msgstr ""

#, elixir-autogen, elixir-format
msgid "The schedluded deletion of your account will be canceled."
msgstr ""

#, elixir-autogen, elixir-format
msgid "You can delete your account at any time. Once you click on the following link you have 7 days to change your mind before your data is wiped from nyght."
msgstr ""

#, elixir-autogen, elixir-format
msgid "Your profile will automatically be deleted in 7 days."
msgstr ""

#, elixir-autogen, elixir-format
msgid "Your profile won't be deleted. Welcome back."
msgstr ""

#, elixir-autogen, elixir-format
msgid "Are you sure you want to delete?"
msgstr ""

#, elixir-autogen, elixir-format
msgid "Your account is scheduled to be deleted from nyght. This was a mistake?"
msgstr ""

#, elixir-autogen, elixir-format
msgid "You can't ask to delete your account while having non-completed shifts."
msgstr ""
