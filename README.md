# nyght

[![pipeline status](https://gitlab.com/nyght/nyght/badges/master/pipeline.svg)](https://gitlab.com/nyght/nyght/-/commits/master)
[![coverage report](https://gitlab.com/nyght/nyght/badges/master/coverage.svg)](https://gitlab.com/nyght/nyght/-/commits/master)
[![translation status](https://hosted.weblate.org/widget/nyght/svg-badge.svg)](https://hosted.weblate.org/engage/nyght/)
[![Latest Release](https://gitlab.com/nyght/nyght/-/badges/release.svg)](https://gitlab.com/nyght/nyght/-/releases)
[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](CODE_OF_CONDUCT.md)

nyght is an event and staff management platform, primarily targeted at
music venues.

## Getting Started

These instructions will get you a copy of the project up and running on
your local machine for development and testing purposes.

### Prerequisites

The preferred way to setup a development environment for nyght is by using
[devenv.sh](https://devenv.sh). Follow the instructions on the official website
before trying to install nyght.

If [direnv](https://direnv.net/) is installed, everything will be
automatically setup.

### Installation

You should start by cloning and installing the nyght repository:

```sh
git clone https://gitlab.com/nyght/nyght.git
cd nyght
```

If direnv is installed, it will probably say `.envrc is not allowed` when
entering the `nyght` folder for the first time. To allow it, run the following
command:

```sh
direnv allow
```

If direnv is not present, install the dev environment by running:

```sh
devenv shell
```

Finally, to apply the migrations and build some stuff, run:

```sh
mix setup
mix seed   # this will create some fake data
```

## Running the server

To start the nyght server and the required services, run the following command:

```sh
devenv up        # to start the required services
mix phx.server   # to start nyght server
```

You can now visit [`localhost:4000`](http://localhost:4000) from your
browser.

## Running the tests

To run the test suite, use the following command (services need to be running):

```sh
mix test
```

## Built With

- [Elixir](https://elixir-lang.org/) - The language
- [Phoenix](https://www.phoenixframework.org/) - The web framework
- [Tailwind CSS](https://tailwindcss.com/) - The CSS framework

## Contributing

Any contribution is welcome, there's as much code as translation, art, bug
hunting, etc. to work on. But before you get started, please see the
following:

- [nyght Code of Conduct](./CODE_OF_CONDUCT.md)
- [nyght Contribution guidelines](./CONTRIBUTING.md)

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions
available, see the [releases](https://gitlab.com/nyght/nyght/-/releases).

All the changes are listed in [the changelog](./CHANGELOG.md).

## Authors

- **Loïc Stankovic** - Initial work

See also the list of [contributors](https://gitlab.com/nyght/nyght/-/graphs/master)
who participated in this project.

## License

This project is licensed under the GNU Affero General Public License v3.0 - see the [LICENSE](./LICENSE)
file for details.

## Sponsors

This project was originally created for and is supported by
[Ebullition](https://ebull.ch). It was also partly funded by the canton
of Fribourg.

We are supported by [Zulip](https://zulip.com). Zulip is an
open-source modern team chat app designed to keep both live and
asynchronous conversations organized.

![Zulip logo](./docs/zulip-org-logo.png){width=250px}

[Weblate](https://weblate.org) also supports the project by offering the
translation platform.

Last but not least, the application performance monitoring on nyght.ch
is sponsored by [AppSignal](https://www.appsignal.com).
