defmodule Nyght.MixProject do
  use Mix.Project

  def project do
    [
      app: :nyght,
      version: "0.9.0",
      elixir: "~> 1.14",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        test: :test,
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.html": :test,
        "coveralls.cobertura": :test,
        "test.watch": :test
      ],
      aliases: aliases(),
      deps: deps(),
      gettext: [
        write_reference_line_numbers: false,
        write_reference_comments: false
      ]
    ] ++ umbrella_config(System.get_env("UMBRELLA") == "true")
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Nyght.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  defp umbrella_config(true) do
    [
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock"
    ]
  end

  defp umbrella_config(_), do: []

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:appsignal, "~> 2.0"},
      {:appsignal_phoenix, "~> 2.0"},
      {:bandit, "~> 1.4"},
      {:bcrypt_elixir, "~> 3.1"},
      {:cors_plug, "~> 3.0"},
      {:credo, "~> 1.7", only: [:dev, :test], runtime: false},
      {:earmark, "~> 1.4"},
      {:ecto_dev_logger, "~> 0.10"},
      {:ecto_sql, "~> 3.11"},
      {:esbuild, "~> 0.8", runtime: Mix.env() == :dev},
      {:ex_aws_s3, "~> 2.5"},
      {:ex_aws, "~> 2.5"},
      {:ex_cldr_calendars, "~> 2.0"},
      {:ex_cldr_dates_times, "~> 2.16"},
      {:ex_cldr_plugs, "~> 1.3"},
      {:ex_cldr_territories, "~> 2.8"},
      {:ex_cldr, "~> 2.37"},
      {:ex_machina, "~> 2.8.0", only: :test},
      {:excoveralls, "~> 0.18.0", only: [:dev, :test]},
      {:faker, "~> 0.17", only: [:dev, :test]},
      {:floki, ">= 0.30.0", only: :test},
      {:flop_phoenix, "~> 0.24.1"},
      {:flop, "~> 0.26.1"},
      {:gettext, "~> 0.24"},
      {:git_hooks, "~> 0.8.0", only: [:dev], runtime: false},
      {:hackney, "1.21.0"},
      {:html_sanitize_ex, "~> 1.4"},
      {:image, "~> 0.39"},
      {:jason, "~> 1.2"},
      {:junit_formatter, "~> 3.3", only: [:test]},
      {:mjml, "~> 4.0"},
      {:mix_test_watch, "~> 1.0", only: [:dev, :test], runtime: false},
      {:nimble_csv, "~> 1.2"},
      {:oban, "~> 2.16"},
      {:phoenix_ecto, "~> 4.6"},
      {:phoenix_html, "~> 4.2"},
      {:phoenix_live_dashboard, "~> 0.8.6"},
      {:phoenix_live_reload, "~> 1.5", only: :dev},
      {:phoenix_live_view, "~> 1.0.5"},
      {:phoenix, "~> 1.7.20"},
      {:postgrex, ">= 0.0.0"},
      {:random_colour, "~> 0.1.0"},
      {:sobelow, "~> 0.13", only: [:dev, :test], runtime: false},
      {:swoosh, "~> 1.5"},
      {:tailwind, "~> 0.3", runtime: Mix.env() == :dev},
      {:telemetry_metrics, "~> 1.1"},
      {:telemetry_poller, "~> 1.0"},
      {:telemetry_registry, "~> 0.3"},
      {:typed_ecto_schema, "~> 0.4.1", runtime: false},
      {:tz_extra, "~> 0.26"},
      {:tz, "~> 0.26"}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to install project dependencies and perform other setup tasks, run:
  #
  #     $ mix setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      setup: [
        "deps.get",
        "git_hooks.install",
        "cmd npm install --prefix assets",
        "ecto.setup",
        "perm.install"
      ],
      seed: ["run priv/repo/dev_seeds.exs"],
      test: ["ecto.create --quiet", "ecto.migrate --quiet", "test"],
      build: [
        "deps.get --only prod",
        "compile --all-warnings --warnings-as-errors",
        "cmd npm ci --prefix assets",
        "esbuild default --minify",
        "tailwind default --minify",
        "phx.digest",
        "release --overwrite"
      ],
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs", "perm.install"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      "assets.deploy": [
        "cmd npm install --prefix assets",
        "esbuild default --minify",
        "tailwind default --minify",
        "phx.digest"
      ]
    ]
  end
end
