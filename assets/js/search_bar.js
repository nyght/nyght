export default SearchBar = {
  mounted() {
    this.handleEvent("selected", ({id: id, selected: selected}) => {
      if(id === this.el.id)
        this.setHiddenInputValue(selected);
    });
  },

  setHiddenInputValue(value) {
    const hidden_input = this.el.querySelector(".search-selected-id > input[type=hidden]")
    hidden_input.value = value
    hidden_input.dispatchEvent(new Event('input', {bubbles: true}))
},
}