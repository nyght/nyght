import EasyMDE from "easymde"

export const MarkdownEditor = {
  mounted() {
    this.updated()
  },
  updated() {
    const editor = new EasyMDE({
      element: this.el,
      forceSync: true,
      autoDownloadFontAwesome: false,
      spellChecker: false,
      toolbar: [
        {
          name: "bold",
          action: EasyMDE.toggleBold,
          className: "ti-bold p-2",
          title: "Bold",
        },
        {
          name: "italic",
          action: EasyMDE.toggleItalic,
          className: "ti-italic",
          title: "Bold",
        },
        {
          name: "heading",
          action: EasyMDE.toggleHeadingSmaller,
          className: "ti-heading",
          title: "Heading",
        },
        "|",
        {
          name: "quote",
          action: EasyMDE.toggleBlockquote,
          className: "ti-quote",
          title: "Quote",
        },
        {
          name: "ul",
          action: EasyMDE.toggleUnorderedList,
          className: "ti-list",
          title: "Unordered List",
        },
        {
          name: "ol",
          action: EasyMDE.toggleOrderedList,
          className: "ti-list-numbers",
          title: "Ordered List",
        },
        "|",
        {
          name: "link",
          action: EasyMDE.drawLink,
          className: "ti-link",
          title: "Create Link",
        },
        "|",
        {
          name: "help",
          action: 'https://www.markdownguide.org/basic-syntax/',
          className: "ti-question-mark",
          title: "Markdown Guide",
        },
      ]
    });
  }
};