import {
  computePosition,
  autoUpdate,
  offset,
  flip,
  autoPlacement
} from '@floating-ui/dom';

export default Dropdown = {
  cleanup: null,
  button: null,
  menu: null,
  isOpen: false,

  show() {
    this.menu.setAttribute('data-show', '');
    this.isOpen = true;
  },

  hide() {
    this.menu.removeAttribute('data-show');
    this.isOpen = false;
  },

  mounted() {
    this.button = this.el.querySelector('button');
    this.menu = this.el.querySelector('.dropdown-content');

    const placement = this.el.dataset.placement;

    this.cleanup = autoUpdate(this.button, this.menu, () => {
      computePosition(this.button, this.menu, {
        placement,
        middleware: [
          offset(6),
          flip()
        ]
      }).then(({x, y}) => {
        Object.assign(this.menu.style, {
          left: `${x}px`,
          top: `${y}px`,
        });
      })
    });

    document.addEventListener('click', (event) => {
      // the menu is showed/hidden in the CSS using the data-show attribute
      if (this.button.contains(event.target) && !this.isOpen) {
        this.show();
      } else {
        this.hide();
      }
    });
  },

  destroyed() {
    this.cleanup();
  }
}
