const fs = require('fs');
const path = require('path');

function getContent(fullPath) {
  return fs
    .readFileSync(fullPath)
    .toString()
    .replace(/\r?\n|\r/g, '')
    .replace('height="24"', '')
    .replace('width="24"', '');
}

function readIcons(iconsDir) {
  let values = {};

  fs.readdirSync(iconsDir).forEach((file) => {
    let name = path.basename(file, '.svg');
    values[name] = { name, fullPath: path.join(iconsDir, file) };
  });

  return values;
}

module.exports = function tablerIcons({ matchComponents, theme }) {
  const iconsDir = path.join(__dirname, '../node_modules/@tabler/icons/icons/outline/');

  console.log(`Start processing Tabler Icons from ${iconsDir}`);

  const values = readIcons(iconsDir);

  matchComponents(
    {
      ti: ({ name, fullPath }) => {
        const content = getContent(fullPath);

        return {
          [`--ti-${name}`]: `url('data:image/svg+xml;utf8,${content}')`,
          '-webkit-mask': `var(--ti-${name})`,
          'mask-repeat': 'no-repeat',
          'background-color': 'currentColor',
          'vertical-align': 'middle',
          mask: `var(--ti-${name})`,
          display: 'inline-block',
          width: theme('spacing.5'),
          height: theme('spacing.5'),
        };
      },
    },
    { values },
  );
}
