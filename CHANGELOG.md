# Changelog

## [0.9.0] - 2024.06.07

### Added

- Add the delete account feature (!200) (@Houlee)
- Add member auto-approval (!199) (@nyght_dev)
- Add staff list export (!203) (@nyght_dev)
- Add default roles (!195) (@nyght_dev)

### Changed

- Improve the shifts page UI (!203) (@nyght_dev)
- Change the member API and remove MemberRequests (!197)  (@nyght_dev)

### Fixed

- Fix email validation banner UI completely (!196) (@Houlee)

## [0.8.9] - 2024.05.11

### Added

- Add confirmation email when an availability shift has been updated (!167) (@nyght_dev)
- Add organizations short description (!168) (@nyght_dev)
- Add AppSignal application monitoring (!170) (@nyght_dev)

### Changed

- Change the randomly generated event posters (!178) (@nyght_dev)
- Change the shift list UI (!184) (@nyght_dev)

### Fixed

- Fix long events name display in the event list (!177) (@nyght_dev)
- Fix dev database seeding (!183) (@nyght_dev)
- Fix event deletion (!182) (@nyght_dev)
- Fix performers list and creation/update form (!185) (@nyght_dev)
- Fix links in emails (!187) (@nyght_dev)
- Fix email validation banner UI (!192) (@Houlee)
- Fix shift assignation with no user (!194) (@Houlee)

### Removed

- Remove home redirection (!188) (@nyght_dev)
- Remove back buttons (!193) (@Houlee)

## [0.8.8] - 2024.02.04

### Added

- Add an "Accept" button to shift applications (!163) (@nyght_dev)

## [0.8.7] - 2024.01.16

### Fixed

- Fix missing status filter for the event list (!162) (@nyght_dev)

## [0.8.6] - 2024.01.15

### Fixed

- Fix some spacing issues in tables (@nyght_dev)

## [0.8.5] - 2024.01.15

### Added

- Use Flop for filtering, sorting and pagination pagination of events
  and members for UI and API (!160) (@nyght_dev)

## [0.8.4] - 2024.01.05

### Added

- Use HyperDX for app monitoring (!153) (@nyght_dev)

### Changed

- Refactor the UI library (!158) (@nyght_dev)
- Upgrade Phoenix, Elixir, Erlang and all the other
  dependencies (!152) (@nyght_dev)

### Fixed

- Fix incorrect link in member welcome email (!159) (@nyght_dev)
- Fix the poster upload (!155) (@nyght_dev)
- Fix buttons size on mobile (!158) (@nyght_dev)

### Removed

- Remove JSON-LD support (!156) (@nyght_dev)
- Remove the calendar component and a few other half-baked
  features (!158) (@nyght_dev)

## [0.8.3] - 2023.12.24

### Changed

- Use user id instead of member id for member pages (!151) (@nyght_dev)

### Fixed

- Fix redirection when closing the "Already member" modal (!150) (@nyght_dev)

### Removed

- Remove unfinished features and palceholders (!149) (@nyght_dev)

## [0.8.2] - 2023.12.19

### Changed

- Add the total number of openings in the shift view (!146) (@Houlee)
- Improve the UX of joining an organization (!148) (@nyght_dev)

### Fixed

- Fix roles name uniqueness across all organizations (!144) (@nyght_dev)
- Fix the next/previous events on the events page (!147) (@nyght_dev)

## [0.8.1] - 2023.11.13

### Changed

- Rewrite the CHANGELOG and update GitLab templates (!143) (@nyght_dev)

### Fixed

- Fix the back button (!140) (@Houlee)
- Fix event cancelling (!141) (@Houlee)
- Fix role name index for multitenancy (!144) (@nyght_dev)

## [0.8.0] - 2023.10.03

### Added

- Add a `page_size` query parameter to the API (!137) (@nyght_dev)
- Add a detailed shifts overview in the event list (@nyght_dev)

### Changed

- Refactor pubsub (@nyght_dev)

### Fixed

- Fix shift list (!138) (@nyght_dev)
- Fix shift application count (!139) (@nyght_dev)

## [0.7.3] - 2023.08.15

### Added

- Add names to ticketing links (!134) (@nyght_dev)
- Add performers to events in the REST API (!135) (@nyght_dev)

### Fixed

- Fix event copy with their shifts (!136) (@nyght_dev)

## [0.7.2] - 2023.07.25

### Added

- Add ticketing links (!133) (@nyght_dev)

### Changed

- Refactor the events API (@nyght_dev)
- Make the shifts start and end times relative to the event they
  belong (!132) (@nyght_dev)
- Refactor the date and time components (@nyght_dev)

### Fixed

- Fix the events actions in the event list (@nyght_dev)

## [0.7.1] - 2023.07.25

### Added

- Add more information in bookings (!130) (@nyght_dev)

### Fixed

- Fix performers list crash when performer without country is
  present (@nyght_dev)
- Fix the performer search bar in the event form when adding multiple
  performers (@nyght_dev)

## [0.7.0] - 2023.07.18

This release adds the basic performer management and a big rewrite of
the UI.

### Added

- Support devcontainers (!122) (@nyght_dev)
- Add performers to events (!126) (@nyght_dev)
- Translate form inputs (!128) (@nyght_dev)

### Changed

- Refactor shift management UI/UX (!124) (@nyght_dev)
- Refactor the event creation UI (!125) (@nyght_dev)
- Refactor liveviews (@nyght_dev)
- Fix line breaks in pills (!123) (@Houlee)

## [0.6.6] - 2023.04.28

### Changed

- **Breaking:** Clean the migrations (!119) (@nyght_dev)
- Use the Phoenix streams API (!120) (@nyght_dev)

### Fixed

- Fix the members-only events in the calendar view (!121) (@nyght_dev)

## [0.6.5] - 2023.03.30

### Fixed

- Fix the events REST API (!117) (@nyght_dev)

## [0.6.4] - 2023.03.27

### Added

- Allow accepting a rejected application (@nyght_dev)

### Fixed

- Fix the event pagination (!115) (@nyght_dev)
- Fix the event page for members (!114) (@nyght_dev)

## [0.6.3] - 2023.03.11

### Changed

- Move the members routes to a new `/app` scope (@nyght_dev)

### Fixed

- Fix the navigation (@nyght_dev)
- Fix dropdowns issues (!112) (@nyght_dev)

## [0.6.2] - 2023.02.14

### Fixed

- Fix empty event posters (!109) (@nyght_dev)

## [0.6.1] - 2023.02.14

### Changed

- Refactor the shift UI (@nyght_dev)

### Fixed

- Fix long event names in the next/previous event links (@nyght_dev)
- Remove the rejected and pending applications from the recent activity list
  of the profile page (!108) (@nyght_dev)

## [0.6.0] - 2023.02.13

### Added

- Add navigation between events (@nyght_dev)
- Support application strategies for shifts (@nyght_dev)

### Changed

- Redirect members to the admin page (!114) (@nyght_dev)
- Support clicking on a user's avatar to go to its member page if the
  current user has the correct permission (`:read, Member`) (@nyght_dev)
- Support empty "total openings" field in the shift form instead of having to
  set it to 0 for shifts without maximum (!124) (@nyght_dev)
- Rename `Opening` to `Application` and add a new `Shifts`
  context (!117) (@nyght_dev)
- Change the status of a shift to `waiting_replacement` when leaving it
  instead of deleting it (!104) (@nyght_dev)
- Change the status of a removed shift application to `rejected` instead
  of removing it (!104) (@nyght_dev)

### Fixed

- Fix the behaviour of the back button (!111) (@nyght_dev)
- Fix JSON-LD to be valid (!105) (@nyght_dev)

## [0.5.2] - 2023.01.26

### Changed

- Bump Phoenix to 1.7-rc2 (!113) (@nyght_dev)

### Fixed

- Fix action buttons on small screens (!99) (@nyght_dev)

## [0.5.1] - 2023.01.11

### Changed

- Use shorter time format (@nyght_dev)

### Fixed

- Fix shifts timezones (!97) (@nyght_dev)

## [0.5.0] - 2023.01.11

### Added

- Add prepublished event status (!95) (@nyght_dev)
- Add event copy (!96) (@nyght_dev)

### Changed

- Bump Phoenix to 1.7-rc0 (!92) (@nyght_dev)
- Use MJML for better email rendering (@nyght_dev)
- Refactor the authorization system for more granular permission and
  better API (!93) (@nyght_dev)
- Refactor some of the UI components for better permission integration (@nyght_dev)

### Fixed

- Fix checkboxes (@nyght_dev)

## [0.4.1] - 2022.12.14

### Added

- Add contact and event export into vCard and iCal files (!85) (@nyght_dev)
- Add a footer (!87) (@nyght_dev)
- Generate random color for users avatars (!90) (@nyght_dev)

### Changed

- Allow shifts without max staff count (!86) (@nyght_dev)

### Fixed

- Fix sorting for different lists (!88) (@nyght_dev)
- Fix page on medium screens (!89) (@nyght_dev)

## [0.4.0] - 2022.11.22

### Added

- Add pronouns, diet and allergies fields to users (!69) (@nyght_dev)
- Add profile and member pages to view users details (!70) (@nyght_dev)
- Add shifts description field (!80) (@nyght_dev)
- Add a calendar view for the admin events management (!83) (@nyght_dev)
- Add basic SEO metadata for events and organizations (!84) (@nyght_dev)

### Changed

- Refactor completely the UI (!68) (@nyght_dev)
- Store the events posters in a S3 compatible object storage (!72) (@nyght_dev)
- Move dates and times formatting to the backend (!82) (@nyght_dev)

### Fixed

- Fix dropdowns in lists (!73) (@nyght_dev)

## [0.3.2] - 2022.10.13

### Added

- Add the events REST API (!67) (@nyght_dev)

### Fixed

- Fix events and members lists loading (@nyght_dev)

## [0.3.1] - 2022.10.11

### Fixed

- Fix the CSP configuration (!65) (@nyght_dev)

## [0.3.0] - 2022.10.04

Not a lot of visible changes and features for this release, but a lot of
small technical improvements, and better developer experience.

### Added

- Add users nickname (!57) (@nyght_dev)
- Add the "members-only" event flag (!58) (@nyght_dev)
- Add organization update page (!61) (@nyght_dev)

### Changed

- Remove the `is_online` flag for events and replace it with the "online"
  event type (!59) (@nyght_dev)
- Refactor the public events page (@nyght_dev)
- Refactor the permission system (!63) (@nyght_dev)

## [0.2.2] - 2022.09.14

### Added

- Show a banner asking the user to validate their account (!46) (@nyght_dev)
- Add member assignation to a shift (!51) (@nyght_dev)
- Add an overview of the shifts in the admin events list (!52) (@nyght_dev)

### Changed

- Show the users full name instead of their email address in the avatar
  component (!54) (@nyght_dev)

### Fixed

- Fix the shifts start and end times convertion to the organizations'
  timezone (!50) (@nyght_dev)

## [0.2.1] - 2022.09.06

### Fixed

- Fix invitation cancelling and resending (!44) (@nyght_dev)
- Fix the Docker image building (!43) (@nyght_dev)

## [0.2.0] - 2022.09.06

### Added

- Add events deletion (!34) (@nyght_dev)
- Add staff management features (!36) (@nyght_dev)
- Add events posters (!42) (@nyght_dev)
- Add membership requests (!39) (@nyght_dev)
- Add a placeholder for empty lists (!41) (@nyght_dev)

### Changed

- Move the event edition page to `/events/edit/:id` (@nyght_dev)
- **Breaking:** Clean the migrations (!40) (@nyght_dev)

### Fixed

- Fix the adminssion price form (!32) (@nyght_dev)
- Make the expired invitations visibles in the user management
  page (!35) (@nyght_dev)

## [0.1.2] - 2022.08.09

### Added

- Support translations (!26) (@nyght_dev)
- Support multiple roles for members (!29) (@nyght_dev)

### Fixed

- Fix UI on smaller screens (!30) (@nyght_dev)

## [0.1.1] - 2022.07.29

### Added

- Add invites cancellation and resending (!24) (@nyght_dev)

### Changed

- Refactor the authentication system (@nyght_dev)
- Change password requirements (@nyght_dev)

### Fixed

- Fix `mix` task aliases using `npm` (@nyght_dev)
- Fix the admin sidebar when scrolling the page (!25) (@nyght_dev)

## [0.1.0] - 2022.07.21

First real release! Contains the basic user and event management, RBAC,
emails and all the basic stuff.

[0.9.0]: https://gitlab.com/nyght/nyght/-/compare/v0.8.9...v0.9.0
[0.8.9]: https://gitlab.com/nyght/nyght/-/compare/v0.8.8...v0.8.9
[0.8.8]: https://gitlab.com/nyght/nyght/-/compare/v0.8.7...v0.8.8
[0.8.7]: https://gitlab.com/nyght/nyght/-/compare/v0.8.6...v0.8.7
[0.8.6]: https://gitlab.com/nyght/nyght/-/compare/v0.8.5...v0.8.6
[0.8.5]: https://gitlab.com/nyght/nyght/-/compare/v0.8.4...v0.8.5
[0.8.4]: https://gitlab.com/nyght/nyght/-/compare/v0.8.3...v0.8.4
[0.8.3]: https://gitlab.com/nyght/nyght/-/compare/v0.8.2...v0.8.3
[0.8.2]: https://gitlab.com/nyght/nyght/-/compare/v0.8.1...v0.8.2
[0.8.1]: https://gitlab.com/nyght/nyght/-/compare/v0.8.0...v0.8.1
[0.8.0]: https://gitlab.com/nyght/nyght/-/compare/v0.7.3...v0.8.0
[0.7.3]: https://gitlab.com/nyght/nyght/-/compare/v0.7.2...v0.7.3
[0.7.2]: https://gitlab.com/nyght/nyght/-/compare/v0.7.1...v0.7.2
[0.7.1]: https://gitlab.com/nyght/nyght/-/compare/v0.7.0...v0.7.1
[0.7.0]: https://gitlab.com/nyght/nyght/-/compare/v0.6.5...v0.7.0
[0.6.5]: https://gitlab.com/nyght/nyght/-/compare/v0.6.4...v0.6.5
[0.6.4]: https://gitlab.com/nyght/nyght/-/compare/v0.6.3...v0.6.4
[0.6.3]: https://gitlab.com/nyght/nyght/-/compare/v0.6.2...v0.6.3
[0.6.2]: https://gitlab.com/nyght/nyght/-/compare/v0.6.1...v0.6.2
[0.6.1]: https://gitlab.com/nyght/nyght/-/compare/v0.6.0...v0.6.1
[0.6.0]: https://gitlab.com/nyght/nyght/-/compare/v0.5.2...v0.6.0
[0.5.2]: https://gitlab.com/nyght/nyght/-/compare/v0.5.1...v0.5.2
[0.5.1]: https://gitlab.com/nyght/nyght/-/compare/v0.5.0...v0.5.1
[0.5.0]: https://gitlab.com/nyght/nyght/-/compare/v0.4.1...v0.5.0
[0.4.1]: https://gitlab.com/nyght/nyght/-/compare/v0.4.0...v0.4.1
[0.4.0]: https://gitlab.com/nyght/nyght/-/compare/v0.3.2...v0.4.0
[0.3.2]: https://gitlab.com/nyght/nyght/-/compare/v0.3.1...v0.3.2
[0.3.1]: https://gitlab.com/nyght/nyght/-/compare/v0.3.0...v0.3.1
[0.3.0]: https://gitlab.com/nyght/nyght/-/compare/v0.2.2...v0.3.0
[0.2.2]: https://gitlab.com/nyght/nyght/-/compare/v0.2.1...v0.2.2
[0.2.1]: https://gitlab.com/nyght/nyght/-/compare/v0.2.0...v0.2.1
[0.2.0]: https://gitlab.com/nyght/nyght/-/compare/v0.1.2...v0.2.0
[0.1.2]: https://gitlab.com/nyght/nyght/-/compare/v0.1.1...v0.1.2
[0.1.1]: https://gitlab.com/nyght/nyght/-/compare/v0.1.0...v0.1.1
[0.1.0]: https://gitlab.com/nyght/nyght/-/commits/v0.1.0
