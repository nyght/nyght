defmodule Nyght.Shifts.ShiftsTest do
  @moduledoc false
  use Nyght.DataCase, async: true

  import Nyght.Factory

  alias Ecto.Changeset
  alias Nyght.Events.Event
  alias Nyght.Organizations
  alias Nyght.Shifts
  alias Nyght.Shifts.{Application, Shift}
  alias Nyght.Shifts.Events.{ShiftCreated, ShiftUpdated}

  setup do
    org = insert(:organization, timezone: "Europe/Zurich")
    event = insert(:event, organization: org)
    role = insert(:role, organization: org)

    shift = insert(:shift, event: event, role: role)
    second_shift = insert(:shift, event: event, role: role)

    user = insert(:user)

    member = insert(:member, user: user, organization: org, roles: [role])

    %{
      org: org,
      event: event,
      shift: shift,
      second_shift: second_shift,
      role: role,
      user: user,
      member: member
    }
  end

  ############################################################
  # Shifts
  ############################################################

  describe "list_shifts_for/1" do
    test "returns all the shift for the given event", %{
      event: event,
      shift: shift,
      second_shift: second_shift
    } do
      {:ok, {shifts, meta}} = Shifts.list_shifts_for(event, %{})
      assert meta.total_count == 2
      assert Enum.any?(shifts, fn s -> s.id == shift.id end)
      assert Enum.any?(shifts, fn s -> s.id == second_shift.id end)
    end

    test "when there is no shift, return an empty list", %{org: org} do
      event = insert(:event, organization: org)
      {:ok, {shifts, _meta}} = Shifts.list_shifts_for(event, %{})
      assert Enum.empty?(shifts)
    end
  end

  describe "get_shift!/1" do
    test "with a valid id, returns the corresponding shift", %{shift: shift} do
      assert Shifts.get_shift!(shift.id).id == shift.id
    end

    test "with an invalid id, raises an error" do
      assert_raise(Ecto.NoResultsError, fn ->
        Shifts.get_shift!("11111111-1111-1111-1111-111111111111")
      end)
    end
  end

  describe "create_shift/2" do
    test "with valid data, creates a shift for an event", %{event: event, role: role} do
      attrs =
        params_for(:shift,
          local_starts_at: DateTime.add(event.starts_at, -10, :minute),
          local_ends_at: DateTime.add(event.ends_at, 10, :minute),
          role_id: role.id
        )

      assert {:ok, %Shift{} = shift} = Shifts.create_shift(event, attrs)
      assert shift.start_offset == -10 * 60
      assert shift.end_offset == 10 * 60
      assert shift.role_id == role.id
      assert shift.title == attrs.title
      assert shift.description == attrs.description
      assert shift.application_strategy == attrs.application_strategy
    end

    test "with successful creation of a shift, sends a message to all subscribers", %{
      event: %Event{id: event_id} = event,
      role: role,
      org: org
    } do
      :ok = Organizations.subscribe(org)

      attrs =
        params_for(:shift,
          local_starts_at: DateTime.add(event.starts_at, -10, :minute),
          local_ends_at: DateTime.add(event.ends_at, 10, :minute),
          role_id: role.id
        )

      assert {:ok, %Shift{} = shift} = Shifts.create_shift(event, attrs)
      assert_received {Nyght.Shifts, %ShiftCreated{event_id: ^event_id, shift: event_shift}}
      assert event_shift.id == shift.id
    end

    test "with invalid data, returns an error", %{event: event} do
      attrs = %{}

      assert {:error, %Changeset{} = changeset} = Shifts.create_shift(event, attrs)
      assert "can't be blank" in errors_on(changeset).role_id
      assert "can't be blank" in errors_on(changeset).application_strategy
      refute_received {Nyght.Shifts, %ShiftCreated{}}
    end

    test "with clock gap in end time because of DST change, returns an error", %{
      org: org,
      role: role
    } do
      summer_time = build(:summer_time)

      event =
        insert(:event,
          organization: org,
          starts_at: DateTime.add(DateTime.from_naive!(summer_time, "Europe/Zurich"), -4, :hour),
          ends_at: summer_time,
          timezone: org.timezone
        )

      attrs =
        params_for(:shift,
          local_starts_at: NaiveDateTime.add(event.starts_at, -10, :minute),
          local_ends_at: NaiveDateTime.add(summer_time, -30, :minute),
          role_id: role.id
        )

      assert {:error, %Changeset{} = changeset} = Shifts.create_shift(event, attrs)

      assert "not a valid time because of daylight saving time" in errors_on(changeset).local_ends_at
    end

    test "with ambiguous end time because of DST change, returns a shift with the correct time shift",
         %{
           org: org,
           role: role
         } do
      winter_time = build(:winter_time)
      {:ambiguous, _, winter_dt} = DateTime.from_naive(winter_time, "Europe/Zurich")

      event =
        insert(:event,
          organization: org,
          starts_at: DateTime.add(winter_dt, -4, :hour),
          ends_at: winter_dt,
          timezone: org.timezone
        )

      attrs =
        params_for(:shift,
          local_starts_at: NaiveDateTime.add(event.starts_at, -10, :minute),
          local_ends_at: winter_time,
          role_id: role.id
        )

      assert {:ok, %Shift{} = shift} = Shifts.create_shift(event, attrs)
      assert shift.end_offset == 0
    end
  end

  describe "update_shift/2" do
    test "with valid data, updates a shift", %{event: event, shift: shift} do
      attrs =
        params_for(:shift,
          local_starts_at: DateTime.add(event.starts_at, -10, :minute),
          local_ends_at: DateTime.add(event.ends_at, 10, :minute),
          application_strategy: :availability,
          total_openings: 8
        )

      assert {:ok, %Shift{} = updated_shift} = Shifts.update_shift(shift, attrs)
      assert updated_shift.id == shift.id
      assert updated_shift.start_offset == -10 * 60
      assert updated_shift.end_offset == 10 * 60
      assert updated_shift.role_id == shift.role_id
      assert updated_shift.title == attrs.title
      assert updated_shift.description == attrs.description
      assert updated_shift.application_strategy == attrs.application_strategy
    end

    test "with successful update of a shift, sends a message to all subscribers", %{
      event: %Event{id: event_id} = event,
      shift: shift,
      org: org
    } do
      :ok = Organizations.subscribe(org)

      attrs =
        params_for(:shift,
          local_starts_at: DateTime.add(event.starts_at, -10, :minute),
          local_ends_at: DateTime.add(event.ends_at, 10, :minute),
          application_strategy: :availability,
          total_openings: 8
        )

      assert {:ok, %Shift{}} = Shifts.update_shift(shift, attrs)
      assert_received {Nyght.Shifts, %ShiftUpdated{event_id: ^event_id, shift: message_shift}}
      assert message_shift.id == shift.id
    end

    test "with clock gap in end time because of DST change, returns an error", %{
      org: org,
      role: role
    } do
      summer_time = build(:summer_time)

      event =
        insert(:event,
          organization: org,
          starts_at: DateTime.add(DateTime.from_naive!(summer_time, "Europe/Zurich"), -4, :hour),
          ends_at: summer_time,
          timezone: org.timezone
        )

      shift = insert(:shift, event: event, role: role)

      attrs =
        params_for(:shift,
          local_starts_at: NaiveDateTime.add(event.starts_at, -10, :minute),
          local_ends_at: NaiveDateTime.add(summer_time, -30, :minute),
          role_id: role.id
        )

      assert {:error, %Changeset{} = changeset} = Shifts.update_shift(shift, attrs)

      assert "not a valid time because of daylight saving time" in errors_on(changeset).local_ends_at
    end

    test "with ambiguous end time because of DST change, returns a shift with the correct time shift",
         %{
           org: org,
           role: role
         } do
      winter_time = build(:winter_time)
      {:ambiguous, _, winter_dt} = DateTime.from_naive(winter_time, "Europe/Zurich")

      event =
        insert(:event,
          organization: org,
          starts_at: DateTime.add(winter_dt, -4, :hour),
          ends_at: winter_dt,
          timezone: org.timezone
        )

      shift = insert(:shift, event: event, role: role)

      attrs =
        params_for(:shift,
          local_starts_at: NaiveDateTime.add(event.starts_at, -10, :minute),
          local_ends_at: winter_time,
          role_id: role.id
        )

      assert {:ok, %Shift{} = shift} = Shifts.update_shift(shift, attrs)
      assert shift.end_offset == 0
    end
  end

  describe "change_shift/2" do
    test "returns a changeset", %{event: event, shift: shift} do
      assert %Ecto.Changeset{} = Shifts.change_shift(shift, event)
    end

    test "with valid attributes, returns a changeset", %{event: event, shift: shift} do
      assert %Ecto.Changeset{} = Shifts.change_shift(shift, event, %{total_openings: 6})
    end

    test "with invalid attributes, returns a changeset", %{event: event, shift: shift} do
      assert %Ecto.Changeset{} =
               changeset = Shifts.change_shift(shift, event, %{total_openings: -2})

      assert "must be greater than 0" in errors_on(changeset).total_openings
    end

    test "with delete boolean to true, returns a delete changeset", %{event: event, shift: shift} do
      assert %Ecto.Changeset{action: :delete} =
               Shifts.change_shift(shift, event, %{"delete" => "true"})
    end
  end

  describe "apply_for_shift/2" do
    test "with valid data, assign a user to the shift", %{member: member, shift: shift} do
      assert shift.opening_count == 0
      assert {:ok, _} = Shifts.apply_for_shift(shift, member)

      shift = Shifts.get_shift!(shift.id)

      assert shift.opening_count == 1
      assert [%Application{} = application] = shift.shift_applications
      assert application.member_id == member.id
    end

    test "when the given user doesn't have the required role, returns an error", %{
      shift: shift,
      org: org
    } do
      user = insert(:user)

      member = insert(:member, user: user, organization: org)
      assert {:error, :unauthorized} = Shifts.apply_for_shift(shift, member)
    end

    test "when the shift is full, returns an error", %{event: event, role: role, member: member} do
      shift = insert(:shift, event: event, role: role, opening_count: 4)

      assert {:error, :full_shift} = Shifts.apply_for_shift(shift, member)
    end

    test "when the user is already registered, returns an error changeset", %{
      member: member,
      shift: shift
    } do
      assert {:ok, _shift_application} = Shifts.apply_for_shift(shift, member)
      assert {:error, changeset} = Shifts.apply_for_shift(shift, member)
      assert "you already applied for this shift" in errors_on(changeset).shift_id
    end

    test "when the user is nil, returns an error" do
      assert {:error, :user_cannot_be_nil} = Shifts.apply_for_shift(nil, nil)
    end

    test "when the user asked for deletion, returns an error", %{
      org: org,
      role: role,
      shift: shift
    } do
      user = insert(:user, deletion_requested_at: DateTime.utc_now())

      member = insert(:member, user: user, organization: org, roles: [role])
      assert {:error, :user_in_deletion} = Shifts.apply_for_shift(shift, member)
    end
  end

  describe "leave_shift/2" do
    test "with valid data, removes the given user from the shift", %{
      shift: shift,
      member: member
    } do
      {:ok, _shift_application} = Shifts.apply_for_shift(shift, member)

      shift = Shifts.get_shift!(shift.id)
      assert shift.opening_count == 1
      assert {:ok, %Application{status: :waiting_replacement}} = Shifts.leave_shift(shift, member)

      shift = Shifts.get_shift!(shift.id)
      assert shift.opening_count == 0
    end

    test "with invalid user, raises an exception", %{
      shift: shift,
      member: member
    } do
      shift = Shifts.get_shift!(shift.id)
      assert_raise Ecto.NoResultsError, fn -> Shifts.leave_shift(shift, member) end
    end
  end

  describe "stream_shifts_export/1" do
    test "with no shifts, returns an empty stream", %{org: org} do
      event = insert(:event, organization: org)

      assert {:ok, true} =
               Repo.transaction(fn ->
                 Shifts.stream_shifts_export(event)
                 |> Enum.empty?()
               end)
    end

    test "with empty shifts, returns an empty stream", %{org: org, role: role} do
      event = insert(:event, organization: org)
      insert_list(4, :shift, event: event, role: role)

      assert {:ok, true} =
               Repo.transaction(fn ->
                 Shifts.stream_shifts_export(event)
                 |> Enum.empty?()
               end)
    end

    test "with applications of deleted members, return stream with nil values", %{
      org: org,
      role: role
    } do
      event = insert(:event, organization: org)
      shift = insert(:shift, event: event, role: role, name: "test shift")
      insert(:shift_application, shift: shift, member: nil, status: :accepted)

      assert {:ok, [export]} =
               Repo.transaction(fn ->
                 Shifts.stream_shifts_export(event)
                 |> Enum.to_list()
               end)

      refute export.first_name
      refute export.last_name
      refute export.email
      refute export.street_line_1
      refute export.street_line_2
      refute export.street_line_3
      refute export.zip_code
      refute export.city
      refute export.phone
      assert export.shift == "test shift"
      assert export.local_starts_at
      assert export.local_ends_at
    end

    test "with valid data, returns a stream of accepted and waiting_replacement applications", %{
      org: org,
      role: role
    } do
      event = insert(:event, organization: org)

      insert_list(4, :shift, event: event, role: role)
      |> Enum.map(fn shift ->
        insert_list(4, :user)
        |> Enum.zip([:accepted, :waiting_replacement, :rejected, :pending])
        |> Enum.map(fn {user, status} ->
          member = insert(:member, user: user, organization: org, roles: [role])
          insert(:shift_application, shift: shift, member: member, status: status)
        end)
      end)

      assert {:ok, exports} =
               Repo.transaction(fn ->
                 Shifts.stream_shifts_export(event)
                 |> Enum.to_list()
               end)

      assert Enum.count(exports) == 8

      exports
      |> Enum.each(fn export ->
        assert export.first_name
        assert export.last_name
        assert export.email
        assert export.street_line_1
        assert export.street_line_2
        assert export.street_line_3
        assert export.zip_code
        assert export.city
        assert export.phone
        assert export.shift
        assert export.local_starts_at
        assert export.local_ends_at
      end)
    end
  end

  describe "applied?/2" do
    test "with an application for member, returns true", %{org: org, role: role, member: member} do
      event = insert(:event, organization: org)

      for status <- [:accepted, :waiting_replacement, :rejected, :pending] do
        shift = insert(:shift, event: event, role: role)
        insert(:shift_application, shift: shift, member: member, status: status)

        shift = Shifts.get_shift!(shift.id)

        assert Shifts.applied?(member, shift)
      end
    end

    test "without any application for member, returns false", %{
      org: org,
      role: role,
      member: member
    } do
      event = insert(:event, organization: org)
      shift = insert(:shift, event: event, role: role)
      shift = Shifts.get_shift!(shift.id)

      refute Shifts.applied?(member, shift)
    end
  end

  describe "applied_for_future_shift/1" do
    setup do
      one_day_later = DateTime.utc_now() |> DateTime.add(1, :day)
      one_day_ago = DateTime.utc_now() |> DateTime.add(-1, :day)

      %{
        user: insert(:user),
        role: insert(:role),
        future_event: insert(:event, starts_at: one_day_later),
        past_event: insert(:event, starts_at: one_day_ago)
      }
    end

    test "with an event in the future and an accepted application, returns true", %{
      member: member,
      role: role,
      future_event: event
    } do
      shift = insert(:shift, role: role, event: event)
      insert(:shift_application, shift: shift, member: member, status: :accepted)

      assert Shifts.applied_for_future_shift?(member)
    end

    test "with an event in the past and an accepted application, should return false", %{
      member: member,
      role: role,
      past_event: event
    } do
      shift = insert(:shift, role: role, event: event)
      insert(:shift_application, shift: shift, member: member, status: :accepted)

      refute Shifts.applied_for_future_shift?(member)
    end

    test "with an event in the future and a pending application, returns true", %{
      member: member,
      role: role,
      future_event: event
    } do
      shift = insert(:shift, role: role, event: event)
      insert(:shift_application, shift: shift, member: member, status: :pending)

      assert Shifts.applied_for_future_shift?(member)
    end

    test "with an event in the past and a pending application, returns false", %{
      member: member,
      role: role,
      past_event: event
    } do
      shift = insert(:shift, role: role, event: event)
      insert(:shift_application, shift: shift, member: member, status: :pending)

      refute Shifts.applied_for_future_shift?(member)
    end

    test "with an event in the future and a waiting_replacement application, returns true", %{
      member: member,
      role: role,
      future_event: event
    } do
      shift = insert(:shift, role: role, event: event)

      insert(:shift_application, shift: shift, member: member, status: :waiting_replacement)

      assert Shifts.applied_for_future_shift?(member)
    end

    test "with an event in the past and a waiting_replacement application, returns false", %{
      member: member,
      role: role,
      past_event: event
    } do
      shift = insert(:shift, role: role, event: event)

      insert(:shift_application, shift: shift, member: member, status: :waiting_replacement)

      refute Shifts.applied_for_future_shift?(member)
    end

    test "with an event in the future and a rejected application, returns false", %{
      member: member,
      role: role,
      future_event: event
    } do
      shift = insert(:shift, role: role, event: event)
      insert(:shift_application, shift: shift, member: member, status: :rejected)

      refute Shifts.applied_for_future_shift?(member)
    end

    test "with an event in the past and a rejected application, returns false", %{
      member: member,
      role: role,
      past_event: event
    } do
      shift = insert(:shift, role: role, event: event)
      insert(:shift_application, shift: shift, member: member, status: :rejected)

      refute Shifts.applied_for_future_shift?(member)
    end
  end

  describe "accepted_or_not_applied?/2" do
    test "with an accepted application for user, returns true", %{
      org: org,
      role: role,
      member: member
    } do
      event = insert(:event, organization: org)
      shift = insert(:shift, event: event, role: role)
      insert(:shift_application, shift: shift, member: member, status: :accepted)

      shift = Shifts.get_shift!(shift.id)

      assert Shifts.accepted_or_not_applied?(member, shift)
    end

    test "with an application in other status than accepted for user, returns false", %{
      org: org,
      role: role,
      member: member
    } do
      event = insert(:event, organization: org)

      for status <- [:waiting_replacement, :rejected, :pending] do
        shift = insert(:shift, event: event, role: role)
        insert(:shift_application, shift: shift, member: member, status: status)

        shift = Shifts.get_shift!(shift.id)

        refute Shifts.accepted_or_not_applied?(member, shift)
      end
    end

    test "without any application for member, returns true", %{
      org: org,
      role: role,
      member: member
    } do
      event = insert(:event, organization: org)
      shift = insert(:shift, event: event, role: role)
      shift = Shifts.get_shift!(shift.id)

      assert Shifts.accepted_or_not_applied?(member, shift)
    end
  end

  describe "copy_shift/2" do
    test "creates a copy of the given shift", %{org: org, role: role} do
      event = insert(:event, organization: org)
      other_event = insert(:event, organization: org)
      shift = insert(:shift, event: event, role: role)

      assert {:ok, copy} = Shifts.copy_shift(shift, other_event)

      assert copy.id != shift.id
      assert copy.event_id == other_event.id
      assert copy.role_id == shift.role_id
      assert copy.description == shift.description
      assert copy.application_strategy == shift.application_strategy
      assert copy.opening_count == shift.opening_count
    end
  end

  ############################################################
  # ShiftApplications
  ############################################################

  describe "change_shift_application/2" do
    test "returns a changeset", %{shift: shift} do
      shift_application = insert(:shift_application, shift: shift)
      assert %Ecto.Changeset{} = Shifts.change_shift_application(shift_application)
    end

    test "with valid attributes, returns a changeset", %{shift: shift} do
      shift_application = insert(:shift_application, shift: shift)

      assert %Ecto.Changeset{} =
               Shifts.change_shift_application(shift_application, %{name: "test"})
    end
  end

  describe "get_shift_application!/1" do
    test "with a valid id, returns the corresponding shift_application", %{shift: shift} do
      shift_application = insert(:shift_application, shift: shift)
      assert Shifts.get_shift_application!(shift_application.id).id == shift_application.id
    end

    test "with an invalid id, raises an error" do
      assert_raise(Ecto.NoResultsError, fn ->
        Shifts.get_shift!("11111111-1111-1111-1111-111111111111")
      end)
    end
  end

  describe "delete_shift_application/1" do
    test "deletes the shift_application", %{shift: shift, member: member} do
      shift_application = insert(:shift_application, shift: shift, member: member)

      assert {:ok, %Application{status: :rejected}} =
               Shifts.delete_shift_application(shift_application)
    end
  end

  describe "list_applications_for/3" do
    test "returns the applications for a user with the associated data", %{
      org: org,
      role: role,
      member: member
    } do
      events = insert_list(3, :event, organization: org)
      other_member = insert(:member, user: insert(:user), organization: org, roles: [role])

      for event <- events do
        shift = insert(:shift, event: event, role: role)
        insert(:shift_application, member: member, shift: shift)
        insert(:shift_application, member: other_member, shift: shift)
      end

      assert {:ok, {[first_application | _], meta}} = Shifts.list_applications_for(member, %{})

      assert meta.total_count == 3

      assert first_application.shift.event.id in Enum.map(events, & &1.id)
      assert first_application.shift.event.organization.name
      assert first_application.shift.local_starts_at
      assert first_application.shift.local_ends_at
      assert first_application.shift.title
      assert first_application.status
    end
  end
end
