defmodule ShiftApplicationUpdateTest do
  @moduledoc false
  use Nyght.DataCase, async: true

  import Nyght.Factory

  alias Swoosh.Email

  alias Nyght.Workers.ShiftApplicationUpdate

  describe "perform/1" do
    setup do
      org = insert(:organization)
      role = insert(:role)
      user = insert(:user)
      member = insert(:member, user: user, organization: org, roles: [role])

      %{org: org, role: role, user: user, member: member}
    end

    test "with multiple shift updated, sends an summary email", %{
      org: org,
      role: role,
      member: member,
      user: user
    } do
      updated_at = DateTime.utc_now() |> DateTime.add(2, :hour)

      [a, b, c] = events = insert_list(3, :event, organization: org)

      events
      |> Enum.map(&insert(:shift, event: &1, role: role))
      |> Enum.map(fn shift ->
        insert(:shift_application,
          shift: shift,
          member: member,
          updated_at: DateTime.to_naive(updated_at)
        )
      end)

      assert {:ok, %Email{} = email} =
               perform_job(
                 ShiftApplicationUpdate,
                 %{"member_id" => member.id},
                 schedule_in: ShiftApplicationUpdate.update_delay()
               )

      assert [{_, address}] = email.to
      assert address == user.email
      assert email.html_body =~ a.name
      assert email.html_body =~ b.name
      assert email.html_body =~ c.name
    end

    test "with no shift updated, cancel the job", %{member: member} do
      assert {:cancel, :empty} =
               perform_job(
                 ShiftApplicationUpdate,
                 %{"member_id" => member.id},
                 schedule_in: ShiftApplicationUpdate.update_delay()
               )
    end
  end
end
