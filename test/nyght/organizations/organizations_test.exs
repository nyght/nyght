defmodule Nyght.Organizations.OrganizationsTest do
  use Nyght.DataCase, async: true

  import Nyght.Factory

  alias Nyght.Organizations
  alias Nyght.Organizations.Organization

  describe "get_organization!/1" do
    setup do
      %{org: insert(:organization)}
    end

    test "with an existing organization id, returns the corresponding organization", %{org: org} do
      assert Organizations.get_organization!(org.id) == org
    end

    test "with an invalid id, raises an error" do
      assert_raise(Ecto.NoResultsError, fn ->
        Organizations.get_organization!(build(:invalid_guid))
      end)
    end
  end

  describe "get_first_organization/0" do
    test "when multiple organizations exist, returns the first one" do
      [org | _] = insert_list(4, :organization)

      assert Organizations.get_first_organization() == org
    end

    test "when no organization exist, returns nil" do
      assert is_nil(Organizations.get_first_organization())
    end
  end

  describe "get_organization_by_slug/1" do
    test "with an existing slug, returns the corresponding organization" do
      org = insert(:organization, slug: "org")

      assert Organizations.get_organization_by_slug("org") == org
    end

    test "with an invalid slug, returns nil" do
      assert is_nil(Organizations.get_organization_by_slug("org"))
    end

    test "with nil, returns nil" do
      assert is_nil(Organizations.get_organization_by_slug(nil))
    end
  end

  describe "create_organization/1" do
    test "with valid attributes, creates a new organization" do
      valid_attrs = %{name: "Ebull", slug: "ebull"}

      assert {:ok, %Organization{} = org} = Organizations.create_organization(valid_attrs)
      assert org.name == valid_attrs.name
      assert org.slug == valid_attrs.slug
      assert is_nil(org.description)
      assert org.timezone == "Etc/UTC"
    end

    test "with valid attributes and a markdown description, creates a new organization" do
      valid_attrs = %{name: "Ebull", slug: "ebull", description: "# Hello\n**test** org"}

      assert {:ok, %Organization{} = org} = Organizations.create_organization(valid_attrs)
      assert org.name == valid_attrs.name
      assert org.slug == valid_attrs.slug
      assert org.description == valid_attrs.description
      assert org.timezone == "Etc/UTC"
    end

    test "with valid attributes and a timezone, creates a new organization" do
      valid_attrs = %{name: "Ebull", slug: "ebull", timezone: "Europe/Zurich"}

      assert {:ok, %Organization{} = org} = Organizations.create_organization(valid_attrs)
      assert org.name == valid_attrs.name
      assert org.slug == valid_attrs.slug
      assert org.timezone == valid_attrs.timezone
      assert is_nil(org.description)
    end

    test "with invalid attributes, returns an error changeset" do
      invalid_attrs = %{name: nil, slug: nil}

      assert {:error, changeset} = Organizations.create_organization(invalid_attrs)
      assert "can't be blank" in errors_on(changeset).name
      assert "can't be blank" in errors_on(changeset).slug
    end

    test "with an invalid timezone, returns an error changeset" do
      invalid_attrs = %{name: "Ebull", slug: "ebull", timezone: "Foo/Bar"}

      assert {:error, changeset} = Organizations.create_organization(invalid_attrs)
      assert "is not a valid time zone" in errors_on(changeset).timezone
    end

    test "with duplicated slug, returns an error changeset" do
      valid_attrs = %{name: "Ebull", slug: "ebull"}

      insert(:organization, slug: valid_attrs.slug)

      assert {:error, changeset} = Organizations.create_organization(valid_attrs)
      assert "has already been taken" in errors_on(changeset).slug
    end
  end

  describe "update_organization/2" do
    setup do
      %{org: insert(:organization)}
    end

    test "with valid attributes, updates the given organization", %{org: org} do
      valid_attrs = %{name: "Ebull", slug: "ebull", description: "# Hello\n**test** org"}

      assert {:ok, %Organization{} = updated_org} =
               Organizations.update_organization(org, valid_attrs)

      assert updated_org.name == valid_attrs.name
      assert updated_org.slug == valid_attrs.slug
      assert updated_org.description == valid_attrs.description
      assert updated_org.timezone == "Etc/UTC"
    end

    test "with invalid attributes, returns an error changeset", %{org: org} do
      invalid_attrs = %{name: nil, slug: nil}

      assert {:error, changeset} = Organizations.update_organization(org, invalid_attrs)
      assert "can't be blank" in errors_on(changeset).name
      assert "can't be blank" in errors_on(changeset).slug
    end
  end

  describe "change_organization/2" do
    setup do
      %{org: insert(:organization)}
    end

    test "returns a changeset", %{org: org} do
      assert %Ecto.Changeset{} = Organizations.change_organization(org)
    end

    test "with valid attributes, returns a changeset", %{org: org} do
      assert %Ecto.Changeset{} = Organizations.change_organization(org, %{description: "bla"})
    end
  end
end
