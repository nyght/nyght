defmodule Nyght.Organizations.MemberCategoriesTest do
  use Nyght.DataCase, async: true

  import Nyght.Factory

  alias Ecto.Changeset
  alias Nyght.Organizations

  alias Nyght.Organizations.Events.{
    MemberCategoryCreated,
    MemberCategoryDeleted,
    MemberCategoryUpdated
  }

  alias Nyght.Organizations.MemberCategory

  setup do
    org = insert(:organization)
    user = insert(:user)
    member = insert(:member, organization: org, user: user, status: :approved)

    %{org: org, user: user, member: member}
  end

  describe "list_member_categories_for/3" do
    setup %{org: org} do
      insert_list(99, :member_category, organization: org)
      insert(:member_category, organization: org, name: "My category")
      %{}
    end

    test "returns a paginated list of all the member categories for a given organization", %{
      org: org
    } do
      assert {:ok, {list, meta}} = Organizations.list_member_categories_for(org, %{})

      assert Enum.count(list) == 50
      assert meta.current_page == 1
      assert meta.page_size == 50
      assert meta.total_count == 100
    end

    test "with flop filter parameters, returns the filtered member categories", %{org: org} do
      assert {:ok, {list, meta}} =
               Organizations.list_member_categories_for(org, %{
                 filters: [
                   %{field: :name, value: "My", op: :ilike}
                 ]
               })

      assert Enum.count(list) == 1
      assert meta.current_page == 1
      assert meta.page_size == 50
      assert meta.total_count == 1
    end
  end

  describe "list_all_member_categories/1" do
    test "returns all the categories for the given organization", %{org: org} do
      insert_list(10, :member_category, organization: org)
      insert_list(2, :member_category)

      res = Organizations.list_all_member_categories(org)
      assert Enum.count(res) == 10
    end
  end

  describe "create_member_category/2" do
    test "with valid data, creates a new member category", %{org: org} do
      attrs = %{name: "Test category"}
      assert {:ok, %MemberCategory{} = res} = Organizations.create_member_category(org, attrs)
      assert res.name == attrs.name
    end

    test "with invalid data, returns an error", %{org: org} do
      attrs = %{name: nil}
      assert {:error, %Changeset{} = changeset} = Organizations.create_member_category(org, attrs)
      assert "can't be blank" in errors_on(changeset).name
    end

    test "when successful category creation, sends a message to all subscribers", %{
      org: org
    } do
      :ok = Organizations.subscribe(org)

      attrs = %{name: "Test category"}
      assert {:ok, %MemberCategory{id: id}} = Organizations.create_member_category(org, attrs)
      assert_received {Nyght.Organizations, %MemberCategoryCreated{category_id: ^id}}
    end
  end

  describe "change_member_category/2" do
    test "returns a changeset" do
      assert %Changeset{} =
               Organizations.change_member_category(%MemberCategory{}, %{name: "test"})
    end
  end

  describe "update_member_category/2" do
    test "with valid data, updates a member category" do
      cat = insert(:member_category)
      attrs = %{name: "New category name"}
      assert {:ok, res} = Organizations.update_member_category(cat, attrs)
      assert res.name == attrs.name
      assert res.id == cat.id
    end

    test "with invalid data, returns an error changeset" do
      cat = insert(:member_category)

      assert {:error, %Changeset{} = changeset} =
               Organizations.update_member_category(cat, %{name: nil})

      assert "can't be blank" in errors_on(changeset).name
    end

    test "when successful category update, sends a message to all subscribers", %{
      org: org
    } do
      :ok = Organizations.subscribe(org)

      cat = insert(:member_category, organization: org)
      attrs = %{name: "New category name"}
      assert {:ok, %MemberCategory{id: id}} = Organizations.update_member_category(cat, attrs)
      assert_received {Nyght.Organizations, %MemberCategoryUpdated{category_id: ^id}}
    end
  end

  describe "delete_member_category/1" do
    test "with existing category, deletes it" do
      cat = insert(:member_category)

      Organizations.delete_member_category(cat)

      assert is_nil(Repo.get(MemberCategory, cat.id))
    end

    test "when successful category deletion, sends a message to all subscribers", %{
      org: org
    } do
      :ok = Organizations.subscribe(org)

      %MemberCategory{id: id} = cat = insert(:member_category, organization: org)
      Organizations.delete_member_category(cat)

      assert_received {Nyght.Organizations, %MemberCategoryDeleted{category_id: ^id}}
    end
  end
end
