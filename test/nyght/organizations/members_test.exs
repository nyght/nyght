defmodule Nyght.Organizations.MembersTest do
  use Nyght.DataCase, async: true

  import Nyght.Factory

  alias Ecto.Changeset
  alias Nyght.Accounts.User
  alias Nyght.Authorizations.Role
  alias Nyght.Organizations
  alias Nyght.Organizations.{Member, MemberCategory, Organization}

  alias Nyght.Organizations.Events.{
    MemberRequestApproved,
    MemberRequestCreated,
    MemberRequestRejected
  }

  alias Nyght.Repo

  setup do
    org = insert(:organization)
    role = insert(:role, organization: org)
    cat = insert(:member_category, organization: org)
    user = insert(:user, first_name: "John")

    %{org: org, user: user, role: role, category: cat}
  end

  describe "list_members/3" do
    setup %{org: org, user: user} do
      insert_list(99, :user)
      |> Enum.each(&insert(:member, organization_id: org.id, user_id: &1.id, status: :approved))

      insert_list(50, :user)
      |> Enum.each(&insert(:member, organization_id: org.id, user_id: &1.id, status: :banned))

      insert_list(50, :user)
      |> Enum.each(
        &insert(:member, organization_id: org.id, user_id: &1.id, status: :waiting_approval)
      )

      insert(:member, organization_id: org.id, user_id: user.id)

      :ok
    end

    test "returns a paginated list of all approved members", %{org: org} do
      assert {:ok, {list, meta}} = Organizations.list_members(org, %{})

      assert Enum.count(list) == 50
      assert meta.current_page == 1
      assert meta.page_size == 50
      assert meta.total_count == 100
    end

    test "with flop filter parameters, returns the filtered approved members", %{org: org} do
      assert {:ok, {list, meta}} =
               Organizations.list_members(org, %{
                 filters: [
                   %{field: :full_name, value: "John", op: :ilike}
                 ]
               })

      assert Enum.count(list) == 1
      assert meta.current_page == 1
      assert meta.page_size == 50
      assert meta.total_count == 1
    end
  end

  describe "list_member_requests/3" do
    setup %{org: org, user: user} do
      insert_list(99, :user)
      |> Enum.each(
        &insert(:member, organization_id: org.id, user_id: &1.id, status: :waiting_approval)
      )

      insert_list(50, :user)
      |> Enum.each(&insert(:member, organization_id: org.id, user_id: &1.id, status: :banned))

      insert_list(50, :user)
      |> Enum.each(&insert(:member, organization_id: org.id, user_id: &1.id, status: :approved))

      insert(:member, organization_id: org.id, user_id: user.id, status: :waiting_approval)

      :ok
    end

    test "returns a paginated list of all approved members", %{org: org} do
      assert {:ok, {list, meta}} = Organizations.list_member_requests(org, %{})

      assert Enum.count(list) == 50
      assert meta.current_page == 1
      assert meta.page_size == 50
      assert meta.total_count == 100
    end

    test "with flop filter parameters, returns the filtered approved members", %{org: org} do
      assert {:ok, {list, meta}} =
               Organizations.list_member_requests(org, %{
                 filters: [
                   %{field: :full_name, value: "John", op: :ilike}
                 ]
               })

      assert Enum.count(list) == 1
      assert meta.current_page == 1
      assert meta.page_size == 50
      assert meta.total_count == 1
    end
  end

  describe "join/3" do
    setup _ do
      org = insert(:organization, is_auto_approval_enabled: true)

      [org2: org]
    end

    test "with valid input and without member auto-approval, create a new member waiting approval",
         %{org: org, user: user} do
      assert {:ok, %Member{} = member} = Organizations.join(org, user)
      assert member.user_id == user.id
      assert member.organization_id == org.id
      assert member.status == :waiting_approval
      refute member.approved_at
    end

    test "with valid input, with member auto-approval and default roles, create a new approved member",
         %{
           org2: org,
           user: user
         } do
      _default_roles = insert_list(3, :role, organization_id: org.id, is_default: true)

      assert {:ok, %Member{} = member} = Organizations.join(org, user)
      assert member.user_id == user.id
      assert member.organization_id == org.id
      assert member.status == :approved
      assert member.approved_at
      assert Enum.count(member.roles) == 3
    end

    test "with duplicated user, returns an error", %{org: org, user: user} do
      insert(:member, organization_id: org.id, user_id: user.id)

      assert {:error, %Changeset{} = changeset} = Organizations.join(org, user)
      assert "is already member" in errors_on(changeset).user
    end

    test "when successful request creation, sends a message to all subscribers", %{
      org: org,
      user: user
    } do
      :ok = Organizations.subscribe(org)

      assert {:ok, %Member{id: id}} = Organizations.join(org, user)
      assert_received {Nyght.Organizations, %MemberRequestCreated{member_id: ^id}}
    end

    test "when successful member creation with auto-approval, sends a message to all subscribers",
         %{
           org2: org,
           user: user
         } do
      :ok = Organizations.subscribe(org)

      assert {:ok, %Member{id: id}} = Organizations.join(org, user)
      assert_received {Nyght.Organizations, %MemberRequestApproved{member_id: ^id}}
    end
  end

  describe "approve_member/1" do
    test "with a valid member request, approves the member", %{org: org, user: user} do
      request =
        insert(:member, organization_id: org.id, user_id: user.id, status: :waiting_approval)

      assert {:ok, %Member{} = member} = Organizations.approve_member(request)
      assert member.status == :approved
      assert member.approved_at
    end

    test "with a valid member request and roles, approves the member", %{org: org, user: user} do
      request =
        insert(:member, organization_id: org.id, user_id: user.id, status: :waiting_approval)

      roles = insert_list(3, :role, organization_id: org.id) |> Enum.map(& &1.id)

      assert {:ok, %Member{} = member} =
               Organizations.approve_member(request, %{roles: roles})

      assert member.status == :approved
      assert member.approved_at
      assert Enum.count(member.roles) == 3
    end

    test "with an already approved member, returns an error", %{org: org, user: user} do
      approved_at = DateTime.utc_now(:second) |> DateTime.add(-3, :day)

      request =
        insert(:member,
          organization_id: org.id,
          user_id: user.id,
          status: :approved,
          approved_at: approved_at
        )

      assert {:error, :already_approved} = Organizations.approve_member(request)

      member = Repo.get(Member, request.id)
      assert member.status == :approved
      assert DateTime.compare(member.approved_at, approved_at) == :eq
    end

    test "with a banned member, approves the member back", %{org: org, user: user} do
      request =
        insert(:member, organization_id: org.id, user_id: user.id, status: :banned)

      assert {:ok, %Member{} = member} = Organizations.approve_member(request)
      assert member.status == :approved
      assert member.approved_at
    end

    test "when successful request approval, sends a message to all subscribers", %{
      org: org,
      user: user
    } do
      :ok = Organizations.subscribe(org)

      request =
        insert(:member, organization_id: org.id, user_id: user.id, status: :waiting_approval)

      assert {:ok, %Member{id: id}} = Organizations.approve_member(request)
      assert_received {Nyght.Organizations, %MemberRequestApproved{member_id: ^id}}
    end
  end

  describe "reject_member/1" do
    test "with a valid member waiting for approval, deletes it", %{org: org, user: user} do
      member = insert(:member, user: user, organization: org, status: :waiting_approval)

      assert {:ok, %Member{} = res} = Organizations.reject_member(member)
      assert res.id == member.id
      refute Repo.get(Member, member.id)
    end

    test "with an approved member, returns an error", %{org: org, user: user} do
      member = insert(:member, user: user, organization: org, status: :approved)

      assert {:error, :not_waiting_approval} = Organizations.reject_member(member)
      assert Repo.get(Member, member.id)
    end

    test "with a banned member, returns an error", %{org: org, user: user} do
      member = insert(:member, user: user, organization: org, status: :banned)

      assert {:error, :not_waiting_approval} = Organizations.reject_member(member)
      assert Repo.get(Member, member.id)
    end

    test "when successful request rejection, sends a message to all subscribers", %{
      org: org,
      user: user
    } do
      :ok = Organizations.subscribe(org)

      member = insert(:member, user: user, organization: org, status: :waiting_approval)

      assert {:ok, %Member{id: id}} = Organizations.reject_member(member)
      assert_received {Nyght.Organizations, %MemberRequestRejected{member_id: ^id}}
    end
  end

  describe "get_member!/1" do
    test "with a valid id, returns the corresponding member with a populated role list", %{
      org: org,
      user: user,
      role: role
    } do
      member = insert(:member, organization_id: org.id, user_id: user.id, roles: [role])

      assert %Member{} = res = Organizations.get_member!(member.id)
      assert res.id == member.id
      assert [role_return] = member.roles
      assert role_return.id == role.id
    end

    test "with an invalid id, raises an error" do
      assert_raise(Ecto.NoResultsError, fn ->
        Organizations.get_member!(build(:invalid_guid))
      end)
    end
  end

  describe "get_member_for/2" do
    setup %{org: org, user: user} do
      other_org = insert(:organization)

      member = insert(:member, organization_id: org.id, user_id: user.id)

      %{member: member, other_org: other_org}
    end

    test "with valid organization and user combination, returns the member", %{
      org: org,
      user: user,
      member: member
    } do
      assert Organizations.get_member_for!(org, user).id == member.id
    end

    test "with inexistent member, raises an error", %{other_org: other_org, user: user} do
      assert_raise(Ecto.NoResultsError, fn ->
        Organizations.get_member_for!(other_org, user)
      end)
    end
  end

  describe "create_member/3" do
    test "with valid data, creates a new member", %{org: org, user: user} do
      assert {:ok, %Member{} = member} = Organizations.create_member(org, user)
      assert member.organization_id == org.id
      assert member.user_id == user.id
    end

    test "with valid data an multiple roles, creates a new member", %{
      org: org,
      user: user,
      role: role
    } do
      other_role = insert(:role, organization: org)

      assert {:ok, %Member{roles: roles} = member} =
               Organizations.create_member(org, user, [role, other_role])

      assert member.organization_id == org.id
      assert member.user_id == user.id
      assert Enum.any?(roles, &(&1.id == role.id))
      assert Enum.any?(roles, &(&1.id == other_role.id))
    end
  end

  describe "update_member/2" do
    test "with valid attributes, updates the given member", %{
      org: org,
      user: user,
      role: role,
      category: category
    } do
      member = insert(:member, organization_id: org.id, user_id: user.id)

      assert {:ok, %Member{roles: [updated_role], categories: [updated_category]}} =
               Organizations.update_member(member, %{roles: [role.id], categories: [category.id]})

      assert updated_role.id == role.id
      assert updated_category.id == category.id
    end

    test "with invalid attributes, returns an error changeset", %{org: org, user: user} do
      member = insert(:member, organization_id: org.id, user_id: user.id)

      assert {:error, changeset} = Organizations.update_member(member, %{user_id: nil})
      assert "can't be blank" in errors_on(changeset).user_id
    end
  end

  describe "change_member/2" do
    test "returns a changeset", %{org: org, user: user} do
      member = insert(:member, organization_id: org.id, user_id: user.id)

      assert %Ecto.Changeset{} = Organizations.change_member(member)
    end

    test "with valid attributes, returns a changeset", %{org: org, user: user} do
      member = insert(:member, organization_id: org.id, user_id: user.id)

      assert %Ecto.Changeset{} = Organizations.change_member(member, %{roles: []})
    end
  end

  describe "delete_member/1" do
    test "with an existing member, deletes it", %{
      org: org,
      user: user,
      role: role,
      category: category
    } do
      member =
        insert(:member,
          organization_id: org.id,
          user_id: user.id,
          roles: [role],
          categories: [category]
        )

      Organizations.delete_member(member)

      assert is_nil(Repo.get(Member, member.id))
      refute is_nil(Repo.get(Organization, org.id))
      refute is_nil(Repo.get(User, user.id))
      refute is_nil(Repo.get(Role, role.id))
      refute is_nil(Repo.get(MemberCategory, category.id))
    end
  end

  describe "member?/2" do
    test "with an existing approved member, returns true", %{org: org, user: user} do
      insert(:member, organization_id: org.id, user_id: user.id)
      assert Organizations.member?(org, user)
    end

    test "with an existing member wating approval, returns false", %{org: org, user: user} do
      insert(:member, organization_id: org.id, user_id: user.id, status: :waiting_approval)
      refute Organizations.member?(org, user)
    end

    test "with an existing banned member, returns false", %{org: org, user: user} do
      insert(:member, organization_id: org.id, user_id: user.id, status: :banned)
      refute Organizations.member?(org, user)
    end

    test "with a non-existing member, returns false", %{org: org, user: user} do
      refute Organizations.member?(org, user)
    end

    test "with no user, returns false", %{org: org} do
      refute Organizations.member?(org, nil)
    end
  end
end
