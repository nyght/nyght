defmodule Nyght.Performers.PerformersTest do
  use Nyght.DataCase, async: true

  import Nyght.Factory

  alias Nyght.Performers
  alias Nyght.Performers.Performer

  @invalid_attrs %{country: nil, email: nil, name: nil, phone_number: nil}

  setup do
    org = insert(:organization, timezone: "Europe/Zurich")

    {:ok, org: org}
  end

  describe "list_performers_for/3" do
    test "returns all performers for an organization", %{org: org} do
      performer = insert(:performer, organization_id: org.id)

      assert {:ok, {[performer_listed], %Flop.Meta{}}} = Performers.list_performers_for(org, %{})
      assert performer.id == performer_listed.id
    end
  end

  describe "get_performer/1" do
    test "returns the performer with given id", %{org: org} do
      performer = insert(:performer, organization_id: org.id)
      assert Performers.get_performer!(performer.id) == performer
    end
  end

  describe "create_performer/1" do
    test "with valid data creates a performer", %{org: org} do
      valid_attrs = %{
        name: "Band"
      }

      assert {:ok, %Performer{} = performer} = Performers.create_performer(org, valid_attrs)

      assert performer.name == valid_attrs.name
      assert performer.organization_id == org.id
    end

    test "with invalid data returns error changeset", %{org: org} do
      assert {:error, %Ecto.Changeset{}} = Performers.create_performer(org, @invalid_attrs)
    end
  end

  describe "update_performer/2" do
    test "with valid data updates the performer", %{org: org} do
      performer = insert(:performer, organization_id: org.id)

      update_attrs = %{
        country: "some updated country",
        email: "some updated email",
        name: "some updated name",
        phone_number: "some updated phone_number"
      }

      assert {:ok, %Performer{} = performer} =
               Performers.update_performer(performer, update_attrs)

      assert performer.country == "some updated country"
      assert performer.email == "some updated email"
      assert performer.name == "some updated name"
      assert performer.phone_number == "some updated phone_number"
    end

    test "with invalid data returns error changeset", %{org: org} do
      performer = insert(:performer, organization_id: org.id)
      assert {:error, %Ecto.Changeset{}} = Performers.update_performer(performer, @invalid_attrs)
      assert performer == Performers.get_performer!(performer.id)
    end
  end

  describe "delete_performer/1" do
    test "deletes the performer", %{org: org} do
      performer = insert(:performer, organization_id: org.id)
      assert {:ok, %Performer{}} = Performers.delete_performer(performer)
      assert_raise Ecto.NoResultsError, fn -> Performers.get_performer!(performer.id) end
    end
  end

  describe "change_performer/1" do
    test "returns a performer changeset", %{org: org} do
      performer = insert(:performer, organization_id: org.id)
      assert %Ecto.Changeset{} = Performers.change_performer(performer)
    end
  end
end
