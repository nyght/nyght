defmodule Nyght.Events.AttachmentsTest do
  @moduledoc false

  use Nyght.DataCase, async: true

  import Nyght.Factory

  alias Ecto.Changeset
  alias Nyght.Authorizations
  alias Nyght.{Events, Files}
  alias Nyght.Events.Attachment

  setup do
    org = insert(:organization)
    event = insert(:event, organization: org)
    role = insert(:role, organization: org)

    entry = %{
      uuid: Ecto.UUID.generate(),
      client_name: "test.pdf",
      client_type: "application/pdf"
    }

    %{event: event, role: role, entry: entry, org: org}
  end

  describe "attach_file/3" do
    test "with valid attributes, create a new attachment for the given event", %{
      event: event,
      entry: entry
    } do
      assert {:ok, %Attachment{} = attachment} = Events.attach_file(event, entry, "asdf")
      assert attachment.event_id == event.id
      assert attachment.file_name == "test.pdf"
    end

    test "with valid attributes and roles, create a new attachment for the given event with the correct roles",
         %{event: event, role: role, entry: entry} do
      assert {:ok, %Attachment{} = attachment} =
               Events.attach_file(event, entry, "asdf", [role.id])

      assert [file_role] = attachment.roles
      assert file_role.id == role.id
    end

    test "with a file name shorter than 4 characters, returns an error", %{
      event: event,
      entry: entry
    } do
      entry = %{entry | client_name: "pdf"}

      assert {:error, %Changeset{} = changeset} = Events.attach_file(event, entry, "asdf")
      assert "should be at least 4 character(s)" in errors_on(changeset).file_name
    end

    test "with uploaded attachment, the data is encrypted", %{event: event, entry: entry} do
      assert {:ok, %Attachment{} = attachment} = Events.attach_file(event, entry, "asdf")

      {:ok, content} = Files.download("uploads", attachment.resource_path)

      assert content
      refute content == "asdf"
    end
  end

  describe "list_attachments_for/2" do
    test "with valid parameters, returns the list of attachments for the event", %{event: event} do
      insert_list(50, :attachment, event: event)

      assert {:ok, {attachments, meta}} = Events.list_attachments_for(event, %{})
      assert Enum.count(attachments) == 50
      assert meta.total_count == 50
    end
  end

  describe "get_attachment/2" do
    test "with valid data, returns the attachment and its content", %{
      event: event,
      entry: entry
    } do
      {:ok, attachment} = Events.attach_file(event, entry, "asdf")
      assert {:ok, {res = %Attachment{}, content}} = Events.get_attachment(attachment.id)

      assert res.id == attachment.id
      assert res.event_id == attachment.event_id
      assert res.content_type == attachment.content_type
      assert res.resource_path == attachment.resource_path
      assert res.file_name == attachment.file_name

      assert content == "asdf"
    end

    test "with valid data and the with_content option to false, returns the attachment and not its content",
         %{
           event: event,
           entry: entry
         } do
      {:ok, attachment} = Events.attach_file(event, entry, "asdf")

      assert {:ok, {res = %Attachment{}, content}} =
               Events.get_attachment(attachment.id, with_content?: false)

      assert res.id == attachment.id
      assert res.event_id == attachment.event_id
      assert res.content_type == attachment.content_type
      assert res.resource_path == attachment.resource_path
      assert res.file_name == attachment.file_name

      assert is_nil(content)
    end

    test "with invalid data, returns an error" do
      assert {:error, :not_found} = Events.get_attachment(build(:invalid_guid))
    end
  end

  describe "change_attachment/2" do
    setup do
      org = insert(:organization)
      event = insert(:event, organization: org)
      attachment = insert(:attachment, event: event)

      %{attachment: attachment}
    end

    test "returns a changeset", %{attachment: attachment} do
      assert %Ecto.Changeset{} = Events.change_attachment(attachment)
    end

    test "with valid attributes, returns a changeset", %{attachment: attachment} do
      attrs = %{file_name: "new name", roles: []}
      assert %Ecto.Changeset{} = Events.change_attachment(attachment, attrs)
    end
  end

  describe "update_attachment/2" do
    setup do
      org = insert(:organization)
      event = insert(:event, organization: org)
      attachment = insert(:attachment, event: event)

      %{org: org, event: event, attachment: attachment}
    end

    test "with valid data, updates the file name", %{attachment: attachment} do
      {:ok, %Attachment{} = res} = Events.update_attachment(attachment, %{file_name: "new name"})

      assert res.file_name == "new name"
      assert res.resource_path == attachment.resource_path
      assert res.event_id == attachment.event_id
      assert res.id == attachment.id
    end

    test "with invalid data, returns an error", %{attachment: attachment} do
      {:error, %Changeset{} = changeset} =
        Events.update_attachment(attachment, %{file_name: "new"})

      assert "should be at least 4 character(s)" in errors_on(changeset).file_name
    end

    test "with valid data, updates the roles", %{org: org, attachment: attachment} do
      roles = insert_list(3, :role, organization: org) |> Enum.map(& &1.id)

      {:ok, %Attachment{} = res} = Events.update_attachment(attachment, %{roles: roles})

      assert Enum.count(res.roles) == 3
      assert res.id == attachment.id
    end

    test "doesn't change the resource_path", %{attachment: attachment} do
      {:ok, %Attachment{} = res} =
        Events.update_attachment(attachment, %{resource_path: "/bla/bla"})

      assert res.resource_path == attachment.resource_path
      assert res.id == attachment.id
    end
  end

  describe "delete_attachment/1" do
    test "with exisiting attachment, deletes the attachment locally and on the object storage", %{
      event: event,
      entry: entry,
      role: role
    } do
      {:ok, attachment} = Events.attach_file(event, entry, "asdf", [role.id])

      assert {:ok, %Attachment{} = res} = Events.delete_attachment(attachment)
      assert {:error, :not_found} = Events.get_attachment(attachment.id)
      assert {:error, {:http_error, 404, _}} = Files.download("uploads", res.resource_path)
    end
  end

  describe "delete_event_attachments/1" do
    test "with exisiting attachment and event, deletes all the attachments locally and on the object storage",
         %{
           event: event,
           entry: entry,
           role: role
         } do
      other_entry = %{entry | uuid: Ecto.UUID.generate()}

      {:ok, attachment1} = Events.attach_file(event, entry, "asdf", [role.id])
      {:ok, attachment2} = Events.attach_file(event, other_entry, "asdf", [role.id])

      assert {:ok, {2, nil}} = Events.delete_event_attachments(event)

      assert {:error, :not_found} = Events.get_attachment(attachment1.id)
      assert {:error, :not_found} = Events.get_attachment(attachment2.id)

      assert {:error, {:http_error, 404, _}} =
               Files.download("uploads", attachment1.resource_path)

      assert {:error, {:http_error, 404, _}} =
               Files.download("uploads", attachment2.resource_path)
    end
  end

  describe "can/4" do
    setup %{org: org, role: role} do
      user = insert(:user)

      member =
        insert(:member,
          organization: org,
          user: user,
          roles: [role],
          status: :approved
        )

      other_role = insert(:role, organization: org)

      %{member: member, other_role: other_role}
    end

    test "with user roles matching attachment roles, allows reading and downloading", %{
      event: event,
      entry: entry,
      role: role,
      member: member
    } do
      {:ok, attachment} = Events.attach_file(event, entry, "asdf", [role.id])

      assert Authorizations.can?(member, attachment, :download)
      assert Authorizations.can?(member, attachment, :read)
    end

    test "with no user roles matching attachment roles, disallows reading and downloading", %{
      event: event,
      entry: entry,
      other_role: role,
      member: member
    } do
      {:ok, attachment} = Events.attach_file(event, entry, "asdf", [role.id])

      refute Authorizations.can?(member, attachment, :read)
      refute Authorizations.can?(member, attachment, :download)
    end
  end
end
