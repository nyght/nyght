defmodule Nyght.Events.EventsTest do
  use Nyght.DataCase, async: true

  import Nyght.Factory

  alias Ecto.Changeset
  alias Nyght.Events
  alias Nyght.Events.{Event, Price, Type}
  alias Nyght.Files
  alias Nyght.Performers.Booking
  alias Nyght.Repo
  alias Nyght.Shifts.Shift

  setup do
    org = insert(:organization, timezone: "Europe/Zurich")
    other_org = insert(:organization, timezone: "Europe/Zurich")

    {:ok, org: org, other_org: other_org}
  end

  describe "list_events/2" do
    test "returns all the published events", %{org: org} do
      insert_list(20, :published_event, organization: org)
      insert_list(20, :published_event, organization: org, is_members_only: true)
      insert_list(20, :prepublished_event, organization: org)
      insert_list(20, :unpublished_event, organization: org)

      assert {:ok, {list, %Flop.Meta{}}} = Events.list_events(%{})
      assert Enum.count(list) == 20
    end

    test "with flop filter parameters, returns the filtered events", %{org: org} do
      insert_list(10, :published_event, organization: org)
      insert_list(10, :published_event, organization: org, is_members_only: true)

      insert_list(10, :unpublished_event,
        organization: org,
        name: "other unpublished special event"
      )

      insert(:published_event, organization: org, name: "my special event")

      assert {:ok, {list, %Flop.Meta{}}} =
               Events.list_events(%{
                 filters: [
                   %{field: :name, op: :ilike, value: "special"}
                 ]
               })

      assert Enum.count(list) == 1
    end

    test "with `status` option set, returns the filtered events including unpublished ones", %{
      org: org
    } do
      insert_list(10, :published_event, organization: org)
      insert_list(10, :published_event, organization: org, is_members_only: true)

      insert_list(10, :unpublished_event,
        organization: org,
        name: "other unpublished special event"
      )

      insert(:published_event, organization: org, name: "my special event")

      assert {:ok, {list, %Flop.Meta{}}} =
               Events.list_events(
                 %{
                   filters: [
                     %{field: :name, op: :ilike, value: "special"}
                   ]
                 },
                 status: :unpublished
               )

      assert Enum.count(list) == 11
    end

    test "with `members_only?` option set, returns filtered events including member only ones", %{
      org: org
    } do
      insert_list(10, :published_event, organization: org)

      insert_list(10, :published_event,
        organization: org,
        is_members_only: true,
        name: "other members only special event"
      )

      insert_list(10, :unpublished_event, organization: org)

      insert(:published_event, organization: org, name: "my special event")

      assert {:ok, {list, %Flop.Meta{}}} =
               Events.list_events(
                 %{
                   filters: [
                     %{field: :name, op: :ilike, value: "special"}
                   ]
                 },
                 members_only?: true
               )

      assert Enum.count(list) == 11
    end
  end

  describe "list_events_for/3" do
    test "with a valid organization, returns all the published events of the given organization",
         %{
           org: org,
           other_org: other_org
         } do
      insert_list(10, :published_event, organization: org)
      insert_list(10, :published_event, organization: org, is_members_only: true)
      insert_list(10, :prepublished_event, organization: org)
      insert_list(10, :unpublished_event, organization: org)
      insert_list(10, :published_event, organization: other_org)

      assert {:ok, {list, %Flop.Meta{}}} = Events.list_events_for(org, %{})
      assert Enum.count(list) == 10
    end

    test "with flop filter parameters and a valid organization, returns the filtered published events for the given organization",
         %{org: org, other_org: other_org} do
      insert_list(10, :published_event, organization: org)
      insert_list(10, :published_event, organization: org, is_members_only: true)
      insert_list(10, :published_event, organization: other_org)
      insert_list(10, :prepublished_event, organization: org)
      insert_list(10, :unpublished_event, organization: org)

      insert(:published_event, organization: org, name: "my special event")
      insert(:published_event, organization: other_org, name: "my special event")

      assert {:ok, {list, %Flop.Meta{}}} =
               Events.list_events_for(org, %{
                 filters: [
                   %{field: :name, op: :ilike, value: "special"}
                 ]
               })

      assert Enum.count(list) == 1
    end

    test "with flop filter parameters, the `status` option set and a valid organization, returns the filtered events for the given organization",
         %{org: org, other_org: other_org} do
      insert_list(10, :published_event, organization: org)
      insert_list(10, :published_event, organization: org, is_members_only: true)
      insert_list(10, :published_event, organization: other_org)
      insert_list(10, :prepublished_event, organization: org)
      insert_list(10, :unpublished_event, organization: org)

      insert(:unpublished_event, organization: org, name: "my special event")
      insert(:unpublished_event, organization: other_org, name: "my special event")

      assert {:ok, {list, %Flop.Meta{}}} =
               Events.list_events_for(
                 org,
                 %{
                   filters: [
                     %{field: :name, op: :ilike, value: "special"}
                   ]
                 },
                 status: :unpublished
               )

      assert Enum.count(list) == 1
    end

    test "with flop filter parameters, the `member_only?` option set and a valid organization, returns the filtered events for the given organization",
         %{org: org, other_org: other_org} do
      insert_list(10, :published_event, organization: org)

      insert_list(10, :published_event,
        organization: org,
        is_members_only: true,
        name: "special members only event"
      )

      insert_list(10, :published_event, organization: other_org)
      insert_list(10, :prepublished_event, organization: org)
      insert_list(10, :unpublished_event, organization: org)

      insert(:unpublished_event, organization: org, name: "my special event")
      insert(:unpublished_event, organization: other_org, name: "my special event")

      assert {:ok, {list, %Flop.Meta{}}} =
               Events.list_events_for(
                 org,
                 %{
                   filters: [
                     %{field: :name, op: :ilike, value: "special"}
                   ]
                 },
                 status: :unpublished
               )

      assert Enum.count(list) == 1
    end
  end

  describe "get_event!/2" do
    setup %{org: org} do
      %{event: insert(:published_event, organization: org, timezone: org.timezone)}
    end

    test "with an existing event id, returns the corresponding event", %{event: event} do
      assert %Event{} = res = Events.get_event!(event.id)
      assert res.id == event.id
      assert res.organization_id == event.organization_id
      assert res.description == event.description
      assert res.status == event.status

      assert res.starts_at == event.starts_at
      assert res.ends_at == event.ends_at
    end

    test "with an event having a specific timezone, returns the the event with local times", %{
      event: event
    } do
      assert %Event{} = res = Events.get_event!(event.id)
      assert res.timezone == "Europe/Zurich"

      assert_time_shift(res, event, :starts_at, :local_starts_at)
      assert_time_shift(res, event, :ends_at, :local_ends_at)
    end

    test "with the `with_local_times?` set to `false`, returns the event without local times", %{
      event: event
    } do
      assert %Event{} = res = Events.get_event!(event.id, with_local_times?: false)
      assert res.timezone == "Europe/Zurich"

      assert is_nil(res.local_starts_at)
      assert is_nil(res.local_ends_at)
    end

    test "with an invalid event id, throws an error" do
      assert_raise(Ecto.NoResultsError, fn ->
        Events.get_event!(build(:invalid_guid))
      end)
    end
  end

  describe "get_neighbors/2" do
    setup %{org: org} do
      starts_at = DateTime.utc_now()
      event = insert(:published_event, organization: org, starts_at: starts_at)

      %{starts_at: starts_at, event: event}
    end

    test "with no neighboring events in the same org, returns a neighbors map with no value", %{
      other_org: other_org,
      event: event,
      starts_at: starts_at
    } do
      insert(:published_event,
        organization: other_org,
        starts_at: DateTime.add(starts_at, -1, :day)
      )

      insert(:published_event,
        organization: other_org,
        starts_at: DateTime.add(starts_at, 1, :day)
      )

      res = Events.get_neighbors(event)

      assert is_nil(res.next_event_id)
      assert is_nil(res.next_event_name)
      assert is_nil(res.prev_event_id)
      assert is_nil(res.prev_event_name)
    end

    test "with only a previous event in the same org, returns a neighbors map with only the previous event data",
         %{
           org: org,
           other_org: other_org,
           event: event,
           starts_at: starts_at
         } do
      prev =
        insert(:published_event, organization: org, starts_at: DateTime.add(starts_at, -1, :day))

      insert(:published_event,
        organization: other_org,
        starts_at: DateTime.add(starts_at, -2, :day)
      )

      res = Events.get_neighbors(event)

      assert is_nil(res.next_event_id)
      assert is_nil(res.next_event_name)
      assert res.prev_event_id == prev.id
      assert res.prev_event_name == prev.name
    end

    test "with only a next event in the same org, returns a neighbors map with only the next event data",
         %{
           org: org,
           other_org: other_org,
           event: event,
           starts_at: starts_at
         } do
      next =
        insert(:published_event, organization: org, starts_at: DateTime.add(starts_at, 2, :day))

      insert(:published_event,
        organization: other_org,
        starts_at: DateTime.add(starts_at, 1, :day)
      )

      res = Events.get_neighbors(event)

      assert res.next_event_id == next.id
      assert res.next_event_name == next.name
      assert is_nil(res.prev_event_id)
      assert is_nil(res.prev_event_name)
    end

    test "with a next and prev event in the same org, returns a neighbors map with all data", %{
      org: org,
      other_org: other_org,
      event: event,
      starts_at: starts_at
    } do
      next =
        insert(:published_event, organization: org, starts_at: DateTime.add(starts_at, 2, :day))

      prev =
        insert(:published_event, organization: org, starts_at: DateTime.add(starts_at, -2, :day))

      insert(:published_event,
        organization: other_org,
        starts_at: DateTime.add(starts_at, 1, :day)
      )

      insert(:published_event,
        organization: other_org,
        starts_at: DateTime.add(starts_at, -1, :day)
      )

      res = Events.get_neighbors(event)

      assert res.next_event_id == next.id
      assert res.next_event_name == next.name
      assert res.prev_event_id == prev.id
      assert res.prev_event_name == prev.name
    end

    test "with a next and prev unpublished event in the same org and the status option set, returns a neighbors map with all data",
         %{
           org: org,
           other_org: other_org,
           event: event,
           starts_at: starts_at
         } do
      next =
        insert(:unpublished_event, organization: org, starts_at: DateTime.add(starts_at, 2, :day))

      prev =
        insert(:prepublished_event,
          organization: org,
          starts_at: DateTime.add(starts_at, -2, :day)
        )

      insert(:published_event,
        organization: other_org,
        starts_at: DateTime.add(starts_at, 1, :day)
      )

      insert(:published_event,
        organization: other_org,
        starts_at: DateTime.add(starts_at, -1, :day)
      )

      res = Events.get_neighbors(event, status: :unpublished)

      assert res.next_event_id == next.id
      assert res.next_event_name == next.name
      assert res.prev_event_id == prev.id
      assert res.prev_event_name == prev.name
    end

    test "with a next and prev members-only event in the same org and the `member_only?` option set, returns a neighbors map with all data",
         %{
           org: org,
           other_org: other_org,
           event: event,
           starts_at: starts_at
         } do
      next =
        insert(:published_event,
          organization: org,
          is_members_only: true,
          starts_at: DateTime.add(starts_at, 2, :day)
        )

      prev =
        insert(:published_event,
          organization: org,
          is_members_only: true,
          starts_at: DateTime.add(starts_at, -2, :day)
        )

      insert(:published_event,
        organization: other_org,
        starts_at: DateTime.add(starts_at, 1, :day)
      )

      insert(:published_event,
        organization: other_org,
        starts_at: DateTime.add(starts_at, -1, :day)
      )

      res = Events.get_neighbors(event, members_only?: true)

      assert res.next_event_id == next.id
      assert res.next_event_name == next.name
      assert res.prev_event_id == prev.id
      assert res.prev_event_name == prev.name
    end
  end

  # TODO: complete the unit tests for create_event/3
  describe "create_event/3" do
    test "with valid attributes, creates a new event", %{org: org} do
      starts_at = DateTime.utc_now()

      attrs = %{
        name: "Test event",
        status: :published,
        starts_at: starts_at,
        ends_at: DateTime.add(starts_at, 4, :hour),
        timezone: org.timezone
      }

      assert {:ok, %Event{} = res} = Events.create_event(org, attrs)
      assert res.name == attrs.name
      assert res.status == attrs.status
      assert res.organization_id == org.id
      assert res.timezone == org.timezone
    end

    test "with clock gap in end time because of DST change, returns an error", %{
      org: org
    } do
      summer_time = build(:summer_time)

      attrs =
        params_for(:event,
          local_starts_at: NaiveDateTime.add(summer_time, -4, :hour),
          local_ends_at: NaiveDateTime.add(summer_time, -30, :minute),
          timezone: org.timezone
        )

      assert {:error, %Changeset{} = changeset} = Events.create_event(org, attrs)

      assert "not a valid time because of daylight saving time" in errors_on(changeset).local_ends_at
    end

    test "with ambiguous end time because of DST change, returns a shift with the correct time shift",
         %{
           org: org
         } do
      winter_time = build(:winter_time)
      {:ambiguous, _, winter_dt} = DateTime.from_naive(winter_time, "Europe/Zurich")
      winter_dt = DateTime.shift_zone!(winter_dt, "Etc/UTC")

      attrs =
        params_for(:event,
          local_starts_at: NaiveDateTime.add(winter_time, -4, :hour),
          local_ends_at: winter_time,
          timezone: org.timezone
        )

      assert {:ok, %Event{} = event} = Events.create_event(org, attrs)
      assert event.ends_at == winter_dt
    end
  end

  # TODO: complete the unit tests for update_event/3
  describe "update_event/3" do
  end

  # TODO: complete the unit tests for change_event/2
  describe "change_event/2" do
  end

  describe "copy_event/1" do
    test "with a valid event, returns a changeset to create a copy", %{org: org} do
      event_types = insert_list(3, :event_type)
      prices = insert_list(3, :price)
      event = insert(:event, organization: org, types: event_types, prices: prices)

      assert %Changeset{} = res = Events.copy_event(event)
      assert res.valid?
      assert res.changes.name == event.name
      assert res.changes.description == event.description
      assert res.changes.organization_id == event.organization_id
      assert Enum.count(res.changes.prices) == 3
      assert Enum.count(res.changes.types) == 3
    end
  end

  describe "unpublish_event/1" do
    test "with a published event, unpublishes the event", %{org: org} do
      {:ok, event} =
        insert(:published_event, organization: org)
        |> Events.unpublish_event()

      assert event.status == :unpublished
    end
  end

  describe "prepublish_event/1" do
    test "with a published event, prepublishes the event", %{org: org} do
      {:ok, event} =
        insert(:published_event, organization: org)
        |> Events.prepublish_event()

      assert event.status == :prepublished
    end
  end

  describe "publish_event/1" do
    test "with an unpublished event, publishes the event", %{org: org} do
      {:ok, event} =
        insert(:unpublished_event, organization: org)
        |> Events.publish_event()

      assert event.status == :published
    end
  end

  describe "cancel_event/1" do
    test "with a valid event, cancels the event", %{org: org} do
      {:ok, event} =
        insert(:event, organization: org)
        |> Events.cancel_event()

      assert event.is_canceled
    end
  end

  describe "delete_event/1" do
    test "with an existing event, deletes the event", %{org: org} do
      event = insert(:event, organization_id: org.id)

      Events.delete_event(event)

      assert is_nil(Repo.get(Event, event.id))
    end

    test "when an event has shifts, deletes the event and the shifts", %{org: org} do
      event = insert(:event, organization_id: org.id)

      role = insert(:role)
      shift = insert(:shift, event: event, role: role)

      Events.delete_event(event)

      assert is_nil(Repo.get(Event, event.id))
      assert is_nil(Repo.get(Shift, shift.id))
    end

    test "when an event has prices, deletes the event and the prices", %{org: org} do
      event = insert(:event, organization_id: org.id)

      price = insert(:price, event: event)

      Events.delete_event(event)

      assert is_nil(Repo.get(Event, event.id))
      assert is_nil(Repo.get(Price, price.id))
    end

    test "when an event has bookings, deletes the event and the bookings", %{org: org} do
      event = insert(:event, organization_id: org.id)

      booking = insert(:booking, event: event)

      Events.delete_event(event)

      assert is_nil(Repo.get(Event, event.id))
      assert is_nil(Repo.get(Booking, booking.id))
    end

    test "when an event has types, deletes the event but not the types", %{org: org} do
      type = insert(:event_type)

      event = insert(:event, organization_id: org.id, types: [type])

      Events.delete_event(event)

      assert is_nil(Repo.get(Event, event.id))
      assert Repo.get(Type, type.id) == type
    end

    test "when an event has attachments, deletes the event and the attachments", %{org: org} do
      event = insert(:event, organization_id: org.id)

      entry = %{
        uuid: Ecto.UUID.generate(),
        client_name: "test.pdf",
        client_type: "application/pdf"
      }

      {:ok, attachment} = Events.attach_file(event, entry, "asdf")

      Events.delete_event(event)

      assert is_nil(Repo.get(Event, event.id))
      assert {:error, :not_found} = Events.get_attachment(attachment.id)
      assert {:error, {:http_error, 404, _}} = Files.download("uploads", attachment.resource_path)
    end
  end

  defp assert_time_shift(res, event, utc_field, local_field) do
    # the local datetime coming from the database is more precise and
    # needs to be truncated
    db_local_time =
      res
      |> Map.fetch!(local_field)
      |> DateTime.from_naive!(res.timezone)
      |> DateTime.truncate(:second)

    local_time =
      event
      |> Map.fetch!(utc_field)
      |> DateTime.shift_zone!(res.timezone)

    assert db_local_time == local_time
  end
end
