defmodule Nyght.Events.TypesTest do
  use Nyght.DataCase, async: true

  import Nyght.Factory

  alias Nyght.Events
  alias Nyght.Events.Type

  describe "create_type/1" do
    test "with valid data, creates an event type" do
      valid_attrs = %{
        name: "some name"
      }

      assert {:ok, %Type{} = type} = Events.create_type(valid_attrs)
      assert type.name == "some name"
    end

    test "with invalid data, returns error changeset" do
      assert {:error, %Ecto.Changeset{} = changeset} = Events.create_type(%{})
      assert "can't be blank" in errors_on(changeset).name
    end
  end

  describe "list_types/0" do
    test "returns all the types" do
      event_types = insert_list(4, :event_type)
      assert event_types == Events.list_types()
    end

    test "when there is no type, returns an empty list" do
      assert Enum.empty?(Events.list_types())
    end
  end

  describe "get_types/1" do
    test "when given invalid ids, returns an empty list" do
      assert Enum.empty?(Events.get_types([build(:invalid_guid)]))
    end

    test "returns the types corresponding to the given ids" do
      types = insert_list(2, :event_type)

      res =
        types
        |> Enum.map(& &1.id)
        |> Events.get_types()

      assert res == types
    end
  end
end
