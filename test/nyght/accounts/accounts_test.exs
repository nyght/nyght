defmodule Nyght.Accounts.AccountsTest do
  use Nyght.DataCase, async: true

  import Nyght.Factory

  alias Nyght.Accounts
  alias Nyght.Accounts.{User, UserToken}

  describe "get_user_by_email/1" do
    test "does not return the user if the email does not exist" do
      refute Accounts.get_user_by_email("unknown@example.com")
    end

    test "returns the user if the email exists" do
      %{id: id, email: email} = insert(:user)
      assert %User{id: ^id} = Accounts.get_user_by_email(email)
    end
  end

  describe "get_user_by_email_and_password/2" do
    test "does not return the user if the email does not exist" do
      refute Accounts.get_user_by_email_and_password("unknown@example.com", "hello world!")
    end

    test "does not return the user if the password is not valid" do
      %{email: email} = insert(:user)
      refute Accounts.get_user_by_email_and_password(email, "invalid")
    end

    test "returns the user if the email and password are valid" do
      %{id: id, email: email} = insert(:user)
      assert %User{id: ^id} = Accounts.get_user_by_email_and_password(email, build(:password))
    end
  end

  describe "get_user!/1" do
    test "raises if id is invalid" do
      assert_raise Ecto.NoResultsError, fn ->
        Accounts.get_user!("11111111-1111-1111-1111-111111111111")
      end
    end

    test "returns the user with the given id" do
      %{id: id} = insert(:user)
      assert %User{id: ^id} = Accounts.get_user!(id)
    end
  end

  describe "full_name/1" do
    test "returns the user's full name with nickname" do
      %User{first_name: first_name, last_name: last_name, nickname: nickname} =
        user = insert(:user, nickname: "JD")

      assert User.full_name(user, redacted?: false) ==
               "#{first_name} \"#{nickname}\" #{last_name}"
    end

    test "returns the user's full name with nickname and redacted last name when" do
      %User{first_name: first_name, nickname: nickname} =
        user = insert(:user, nickname: "JD")

      assert User.full_name(user) == "#{first_name} \"#{nickname}\" D."
    end

    test "returns the user's full name" do
      %User{first_name: first_name, last_name: last_name, nickname: nil} = user = insert(:user)

      assert User.full_name(user, redacted?: false) == "#{first_name} #{last_name}"
    end

    test "returns the user's full name with redacted last name" do
      %User{first_name: first_name, nickname: nil} = user = insert(:user)

      assert User.full_name(user) == "#{first_name} D."
    end
  end

  describe "register_user/1" do
    test "requires email and password and other fields to be set" do
      {:error, changeset} = Accounts.register_user(%{})

      assert %{
               password: ["can't be blank"],
               email: ["can't be blank"],
               street_line_1: ["can't be blank"],
               zip_code: ["can't be blank"],
               city: ["can't be blank"],
               first_name: ["can't be blank"],
               last_name: ["can't be blank"],
               phone_number: ["can't be blank"]
             } = errors_on(changeset)
    end

    test "validates email and password when given" do
      {:error, changeset} = Accounts.register_user(%{email: "not valid", password: "invalid"})

      assert %{
               email: ["must have the @ sign and no spaces"],
               password: [
                 "at least one digit or punctuation character",
                 "at least one upper case character",
                 "should be at least 8 character(s)"
               ]
             } = errors_on(changeset)
    end

    test "validates maximum values for email and password for security" do
      too_long = String.duplicate("db", 100)
      {:error, changeset} = Accounts.register_user(%{email: too_long, password: too_long})
      assert "should be at most 160 character(s)" in errors_on(changeset).email
      assert "should be at most 72 character(s)" in errors_on(changeset).password
    end

    test "validates email uniqueness" do
      %{email: email} = insert(:user)
      {:error, changeset} = Accounts.register_user(%{email: email})
      assert "has already been taken" in errors_on(changeset).email

      # Now try with the upper cased email too, to check that email case is ignored.
      {:error, changeset} = Accounts.register_user(%{email: String.upcase(email)})
      assert "has already been taken" in errors_on(changeset).email
    end

    test "registers users with a hashed password" do
      attrs =
        build(:user)
        |> for_registration()
        |> Map.from_struct()

      {:ok, user} = Accounts.register_user(attrs)
      assert user.email == attrs.email
      assert is_binary(user.hashed_password)
      assert is_nil(user.confirmed_at)
      assert is_nil(user.password)
    end
  end

  describe "change_user_registration/2" do
    test "returns a changeset" do
      assert %Ecto.Changeset{} = changeset = Accounts.change_user_registration(%User{})

      assert changeset.required == [
               :street_line_1,
               :zip_code,
               :city,
               :password,
               :email,
               :first_name,
               :last_name,
               :phone_number
             ]
    end

    test "allows fields to be set" do
      attrs = params_for(:user, password: build(:password))
      changeset = Accounts.change_user_registration(%User{}, attrs)

      assert changeset.valid?
      assert get_change(changeset, :email) == attrs.email
      assert get_change(changeset, :password) == attrs.password
      assert is_nil(get_change(changeset, :hashed_password))
    end
  end

  describe "change_user_email/2" do
    test "returns a user changeset" do
      assert %Ecto.Changeset{} = changeset = Accounts.change_user_email(%User{})
      assert changeset.required == [:email]
    end
  end

  describe "apply_user_email/3" do
    setup do
      %{user: insert(:user)}
    end

    test "requires email to change", %{user: user} do
      {:error, changeset} = Accounts.apply_user_email(user, build(:password), %{})
      assert %{email: ["did not change"]} = errors_on(changeset)
    end

    test "validates email", %{user: user} do
      {:error, changeset} =
        Accounts.apply_user_email(user, build(:password), %{email: "not valid"})

      assert %{email: ["must have the @ sign and no spaces"]} = errors_on(changeset)
    end

    test "validates maximum value for email for security", %{user: user} do
      too_long = String.duplicate("db", 100)

      {:error, changeset} = Accounts.apply_user_email(user, build(:password), %{email: too_long})

      assert "should be at most 160 character(s)" in errors_on(changeset).email
    end

    test "validates email uniqueness", %{user: user} do
      %{email: email} = insert(:user)

      {:error, changeset} = Accounts.apply_user_email(user, build(:password), %{email: email})

      assert "has already been taken" in errors_on(changeset).email
    end

    test "validates current password", %{user: user} do
      {:error, changeset} = Accounts.apply_user_email(user, "invalid", %{email: build(:email)})

      assert %{current_password: ["is not valid"]} = errors_on(changeset)
    end

    test "applies the email without persisting it", %{user: user} do
      email = build(:email)
      {:ok, user} = Accounts.apply_user_email(user, build(:password), %{email: email})
      assert user.email == email
      assert Accounts.get_user!(user.id).email != email
    end
  end

  describe "deliver_update_email_instructions/3" do
    setup do
      %{user: insert(:user)}
    end

    test "sends token through notification", %{user: user} do
      token =
        Nyght.TestUtils.extract_user_token(fn url ->
          Accounts.deliver_update_email_instructions(user, "current@example.com", url)
        end)

      {:ok, token} = Base.url_decode64(token, padding: false)
      assert user_token = Repo.get_by(UserToken, token: :crypto.hash(:sha256, token))
      assert user_token.user_id == user.id
      assert user_token.sent_to == user.email
      assert user_token.context == "change:current@example.com"
    end
  end

  describe "update_user_email/2" do
    setup do
      user = insert(:user)
      new_email = build(:email)

      token =
        Nyght.TestUtils.extract_user_token(fn url ->
          Accounts.deliver_update_email_instructions(%{user | email: new_email}, user.email, url)
        end)

      %{user: user, token: token, email: new_email}
    end

    test "updates the email with a valid token", %{user: user, token: token, email: email} do
      assert Accounts.update_user_email(user, token) == :ok
      changed_user = Repo.get!(User, user.id)
      assert changed_user.email != user.email
      assert changed_user.email == email
      assert changed_user.confirmed_at
      assert changed_user.confirmed_at != user.confirmed_at
      refute Repo.get_by(UserToken, user_id: user.id)
    end

    test "does not update email with invalid token", %{user: user} do
      assert Accounts.update_user_email(user, "oops") == :error
      assert Repo.get!(User, user.id).email == user.email
      assert Repo.get_by(UserToken, user_id: user.id)
    end

    test "does not update email if user email changed", %{user: user, token: token} do
      assert Accounts.update_user_email(%{user | email: "current@example.com"}, token) == :error
      assert Repo.get!(User, user.id).email == user.email
      assert Repo.get_by(UserToken, user_id: user.id)
    end

    test "does not update email if token expired", %{user: user, token: token} do
      {1, nil} = Repo.update_all(UserToken, set: [inserted_at: ~N[2020-01-01 00:00:00]])
      assert Accounts.update_user_email(user, token) == :error
      assert Repo.get!(User, user.id).email == user.email
      assert Repo.get_by(UserToken, user_id: user.id)
    end
  end

  describe "change_user_password/2" do
    test "returns a user changeset" do
      assert %Ecto.Changeset{} = changeset = Accounts.change_user_password(%User{})
      assert changeset.required == [:password]
    end

    test "allows fields to be set" do
      changeset =
        Accounts.change_user_password(%User{}, %{
          "password" => "Passw0rd!"
        })

      assert changeset.valid?
      assert get_change(changeset, :password) == "Passw0rd!"
      assert is_nil(get_change(changeset, :hashed_password))
    end
  end

  describe "update_user_password/3" do
    setup do
      %{user: insert(:user)}
    end

    test "validates password", %{user: user} do
      {:error, changeset} =
        Accounts.update_user_password(user, build(:password), %{
          password: "invalid",
          password_confirmation: "another"
        })

      assert %{
               password: [
                 "at least one digit or punctuation character",
                 "at least one upper case character",
                 "should be at least 8 character(s)"
               ],
               password_confirmation: ["does not match password"]
             } = errors_on(changeset)
    end

    test "validates maximum values for password for security", %{user: user} do
      too_long = String.duplicate("db", 100)

      {:error, changeset} =
        Accounts.update_user_password(user, build(:password), %{password: too_long})

      assert "should be at most 72 character(s)" in errors_on(changeset).password
    end

    test "validates current password", %{user: user} do
      {:error, changeset} =
        Accounts.update_user_password(user, "invalid", %{password: build(:password)})

      assert %{current_password: ["is not valid"]} = errors_on(changeset)
    end

    test "updates the password", %{user: user} do
      new_password = "#{build(:password)}2"

      {:ok, updated_user} =
        Accounts.update_user_password(user, build(:password), %{
          password: new_password
        })

      assert is_nil(updated_user.password)
      assert Accounts.get_user_by_email_and_password(updated_user.email, new_password)
    end

    test "deletes all tokens for the given user", %{user: user} do
      _ = Accounts.generate_user_session_token(user)

      {:ok, _} =
        Accounts.update_user_password(user, build(:password), %{
          password: build(:password)
        })

      refute Repo.get_by(UserToken, user_id: user.id)
    end
  end

  describe "delete_user" do
    test "does delete if deletion_reuqested_at is set at least a week ago" do
      one_week_ago = NaiveDateTime.utc_now() |> NaiveDateTime.add(-60 * 60 * 24 * 7)
      insert_list(2, :user, deletion_requested_at: one_week_ago)

      Accounts.delete_users()

      refute Repo.exists?(User)
    end

    test "does not delete if deletion_reuqested_at is set less than a week ago" do
      now = NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)
      insert_list(2, :user, deletion_requested_at: now)

      Accounts.delete_users()

      assert Repo.all(User) |> Enum.count() == 2
    end

    test "does not delete if deletion_reuqested_at is not set" do
      insert_list(2, :user, deletion_requested_at: nil)

      Accounts.delete_users()

      assert Repo.all(User) |> Enum.count() == 2
    end
  end

  describe "inspect/2" do
    test "does not include password" do
      refute inspect(%User{password: "123456"}) =~ "password: \"123456\""
    end
  end
end
