defmodule Nyght.Authorizations.AuthorizationTest do
  use Nyght.DataCase, async: true

  import Nyght.Factory

  alias Nyght.Authorizations

  setup do
    org = insert(:organization)
    role = insert(:role, organization: org)
    other_org = insert(:organization)

    %{org: org, role: role, other_org: other_org}
  end

  ############################################################
  # Roles
  ############################################################

  describe "list_roles/1" do
    test "returns all the roles for an organization", %{org: org} do
      assert Enum.count(Authorizations.list_roles(org)) == 1
    end

    test "when there is no role, returns an empty list", %{other_org: org} do
      assert Enum.empty?(Authorizations.list_roles(org))
    end
  end

  describe "get_role!/1" do
    test "with a valid id, returns the corresponding role", %{org: org} do
      role = insert(:role, organization: org)
      assert Authorizations.get_role!(role.id).id == role.id
    end

    test "with an invalid id, raises an error" do
      assert_raise(Ecto.NoResultsError, fn ->
        Authorizations.get_role!("11111111-1111-1111-1111-111111111111")
      end)
    end
  end

  describe "create_role/2" do
    test "with valid attributes, creates a new role", %{org: org} do
      valid_attrs = %{"name" => "New role"}
      assert {:ok, role} = Authorizations.create_role(org, valid_attrs)
      assert role.name == valid_attrs["name"]
      assert role.organization_id == org.id
      assert is_nil(role.description)
    end

    test "with valid attributes and a description, creates a new role", %{org: org} do
      valid_attrs = %{"name" => "New role", "description" => "This is a test role"}
      assert {:ok, role} = Authorizations.create_role(org, valid_attrs)
      assert role.name == valid_attrs["name"]
      assert role.organization_id == org.id
      assert role.description == valid_attrs["description"]
    end

    test "with valid attributes and permissions, creates a new role", %{org: org} do
      valid_attrs = %{
        "name" => "New role",
        "description" => "This is a test role",
        "permission_list" => [insert(:permission).id, insert(:permission).id]
      }

      assert {:ok, role} = Authorizations.create_role(org, valid_attrs)
      assert role.name == valid_attrs["name"]
      assert role.organization_id == org.id
      assert Enum.count(role.permissions) == 2
    end

    test "with invalid attributes, returns an error changeset", %{org: org} do
      invalid_attrs = %{"name" => nil}
      assert {:error, changeset} = Authorizations.create_role(org, invalid_attrs)
      assert "can't be blank" in errors_on(changeset).name
    end

    test "with duplicated name, returns an error changeset", %{org: org} do
      valid_attrs = %{"name" => "New role"}
      insert(:role, organization: org, name: valid_attrs["name"])

      assert {:error, changeset} = Authorizations.create_role(org, valid_attrs)
      assert "has already been taken" in errors_on(changeset).name
    end
  end

  describe "update_role/2" do
    test "with valid attributes, updates a new role", %{org: org} do
      role = insert(:role, organization: org)
      valid_attrs = %{name: "New name", description: "New description"}

      assert {:ok, role} = Authorizations.update_role(role, valid_attrs)
      assert role.name == valid_attrs.name
      assert role.description == valid_attrs.description
      assert role.organization_id == org.id
    end

    test "with valid attributes and permissions, updates a new role", %{org: org} do
      role =
        build(:role, organization: org)
        |> with_permission()
        |> insert()

      valid_attrs = %{
        name: "New name",
        description: "New description",
        permission_list: [insert(:permission).id, insert(:permission).id]
      }

      assert {:ok, role} = Authorizations.update_role(role, valid_attrs)
      assert role.name == valid_attrs.name
      assert role.organization_id == org.id
      assert Enum.count(role.permissions) == 2
    end

    test "with invalid attributes, returns an error changeset", %{org: org} do
      role = insert(:role, organization: org)
      invalid_attrs = %{name: nil}
      assert {:error, changeset} = Authorizations.update_role(role, invalid_attrs)
      assert "can't be blank" in errors_on(changeset).name
    end

    test "with duplicated name, returns an error changeset", %{org: org} do
      valid_attrs = %{name: "Role"}
      insert(:role, organization: org, name: valid_attrs.name)
      role = insert(:role, organization: org)

      assert {:error, changeset} = Authorizations.update_role(role, valid_attrs)
      assert "has already been taken" in errors_on(changeset).name
    end
  end

  describe "delete_role/1" do
    test "with an existing role, deletes it", %{org: org} do
      role = insert(:role, organization: org)
      assert {:ok, role} = Authorizations.delete_role(role)
      assert_raise Ecto.NoResultsError, fn -> Authorizations.get_role!(role.id) end
    end
  end

  describe "change_role/2" do
    test "returns a changeset", %{org: org} do
      assert %Ecto.Changeset{} =
               insert(:role, organization: org)
               |> Authorizations.change_role()
    end

    test "with valid attributes, returns a changeset", %{org: org} do
      assert %Ecto.Changeset{} =
               insert(:role, organization: org)
               |> Authorizations.change_role(%{name: "Role"})
    end
  end

  describe "list_default_roles/1" do
    test "returns all default roles for the given organization", %{org: org, other_org: other_org} do
      insert_list(10, :role, organization: org)
      insert_list(10, :role, organization: other_org)
      insert_list(3, :role, organization: other_org, is_default: true)
      default_roles = insert_list(3, :role, organization: org, is_default: true)

      res = Authorizations.list_default_roles(org)

      assert length(res) == 3

      assert Enum.reduce(default_roles, true, fn default_role, acc ->
               Enum.any?(res, &(&1.id == default_role.id)) and acc
             end)
    end
  end

  ############################################################
  # Permissions
  ############################################################

  describe "get_permission!/1" do
    test "with valid action, returns the corresponding permission" do
      insert(:permission, action: :create, resource: Nyght.Events.Event)
      assert %{action: :create} = Authorizations.get_permission!(:create, Nyght.Events.Event)
    end

    test "with invalid action, returns the corresponding permission" do
      assert_raise Ecto.NoResultsError, fn -> Authorizations.get_permission!(:test, :resource) end
    end
  end

  describe "list_permissions/0" do
    test "returns all the permissions" do
      insert(:permission)
      insert(:permission)

      assert Enum.count(Authorizations.list_permissions()) == 2
    end

    test "when there is no permission, returns an empty list" do
      assert Enum.empty?(Authorizations.list_permissions())
    end
  end

  describe "create_permission/1" do
    test "with valid data, creates a new permission" do
      assert {:ok, %{action: :update, resource: Nyght.Organizations.Organization}} =
               Authorizations.create_permission(:update, Nyght.Organizations.Organization)
    end

    test "with duplicated action, returns an error changeset" do
      insert(:permission, action: :delete, resource: Nyght.Events.Event)
      assert {:error, changeset} = Authorizations.create_permission(:delete, Nyght.Events.Event)
      assert "already exists" in errors_on(changeset).resource
    end
  end

  describe "grant!/2" do
    test "with valid data, grants a permission to a role", %{org: org} do
      role = insert(:role, organization: org)
      perm = insert(:permission)

      assert {:ok, %{permissions: [^perm]}} = Authorizations.grant!(role, perm)
    end

    @tag :skip
    test "with duplicated permission, returns an error changeset", %{org: org} do
      role = insert(:role, organization: org)
      perm = insert(:permission)

      Authorizations.grant!(role, perm)

      # TODO: add unique constraints on the roles_permissions table to
      #       satisfy this test. We need to create a new schema for this.
      assert {:error, _changeset} = Authorizations.grant!(role, perm)
    end
  end
end
