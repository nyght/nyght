ExUnit.configure(formatters: [JUnitFormatter, ExUnit.CLIFormatter])
ExUnit.start(capture_log: true)
Ecto.Adapters.SQL.Sandbox.mode(Nyght.Repo, :manual)
{:ok, _} = Application.ensure_all_started(:ex_machina)
