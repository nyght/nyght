defmodule NyghtWeb.EventControllerTest do
  use NyghtWeb.ConnCase

  import Nyght.Factory

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json"), org: insert(:organization)}
  end

  describe "index" do
    test "lists all event", %{conn: conn} do
      conn = get(conn, ~p"/api/v1/events")
      assert json_response(conn, 200)["data"] == []
    end
  end
end
