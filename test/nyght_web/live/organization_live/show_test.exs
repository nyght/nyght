defmodule NyghtWeb.OrganizationLive.ShowTest do
  @moduledoc """
  This module defines tests for the home live view.
  """

  use NyghtWeb.ConnCase

  import Phoenix.LiveViewTest
  import Nyght.Factory

  setup do
    %{organization: insert(:organization, name: "My Org")}
  end

  describe "Organization homepage" do
    test "renders the organization homepage", %{conn: conn} do
      {:ok, _lv, html} = live(conn, ~p"/")
      assert html =~ "My Org"
    end
  end
end
