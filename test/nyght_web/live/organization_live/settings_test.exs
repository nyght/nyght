defmodule NyghtWeb.OrganizationLive.SettingsTest do
  @moduledoc """
  This module defines tests for the organization settings live view.
  """
  alias Nyght.Organizations

  use NyghtWeb.ConnCase

  import Phoenix.LiveViewTest
  import Nyght.Factory

  describe "Organization settings page" do
    setup do
      org = insert(:organization, name: "My Org")

      admin_role =
        build(:role, name: "admin", organization: org)
        |> with_permission(action: :update, resource: Nyght.Organizations.Organization)
        |> insert()

      other_role = insert(:role, organization: org)

      admin = insert(:user)
      user = insert(:user)

      insert(:member, user: admin, organization: org, roles: [admin_role])
      insert(:member, user: user, organization: org, roles: [other_role])

      %{organization: org, user: user, admin: admin}
    end

    test "redirects if user is not logged in", %{conn: conn} do
      assert {:error, redirect} = live(conn, ~p"/app/organizations")

      assert {:redirect, %{to: path, flash: flash}} = redirect
      assert path == ~p"/users/log_in"
      assert %{"error" => "You must log in to access this page."} = flash
    end

    test "redirects if the user doesn't have the right permission", %{conn: conn, user: user} do
      result =
        conn
        |> log_in_user(user)
        |> live(~p"/app/organizations")
        |> follow_redirect(conn, ~p"/")

      assert {:ok, _conn} = result
    end

    test "render the page if the user has the right permission", %{conn: conn, admin: admin} do
      {:ok, _lv, html} =
        conn
        |> log_in_user(admin)
        |> live(~p"/app/organizations")

      assert html =~ "My Org"
    end
  end

  describe "update organization form" do
    setup %{conn: conn} do
      org = insert(:organization, name: "My Org")

      admin_role =
        build(:role, name: "admin", organization: org)
        |> with_permission(action: :update, resource: Nyght.Organizations.Organization)
        |> insert()

      user = insert(:user)
      insert(:member, user: user, organization: org, roles: [admin_role])

      %{conn: log_in_user(conn, user), organization: org, user: user}
    end

    test "updates the organization description", %{conn: conn, organization: org} do
      new_desc = Faker.Markdown.markdown()

      {:ok, lv, _html} = live(conn, ~p"/app/organizations")

      {:ok, _lv, html} =
        lv
        |> form("#organization_form", %{
          organization: %{description: new_desc}
        })
        |> render_submit()
        |> follow_redirect(conn, ~p"/app/organizations")

      assert html =~ "The organization has been successfully updated."

      assert Organizations.get_organization!(org.id).description == new_desc
    end
  end
end
