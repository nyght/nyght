defmodule Nyght.Factory do
  @moduledoc false

  # credo:disable-for-this-file

  use ExMachina.Ecto, repo: Nyght.Repo

  alias Nyght.Organizations.MemberCategory
  alias Nyght.Events.Attachment
  alias Nyght.Crypto
  alias Nyght.Accounts.{Password, User}
  alias Nyght.Authorizations.{Permission, Role}
  alias Nyght.Events.{Event, Price, Type}
  alias Nyght.Organizations.{Member, Organization}
  alias Nyght.Performers.{Booking, Performer}
  alias Nyght.Shifts.{Application, Shift}

  @doc false
  def invalid_guid_factory(_attrs), do: "11111111-1111-1111-1111-111111111111"

  @doc false
  def password_factory(_attrs), do: "Passw0rd!"

  @doc false
  def email_factory(_attrs), do: sequence(:email, &"janedoe#{&1}@foo.com")

  @doc "Factory for the start of the summer time in European timezone"
  def summer_time_factory(_attrs),
    do: NaiveDateTime.new!(Date.new!(2024, 03, 31), Time.new!(3, 0, 0))

  @doc "Factory for the start of the winter time in European timezone"
  def winter_time_factory(_attrs),
    do: NaiveDateTime.new!(Date.new!(2024, 10, 27), Time.new!(2, 0, 0))

  @doc false
  def user_factory(attrs) do
    pw = Map.get(attrs, :password, build(:password))

    user = %User{
      first_name: "Jane",
      last_name: sequence("Doe"),
      email: build(:email),
      hashed_password: Password.hash(pw),
      street_line_1: "Rue de Vevey 34",
      street_line_2: "Case Postale 1234",
      street_line_3: "c/o John Doe",
      zip_code: 1630,
      city: "Bulle",
      phone_number: "+41 12 345 67 89"
    }

    user
    |> merge_attributes(attrs)
    |> evaluate_lazy_attributes()
  end

  @doc false
  def organization_factory do
    %Organization{
      name: sequence("Ebullition"),
      slug: sequence("ebull"),
      timezone: "Etc/UTC",
      description: Faker.Markdown.markdown()
    }
  end

  @doc false
  def member_factory, do: %Member{}

  def member_category_factory, do: %MemberCategory{name: sequence("category")}

  @doc false
  def role_factory do
    %Role{
      name: sequence("role"),
      description: Faker.Lorem.sentence(),
      permissions: []
    }
  end

  @doc false
  def permission_factory do
    %Permission{action: sequence("perm"), resource: sequence("resource")}
  end

  @doc false
  def event_type_factory do
    %Type{
      name: sequence("type")
    }
  end

  @doc false
  def event_factory(attrs) do
    starts_at = Map.get(attrs, :starts_at, Faker.DateTime.forward(30))
    ends_at = Map.get(attrs, :ends_at, DateTime.add(starts_at, 5 * 60 * 60))

    event = %Event{
      name: sequence("Event"),
      description: Faker.Markdown.markdown(),
      additional_info: Faker.Lorem.sentence(),
      encryption_key: Crypto.generate_secure_string(32),
      starts_at: starts_at,
      ends_at: ends_at,
      timezone: "Etc/UTC"
    }

    event
    |> merge_attributes(attrs)
    |> evaluate_lazy_attributes()
  end

  @doc false
  def attachment_factory do
    %Attachment{
      file_name: Faker.File.file_name(),
      resource_path: Faker.File.file_name(),
      content_type: Faker.File.mime_type(),
      encryption_iv: Crypto.generate_secure_string(16)
    }
  end

  @doc false
  def published_event_factory(attrs) do
    struct!(
      event_factory(attrs),
      %{
        status: :published
      }
    )
  end

  @doc false
  def prepublished_event_factory(attrs) do
    struct!(
      event_factory(attrs),
      %{
        status: :prepublished
      }
    )
  end

  @doc false
  def unpublished_event_factory(attrs) do
    struct!(
      event_factory(attrs),
      %{
        status: :unpublished
      }
    )
  end

  @doc false
  def canceled_event_factory(attrs) do
    struct!(
      event_factory(attrs),
      %{
        is_canceled: true
      }
    )
  end

  @doc false
  def price_factory do
    %Price{
      position: 1,
      label: sequence("Admission price"),
      amount: 10
    }
  end

  @doc false
  def shift_factory(attrs) do
    starts_at = Map.get(attrs, :starts_at, Faker.DateTime.forward(30))
    ends_at = Map.get(attrs, :ends_at, DateTime.add(starts_at, 5 * 60 * 60))

    shift = %Shift{
      application_strategy: :fcfs,
      total_openings: 4,
      starts_at: starts_at,
      ends_at: ends_at,
      name: sequence("Shift"),
      description: Faker.Lorem.sentence()
    }

    merge_attributes(shift, attrs)
  end

  def booking_factory(attrs) do
    booking = %Booking{}

    merge_attributes(booking, attrs)
  end

  def performer_factory do
    %Performer{
      name: sequence("Band"),
      email: Faker.Internet.email(),
      country: Faker.Address.country_code(),
      phone_number: Faker.Phone.EnUs.phone()
    }
  end

  @doc false
  def shift_application_factory do
    %Application{}
  end

  ### Utilities ###

  @doc """
  Removes the hashed password field and sets a password.
  """
  def for_registration(user) do
    %{user | hashed_password: nil, password: build(:password)}
  end

  @doc """
  Adds a permission to the given role.
  """
  def with_permission(%Role{} = role, attrs \\ []) do
    %{role | permissions: [build(:permission, attrs) | role.permissions]}
  end
end
